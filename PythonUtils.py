
import subprocess
import zipfile
import shutil
import sys
import os
os.system('color') # setup colors in console

CONFIG_VERBOSE = True


# Print utils #################################################################

TXTSTYLE_DEFAULT =   '\033[0m'
TXTSTYLE_RED =       '\033[91m'
TXTSTYLE_GREEN =     '\033[92m'
TXTSTYLE_YELLOW =    '\033[93m'
TXTSTYLE_BLUE =      '\033[94m'
TXTSTYLE_MAGENTA =   '\033[95m'
TXTSTYLE_CYAN =      '\033[96m'
TXTSTYLE_WHITE =     '\033[97m'
TXTSTYLE_BRIGHT =    '\033[1m'
TXTSTYLE_UNDERLINE = '\033[4m'

def PrintStyle(_obj, _style = TXTSTYLE_DEFAULT):
    print(_style + str(_obj) + TXTSTYLE_DEFAULT)
def PrintError(_obj):
    PrintStyle('[ERROR] ' + str(_obj), TXTSTYLE_RED)
def PrintWarning(_obj):
    PrintStyle('[WARNING] ' + str(_obj), TXTSTYLE_YELLOW)
def PrintInfo(_obj):
    PrintStyle('[INFO] ' + str(_obj), TXTSTYLE_CYAN)
def NextLine():
    print()
def PrintSeparator(_style = TXTSTYLE_DEFAULT):
    PrintStyle('\n----------------------------------------\n', _style)
def PrintLargeSeparator(_style = TXTSTYLE_DEFAULT):
    PrintStyle('\n======================================================', _style)
    PrintStyle(  '======================================================\n', _style)

def FatalError(_obj):
    PrintError('[FATAL] ' + str(_obj))
    input('\nPress Enter to terminate...')
    exit()


# Menu utils ##################################################################

class Action():
    STATE_NOT_EVALUATED = 0
    STATE_FAILED = 1
    STATE_OK = 2

    def __init__(self, _desc, _action, *_args):
        self.desc = _desc
        self.action = _action
        self.args = _args
        self.state = Action.STATE_NOT_EVALUATED

    def Do(self):
        result = self.action(*self.args)
        self.state = Action.STATE_OK if result else Action.STATE_FAILED

class Menu():
    def __init__(self, _desc):
        self.desc = _desc
        self.actions = []

    def AddAction(self, _action):
        self.actions.append(_action)

    def DisplayLoop(self):
        stateStyle = [TXTSTYLE_WHITE, TXTSTYLE_RED, TXTSTYLE_GREEN]
        quit = False
        while not quit:
            # display
            PrintLargeSeparator(TXTSTYLE_WHITE)
            PrintStyle('([ ' + self.desc + ' ])', TXTSTYLE_WHITE)
            NextLine()
            i = 1
            for action in self.actions:
                PrintStyle(f' o [{i}]  {action.desc}'.format(), stateStyle[action.state])
                i += 1
            PrintStyle(f' o [a]  Do all'.format(), TXTSTYLE_YELLOW)
            PrintStyle(f' o [q]  Quit when finished'.format(), TXTSTYLE_YELLOW)
            PrintLargeSeparator(TXTSTYLE_WHITE)

            # input
            fullInput = input('Type your choices: ')
            inputInts = []
            for s in fullInput.split():
                isValid = False
                if s == 'q':
                    quit = True
                    isValid = True
                elif s == 'a':
                    for i in range(len(self.actions)):
                        inputInts.append(i + 1)
                    isValid = True
                elif s.isdigit():
                    i = int(s)
                    if 0 < i and i <= len(self.actions):
                        inputInts.append(i)
                        isValid = True
                if not isValid:
                    PrintError(f'Invalid action \'{s}\': ignored'.format())
            NextLine()

            if len(inputInts) > 0:
                # actions
                for i in inputInts:
                    action = self.actions[i - 1]
                    PrintSeparator(TXTSTYLE_WHITE)
                    PrintInfo(f'>>> [{i}] {action.desc}'.format())
                    action.Do()
                NextLine()

                # summary
                PrintSeparator(TXTSTYLE_WHITE)
                PrintInfo('Summary:')
                for i in inputInts:
                    action = self.actions[i - 1]
                    state = 'OK' if action.state == Action.STATE_OK else 'FAILED'
                    PrintStyle(f' > [{i}] {action.desc}: {state}'.format(), stateStyle[action.state])
        NextLine()


# Command utils ###############################################################

def SystemCommand(_command, _returnTestfn = None):
    if CONFIG_VERBOSE:
        PrintInfo('System command called: ' + str(_command))
    returnCode = subprocess.call(_command)
    if _returnTestfn:
        result = _returnTestfn(returnCode)
        if not result:
            PrintError('System command: ' + str(_command))
            PrintError('returned: ' + str(returnCode))
        return result
    return None

def RobocopyReturnTest(_returnCode):
    return _returnCode == 1

def RobocopyCommand(_sourceDir, _destDir, _files, _args = ''):
    return SystemCommand(f'robocopy "{_sourceDir}" "{_destDir}" {_files} {_args}'.format(), RobocopyReturnTest)

def MsBuildReturnTest(_returnCode):
    return _returnCode == 0

def MsBuildCommand(_msbuild, _file, _args = ''):
    return SystemCommand(f'"{_msbuild}" "{_file}" {_args}'.format(), MsBuildReturnTest)

def PremakeReturnTest(_returnCode):
    return _returnCode == 0

def PremakeCommand(_premake, _target, _file, _args = ''):
    return SystemCommand(f'"{_premake}" "{_target}" --file="{_file}" {_args}'.format(), PremakeReturnTest)

def GlslcReturnTest(_returnCode):
    return _returnCode == 0

def GlslcCommand(_glslc, _file, _args = ''):
    return SystemCommand(f'"{_glslc}" "{_file}" -o "{_file}.spv" {_args}'.format(), GlslcReturnTest)


def CreateFolder(_dir):
    if not os.path.isdir(_dir):
        PrintInfo(f'Creating folder {_dir}...'.format())
        os.makedirs(_dir)
        PrintInfo('Done')

def DeleteFolder(_dir):
    if os.path.isdir(_dir):
        PrintInfo(f'Deleting folder {_dir}...'.format())
        shutil.rmtree(_dir)
        PrintInfo('Done')


# Ini file read #####################################################################

# custon ini file reading, NOT 'standard'

class IniFile():
    def __init__(self, _filePath):
        self.filePath = _filePath
        self.content = {}

    def Read(self):
        if CONFIG_VERBOSE:
            PrintSeparator()
            PrintInfo('Reading ini file ' + self.filePath + ' ...')
        fileContent = None
        with open(self.filePath, 'r') as f:
            fileContent = f.read().splitlines()
        if not fileContent:
            PrintError('Failed reading file ' + self.filePath)
            return False
        self.content = {}
        sectionName = None
        for line in fileContent:
            line = line.split('#')[0] # comment
            if len(line) > 0:
                if line[0] == '[': # section
                    sectionName = line[1:-1]
                else: # data
                    c = line.split('=')
                    if len(c) == 2:
                        self.content[(sectionName, c[0])] = c[1]
                    else:
                        PrintError('Badly formated line (ignored): ' + line)
        if CONFIG_VERBOSE:
            PrintInfo('Read content:')
            for k, v in self.content.items():
                PrintInfo(f'[{k}] : {v}'.format())
            PrintSeparator()
        return True

    def GetValue(self, _section, _entry):
        return self.content.get((_section, _entry))


def ReadMandatoryIniFile(_file):
    ini = IniFile(_file)
    if not ini.Read():
        FatalError(f'Can\'t read {_file}'.format())
    return ini

def GetMandatoryIniValue(_ini, _section, _entry):
    value = _ini.GetValue(_section, _entry)
    if not value:
        FatalError(f'Can\'t find required entry : [{_section}] {_entry} {value}'.format())
    return value


# Requests wrappers ###########################################################

# lazy 'requests' module download
try:
    import requests
except ModuleNotFoundError:
    PrintSeparator()
    PrintInfo('Installing required \'requests\' python module')
    python = sys.executable
    subprocess.call([python, '-m', 'pip', 'install', 'requests'])
    PrintSeparator()
    import requests

def PostUrl(_url, _data):
    if CONFIG_VERBOSE:
        PrintInfo('Post ' + _url + ' ...')
        PrintInfo(str(_data))
    try:
        r = requests.post(_url, data = _data)
        if CONFIG_VERBOSE:
            PrintInfo('Redirected to ' + r.url)
        return r.url
    except requests.exceptions.RequestException as e:
        PrintError('Post failed!')

def DownloadUrl(_url, _file):
    if CONFIG_VERBOSE:
        PrintInfo('Downloading ' + _url + ' ...')
    try:
        r = requests.get(_url)
        if CONFIG_VERBOSE:
            PrintInfo('Got it!')
            PrintInfo('Writing to ' + _file + ' ...')
        with open(_file, 'wb') as f:
            f.write(r.content)
        if CONFIG_VERBOSE:
            PrintInfo('Done!')
        return True
    except requests.exceptions.RequestException as e:
        PrintError('Download failed!')
        return False


# Zip file ####################################################################

def UnzipFile(_file):
    folder = _file[:-4] # remove .zip extention
    if CONFIG_VERBOSE:
        PrintInfo('Delete folder ' + folder)
    DeleteFolder(folder)
    if CONFIG_VERBOSE:
        PrintInfo('Unzipping ' + _file + ' ...')
    with zipfile.ZipFile(_file, 'r') as z:
        z.extractall(folder)
    return True
