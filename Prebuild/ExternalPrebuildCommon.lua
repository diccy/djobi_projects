#!lua
require("Directories")
require("ProjectCommon")
require("Utils")

function GetRootDirFromExternalManagement()

    newoption { trigger = "rootDir", value = "path", description = "External lib root directory path" }
    return _OPTIONS["rootDir"]
end

function NewExternalLibProjectCommon(_def)

    local buildDir = _def.rootDir.."build/"
    
    local project = NewProjectCommon({
        name =              _def.name,
        sourcesDir =        _def.rootDir,
        buildDir =          buildDir,
        targetDir =         buildDir.."lib/",
        debugDir =          buildDir,
        SetupDependencies = function (_project) end,
        SetupIncludes =     _def.SetupIncludes,
        SetupSources =      _def.SetupSources,
        SetupLinks =        _def.SetupLinks,
        SetupPCH =          function (_project) end,
        SetupAdditional =   _def.SetupAdditional,
        DefineProject =     CommonProjectDefinition_Lib,
    })
    return project
end

function ExternalLibSolutionDefinition(_project)

    workspace (_project.name)
        location (_project.buildDir)
        SetWorkspacePlatforms()
        SetWorkspaceConfigurations()
        SetArchitectures()

        DefineProject(_project)
end
