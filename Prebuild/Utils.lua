#!lua
require("Config")


table_insert = table.insert


-- Filters

function FilterPlatform(_platform, _do)

    if (SupportedPlatforms[_platform] == true) then
        filter {"platforms:".._platform}
            _do()
        filter {}
    end
end

function FilterConfiguration(_config, _do)

    if (SupportedConfigurations[_config] == true) then
        filter {"configurations:".._config}
            _do()
        filter {}
    end
end

function FilterPlatformAndConfiguration(_platform, _config, _do)

    if (SupportedPlatforms[_platform] == true and SupportedConfigurations[_config] == true) then
        filter {"platforms:".._platform, "configurations:".._config}
            _do()
        filter {}
    end
end

function ForEachSupportedPlatform(_do)

    for p, supportedPlatform in pairs(SupportedPlatforms) do
        if (supportedPlatform == true) then
            filter {"platforms:"..p}
                _do(p)
        end
    end
    filter {}
end

function ForEachSupportedConfig(_do)

    for c, supportedConfig in pairs(SupportedConfigurations) do
        if (supportedConfig == true) then
            filter {"configurations:"..c}
                _do(c)
        end
    end
    filter {}
end

function ForEachSupportedPlatformAndConfig(_do)

    for p, supportedPlatform in pairs(SupportedPlatforms) do
        if (supportedPlatform == true) then
            for c, supportedConfig in pairs(SupportedConfigurations) do
                if (supportedConfig == true) then
                    filter {"platforms:"..p, "configurations:"..c}
                        _do(p, c)
                end
            end
        end
    end
    filter {}
end


-- Application

function SetWorkspacePlatforms()

    PlatformsTable = {}
    for p, supported in pairs(SupportedPlatforms) do
        if (supported == true) then
            table_insert(PlatformsTable, p)
        end
    end
    platforms (PlatformsTable)
end

function SetWorkspaceConfigurations()

    ConfigsTable = {}
    for c, supported in pairs(SupportedConfigurations) do
        if (supported == true) then
            table_insert(ConfigsTable, c)
        end
    end
    configurations (ConfigsTable)
end

function SetArchitectures()

    ForEachSupportedPlatform(function(_p)
        architecture (_p)
    end )
    filter {}
end


-- Target

function GetSuffix(_platform, _config)

    return Suffixes[_platform]..Suffixes[_config]
end

function GetDir(_dir, _platform, _config)

    return _dir.._platform.."/".._config.."/"
end

function GetTarget(_dir, _name, _p, _c)

    return GetDir(_dir, _p, _c).._name..GetSuffix(_p, _c)
end

function SetTarget(_dir)

    ForEachSupportedPlatformAndConfig( function(_p, _c)
        targetdir    (GetDir(_dir, _p, _c))
        targetsuffix (GetSuffix(_p, _c))
    end )
end
