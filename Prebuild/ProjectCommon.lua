#!lua
require("Directories")


local indentation = ""

local function PrintWithIndent(_text)

    print(indentation.._text)
end

local function Indent()

    indentation = indentation.."."
end

local function Outdent()

    indentation = indentation:sub(1, -2)
end

local function SetupDependencies(_project)

    PrintWithIndent("SetupDependencies - ".._project.name)
    Indent()
    _project:SetupDependencies()
    Outdent()
end

local function SetupIncludes(_project)

    PrintWithIndent("SetupIncludes - ".._project.name)
    Indent()
    _project:SetupIncludes()
    Outdent()
end

local function SetupSources(_project)

    PrintWithIndent("SetupSources - ".._project.name)
    Indent()
    _project:SetupSources()
    Outdent()
end

local function SetupLinks(_project)

    PrintWithIndent("SetupLinks - ".._project.name)
    Indent()
    _project:SetupLinks()
    Outdent()
end

local function SetupPCH(_project)

    PrintWithIndent("SetupPCH - ".._project.name)
    Indent()
    _project:SetupPCH()
    Outdent()
end

local function SetupAdditional(_project)

    PrintWithIndent("SetupAdditional - ".._project.name)
    Indent()
    _project:SetupAdditional()
    Outdent()
end

function DefaultSetupDependencies(_project)

    for _, dep in ipairs(_project.dependencies) do
        SetupDependencies(dep)
        SetupIncludes(dep)
        SetupLinks(dep)
        SetupAdditional(dep)
    end
end

function DefaultSetupIncludes(_project)

    local includeDir = _project.sourcesDir..IncludeDir
    includedirs {includeDir, includeDir.._project.name.."/"}
end

function DefaultSetupSources(_project)

    SetupIncludes(_project)
    SourceFiles(_project.sourcesDir)
    vpaths {["*"] = {
        _project.sourcesDir..IncludeDir.._project.name.."/**.*",
        _project.sourcesDir..SourcesDir.."**.*"}
    }
end

function DefaultSetupLinks(_project)

    ForEachSupportedPlatformAndConfig( function(_p, _c)
        dependson   { _project.name }
        libdirs     { GetDir(_project.targetDir, _p, _c) }
        links       { _project.name..GetSuffix(_p, _c) }
    end )
    filter {}
end

function DefaultSetupPCH(_project)

    ForEachSupportedPlatformAndConfig( function(_p, _c)
        pchheader (_project.name.."_pch.h")
        pchsource (_project.sourcesDir..SourcesDir.._project.name.."_pch.cpp")
    end )
    filter {}
end

function NewProjectCommon(_def)

    local project = {       -- user definition     -- default values
        name =              _def.name,
        sourcesDir =        _def.sourcesDir        or ProjectsDir.._def.name.."/",
        buildDir =          _def.buildDir          or BuildDir.._def.name.."/",
        targetDir =         _def.targetDir         or BinDir,
        debugDir =          _def.debugDir          or ProjectsDir.._def.name.."/",
        dependencies =      _def.dependencies      or {},
        exceptions =        _def.exceptions        or "Off",
        SetupDependencies = _def.SetupDependencies or DefaultSetupDependencies,
        SetupIncludes =     _def.SetupIncludes     or DefaultSetupIncludes,
        SetupSources =      _def.SetupSources      or DefaultSetupSources,
        SetupLinks =        _def.SetupLinks        or DefaultSetupLinks,
        SetupPCH =          _def.SetupPCH          or DefaultSetupPCH,
        SetupAdditional =   _def.SetupAdditional   or function (_project) end,
        DefineProject =     _def.DefineProject
    }
    return project
end

function DefineProject(_project)

    PrintWithIndent("<DefineProject> - ".._project.name)
    Indent()
    _project:DefineProject()
    Outdent()
end


function NewDependencyCommon(_def)

    local dependency = {    -- user definition     -- default values
        name =              _def.name,
        sourcesDir =        _def.sourcesDir,
        targetDir =         _def.libDir,
        dependencies =      _def.dependencies      or {},
        SetupDependencies = _def.SetupDependencies or DefaultSetupDependencies,
        SetupIncludes =     _def.SetupIncludes     or DefaultSetupIncludes,
        SetupLinks =        _def.SetupLinks        or DefaultSetupLinks,
        SetupAdditional =   _def.SetupAdditional   or function (_dep) end,
    }
    return dependency
end


local function CommonProjectDefinition(_project, _kind)

    filter {}

    project (_project.name)
        location (_project.buildDir)

        kind        (_kind)
        debugdir    (_project.debugDir)
        targetname  (_project.name)
        flags       {"NoMinimalRebuild", "MultiProcessorCompile"}

        FilterConfiguration("Release", function()
            warnings    "Extra"
            optimize    "Speed"
            flags       {"FatalWarnings"}
        end )
        FilterConfiguration("Debug", function()
            warnings    "Extra"
            symbols     "On"
        end )
        filter {}

        SetCppLanguage()
        SetTarget(_project.targetDir)
        SetCharacterSet()

        SetupSources(_project)
        SetupDependencies(_project)
        SetupPCH(_project)

        SetupAdditional(_project)

    filter {}
end

function CommonProjectDefinition_Exe(_project)

    CommonProjectDefinition(_project, "WindowedApp")
    entrypoint  "mainCRTStartup"
end

function CommonProjectDefinition_Lib(_project)

    CommonProjectDefinition(_project, "StaticLib")
end
