#!lua
require("Directories")
require("Utils")

package.path = _PREMAKE_DIR.."/?/Dependencies.lua;"..package.path
package.path = ProjectsDir.."?/Prebuild.lua;"..package.path

require("DjobiCore")
require("DjobiTests")
require("Tests_DjobiCore")
require("Window")
require("Components")


-- Workspace -------------------------------------------------------------------

workspace ("Djobi_Sandbox")
    location (BuildDir)
    SetWorkspacePlatforms()
    SetWorkspaceConfigurations()
    SetArchitectures()

    FilterPlatform("x64", function()
        defines {"__DJO_x64__=1"}
    end )
    filter {}
    FilterConfiguration("Release", function()
        defines {"__DJO_RELEASE__=1"}
    end )
    FilterConfiguration("Debug", function()
        defines {"__DJO_DEBUG__=1"}
    end )
    filter {}

    -- Engine
    group "00_Engine"
    DefineProject(Project_DjobiCore)

    -- Tests
    group "01_Tests"
    DefineProject(Project_DjobiTests)
    DefineProject(Project_Tests_DjobiCore)
    
    -- Sandbox
    group "02_Doodles"
    DefineProject(Project_Window)
    DefineProject(Project_Components)