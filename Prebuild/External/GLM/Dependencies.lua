#!lua
require("Directories")
require("ProjectCommon")


local GlmName = "glm"
local GlmDir = ExternalDir..GlmName.."/"

local function IncludeGLM(self)

    defines {"GLM_ENABLE_EXPERIMENTAL=1"}
    includedirs {self.sourcesDir}
end

Dependency_GLM = NewDependencyCommon({
    name =              GlmName,
    sourcesDir =        GlmDir..IncludeName.."/",
    SetupDependencies = function (self) end,
    SetupIncludes =     IncludeGLM,
    SetupSources =      function (self) end,
    SetupLinks =        function (self) end,
})
