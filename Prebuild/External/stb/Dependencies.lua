#!lua
require("Directories")
require("ProjectCommon")


local StbName = "stb"
local StbDir = ExternalDir..StbName.."/"

local function IncludeStb(self)

    includedirs {self.sourcesDir}
end

Dependency_Stb = NewDependencyCommon({
    name =              StbName,
    sourcesDir =        StbDir..IncludeName.."/",
    SetupDependencies = function (self) end,
    SetupIncludes =     IncludeStb,
    SetupSources =      function (self) end,
    SetupLinks =        function (self) end,
})
