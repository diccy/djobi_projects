#!lua
require("Directories")
require("ProjectCommon")


local CGltfName = "cgltf"
local CGltfDir = ExternalDir..CGltfName.."/"

local function IncludeCGltf(self)

    includedirs {self.sourcesDir}
end

Dependency_CGltf = NewDependencyCommon({
    name =              CGltfName,
    sourcesDir =        CGltfDir..IncludeName.."/",
    SetupDependencies = function (self) end,
    SetupIncludes =     IncludeCGltf,
    SetupSources =      function (self) end,
    SetupLinks =        function (self) end,
})
