#!lua
require("Directories")
require("ProjectCommon")


os_getenv = os.getenv

local vulkanSdkDir = os_getenv("VK_SDK_PATH") or os_getenv("VULKAN_SDK")
local vulkanSdkIncludeDir = vulkanSdkDir.."/Include/"

local function IncludeVulkanSDK(self)

    includedirs {vulkanSdkIncludeDir}
end

local function LinkVulkanSDK(self)

    links   {"vulkan-1"}
    libdirs {vulkanSdkDir.."/Lib/"}
end

Dependency_VulkanSDK = NewDependencyCommon({
    name = "VulkanSDK",
    sourcesDir =        vulkanSdkIncludeDir,
    SetupDependencies = function (self) end,
    SetupIncludes =     IncludeVulkanSDK,
    SetupSources =      function (self) end,
    SetupLinks =        LinkVulkanSDK,
})

local VmaName = "vma"
local VmaDir = ExternalDir..VmaName.."/"
local VmaIncludeDir = VmaDir..IncludeName.."/"

local function IncludeVma(self)

    includedirs {VmaIncludeDir}
end

Dependency_Vma = NewDependencyCommon({
    name =              VmaName,
    sourcesDir =        VmaIncludeDir,
    SetupDependencies = function (self) end,
    SetupIncludes =     IncludeVma,
    SetupSources =      function (self) end,
    SetupLinks =        function (self) end,
})
