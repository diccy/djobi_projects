#!lua
require("Directories")
require("ExternalPrebuildCommon")


local DearImGuiName = "DearImGui"

local function SetupDearImGuiIncludes(_project)

    includedirs {_project.sourcesDir}
end

local function SetupDearImGuiSources(_project)

    files {
        _project.sourcesDir.."imgui.cpp",
        _project.sourcesDir.."imgui_draw.cpp",
        _project.sourcesDir.."imgui_tables.cpp",
        _project.sourcesDir.."imgui_widgets.cpp",
        _project.sourcesDir.."imgui_demo.cpp", -- allow to show demo window in app
    }
end

local Project_DearImGui = NewExternalLibProjectCommon({
    name =          DearImGuiName,
    rootDir =       GetRootDirFromExternalManagement(),
    SetupIncludes = SetupDearImGuiIncludes,
    SetupSources =  SetupDearImGuiSources,
    SetupLinks =    function (self) end,
})

ExternalLibSolutionDefinition(Project_DearImGui)
