#!lua
require("Directories")
require("ProjectCommon")


local DearImGuiName = "DearImGui"
local DearImGuiDir = ExternalDir..DearImGuiName.."/"

local function SetupDearImGuiIncludes(_project)

    includedirs {_project.sourcesDir.."include/"}
end

Dependency_DearImGui = NewDependencyCommon({
    name =          DearImGuiName,
    sourcesDir =    DearImGuiDir,
    libDir =        DearImGuiDir.."lib/",
    SetupIncludes = SetupDearImGuiIncludes,
})
