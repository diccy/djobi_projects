#!lua


-- Main directory tree

BinName = "Bin"
BuildName = "Build"
ExternalName = "External"
ProjectsName = "Projects"

RootDir = _WORKING_DIR.."/"
    BinDir = RootDir..BinName.."/"
    BuildDir = RootDir..BuildName.."/"
    ExternalDir = RootDir..ExternalName.."/"
    ProjectsDir = RootDir..ProjectsName.."/"


-- Common project directory tree

CodeName = "Code"
DataName = "Data"
IncludeName = "include"
SourcesName = "src"

CodeDir = CodeName.."/"
    IncludeDir = CodeDir..IncludeName.."/"
    SourcesDir = CodeDir..SourcesName.."/"
DataDir = DataName.."/"
