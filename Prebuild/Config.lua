#!lua


SupportedPlatforms =
{
    ["x64"] = true,
}

SupportedConfigurations =
{
    ["Release"] = true,
    ["Debug"]   = true,
}

Suffixes =
{
    ["x64"]     = "_x64",
    ["Release"] = "_r",
    ["Debug"]   = "_d",
}

function SetCharacterSet()

    -- if Windows
    characterset "MBCS"
    -- else let default (Unicode) value
end

function SetCppLanguage()

    language            "C++"
    cppdialect          "C++17"
    exceptionhandling   "Off"
end

function SourceFiles(_sourcesDir)

    files       {_sourcesDir.."**.h",
                 _sourcesDir.."**.hpp",
                 _sourcesDir.."**.c",
                 _sourcesDir.."**.cpp",
                 _sourcesDir.."**.inl",
                 _sourcesDir.."**.natvis"}

    excludes    {_sourcesDir.."**/*.bak"}
end
