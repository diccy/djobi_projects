#pragma once

#include <DjobiCore/Math/Matrix44.hpp>


namespace Djo
{
    struct CameraDesc
    {
        Mat44f view;
        f32 fov; // rad
    };
}
