#pragma once

#include "InputKeyboard.hpp"
#include "InputMouse.hpp"

#include <DjobiCore/Core/Types.hpp>
#include <DjobiCore/Math/Vector2.hpp>


namespace Djo
{
    namespace Input
    {
        enum class EEvent : u8
        {
            None = 0,
            Pressed,
            Repeated,
            Released,
        };

        struct State
        {
            struct MouseState
            {
                Vec2i pos;
                Vec2i move;
                i32 wheelMove;
                bool down[Input::MouseButton::Count];
                EEvent event[Input::MouseButton::Count];
            };
            struct KeyboardState
            {
                bool down[Input::KbScanCode::Count];
                EEvent event[Input::KbScanCode::Count];
            };

            MouseState mouse;
            KeyboardState kb;
        };

        void ClearInputEvents(State* _state) noexcept;

        void OnPressed(State* _state, MouseButton::EValue _mouseButton) noexcept;
        void OnReleased(State* _state, MouseButton::EValue _mouseButton) noexcept;
        void OnPressed(State* _state, KbScanCode::EValue _kbScanCode) noexcept;
        void OnPressed(State* _state, KbScanCode::EValue _kbScanCode) noexcept;
        void OnReleased(State* _state, KbScanCode::EValue _kbScanCode) noexcept;
        void OnMouseMove(State* _state, i32 _moveX, i32 _moveY) noexcept;
        void OnMouseWheelMove(State* _state, i32 _move) noexcept;
        void SetMousePos(State* _state, i32 _posX, i32 _posY) noexcept;

        bool IsDown(const State& _state, MouseButton::EValue _mouseButton) noexcept;
        bool IsPressedEvent(const State& _state, MouseButton::EValue _mouseButton) noexcept;
        bool IsUp(const State& _state, MouseButton::EValue _mouseButton) noexcept;
        bool IsReleasedEvent(const State& _state, MouseButton::EValue _mouseButton) noexcept;

        bool IsDown(const State& _state, KbScanCode::EValue _kbScanCode) noexcept;
        bool IsPressedEvent(const State& _state, KbScanCode::EValue _kbScanCode) noexcept;
        bool IsPressedOrRepeatedEvent(const State& _state, KbScanCode::EValue _kbScanCode) noexcept;
        bool IsUp(const State& _state, KbScanCode::EValue _kbScanCode) noexcept;
        bool IsReleasedEvent(const State& _state, KbScanCode::EValue _kbScanCode) noexcept;
    }
}
