#include "Window_pch.h"

#include "VulkanRenderer.hpp"
#include "DearImGuiHandler.hpp"
#include "CGltf.hpp"
#include "CameraController.hpp"

#include <DjobiCore/Format/Format.hpp>
#include <DjobiCore/Thread/ThreadContext.hpp>
#include <DjobiCore/OS/IDE.hpp>
#include <DjobiCore/OS/Time.hpp>
#include <DjobiCore/Core/Core.hpp>


// note: winmain from https://github.com/cmuratori/refterm/blob/main/refterm.c
// hadopire: https://pastebin.com/uxRVKY1b

#define WINDOW_NAME "Grosse fenetre mon pote"
::DWORD ApplicationThreadId = 0;

// standalone WM_INPUT messages breakdown
// +111 is totally arbitrary
#define DJO_WM_INPUT_BASE       (WM_APP + 111)
#define DJO_WM_INPUT_MOUSE      DJO_WM_INPUT_BASE

// ::RAWMOUSE decomposed
struct DjoMouseInputWParam
{
    ::USHORT usFlags;
    ::USHORT usButtonFlags;
    ::SHORT  sWheelMove; // from usButtonData
    ::USHORT unused;
};
static_assert(sizeof(DjoMouseInputWParam) == sizeof(::WPARAM));
struct DjoMouseInputLParam
{
    ::LONG lLastX;
    ::LONG lLastY;
};
static_assert(sizeof(DjoMouseInputLParam) == sizeof(::LPARAM));

#define DJO_WM_INPUT_KEYBOARD   (DJO_WM_INPUT_BASE + 1)

// ::RAWKEYBOARD reinterpreted
struct DjoKbInputWParam
{
    ::USHORT usScancode;
    ::USHORT usVKey;
    ::USHORT usFlags;
    ::USHORT unused;
};
static_assert(sizeof(DjoKbInputWParam) == sizeof(::WPARAM));

#define DJO_APP_THREAD_MESSAGE_CASES \
    case WM_QUIT:\
    case WM_KEYDOWN:\
    case WM_KILLFOCUS:\
    case WM_CHAR:\
    case WM_SIZE:\
    case WM_ENTERSIZEMOVE:\
    case WM_EXITSIZEMOVE

static ::LRESULT CALLBACK WindowProc(const ::HWND _windowHandle, const ::UINT _message, const ::WPARAM _wparam, const ::LPARAM _lparam)
{
    ::LRESULT result = 0;
    switch (_message)
    {
        case WM_CLOSE:
        case WM_DESTROY:
        {
            if (_message == WM_CLOSE)
                Djo::IDE::Output("WindowProc WM_CLOSE\n");
            else if (_message == WM_DESTROY)
                Djo::IDE::Output("WindowProc WM_DESTROY\n");

            ::PostQuitMessage(0);
            break;
        }

        case WM_MENUCHAR:
        {
            // Note: removes beeping sound when pressing an invalid alt + key combination
            // https://docs.microsoft.com/en-us/windows/win32/menurc/wm-menuchar
            return MNC_CLOSE << 16;
        }

        DJO_APP_THREAD_MESSAGE_CASES:
        {
            ::PostThreadMessageA(ApplicationThreadId, _message, _wparam, _lparam);
            DJO_FALLTHROUGH;
        }

        default:
        {
            result = ::DefWindowProcA(_windowHandle, _message, _wparam, _lparam);
            break;
        }
    }

    return result;
}

static void MainLoop(const ::HWND _windowHandle, Djo::Vk::VulkanRenderer* const _vkRenderer)
{
    using namespace ::Djo;

    struct MainLoopData
    {
        String8OStream logger;
        Input::State inputState;
        CameraController camControl;
        DearImGuiHandler imgui;
    };

    // Init

    Memory::Arena* const arena = DJO_MEMORY_ARENA_ALLOCATE_SIZES("MainLoop arena", 64 * 1024, 1024);
    MainLoopData* const data = DJO_MEMORY_ARENA_PUSH_TYPE(arena, MainLoopData);
    ZeroMem(data);

    String8OStream* const logger = &data->logger;
    *logger = IDE::MoldOutputStringStream();

    Input::State* const inputState = &data->inputState;

    CameraController* const camControl = &data->camControl;
    camControl->targetWorldPos = Vec3f{ 0.f, 0.f, 0.f };
    camControl->targetGoalWorldPos = camControl->targetWorldPos;
    camControl->cameraGoalWorldPos = Vec3f{ 2.f, 2.f, 2.f };
    camControl->cameraWorld = glm::inverse(glm::lookAt(camControl->cameraGoalWorldPos, camControl->targetWorldPos, c_cameraUp));
    camControl->moveSpeed = 3.f;

    DearImGuiHandler* const imgui = &data->imgui;
    Init(imgui, _windowHandle);
    ::VkResult imguiVulkanInitResult = InitVulkanRender(imgui, _vkRenderer->ctx, _vkRenderer->stagingCommandPool, _vkRenderer->mainRenderPass);
    if (imguiVulkanInitResult != ::VK_SUCCESS)
    {
        DJO_FORMAT_LF(logger, "% Imgui Vulkan init error: %",
            Format::ELogLevel::Error,
            Vk::VkResultToString(imguiVulkanInitResult));
    }

    // Show window

    ::ShowWindow(_windowHandle, SW_SHOWDEFAULT);
    ::SetForegroundWindow(_windowHandle);
    ::SetFocus(_windowHandle);
    bool isMinimized = false; // is there a way to init it properly?

    // Main loop

    OS::Time::TimePoint lastLoop{ OS::Time::Now().t - ConvertTime<OS::Time::TimePoint>(Seconds{ 1. / 120. }).t };

    bool quit = false;
    while (!quit)
    {
        // Loop init

        const OS::Time::TimePoint startLoop = OS::Time::Now();
        const OS::Time::TimePoint dt{ startLoop.t - lastLoop.t };
        const Milliseconds dtMs = ConvertTime<Milliseconds>(dt);

        Input::ClearInputEvents(inputState);

        // Window messages loop
        u32 messageCount = 0;
        ::MSG msg;
        while (::PeekMessageA(&msg, 0, 0, 0, PM_REMOVE))
        {
            DJO_FORMAT_LF(logger, "% PeekMessageA: % % %",
                Djo::Format::ELogLevel::Info,
                msg.message, msg.wParam, msg.lParam);

            switch (msg.message)
            {
                case WM_QUIT:
                {
                    DJO_FORMAT_LF(logger, "ApplicationThread WM_QUIT");
                    quit = true;
                    break;
                }
                case DJO_WM_INPUT_MOUSE:
                {
                    const ::DjoMouseInputWParam wParam = reinterpret_cast<const ::DjoMouseInputWParam&>(msg.wParam);
                    const ::DjoMouseInputLParam lParam = reinterpret_cast<const ::DjoMouseInputLParam&>(msg.lParam);

                    DJO_FORMAT_LF(logger, "ApplicationThread DJO_WM_INPUT_MOUSE : [%,%] % [% %]",
                        lParam.lLastX, lParam.lLastY,
                        wParam.usFlags,
                        wParam.usButtonFlags, wParam.sWheelMove);

                    if (OnMouseEvent(imgui, _windowHandle, wParam.usButtonFlags, lParam.lLastX, lParam.lLastY, wParam.sWheelMove))
                    {
                        // imgui grabbed event
                    }
                    else
                    {
                        Input::OnMouseMove(inputState, lParam.lLastX, lParam.lLastY);

                        if (TestFlag<::USHORT>(wParam.usButtonFlags, RI_MOUSE_LEFT_BUTTON_DOWN))
                            Input::OnPressed(inputState, Input::MouseButton::Left);

                        if (TestFlag<::USHORT>(wParam.usButtonFlags, RI_MOUSE_LEFT_BUTTON_UP))
                            Input::OnReleased(inputState, Input::MouseButton::Left);

                        if (TestFlag<::USHORT>(wParam.usButtonFlags, RI_MOUSE_RIGHT_BUTTON_DOWN))
                            Input::OnPressed(inputState, Input::MouseButton::Right);

                        if (TestFlag<::USHORT>(wParam.usButtonFlags, RI_MOUSE_RIGHT_BUTTON_UP))
                            Input::OnReleased(inputState, Input::MouseButton::Right);

                        if (TestFlag<::USHORT>(wParam.usButtonFlags, RI_MOUSE_MIDDLE_BUTTON_DOWN))
                            Input::OnPressed(inputState, Input::MouseButton::Middle);

                        if (TestFlag<::USHORT>(wParam.usButtonFlags, RI_MOUSE_MIDDLE_BUTTON_UP))
                            Input::OnReleased(inputState, Input::MouseButton::Middle);

                        if (TestFlag<::USHORT>(wParam.usButtonFlags, RI_MOUSE_WHEEL))
                            Input::OnMouseWheelMove(inputState, wParam.sWheelMove / WHEEL_DELTA);
                    }
                    break;
                }
                case DJO_WM_INPUT_KEYBOARD:
                {
                    const ::DjoKbInputWParam wParam = reinterpret_cast<const ::DjoKbInputWParam&>(msg.wParam);

                    DJO_FORMAT_LF(logger, "ApplicationThread DJO_WM_INPUT_KEYBOARD : % [%] [%]",
                        wParam.usFlags,
                        wParam.usScancode, wParam.usVKey);

                    const bool isPressedEvent = !::Djo::TestFlag<::USHORT>(wParam.usFlags, RI_KEY_BREAK);
                    const ::USHORT scanCode = wParam.usScancode
                        | (::Djo::TestFlag<::USHORT>(wParam.usFlags, RI_KEY_E0) ? 0xE000 : 0)
                        | (::Djo::TestFlag<::USHORT>(wParam.usFlags, RI_KEY_E1) ? 0xE100 : 0);

                    if (OnKeyEvent(imgui, LOBYTE(wParam.usVKey), isPressedEvent))
                    {
                        // imgui grabbed event
                    }
                    else
                    {
                        const Input::KbScanCode::EValue kbScanCode = static_cast<Input::KbScanCode::EValue>(scanCode & 0xFF);

                        DJO_FORMAT_LF(logger, "Input keyboard : % % (%)",
                            isPressedEvent, Input::KeyboardScanCodeToString(kbScanCode), (u32)kbScanCode);

                        if (isPressedEvent)
                            Input::OnPressed(inputState, kbScanCode);
                        else
                            Input::OnReleased(inputState, kbScanCode);
                    }

                    break;
                }
                case WM_KEYDOWN:
                {
                    DJO_FORMAT_LF(logger, "ApplicationThread WM_KEYDOWN");
                    break;
                }
                case WM_CHAR:
                {
                    DJO_FORMAT_LF(logger, "ApplicationThread WM_CHAR");

                    if (msg.wParam > 0 && msg.wParam < 0x10000 && OnCharEvent(imgui, LOWORD(msg.wParam)))
                    {
                        // imgui grabbed event
                    }
                    else
                    {

                    }
                    break;
                }
                case WM_SIZE:
                {
                    DJO_FORMAT_LF(logger, "ApplicationThread WM_SIZE");

                    isMinimized = (msg.wParam == SIZE_MINIMIZED);

                    DJO_FORMAT_LF(logger, "[%,%] min:% max:%",
                        LOWORD(msg.lParam), HIWORD(msg.lParam),
                        isMinimized, (msg.wParam == SIZE_MAXIMIZED));

                    Resize(imgui, LOWORD(msg.lParam), HIWORD(msg.lParam));
                    break;
                }
                case WM_ENTERSIZEMOVE:
                case WM_EXITSIZEMOVE:
                {
                    break;
                }
            }
            ++messageCount;
        }

        if (messageCount > 0)
        {
            DJO_FORMAT_LF(logger, "% messages this frame", messageCount);
        }

        if (!isMinimized)
        {
            // Gameplay loop

            ::POINT mousePos{};
            if (::GetCursorPos(&mousePos) && ::ScreenToClient(_windowHandle, &mousePos))
                Input::SetMousePos(inputState, mousePos.x, mousePos.y);

            UpdateFreeCamera(camControl, data->inputState, dtMs);

            // Imgui

            ::ImGui::SetCurrentContext(imgui->ctx);
            ::ImGui::GetIO().DeltaTime = (f32)Sec(dtMs).t;
            ::ImGui::NewFrame();
            ::ImGui::ShowDemoWindow();
            ::ImGui::Render();

            // Render loop

            CameraDesc camera;
            camera.view = GetView(*camControl);
            camera.fov = glm::radians(70.f);

            _vkRenderer->ctx->hasError = false;
            _vkRenderer->camera = camera;
            if (Vk::BeginFrame(_vkRenderer))
            {
                Vk::BeginMainRenderPass(_vkRenderer);
                Vk::Draw(_vkRenderer);

                RenderVulkan(imgui, _vkRenderer->ctx, _vkRenderer->perFrameData[_vkRenderer->currentFrameIndex].commandBuffer);

                Vk::EndMainRenderPass(_vkRenderer);
                Vk::PresentFrame(_vkRenderer);
            }

            if (_vkRenderer->ctx->hasError)
            {
                DJO_FORMAT_LF(
                    logger, "% Vulkan renderer draw error: %",
                    Format::ELogLevel::Error,
                    _vkRenderer->ctx->errorMessage);
            }

            ::Sleep(1); // do not eat all my power please
        }
        else
        {
            // sleep thread
            ::Sleep(100);
        }

        lastLoop = startLoop;
    }

    ShutdownVulkanRender(imgui, _vkRenderer->ctx);
    Shutdown(imgui);
}

static ::DWORD WINAPI ApplicationThread(const ::LPVOID _userData)
{
    using namespace ::Djo;

    const ::HWND windowHandle = reinterpret_cast<::HWND>(_userData);

    // Init thread

    ThreadContext threadCtx{};
    InitAndSetThreadContext(&threadCtx);

    // Init Vulkan

    Vk::VulkanContext vkContext = Vk::MoldContext();
    vkContext.logger = IDE::MoldOutputStringStream();
    if (!Vk::InitContext(&vkContext, windowHandle))
    {
        IDE::Output(vkContext.errorMessage);
        goto ApplicationThread_VulkanShutdown_exit;
    }

    Vk::VulkanRenderer vkRenderer = Vk::MoldRenderer(&vkContext);
    if (!Vk::InitRenderer(&vkRenderer))
    {
        IDE::Output(vkRenderer.ctx->errorMessage);
        goto ApplicationThread_VulkanShutdown_exit;
    }

    // Init raw input

    const ::RAWINPUTDEVICE rids[] =
    {
        { HID_USAGE_PAGE_GENERIC, HID_USAGE_GENERIC_MOUSE,    0, windowHandle },
        { HID_USAGE_PAGE_GENERIC, HID_USAGE_GENERIC_KEYBOARD, 0, windowHandle },
    };
    if (!::RegisterRawInputDevices(rids, ArrayCount(rids), sizeof(::RAWINPUTDEVICE)))
    {
        IDE::Output("RegisterRawInputDevices failed");
        goto ApplicationThread_exit;
    }

    MainLoop(windowHandle, &vkRenderer);

ApplicationThread_VulkanShutdown_exit:

#if __DJO_DEBUG__

    // Shutdown Vulkan

    Vk::ShutdownRenderer(&vkRenderer);
    Vk::ShutdownContext(&vkContext);

#endif

ApplicationThread_exit:

    // Exit

    ::ExitProcess(0);
}

static bool DoPostToApplicationThread(::UINT _msg)
{
    switch (_msg)
    {
        DJO_APP_THREAD_MESSAGE_CASES:
        case WM_INPUT:
            return true;
    }
    return false;
}

int main()
{
    Djo::SetupSystemMemoryLeakWatcher();

    ::WNDCLASSEXA windowClass{};
    windowClass.cbSize = sizeof(windowClass);
    windowClass.lpfnWndProc = ::WindowProc;
    windowClass.hInstance = ::GetModuleHandleA(NULL);
    windowClass.hIcon = ::LoadIconA(NULL, IDI_APPLICATION);
    windowClass.hCursor = ::LoadCursorA(NULL, IDC_ARROW);
    windowClass.hbrBackground = reinterpret_cast<HBRUSH>(::GetStockObject(BLACK_BRUSH));
    windowClass.lpszClassName = WINDOW_NAME;

    ::HWND windowHandle = 0;
    if (::RegisterClassExA(&windowClass))
    {
        const ::DWORD style = WS_EX_APPWINDOW;
        windowHandle = ::CreateWindowExA(
            style, windowClass.lpszClassName, WINDOW_NAME, WS_OVERLAPPEDWINDOW,
            CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
            0, 0, windowClass.hInstance, 0);
    }
    DJO_ASSERT(::IsWindow(windowHandle));

    ::CreateThread(0, 0, ApplicationThread, windowHandle, 0, &ApplicationThreadId);

    for (;;)
    {
        ::MSG msg;
        ::GetMessageA(&msg, 0, 0, 0);
        ::TranslateMessage(&msg);
        if (DoPostToApplicationThread(msg.message))
        {
            if (msg.message == WM_INPUT)
            {
                // GetRawInputData call has great chances to fail if called from another thread,
                // due to HRAWINPUT handle being outdated.
                // So, let's build a standalone message to post to main app update thread.

                ::RAWINPUT raw{};
                ::UINT size;
                const ::HRAWINPUT rawInputHandle = (::HRAWINPUT)msg.lParam;

                ::UINT written = ::GetRawInputData(rawInputHandle, RID_INPUT, NULL, &size, sizeof(::RAWINPUTHEADER));
                if (written != 0 || size > sizeof(::RAWINPUT))
                {
                    DJO_ASSERT(false);
                    continue;
                }
                written = ::GetRawInputData(rawInputHandle, RID_INPUT, &raw, &size, sizeof(::RAWINPUTHEADER));
                if (written != size || written > sizeof(::RAWINPUT))
                {
                    DJO_ASSERT(false);
                    continue;
                }

                if (raw.header.dwType == RIM_TYPEMOUSE)
                {
                    const ::RAWMOUSE rm = raw.data.mouse;
                    ::DjoMouseInputWParam mWParam{};
                    mWParam.usFlags = rm.usFlags;
                    mWParam.usButtonFlags = rm.usButtonFlags;
                    // msdn doc uses plain value cast (short)rm.usButtonData, no reinterpret cast, TODO check results and differences.
                    mWParam.sWheelMove = reinterpret_cast<const ::SHORT&>(rm.usButtonData);
                    ::DjoMouseInputLParam mLParam;
                    mLParam.lLastX = rm.lLastX;
                    mLParam.lLastY = rm.lLastY;

                    msg.message = DJO_WM_INPUT_MOUSE;
                    msg.wParam = reinterpret_cast<const ::WPARAM&>(mWParam);
                    msg.lParam = reinterpret_cast<const ::LPARAM&>(mLParam);
                }
                else if (raw.header.dwType == RIM_TYPEKEYBOARD)
                {
                    // see https://handmade.network/forums/t/2011-keyboard_inputs_-_scancodes,_raw_input,_text_input,_key_names

                    const ::RAWKEYBOARD rkb = raw.data.keyboard;
                    DjoKbInputWParam mWParam{};
                    mWParam.usScancode = rkb.MakeCode;
                    mWParam.usVKey = rkb.VKey;
                    mWParam.usFlags = rkb.Flags;

                    msg.message = DJO_WM_INPUT_KEYBOARD;
                    msg.wParam = reinterpret_cast<const ::WPARAM&>(mWParam);
                    ::Djo::ZeroMem(&msg.lParam); // unused
                }
            }

            ::PostThreadMessageA(ApplicationThreadId, msg.message, msg.wParam, msg.lParam);
        }
        else
        {
            ::DispatchMessageA(&msg);
        }
    }
}
