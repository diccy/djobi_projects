#pragma once

#include "VulkanRenderer.hpp"

#include <DjobiCore/Memory/Arena.hpp>
#include <DjobiCore/Math/Vector2.hpp>

#include <imgui.h>


namespace Djo
{
    struct DearImGuiHandler
    {
        ::ImGuiContext* ctx;

        // render
        struct VkRender
        {
            ::VkDescriptorPool descriptorPool;

            struct MaterialPipeline
            {
                ::VkDescriptorSetLayout layout;
                ::VkPipelineLayout pipelineLayout;
                ::VkPipeline graphicsPipeline;
            };
            MaterialPipeline materialPipeline;

            struct MaterialInstance
            {
                const MaterialPipeline* pipeline;

                struct Resources
                {
                    Vk::AllocatedVkImage fontAtlasImage;
                    ::VkImageView fontAtlasImageView;
                    ::VkSampler fontTextureSampler;
                };
                Resources resources;

                struct Constants
                {
                    Vec2f scale;
                    Vec2f translate;
                };
                Constants constants;

                ::VkDescriptorSet descriptorSet;
            };
            MaterialInstance material;

            static constexpr u32 c_concurrentFrameCount{ 2 };
            struct PerFrameData
            {
                Vk::AllocatedVkBuffer vertexBuffer;
                Vk::AllocatedVkBuffer indexBuffer;
                sz vertexCount;
                sz indexCount;
            };
            PerFrameData perFrameData[c_concurrentFrameCount];
            u32 currentFrameIndex;
        };
        VkRender vk;
    };

    void Init(DearImGuiHandler* _imgui, void* _windowHandle) noexcept;

    ::VkResult InitVulkanRender(
        DearImGuiHandler* _imgui,
        Vk::VulkanContext* _vulkanContext,
        ::VkCommandPool _stagingCommandPool,
        ::VkRenderPass _renderPass) noexcept;

    void RenderVulkan(DearImGuiHandler* _imgui, Vk::VulkanContext* _vulkanContext, ::VkCommandBuffer _cmd) noexcept;

    void ShutdownVulkanRender(DearImGuiHandler* _imgui, Vk::VulkanContext* _vulkanContext) noexcept;
    void Shutdown(DearImGuiHandler* _imgui) noexcept;

    void Resize(DearImGuiHandler* _imgui, u32 _width, u32 _height) noexcept;
    bool OnKeyEvent(DearImGuiHandler* _imgui, u8 _virtualKey, bool _pressed) noexcept;
    bool OnCharEvent(DearImGuiHandler* _imgui, u16 _char) noexcept;
    bool OnMouseEvent(DearImGuiHandler* _imgui, void* _windowHandle, u16 _buttonFlags, i32 _moveX, i32 _moveY, i16 _wheelMove) noexcept;
}
