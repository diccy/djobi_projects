#include "Window_pch.h"

#include "CameraController.hpp"

#include <DjobiCore/Math/Quaternion.hpp>
#include <DjobiCore/Math/Matrix33.hpp>
#include <DjobiCore/Math/Math.hpp>
#include <DjobiCore/Core/Utils.hpp>


namespace Djo
{
    namespace _Internal
    {
        void ZoomToTarget(CameraController* const _cameraCtrl, const f32 _factor) noexcept
        {
            constexpr f32 c_maxSmooth = 5.f;
            constexpr f32 c_minSmooth = 1.f;

            const Vec3f fromOrigin = _cameraCtrl->targetGoalWorldPos - _cameraCtrl->cameraGoalWorldPos;
            const f32 dirLength = glm::length(fromOrigin);
            const Vec3f dir = fromOrigin / dirLength;
            f32 smoothFactor = 1.f;
            if (dirLength < c_maxSmooth && _factor > 0.f)
                smoothFactor = Lerp(0.f, 1.f, (Max(c_minSmooth, dirLength) - c_minSmooth) / (c_maxSmooth - c_minSmooth));

            _cameraCtrl->cameraGoalWorldPos += dir * (_factor * smoothFactor);
        }

        void MoveCameraAroundTarget(CameraController* const _cameraCtrl, const f32 _angle, const Vec3f& _axis) noexcept
        {
            const Vec3f fromOrigin = _cameraCtrl->cameraGoalWorldPos - _cameraCtrl->targetGoalWorldPos;
            _cameraCtrl->cameraGoalWorldPos = (glm::angleAxis(_angle, _axis) * fromOrigin) + _cameraCtrl->targetGoalWorldPos;
        }

        void MoveTargetAroundCamera(CameraController* const _cameraCtrl, const f32 _angle, const Vec3f& _axis) noexcept
        {
            const Vec3f fromOrigin = _cameraCtrl->targetGoalWorldPos - _cameraCtrl->cameraGoalWorldPos;
            _cameraCtrl->targetGoalWorldPos = (glm::angleAxis(_angle, _axis) * fromOrigin) + _cameraCtrl->cameraGoalWorldPos;
        }

        void AvoidUpAxis(Vec3f* const _pos, const Vec3f& _targetPos) noexcept
        {
            constexpr f32 c_maxDot = 0.95f;

            const Vec3f fromOrigin = _targetPos - *_pos;
            const f32 dirLength = glm::length(fromOrigin);
            const Vec3f dir = fromOrigin / dirLength;
            const f32 dot = glm::dot(dir, c_cameraUp);
            const f32 absDot = Abs(dot);
            if (absDot > c_maxDot)
            {
                const Vec3f axis = (Equals(absDot, 1.f, 0.01f)) ? c_cameraRight : glm::normalize(glm::cross(c_cameraUp, dir));
                const f32 angle = ::acosf(dot > 0.f ? c_maxDot : -c_maxDot) - glm::radians(180.f);
                const Vec3f newPos = ((glm::angleAxis(angle, axis) * c_cameraUp) * dirLength) + _targetPos;
                *_pos = newPos;
            }
        }

        void SetRotation(Mat44f* const _transform, const Quatf& _rot) noexcept
        {
            const Mat33f rotMatrix{ glm::mat3_cast(_rot) };
            reinterpret_cast<Vec3f&>((*_transform)[0]) = rotMatrix[0];
            reinterpret_cast<Vec3f&>((*_transform)[1]) = rotMatrix[1];
            reinterpret_cast<Vec3f&>((*_transform)[2]) = rotMatrix[2];
        }

        void LookAtTarget(CameraController* const _cameraCtrl) noexcept
        {
            AvoidUpAxis(&_cameraCtrl->cameraGoalWorldPos, _cameraCtrl->targetGoalWorldPos);
            const Vec3f dir = glm::normalize(_cameraCtrl->targetWorldPos - Mat44Pos(_cameraCtrl->cameraWorld));
            const Quatf rotLookAt = glm::quatLookAt(dir, c_cameraUp);
            SetRotation(&_cameraCtrl->cameraWorld, rotLookAt);
        }
    }

    void UpdateFreeCamera(CameraController* const _cameraCtrl, const Input::State& _input, Milliseconds _dt) noexcept
    {
        const f64 dtSec = Sec(_dt).t;

        // Slide
        const bool moveForwardPressed = Input::IsDown(_input, Input::KbScanCode::W) || Input::IsDown(_input, Input::KbScanCode::UpArrow);
        const bool moveLeftPressed =    Input::IsDown(_input, Input::KbScanCode::A) || Input::IsDown(_input, Input::KbScanCode::LeftArrow);
        const bool moveBackPressed =    Input::IsDown(_input, Input::KbScanCode::S) || Input::IsDown(_input, Input::KbScanCode::DownArrow);
        const bool moveRightPressed =   Input::IsDown(_input, Input::KbScanCode::D) || Input::IsDown(_input, Input::KbScanCode::RightArrow);
        const Vec2f keyboardMove
        {
            (moveForwardPressed ? 1.f : 0.f) - (moveBackPressed ? 1.f : 0.f),
            (moveRightPressed ?   1.f : 0.f) - (moveLeftPressed ? 1.f : 0.f),
        };
        if (!Equals(glm::dot(keyboardMove, keyboardMove), 0.f, 0.01f))
        {
            constexpr f32 c_moveFactorMultiplier{ 0.5f };
            const f32 distFromTarget = glm::length(_cameraCtrl->targetGoalWorldPos - _cameraCtrl->cameraGoalWorldPos);
            const f64 moveFactor = Max(1.f, _cameraCtrl->moveSpeed * distFromTarget * c_moveFactorMultiplier) * dtSec;
            const Vec3f right = glm::normalize(Mat44Right(_cameraCtrl->cameraWorld)); // avoid cross products with goals positions, use direct camera transform as approximation
            const Vec3f forward = glm::normalize(Mat44Forward(_cameraCtrl->cameraWorld));
            const Vec3f move = (forward * (f32)moveFactor * keyboardMove.x) + (right * (f32)moveFactor * keyboardMove.y);
            _cameraCtrl->cameraGoalWorldPos += move;
            _cameraCtrl->targetGoalWorldPos += move;
        }

        // Zoom
        const i32 mouseScroll = _input.mouse.wheelMove;
        if (mouseScroll != 0)
        {
            constexpr f32 c_moveFactorMultiplier{ 0.002f };
            const f32 distFromTarget = glm::length(_cameraCtrl->targetGoalWorldPos - _cameraCtrl->cameraGoalWorldPos);
            const f64 moveFactor = Max(1.f, _cameraCtrl->moveSpeed * distFromTarget * c_moveFactorMultiplier); // do not apply dt on mouse wheel
            const f32 moveFactorY = static_cast<f32>(moveFactor * mouseScroll);
            _Internal::ZoomToTarget(_cameraCtrl, moveFactorY);
        }

        // Look around
        const bool doMoveTargetAroundCamera = Input::IsDown(_input, Input::MouseButton::Middle);
        const bool doMoveCameraAroundTarget = Input::IsDown(_input, Input::MouseButton::Right);
        if (doMoveTargetAroundCamera || doMoveCameraAroundTarget)
        {
            constexpr f32 c_rotFactorMultiplier{ 0.001f };
            const Vec2i mouseMove = _input.mouse.move;
            const f32 rotFactor = _cameraCtrl->moveSpeed * c_rotFactorMultiplier;
            if (Abs(mouseMove.x) > 0.f)
            {
                const f32 rotFactorX = rotFactor * mouseMove.x;
                if (doMoveCameraAroundTarget)
                    _Internal::MoveCameraAroundTarget(_cameraCtrl, rotFactorX, c_cameraUp);
                else
                    _Internal::MoveTargetAroundCamera(_cameraCtrl, rotFactorX, c_cameraUp);
            }
            if (Abs(mouseMove.y) > 0.f)
            {
                const Vec3f dir = glm::normalize(_cameraCtrl->targetGoalWorldPos - _cameraCtrl->cameraGoalWorldPos);
                const Vec3f axis = glm::normalize(glm::cross(c_cameraUpClose, dir));
                const f32 angularSpeedFactor = Min(1.f, 1.2f - Abs(glm::dot(dir, c_cameraUp)));
                const f32 rotFactorY = rotFactor * angularSpeedFactor * mouseMove.y;
                if (doMoveCameraAroundTarget)
                    _Internal::MoveCameraAroundTarget(_cameraCtrl, rotFactorY, axis);
                else
                    _Internal::MoveTargetAroundCamera(_cameraCtrl, rotFactorY, axis);
            }
        }

        // Update
        // adapt lerp factor to framerate
        const f32 lerpFactor = Lerp(0.3f, 0.6f, (f32)LerpFactor<f64>(1. / 120., 0.1, dtSec));
        _cameraCtrl->targetWorldPos = Lerp(_cameraCtrl->targetWorldPos, _cameraCtrl->targetGoalWorldPos, lerpFactor);
        Mat44Pos(_cameraCtrl->cameraWorld) = Lerp(Mat44Pos(_cameraCtrl->cameraWorld), _cameraCtrl->cameraGoalWorldPos, lerpFactor);
        _Internal::LookAtTarget(_cameraCtrl);
    }
}
