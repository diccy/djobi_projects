#include "Window_pch.h"

#define VMA_IMPLEMENTATION
// 4100 : unreferenced formal parameter
// 4127 : conditional expression is constant
// 4189 : local variable is initialized but not referenced
// 4324 : structure was padded due to alignment specifier
#pragma warning(push)
#pragma warning(disable : 4100 4127 4189 4324)
#include <vma/vk_mem_alloc.h>
#pragma warning(pop)
