#include "Window_pch.h"

#include "InputState.hpp"

#include <DjobiCore/Core/Utils.hpp>


namespace Djo
{
    namespace Input
    {
        void ClearInputEvents(State* const _state) noexcept
        {
            ZeroMem(&_state->mouse.event);
            _state->mouse.move = { 0, 0 };
            _state->mouse.wheelMove = 0;
            ZeroMem(&_state->kb.event);
        }

        void OnPressed(State* const _state, const MouseButton::EValue _mouseButton) noexcept
        {
            _state->mouse.event[_mouseButton] = EEvent::Pressed;
            _state->mouse.down[_mouseButton] = true;
        }

        void OnReleased(State* const _state, const MouseButton::EValue _mouseButton) noexcept
        {
            _state->mouse.event[_mouseButton] = EEvent::Released;
            _state->mouse.down[_mouseButton] = false;
        }

        void OnPressed(State* const _state, const KbScanCode::EValue _kbScanCode) noexcept
        {
            _state->kb.event[_kbScanCode] = _state->kb.down[_kbScanCode] ? EEvent::Repeated : EEvent::Pressed;
            _state->kb.down[_kbScanCode] = true;
        }

        void OnReleased(State* const _state, const KbScanCode::EValue _kbScanCode) noexcept
        {
            _state->kb.event[_kbScanCode] = EEvent::Released;
            _state->kb.down[_kbScanCode] = false;
        }

        void OnMouseMove(State* const _state, const i32 _moveX, const i32 _moveY) noexcept
        {
            _state->mouse.move += Vec2i{ _moveX, _moveY };
        }

        void OnMouseWheelMove(State* const _state, const i32 _move) noexcept
        {
            _state->mouse.wheelMove += _move;
        }

        void SetMousePos(State* const _state, const i32 _posX, const i32 _posY) noexcept
        {
            _state->mouse.pos = Vec2i{ _posX, _posY };
        }

        bool IsDown(const State& _state, const MouseButton::EValue _mouseButton) noexcept
        {
            return _state.mouse.down[_mouseButton];
        }

        bool IsPressedEvent(const State& _state, const MouseButton::EValue _mouseButton) noexcept
        {
            return IsDown(_state, _mouseButton) && _state.mouse.event[_mouseButton] == EEvent::Pressed;
        }

        bool IsUp(const State& _state, const MouseButton::EValue _mouseButton) noexcept
        {
            return !IsDown(_state, _mouseButton);
        }

        bool IsReleasedEvent(const State& _state, const MouseButton::EValue _mouseButton) noexcept
        {
            return IsDown(_state, _mouseButton) && _state.mouse.event[_mouseButton] == EEvent::Released;
        }

        bool IsDown(const State& _state, const KbScanCode::EValue _kbScanCode) noexcept
        {
            return _state.kb.down[_kbScanCode];
        }

        bool IsPressedEvent(const State& _state, const KbScanCode::EValue _kbScanCode) noexcept
        {
            return IsDown(_state, _kbScanCode) && _state.kb.event[_kbScanCode] == EEvent::Pressed;
        }

        bool OnPressedOrRepeatedEvent(const State& _state, const KbScanCode::EValue _kbScanCode) noexcept
        {
            const EEvent event = _state.kb.event[_kbScanCode];
            return IsDown(_state, _kbScanCode) && event == EEvent::Pressed || event == EEvent::Repeated;
        }

        bool IsUp(const State& _state, const KbScanCode::EValue _kbScanCode) noexcept
        {
            return !IsDown(_state, _kbScanCode);
        }

        bool IsReleasedEvent(const State& _state, const KbScanCode::EValue _kbScanCode) noexcept
        {
            return IsDown(_state, _kbScanCode) && _state.kb.event[_kbScanCode] == EEvent::Released;
        }
    }
}
