// no pragma once

#define STBI_ASSERT(condition_)  DJO_ASSERT(condition_)

// TODO : how to setup arena logic here ?
#define STBI_MALLOC(sz_)          ::malloc(sz_)
#define STBI_REALLOC(p_, newsz_)  ::realloc(p_, newsz_)
#define STBI_FREE(p_)             ::free(p_)

#define STBI_NO_STDIO

#define STBI_NO_PSD
#define STBI_NO_GIF
#define STBI_NO_PIC
#define STBI_NO_PNM

#define STBI_MAX_DIMENSIONS  (1 << 16)

#include <stb/stb_image.h>

#undef STBI_MAX_DIMENSIONS

#undef STBI_NO_PSD
#undef STBI_NO_GIF
#undef STBI_NO_PIC
#undef STBI_NO_PNM

#undef STBI_NO_STDIO

#undef STBI_MALLOC
#undef STBI_REALLOC
#undef STBI_FREE

#undef STBI_ASSERT
