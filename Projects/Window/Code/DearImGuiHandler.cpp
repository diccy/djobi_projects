#include "Window_pch.h"

#include "DearImGuiHandler.hpp"
#include "DearImGuiShaders.hpp"
#include "VulkanStructs.hpp"

#include <imgui.h>


namespace Djo
{
    void Init(DearImGuiHandler* const _imgui, void* const _windowHandle) noexcept
    {
        ::ImGuiContext* const ctx = ::ImGui::CreateContext();
        ::ImGui::SetCurrentContext(ctx);

        ::ImGui::StyleColorsDark();

        ::ImGuiIO* const io = &::ImGui::GetIO();
        io->DisplayFramebufferScale = ::ImVec2{ 1.f, 1.f };
        io->AddMouseSourceEvent(::ImGuiMouseSource_Mouse);

        const ::HWND windowHandle = reinterpret_cast<::HWND>(_windowHandle);
        ::RECT area;
        ::GetClientRect(windowHandle, &area);
        io->DisplaySize = ::ImVec2{ (f32)area.right, (f32)area.bottom };

        _imgui->ctx = ctx;
    }

    ::VkResult InitVulkanRender(
        DearImGuiHandler* const _imgui,
        Vk::VulkanContext* const _vulkanContext,
        const ::VkCommandPool _stagingCommandPool,
        const ::VkRenderPass _renderPass) noexcept
    {
        ::VkResult result;

        ::ImGui::SetCurrentContext(_imgui->ctx);
        ::ImGuiIO* const io = &::ImGui::GetIO();
        const ::VkDevice vkDevice = _vulkanContext->device;
        const ::VmaAllocator vmaAlloc = _vulkanContext->allocator;

        ZeroMem(&_imgui->vk);

        // Font

        unsigned char* fontAtlasData;
        int fontAtlasWidth, fontAtlasHeight;
        io->Fonts->GetTexDataAsRGBA32(&fontAtlasData, &fontAtlasWidth, &fontAtlasHeight);

        Vk::AllocatedVkImage fontAtlasImage{};
        ::VkImageView fontAtlasImageView;
        result = Vk::CreateAndPrepareImage2DForFragmentShaderRead(
            vkDevice, vmaAlloc,
            fontAtlasData, fontAtlasWidth * fontAtlasHeight * 4, ::VkExtent3D{ (u32)fontAtlasWidth, (u32)fontAtlasHeight, 1 },
            ::VK_FORMAT_R8G8B8A8_UNORM,
            _stagingCommandPool, _vulkanContext->graphicsQueue,
            &fontAtlasImage, &fontAtlasImageView);
        if (result != ::VK_SUCCESS)
        {
            DJO_ASSERT(false);
            return result;
        }

        _imgui->vk.material.resources.fontAtlasImage = fontAtlasImage;
        _imgui->vk.material.resources.fontAtlasImageView = fontAtlasImageView;

        ::VkSamplerCreateInfo fontTextureSamplerCreateInfo = Vk::SamplerCreateInfo();
        fontTextureSamplerCreateInfo.magFilter = ::VK_FILTER_LINEAR;
        fontTextureSamplerCreateInfo.minFilter = ::VK_FILTER_LINEAR;
        fontTextureSamplerCreateInfo.mipmapMode = ::VK_SAMPLER_MIPMAP_MODE_LINEAR;
        fontTextureSamplerCreateInfo.addressModeU = ::VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        fontTextureSamplerCreateInfo.addressModeV = ::VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        fontTextureSamplerCreateInfo.addressModeW = ::VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        fontTextureSamplerCreateInfo.borderColor = ::VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;

        ::VkSampler fontTextureSampler;
        result = ::vkCreateSampler(vkDevice, &fontTextureSamplerCreateInfo, nullptr, &fontTextureSampler);
        if (result != ::VK_SUCCESS)
        {
            DJO_ASSERT(false);
            return result;
        }

        _imgui->vk.material.resources.fontTextureSampler = fontTextureSampler;

        // Descriptor set

        ::VkDescriptorPoolSize poolSize{};
        poolSize.type = ::VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        poolSize.descriptorCount = 1;

        ::VkDescriptorPoolCreateInfo descriptorPoolCreateInfo = Vk::DescriptorPoolCreateInfo();
        descriptorPoolCreateInfo.maxSets = 1;
        descriptorPoolCreateInfo.poolSizeCount = 1;
        descriptorPoolCreateInfo.pPoolSizes = &poolSize;

        ::VkDescriptorPool descriptorPool;
        result = ::vkCreateDescriptorPool(vkDevice, &descriptorPoolCreateInfo, nullptr, &descriptorPool);
        if (result != ::VK_SUCCESS)
        {
            DJO_ASSERT(false);
            return result;
        }

        _imgui->vk.descriptorPool = descriptorPool;

        ::VkDescriptorSetLayoutBinding descriptorSetLayoutBinding{};
        descriptorSetLayoutBinding.binding = 0;
        descriptorSetLayoutBinding.descriptorType = ::VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        descriptorSetLayoutBinding.descriptorCount = 1;
        descriptorSetLayoutBinding.stageFlags = ::VK_SHADER_STAGE_FRAGMENT_BIT;
        descriptorSetLayoutBinding.pImmutableSamplers = nullptr;

        ::VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo = Vk::DescriptorSetLayoutCreateInfo();
        descriptorSetLayoutCreateInfo.bindingCount = 1;
        descriptorSetLayoutCreateInfo.pBindings = &descriptorSetLayoutBinding;

        ::VkDescriptorSetLayout descriptorSetLayout;
        result = ::vkCreateDescriptorSetLayout(vkDevice, &descriptorSetLayoutCreateInfo, nullptr, &descriptorSetLayout);
        if (result != ::VK_SUCCESS)
        {
            DJO_ASSERT(false);
            return result;
        }

        _imgui->vk.materialPipeline.layout = descriptorSetLayout;

        ::VkDescriptorSetAllocateInfo descriptorSetAllocateInfo = Vk::DescriptorSetAllocateInfo();
        descriptorSetAllocateInfo.descriptorPool = _imgui->vk.descriptorPool;
        descriptorSetAllocateInfo.descriptorSetCount = 1;
        descriptorSetAllocateInfo.pSetLayouts = &_imgui->vk.materialPipeline.layout;

        ::VkDescriptorSet descriptorSet;
        result = ::vkAllocateDescriptorSets(vkDevice, &descriptorSetAllocateInfo, &descriptorSet);
        if (result != ::VK_SUCCESS)
        {
            DJO_ASSERT(false);
            return result;
        }

        _imgui->vk.material.descriptorSet = descriptorSet;

        ::VkDescriptorImageInfo fontDescriptorImageInfo{};
        fontDescriptorImageInfo.sampler = _imgui->vk.material.resources.fontTextureSampler;
        fontDescriptorImageInfo.imageView = _imgui->vk.material.resources.fontAtlasImageView;
        fontDescriptorImageInfo.imageLayout = ::VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

        ::VkWriteDescriptorSet descriptorSetWriteInfo = Vk::DescriptorSetWriteInfo();
        descriptorSetWriteInfo.dstSet = _imgui->vk.material.descriptorSet;
        descriptorSetWriteInfo.dstBinding = 0;
        descriptorSetWriteInfo.dstArrayElement = 0;
        descriptorSetWriteInfo.descriptorCount = 1;
        descriptorSetWriteInfo.descriptorType = ::VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        descriptorSetWriteInfo.pImageInfo = &fontDescriptorImageInfo;
        descriptorSetWriteInfo.pBufferInfo = nullptr;
        descriptorSetWriteInfo.pTexelBufferView = nullptr;
        ::vkUpdateDescriptorSets(vkDevice, 1, &descriptorSetWriteInfo, 0, nullptr);

        // Graphics pipeline

        ::VkPushConstantRange pushConstantRange{};
        pushConstantRange.stageFlags = ::VK_SHADER_STAGE_VERTEX_BIT;
        pushConstantRange.size = sizeof(DearImGuiHandler::VkRender::MaterialInstance::Constants);

        ::VkPipelineLayoutCreateInfo layoutCreateInfo = Vk::PipelineLayoutCreateInfo();
        layoutCreateInfo.setLayoutCount = 1;
        layoutCreateInfo.pSetLayouts = &_imgui->vk.materialPipeline.layout;
        layoutCreateInfo.pushConstantRangeCount = 1;
        layoutCreateInfo.pPushConstantRanges = &pushConstantRange;

        ::VkPipelineLayout pipelineLayout;
        result = ::vkCreatePipelineLayout(vkDevice, &layoutCreateInfo, nullptr, &pipelineLayout);
        if (result != ::VK_SUCCESS)
        {
            DJO_ASSERT(false);
            return result;
        }

        _imgui->vk.materialPipeline.pipelineLayout = pipelineLayout;

        ::VkPipelineInputAssemblyStateCreateInfo inputAssemblyStateCreateInfo = Vk::PipelineInputAssemblyStateCreateInfo();
        inputAssemblyStateCreateInfo.topology = ::VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;

        const ::VkVertexInputBindingDescription vertexInputBindingDesc{
            0, sizeof(ImDrawVert), ::VK_VERTEX_INPUT_RATE_VERTEX
        };
        const ::VkVertexInputAttributeDescription vertexInputAttributeDescription[3]{
            { 0, 0, ::VK_FORMAT_R32G32_SFLOAT,  offsetof(ImDrawVert, pos), },
            { 1, 0, ::VK_FORMAT_R32G32_SFLOAT,  offsetof(ImDrawVert, uv), },
            { 2, 0, ::VK_FORMAT_R8G8B8A8_UNORM, offsetof(ImDrawVert, col), },
        };
        ::VkPipelineVertexInputStateCreateInfo vertexInputStateCreateInfo = Vk::PipelineVertexInputStateCreateInfo();
        vertexInputStateCreateInfo.vertexBindingDescriptionCount = 1;
        vertexInputStateCreateInfo.pVertexBindingDescriptions = &vertexInputBindingDesc;
        vertexInputStateCreateInfo.vertexAttributeDescriptionCount = ArrayCount(vertexInputAttributeDescription);
        vertexInputStateCreateInfo.pVertexAttributeDescriptions = vertexInputAttributeDescription;

        const ::VkDynamicState dynamicStates[] = { ::VK_DYNAMIC_STATE_VIEWPORT, ::VK_DYNAMIC_STATE_SCISSOR };
        ::VkPipelineDynamicStateCreateInfo dynamicStateCreateInfo = Vk::PipelineDynamicStateCreateInfo();
        dynamicStateCreateInfo.dynamicStateCount = ArrayCount(dynamicStates);
        dynamicStateCreateInfo.pDynamicStates = dynamicStates;

        ::VkPipelineViewportStateCreateInfo viewportStateCreateInfo = Vk::PipelineViewportStateCreateInfo();
        viewportStateCreateInfo.viewportCount = 1;
        viewportStateCreateInfo.pViewports = nullptr; // dynamic
        viewportStateCreateInfo.scissorCount = 1;
        viewportStateCreateInfo.pScissors = nullptr; // dynamic

        ::VkPipelineRasterizationStateCreateInfo rasterizationStateCreateInfo = Vk::PipelineRasterizationStateCreateInfo();
        rasterizationStateCreateInfo.polygonMode = ::VK_POLYGON_MODE_FILL;
        rasterizationStateCreateInfo.cullMode = ::VK_CULL_MODE_NONE;
        rasterizationStateCreateInfo.frontFace = ::VK_FRONT_FACE_COUNTER_CLOCKWISE;
        rasterizationStateCreateInfo.lineWidth = 1.f;

        ::VkPipelineMultisampleStateCreateInfo multisampleStateCreateInfo = Vk::PipelineMultisampleStateCreateInfo();
        multisampleStateCreateInfo.rasterizationSamples = ::VK_SAMPLE_COUNT_1_BIT;

        ::VkPipelineDepthStencilStateCreateInfo depthStencilStateCreateInfo = Vk::PipelineDepthStencilStateCreateInfo();
        depthStencilStateCreateInfo.depthCompareOp = ::VK_COMPARE_OP_LESS_OR_EQUAL;

        ::VkPipelineColorBlendAttachmentState colorBlendAttachmentState{};
        colorBlendAttachmentState.blendEnable = VK_TRUE;
        colorBlendAttachmentState.srcColorBlendFactor = ::VK_BLEND_FACTOR_SRC_ALPHA;
        colorBlendAttachmentState.dstColorBlendFactor = ::VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
        colorBlendAttachmentState.colorBlendOp = ::VK_BLEND_OP_ADD;
        colorBlendAttachmentState.srcAlphaBlendFactor = ::VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
        colorBlendAttachmentState.dstAlphaBlendFactor = ::VK_BLEND_FACTOR_ZERO;
        colorBlendAttachmentState.alphaBlendOp = ::VK_BLEND_OP_ADD;
        colorBlendAttachmentState.colorWriteMask = ::VK_COLOR_COMPONENT_R_BIT | ::VK_COLOR_COMPONENT_G_BIT | ::VK_COLOR_COMPONENT_B_BIT | ::VK_COLOR_COMPONENT_A_BIT;

        ::VkPipelineColorBlendStateCreateInfo colorBlendStateCreateInfo = Vk::PipelineColorBlendStateCreateInfo();
        colorBlendStateCreateInfo.attachmentCount = 1;
        colorBlendStateCreateInfo.pAttachments = &colorBlendAttachmentState;

        // Shader modules

        ::VkShaderModuleCreateInfo meshShaderModuleCreateInfos[2]{};

        meshShaderModuleCreateInfos[0] = Vk::ShaderModuleCreateInfo();
        meshShaderModuleCreateInfos[0].codeSize = ArrayCount(s_DearImGui_vert_glsl_spv) * sizeof(u32);
        meshShaderModuleCreateInfos[0].pCode = s_DearImGui_vert_glsl_spv;
        meshShaderModuleCreateInfos[1] = Vk::ShaderModuleCreateInfo();
        meshShaderModuleCreateInfos[1].codeSize = ArrayCount(s_DearImGui_frag_glsl_spv) * sizeof(u32);
        meshShaderModuleCreateInfos[1].pCode = s_DearImGui_frag_glsl_spv;

        ::VkShaderModule meshShaderModules[2]{};
        result = ::vkCreateShaderModule(vkDevice, &meshShaderModuleCreateInfos[0], nullptr, &meshShaderModules[0]);
        if (result != ::VK_SUCCESS)
        {
            DJO_ASSERT(false);
            return result;
        }
        DJO_DEFER_LAMBDA([vkDevice, shaderModule = meshShaderModules[0]]() { ::vkDestroyShaderModule(vkDevice, shaderModule, nullptr); });

        result = ::vkCreateShaderModule(vkDevice, &meshShaderModuleCreateInfos[1], nullptr, &meshShaderModules[1]);
        if (result != ::VK_SUCCESS)
        {
            DJO_ASSERT(false);
            return result;
        }
        DJO_DEFER_LAMBDA([vkDevice, shaderModule = meshShaderModules[1]]() { ::vkDestroyShaderModule(vkDevice, shaderModule, nullptr); });

        ::VkPipelineShaderStageCreateInfo pipelineShaderStageCreateInfo[2]{};
        pipelineShaderStageCreateInfo[0] = Vk::PipelineShaderStageCreateInfo();
        pipelineShaderStageCreateInfo[0].stage = ::VK_SHADER_STAGE_VERTEX_BIT;
        pipelineShaderStageCreateInfo[0].module = meshShaderModules[0];
        pipelineShaderStageCreateInfo[0].pName = "main";
        pipelineShaderStageCreateInfo[0].pSpecializationInfo = nullptr;
        pipelineShaderStageCreateInfo[1] = Vk::PipelineShaderStageCreateInfo();
        pipelineShaderStageCreateInfo[1].stage = ::VK_SHADER_STAGE_FRAGMENT_BIT;
        pipelineShaderStageCreateInfo[1].module = meshShaderModules[1];
        pipelineShaderStageCreateInfo[1].pName = "main";
        pipelineShaderStageCreateInfo[1].pSpecializationInfo = nullptr;

        ::VkGraphicsPipelineCreateInfo graphicsPipelineCreateInfo = Vk::GraphicsPipelineCreateInfo();
        graphicsPipelineCreateInfo.stageCount = ArrayCount(pipelineShaderStageCreateInfo);
        graphicsPipelineCreateInfo.pStages = pipelineShaderStageCreateInfo;
        graphicsPipelineCreateInfo.pVertexInputState = &vertexInputStateCreateInfo;
        graphicsPipelineCreateInfo.pInputAssemblyState = &inputAssemblyStateCreateInfo;
        graphicsPipelineCreateInfo.pTessellationState = nullptr;
        graphicsPipelineCreateInfo.pViewportState = &viewportStateCreateInfo;
        graphicsPipelineCreateInfo.pRasterizationState = &rasterizationStateCreateInfo;
        graphicsPipelineCreateInfo.pMultisampleState = &multisampleStateCreateInfo;
        graphicsPipelineCreateInfo.pDepthStencilState = &depthStencilStateCreateInfo;
        graphicsPipelineCreateInfo.pColorBlendState = &colorBlendStateCreateInfo;
        graphicsPipelineCreateInfo.pDynamicState = &dynamicStateCreateInfo;
        graphicsPipelineCreateInfo.layout = pipelineLayout;
        graphicsPipelineCreateInfo.renderPass = _renderPass;
        graphicsPipelineCreateInfo.subpass = 0;
        graphicsPipelineCreateInfo.basePipelineHandle = VK_NULL_HANDLE;
        graphicsPipelineCreateInfo.basePipelineIndex = -1;

        ::VkPipeline graphicsPipeline;
        result = ::vkCreateGraphicsPipelines(vkDevice, VK_NULL_HANDLE, 1, &graphicsPipelineCreateInfo, nullptr, &graphicsPipeline);
        if (result != ::VK_SUCCESS)
        {
            DJO_ASSERT(false);
            return result;
        }

        _imgui->vk.materialPipeline.graphicsPipeline = graphicsPipeline;
        _imgui->vk.material.pipeline = &_imgui->vk.materialPipeline;
        return ::VK_SUCCESS;
    }

    void RenderVulkan(DearImGuiHandler* const _imgui, Vk::VulkanContext* const _vulkanContext, const ::VkCommandBuffer _cmd) noexcept
    {
        ::ImGui::SetCurrentContext(_imgui->ctx);
        ::ImGuiIO* const io = &::ImGui::GetIO();
        ::ImDrawData* const dd = ::ImGui::GetDrawData();
        DJO_ASSERT(dd->Valid);
        if (dd->CmdListsCount <= 0 || dd->TotalVtxCount <= 0)
            return;

        int frameBufferWidth = (int)(dd->DisplaySize.x * dd->FramebufferScale.x);
        int frameBufferHeight = (int)(dd->DisplaySize.y * dd->FramebufferScale.y);
        if (frameBufferWidth <= 0 || frameBufferHeight <= 0)
            return; // minimized

        DearImGuiHandler::VkRender* const vk = &_imgui->vk;
        DearImGuiHandler::VkRender::PerFrameData* const frameData = &vk->perFrameData[vk->currentFrameIndex];

        if (frameData->vertexBuffer.buffer == VK_NULL_HANDLE || dd->TotalVtxCount > frameData->vertexCount)
        {
            Vk::DestroyAllocatedVkBuffer(_vulkanContext->allocator, &frameData->vertexBuffer);
            Vk::CreateAllocatedVkBuffer(
                _vulkanContext->allocator, dd->TotalVtxCount * sizeof(::ImDrawVert),
                ::VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
                ::VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
                0,
                &frameData->vertexBuffer);
            frameData->vertexCount = dd->TotalVtxCount;
        }
        if (frameData->indexBuffer.buffer == VK_NULL_HANDLE || dd->TotalIdxCount > frameData->indexCount)
        {
            Vk::DestroyAllocatedVkBuffer(_vulkanContext->allocator, &frameData->indexBuffer);
            Vk::CreateAllocatedVkBuffer(
                _vulkanContext->allocator, dd->TotalIdxCount * sizeof(::ImDrawIdx),
                ::VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
                ::VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
                0,
                &frameData->indexBuffer);
            frameData->indexCount = dd->TotalIdxCount;
        }

        ::ImDrawVert* vertexBufferData;
        ::ImDrawIdx* indexBufferData;
        ::vmaMapMemory(_vulkanContext->allocator, frameData->vertexBuffer.alloc, (void**)&vertexBufferData);
        ::vmaMapMemory(_vulkanContext->allocator, frameData->indexBuffer.alloc, (void**)&indexBufferData);
        for (int n = 0; n < dd->CmdListsCount; n++)
        {
            const ::ImDrawList* const imDrawList = dd->CmdLists[n];
            ::memcpy(vertexBufferData, imDrawList->VtxBuffer.Data, imDrawList->VtxBuffer.Size * sizeof(::ImDrawVert));
            ::memcpy(indexBufferData, imDrawList->IdxBuffer.Data, imDrawList->IdxBuffer.Size * sizeof(::ImDrawIdx));
            vertexBufferData += imDrawList->VtxBuffer.Size;
            indexBufferData += imDrawList->IdxBuffer.Size;
        }
        ::vmaFlushAllocation(_vulkanContext->allocator, frameData->vertexBuffer.alloc, 0, VK_WHOLE_SIZE);
        ::vmaFlushAllocation(_vulkanContext->allocator, frameData->indexBuffer.alloc, 0, VK_WHOLE_SIZE);
        ::vmaUnmapMemory(_vulkanContext->allocator, frameData->vertexBuffer.alloc);
        ::vmaUnmapMemory(_vulkanContext->allocator, frameData->indexBuffer.alloc);

        ::vkCmdBindPipeline(_cmd, ::VK_PIPELINE_BIND_POINT_GRAPHICS, vk->material.pipeline->graphicsPipeline);

        const ::VkDeviceSize vertexOffsets[]{ 0 };
        ::vkCmdBindVertexBuffers(_cmd, 0, 1, &frameData->vertexBuffer.buffer, vertexOffsets);
        ::vkCmdBindIndexBuffer(_cmd, frameData->indexBuffer.buffer, 0, (sizeof(ImDrawIdx) == 2) ? ::VK_INDEX_TYPE_UINT16 : ::VK_INDEX_TYPE_UINT32);

        ::VkViewport viewport;
        viewport.x = 0;
        viewport.y = 0;
        viewport.width = (float)frameBufferWidth;
        viewport.height = (float)frameBufferHeight;
        viewport.minDepth = 0.f;
        viewport.maxDepth = 1.f;
        ::vkCmdSetViewport(_cmd, 0, 1, &viewport);

        vk->material.constants.scale = Vec2f{ 2.f / io->DisplaySize.x, 2.f / io->DisplaySize.y };
        vk->material.constants.translate = Vec2f{ -1.f };
        ::vkCmdPushConstants(_cmd, vk->material.pipeline->pipelineLayout, ::VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(DearImGuiHandler::VkRender::MaterialInstance::Constants), &vk->material.constants);

        const ::ImVec2 clipOffset = dd->DisplayPos;
        const ::ImVec2 clipScale = dd->FramebufferScale;
        int vertexIdOffset = 0;
        int indexIdOffset = 0;
        for (int l = 0; l < dd->CmdListsCount; ++l)
        {
            const ImDrawList* const imDrawList = dd->CmdLists[l];
            for (int c = 0; c < imDrawList->CmdBuffer.Size; ++c)
            {
                const ImDrawCmd* const imDrawCmd = &imDrawList->CmdBuffer[c];

                ::ImVec2 clipMin{
                    Max(0.f, (imDrawCmd->ClipRect.x - clipOffset.x) * clipScale.x),
                    Max(0.f, (imDrawCmd->ClipRect.y - clipOffset.y) * clipScale.y)
                };
                ::ImVec2 clipMax{
                    Min((f32)frameBufferWidth, (imDrawCmd->ClipRect.z - clipOffset.x) * clipScale.x),
                    Min((f32)frameBufferHeight, (imDrawCmd->ClipRect.w - clipOffset.y) * clipScale.y)
                };
                if (clipMax.x <= clipMin.x || clipMax.y <= clipMin.y)
                    continue;

                ::VkRect2D scissor;
                scissor.offset = { (i32)clipMin.x, (i32)clipMin.y };
                scissor.extent = { (u32)(clipMax.x - clipMin.x), (u32)(clipMax.y - clipMin.y) };
                ::vkCmdSetScissor(_cmd, 0, 1, &scissor);

                // Bind DescriptorSet with font or user texture
                ::VkDescriptorSet descriptorSet = imDrawCmd->TextureId != nullptr ? (VkDescriptorSet)imDrawCmd->TextureId : vk->material.descriptorSet;
                ::vkCmdBindDescriptorSets(_cmd, ::VK_PIPELINE_BIND_POINT_GRAPHICS, vk->material.pipeline->pipelineLayout, 0, 1, &descriptorSet, 0, nullptr);

                ::vkCmdDrawIndexed(_cmd, imDrawCmd->ElemCount, 1, imDrawCmd->IdxOffset + indexIdOffset, imDrawCmd->VtxOffset + vertexIdOffset, 0);
            }
            vertexIdOffset += imDrawList->VtxBuffer.Size;
            indexIdOffset += imDrawList->IdxBuffer.Size;
        }

        vk->currentFrameIndex = (vk->currentFrameIndex + 1) % DearImGuiHandler::VkRender::c_concurrentFrameCount;
    }

    void ShutdownVulkanRender(DearImGuiHandler* const _imgui, Vk::VulkanContext* const _vulkanContext) noexcept
    {
        const ::VkDevice vkDevice = _vulkanContext->device;
        const ::VmaAllocator vmaAlloc = _vulkanContext->allocator;

        ::vkDeviceWaitIdle(vkDevice);

        for (u32 f = 0; f < DearImGuiHandler::VkRender::c_concurrentFrameCount; ++f)
        {
            Vk::DestroyAllocatedVkBuffer(vmaAlloc, &_imgui->vk.perFrameData[f].indexBuffer);
            Vk::DestroyAllocatedVkBuffer(vmaAlloc, &_imgui->vk.perFrameData[f].vertexBuffer);
        }

        ::vkDestroyPipeline(vkDevice, _imgui->vk.materialPipeline.graphicsPipeline, nullptr);
        ::vkDestroyPipelineLayout(vkDevice, _imgui->vk.materialPipeline.pipelineLayout, nullptr);
        ::vkDestroyDescriptorSetLayout(vkDevice, _imgui->vk.materialPipeline.layout, nullptr);

        ::vkDestroyDescriptorPool(vkDevice, _imgui->vk.descriptorPool, nullptr);

        ::vkDestroySampler(vkDevice, _imgui->vk.material.resources.fontTextureSampler, nullptr);
        ::vkDestroyImageView(vkDevice, _imgui->vk.material.resources.fontAtlasImageView, nullptr);
        Vk::DestroyAllocatedVkImage(vmaAlloc, &_imgui->vk.material.resources.fontAtlasImage);

        ZeroMem(&_imgui->vk);
    }

    void Shutdown(DearImGuiHandler* const _imgui) noexcept
    {
        if (_imgui->ctx != nullptr)
            ::ImGui::DestroyContext(_imgui->ctx);
    }

    void Resize(DearImGuiHandler* const _imgui, const u32 _width, const u32 _height) noexcept
    {
        ::ImGui::SetCurrentContext(_imgui->ctx);
        ::ImGuiIO* const io = &::ImGui::GetIO();
        io->DisplaySize = ::ImVec2{ (f32)_width, (f32)_height };
    }

    namespace _Internal
    {
        // from imgui-1.90.4\backends\imgui_impl_win32.cpp
        ::ImGuiKey VirtualKeyToImGuiKey(const u8 _virtualKey)
        {
            switch (_virtualKey)
            {
                case VK_TAB:                return ImGuiKey_Tab;
                case VK_LEFT:               return ImGuiKey_LeftArrow;
                case VK_RIGHT:              return ImGuiKey_RightArrow;
                case VK_UP:                 return ImGuiKey_UpArrow;
                case VK_DOWN:               return ImGuiKey_DownArrow;
                case VK_PRIOR:              return ImGuiKey_PageUp;
                case VK_NEXT:               return ImGuiKey_PageDown;
                case VK_HOME:               return ImGuiKey_Home;
                case VK_END:                return ImGuiKey_End;
                case VK_INSERT:             return ImGuiKey_Insert;
                case VK_DELETE:             return ImGuiKey_Delete;
                case VK_BACK:               return ImGuiKey_Backspace;
                case VK_SPACE:              return ImGuiKey_Space;
                case VK_RETURN:             return ImGuiKey_Enter;
                case VK_ESCAPE:             return ImGuiKey_Escape;
                case VK_OEM_7:              return ImGuiKey_Apostrophe;
                case VK_OEM_COMMA:          return ImGuiKey_Comma;
                case VK_OEM_MINUS:          return ImGuiKey_Minus;
                case VK_OEM_PERIOD:         return ImGuiKey_Period;
                case VK_OEM_2:              return ImGuiKey_Slash;
                case VK_OEM_1:              return ImGuiKey_Semicolon;
                case VK_OEM_PLUS:           return ImGuiKey_Equal;
                case VK_OEM_4:              return ImGuiKey_LeftBracket;
                case VK_OEM_5:              return ImGuiKey_Backslash;
                case VK_OEM_6:              return ImGuiKey_RightBracket;
                case VK_OEM_3:              return ImGuiKey_GraveAccent;
                case VK_CAPITAL:            return ImGuiKey_CapsLock;
                case VK_SCROLL:             return ImGuiKey_ScrollLock;
                case VK_NUMLOCK:            return ImGuiKey_NumLock;
                case VK_SNAPSHOT:           return ImGuiKey_PrintScreen;
                case VK_PAUSE:              return ImGuiKey_Pause;
                case VK_NUMPAD0:            return ImGuiKey_Keypad0;
                case VK_NUMPAD1:            return ImGuiKey_Keypad1;
                case VK_NUMPAD2:            return ImGuiKey_Keypad2;
                case VK_NUMPAD3:            return ImGuiKey_Keypad3;
                case VK_NUMPAD4:            return ImGuiKey_Keypad4;
                case VK_NUMPAD5:            return ImGuiKey_Keypad5;
                case VK_NUMPAD6:            return ImGuiKey_Keypad6;
                case VK_NUMPAD7:            return ImGuiKey_Keypad7;
                case VK_NUMPAD8:            return ImGuiKey_Keypad8;
                case VK_NUMPAD9:            return ImGuiKey_Keypad9;
                case VK_DECIMAL:            return ImGuiKey_KeypadDecimal;
                case VK_DIVIDE:             return ImGuiKey_KeypadDivide;
                case VK_MULTIPLY:           return ImGuiKey_KeypadMultiply;
                case VK_SUBTRACT:           return ImGuiKey_KeypadSubtract;
                case VK_ADD:                return ImGuiKey_KeypadAdd;
                //case IM_VK_KEYPAD_ENTER:    return ImGuiKey_KeypadEnter;
                case VK_LSHIFT:             return ImGuiKey_LeftShift;
                case VK_LCONTROL:           return ImGuiKey_LeftCtrl;
                case VK_LMENU:              return ImGuiKey_LeftAlt;
                case VK_LWIN:               return ImGuiKey_LeftSuper;
                case VK_RSHIFT:             return ImGuiKey_RightShift;
                case VK_RCONTROL:           return ImGuiKey_RightCtrl;
                case VK_RMENU:              return ImGuiKey_RightAlt;
                case VK_RWIN:               return ImGuiKey_RightSuper;
                case VK_APPS:               return ImGuiKey_Menu;
                case '0':                   return ImGuiKey_0;
                case '1':                   return ImGuiKey_1;
                case '2':                   return ImGuiKey_2;
                case '3':                   return ImGuiKey_3;
                case '4':                   return ImGuiKey_4;
                case '5':                   return ImGuiKey_5;
                case '6':                   return ImGuiKey_6;
                case '7':                   return ImGuiKey_7;
                case '8':                   return ImGuiKey_8;
                case '9':                   return ImGuiKey_9;
                case 'A':                   return ImGuiKey_A;
                case 'B':                   return ImGuiKey_B;
                case 'C':                   return ImGuiKey_C;
                case 'D':                   return ImGuiKey_D;
                case 'E':                   return ImGuiKey_E;
                case 'F':                   return ImGuiKey_F;
                case 'G':                   return ImGuiKey_G;
                case 'H':                   return ImGuiKey_H;
                case 'I':                   return ImGuiKey_I;
                case 'J':                   return ImGuiKey_J;
                case 'K':                   return ImGuiKey_K;
                case 'L':                   return ImGuiKey_L;
                case 'M':                   return ImGuiKey_M;
                case 'N':                   return ImGuiKey_N;
                case 'O':                   return ImGuiKey_O;
                case 'P':                   return ImGuiKey_P;
                case 'Q':                   return ImGuiKey_Q;
                case 'R':                   return ImGuiKey_R;
                case 'S':                   return ImGuiKey_S;
                case 'T':                   return ImGuiKey_T;
                case 'U':                   return ImGuiKey_U;
                case 'V':                   return ImGuiKey_V;
                case 'W':                   return ImGuiKey_W;
                case 'X':                   return ImGuiKey_X;
                case 'Y':                   return ImGuiKey_Y;
                case 'Z':                   return ImGuiKey_Z;
                case VK_F1:                 return ImGuiKey_F1;
                case VK_F2:                 return ImGuiKey_F2;
                case VK_F3:                 return ImGuiKey_F3;
                case VK_F4:                 return ImGuiKey_F4;
                case VK_F5:                 return ImGuiKey_F5;
                case VK_F6:                 return ImGuiKey_F6;
                case VK_F7:                 return ImGuiKey_F7;
                case VK_F8:                 return ImGuiKey_F8;
                case VK_F9:                 return ImGuiKey_F9;
                case VK_F10:                return ImGuiKey_F10;
                case VK_F11:                return ImGuiKey_F11;
                case VK_F12:                return ImGuiKey_F12;
                case VK_F13:                return ImGuiKey_F13;
                case VK_F14:                return ImGuiKey_F14;
                case VK_F15:                return ImGuiKey_F15;
                case VK_F16:                return ImGuiKey_F16;
                case VK_F17:                return ImGuiKey_F17;
                case VK_F18:                return ImGuiKey_F18;
                case VK_F19:                return ImGuiKey_F19;
                case VK_F20:                return ImGuiKey_F20;
                case VK_F21:                return ImGuiKey_F21;
                case VK_F22:                return ImGuiKey_F22;
                case VK_F23:                return ImGuiKey_F23;
                case VK_F24:                return ImGuiKey_F24;
                case VK_BROWSER_BACK:       return ImGuiKey_AppBack;
                case VK_BROWSER_FORWARD:    return ImGuiKey_AppForward;

                default: return ImGuiKey_None;
            }
        }
    }

    bool OnKeyEvent(DearImGuiHandler* const _imgui, const u8 _virtualKey, const bool _pressed) noexcept
    {
        ::ImGui::SetCurrentContext(_imgui->ctx);
        ::ImGuiIO* const io = &::ImGui::GetIO();

        const ::ImGuiKey imguiKey = _Internal::VirtualKeyToImGuiKey(_virtualKey);
        io->AddKeyEvent(imguiKey, _pressed);

        return io->WantCaptureKeyboard;
    }

    bool OnCharEvent(DearImGuiHandler* const _imgui, const u16 _char) noexcept
    {
        ::ImGui::SetCurrentContext(_imgui->ctx);
        ::ImGuiIO* const io = &::ImGui::GetIO();

        io->AddInputCharacterUTF16(_char);
        return io->WantCaptureKeyboard;
    }

    bool OnMouseEvent(
        DearImGuiHandler* const _imgui,
        void* const _windowHandle,
        const u16 _buttonFlags,
        const i32 _moveX, const i32 _moveY,
        const i16 _wheelMove) noexcept
    {
        ::ImGui::SetCurrentContext(_imgui->ctx);
        ::ImGuiIO* const io = &::ImGui::GetIO();

        if (_moveX != 0 || _moveY != 0)
        {
            const ::HWND windowHandle = reinterpret_cast<::HWND>(_windowHandle);
            ::POINT p;
            if (::GetCursorPos(&p) && ::ScreenToClient(windowHandle, &p))
                io->AddMousePosEvent((f32)p.x, (f32)p.y);
        }

        if (_buttonFlags != 0)
        {
            if (TestFlag<u16>(_buttonFlags, RI_MOUSE_LEFT_BUTTON_DOWN))
                io->AddMouseButtonEvent(0, true);

            if (TestFlag<u16>(_buttonFlags, RI_MOUSE_LEFT_BUTTON_UP))
                io->AddMouseButtonEvent(0, false);

            if (TestFlag<u16>(_buttonFlags, RI_MOUSE_RIGHT_BUTTON_DOWN))
                io->AddMouseButtonEvent(1, true);

            if (TestFlag<u16>(_buttonFlags, RI_MOUSE_RIGHT_BUTTON_UP))
                io->AddMouseButtonEvent(1, false);

            if (TestFlag<u16>(_buttonFlags, RI_MOUSE_MIDDLE_BUTTON_DOWN))
                io->AddMouseButtonEvent(2, true);

            if (TestFlag<u16>(_buttonFlags, RI_MOUSE_MIDDLE_BUTTON_UP))
                io->AddMouseButtonEvent(2, false);

            if (TestFlag<u16>(_buttonFlags, RI_MOUSE_WHEEL))
                io->AddMouseWheelEvent(0.f, (f32)_wheelMove / (f32)WHEEL_DELTA);
        }

        return io->WantCaptureMouse; // || io->WantCaptureMouseUnlessPopupClose;
    }
}
