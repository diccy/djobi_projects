#pragma once

#include <DjobiCore/Memory/Memory.hpp>


namespace Djo
{
    struct MappedFile
    {
        void* fileHandle;
        void* mapHandle;
        Memory::Area mappedBuffer;
    };

    MappedFile MapFileReadOnly(const char* _filePath);
    void UnmapFile(MappedFile* _mappedFile);
}
