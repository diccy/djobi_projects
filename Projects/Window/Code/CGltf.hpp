// no pragma once

#pragma warning(push)

// warning C4996: 'fopen': This function or variable may be unsafe. Consider using fopen_s instead.
#pragma warning(disable : 4996)

#include <cgltf/cgltf.h>

#pragma warning(pop)
