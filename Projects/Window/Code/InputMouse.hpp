#pragma once

#include <DjobiCore/Core/Types.hpp>


// from : https://github.com/glfw/glfw/blob/master/include/GLFW/glfw3.h

namespace Djo
{
    namespace Input
    {
        struct MouseButton
        {
            enum EValue : u8
            {
                n1 = 0,
                n2,
                n3,
                n4,
                n5,
                n6,
                n7,
                n8,

                Count,

                Left = n1,
                Right = n2,
                Middle = n3,
            };
        };

        constexpr const char* MouseButtonToString(const MouseButton::EValue _mouseButton) noexcept
        {
        #define DJO_MOUSE_BUTTON_STRING_CASE(enum_)     case enum_: return DJO_STR(enum_)

            switch (_mouseButton)
            {
                DJO_MOUSE_BUTTON_STRING_CASE(MouseButton::Left);
                DJO_MOUSE_BUTTON_STRING_CASE(MouseButton::Right);
                DJO_MOUSE_BUTTON_STRING_CASE(MouseButton::Middle);
                DJO_MOUSE_BUTTON_STRING_CASE(MouseButton::n4);
                DJO_MOUSE_BUTTON_STRING_CASE(MouseButton::n5);
                DJO_MOUSE_BUTTON_STRING_CASE(MouseButton::n6);
                DJO_MOUSE_BUTTON_STRING_CASE(MouseButton::n7);
                DJO_MOUSE_BUTTON_STRING_CASE(MouseButton::n8);
                default: return "Unknown MouseButton";
            }

        #undef DJO_MOUSE_BUTTON_STRING_CASE
        }
    };
}
