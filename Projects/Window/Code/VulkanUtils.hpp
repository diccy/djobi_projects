#pragma once

#include <DjobiCore/Core/StructUtils.hpp>

#include "VulkanStructs.hpp"


namespace Djo
{
    namespace Vk
    {
        struct AllocatedVkBuffer
        {
            ::VkBuffer buffer;
            ::VmaAllocation alloc;
        };

        struct AllocatedVkImage
        {
            ::VkImage image;
            ::VmaAllocation alloc;
        };

        ::VkResult CreateAllocatedVkBuffer(
            ::VmaAllocator _allocator,
            ::VkDeviceSize _size,
            ::VkBufferUsageFlags _usage,
            ::VkMemoryPropertyFlags _required,
            ::VkMemoryPropertyFlags _preferred,
            AllocatedVkBuffer* _buffer) noexcept;

        void DestroyAllocatedVkBuffer(
            ::VmaAllocator _allocator,
            AllocatedVkBuffer* _buffer) noexcept;

        void DestroyAllocatedVkImage(
            const ::VmaAllocator _allocator,
            AllocatedVkImage* const _image) noexcept;

        ::VkResult BeginOnceCommands(
            ::VkDevice _device,
            ::VkCommandPool _commandPool,
            ::VkCommandBuffer* _commandBuffer) noexcept;

        ::VkResult EndSubmitFreeOnceCommands(
            ::VkDevice _device,
            ::VkCommandPool _commandPool,
            ::VkCommandBuffer _commandBuffer,
            ::VkQueue _queue) noexcept;

        ::VkResult InitBufferFromMemory(
            ::VkDevice _device,
            ::VmaAllocator _allocator,
            const void* _mem,
            sz _size,
            ::VkBufferUsageFlagBits _usage,
            ::VkCommandPool _stagingCommandPool,
            ::VkQueue _transferQueue,
            AllocatedVkBuffer* _buffer) noexcept;

        ::VkResult ChangeImageLayout(
            ::VkDevice _device,
            ::VkImage _image,
            ::VkFormat _format,
            ::VkImageLayout _oldLayout,
            ::VkImageLayout _newLayout,
            ::VkCommandPool _stagingCommandPool,
            ::VkQueue _transferQueue) noexcept;

        ::VkResult CreateAndPrepareImage2DForFragmentShaderRead(
            const ::VkDevice _device,
            const ::VmaAllocator _allocator,
            const void* const _imageData,
            const ::VkDeviceSize _imageSize,
            const ::VkExtent3D _imageExtent,
            const ::VkFormat _imageFormat,
            const ::VkCommandPool _stagingCommandPool,
            const ::VkQueue _transferQueue,
            AllocatedVkImage* const _image,
            ::VkImageView* const _imageView) noexcept;

        constexpr const char* VkResultToString(const ::VkResult _vkResult) noexcept
        {
        #define DJO_VKRESULT_TO_STR_CASE(enum_)  case enum_: return DJO_STR(enum_)

            switch (_vkResult)
            {
                DJO_VKRESULT_TO_STR_CASE(VK_SUCCESS);
                DJO_VKRESULT_TO_STR_CASE(VK_NOT_READY);
                DJO_VKRESULT_TO_STR_CASE(VK_TIMEOUT);
                DJO_VKRESULT_TO_STR_CASE(VK_EVENT_SET);
                DJO_VKRESULT_TO_STR_CASE(VK_EVENT_RESET);
                DJO_VKRESULT_TO_STR_CASE(VK_INCOMPLETE);
                DJO_VKRESULT_TO_STR_CASE(VK_ERROR_OUT_OF_HOST_MEMORY);
                DJO_VKRESULT_TO_STR_CASE(VK_ERROR_OUT_OF_DEVICE_MEMORY);
                DJO_VKRESULT_TO_STR_CASE(VK_ERROR_INITIALIZATION_FAILED);
                DJO_VKRESULT_TO_STR_CASE(VK_ERROR_DEVICE_LOST);
                DJO_VKRESULT_TO_STR_CASE(VK_ERROR_MEMORY_MAP_FAILED);
                DJO_VKRESULT_TO_STR_CASE(VK_ERROR_LAYER_NOT_PRESENT);
                DJO_VKRESULT_TO_STR_CASE(VK_ERROR_EXTENSION_NOT_PRESENT);
                DJO_VKRESULT_TO_STR_CASE(VK_ERROR_FEATURE_NOT_PRESENT);
                DJO_VKRESULT_TO_STR_CASE(VK_ERROR_INCOMPATIBLE_DRIVER);
                DJO_VKRESULT_TO_STR_CASE(VK_ERROR_TOO_MANY_OBJECTS);
                DJO_VKRESULT_TO_STR_CASE(VK_ERROR_FORMAT_NOT_SUPPORTED);
                DJO_VKRESULT_TO_STR_CASE(VK_ERROR_FRAGMENTED_POOL);
                DJO_VKRESULT_TO_STR_CASE(VK_ERROR_UNKNOWN);
                DJO_VKRESULT_TO_STR_CASE(VK_ERROR_OUT_OF_POOL_MEMORY);
                DJO_VKRESULT_TO_STR_CASE(VK_ERROR_INVALID_EXTERNAL_HANDLE);
                DJO_VKRESULT_TO_STR_CASE(VK_ERROR_FRAGMENTATION);
                DJO_VKRESULT_TO_STR_CASE(VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS);
                DJO_VKRESULT_TO_STR_CASE(VK_PIPELINE_COMPILE_REQUIRED);
                DJO_VKRESULT_TO_STR_CASE(VK_ERROR_SURFACE_LOST_KHR);
                DJO_VKRESULT_TO_STR_CASE(VK_ERROR_NATIVE_WINDOW_IN_USE_KHR);
                DJO_VKRESULT_TO_STR_CASE(VK_SUBOPTIMAL_KHR);
                DJO_VKRESULT_TO_STR_CASE(VK_ERROR_OUT_OF_DATE_KHR);
                DJO_VKRESULT_TO_STR_CASE(VK_ERROR_INCOMPATIBLE_DISPLAY_KHR);
                DJO_VKRESULT_TO_STR_CASE(VK_ERROR_VALIDATION_FAILED_EXT);
                DJO_VKRESULT_TO_STR_CASE(VK_ERROR_INVALID_SHADER_NV);
                DJO_VKRESULT_TO_STR_CASE(VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT);
                DJO_VKRESULT_TO_STR_CASE(VK_ERROR_NOT_PERMITTED_KHR);
                DJO_VKRESULT_TO_STR_CASE(VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT);
                DJO_VKRESULT_TO_STR_CASE(VK_THREAD_IDLE_KHR);
                DJO_VKRESULT_TO_STR_CASE(VK_THREAD_DONE_KHR);
                DJO_VKRESULT_TO_STR_CASE(VK_OPERATION_DEFERRED_KHR);
                DJO_VKRESULT_TO_STR_CASE(VK_OPERATION_NOT_DEFERRED_KHR);
                default: return "Unknown VkResult";
            }

        #undef DJO_VKRESULT_TO_STR_CASE
        }
    }
}
