#pragma once

#define VK_USE_PLATFORM_WIN32_KHR 1
#include <vulkan/vulkan.h>
#include <vma/vk_mem_alloc.h>


#define DJO_VK_API_VERSION  VK_API_VERSION_1_3

#if __DJO_DEBUG__
    #define DJO_VK_ENABLE_VALIDATION_LAYERS 1
#endif
