#include "Window_pch.h"

#include "VulkanContext.hpp"

#include "VulkanUtils.hpp"
#include "VulkanStructs.hpp"

#include <DjobiCore/Containers/String8Alloc.hpp>
#include <DjobiCore/Thread/ThreadContext.hpp>
#include <DjobiCore/Format/Format.hpp>


// note: see https://github.com/glfw/glfw/blob/master/src/vulkan.c

namespace Djo
{
    namespace Vk
    {
        #define DJO_VK_CONTEXT_LOG(msg_, ...)\
            DJO_FORMAT_LF(&_ctx->logger, "% [Vulkan Context] " msg_, Format::ELogLevel::Info, __VA_ARGS__)

        #define DJO_VK_CONTEXT_ERROR_RETURN(msg_)\
            {\
                SetError(_ctx, ToStringView("[Vulkan Context] " msg_ "\n"));\
                return false;\
            }

        namespace _Internal
        {
            // Not needed for compute shaders only?
            static const char* s_requiredExtensionsNamesForWindow[] =
            {
                VK_KHR_SURFACE_EXTENSION_NAME,
                "VK_KHR_win32_surface",
            };
            static const char* s_requiredDeviceExtensionsNamesForWindow[] =
            {
                VK_KHR_SWAPCHAIN_EXTENSION_NAME,
            };

        #if DJO_VK_ENABLE_VALIDATION_LAYERS

            static const char* s_requiredValidationLayers[]
            {
                "VK_LAYER_KHRONOS_validation",
            };

            static const char* s_requiredExtensionsNamesForDev[] =
            {
                VK_EXT_DEBUG_UTILS_EXTENSION_NAME,
            };

        #endif 

            DJO_INLINE const char* GetName(const ::VkExtensionProperties& _p)  { return _p.extensionName; }
            DJO_INLINE const char* GetName(const ::VkLayerProperties& _p)      { return _p.layerName; }

            template <typename T>
            DJO_INLINE bool CheckAvailability(const Buffer<const T>& _properties, const Buffer<const char*>& _names) noexcept
            {
                bool available = true;
                for (sz n = 0; n < _names.count; ++n)
                {
                    bool found = false;
                    const char* const extensionName = _names[n];
                    for (sz p = 0; p < _properties.count; ++p)
                    {
                        if (::strcmp(GetName(_properties[p]), extensionName) == 0)
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        available = false;
                        break;
                    }
                }
                return available;
            }

            DJO_INLINE bool DoesSupportInstanceExtensions(const VulkanContext& _ctx, const Buffer<const char*>& _extensionNames)
            {
                return _Internal::CheckAvailability<::VkExtensionProperties>(_ctx.extensionProperties, _extensionNames);
            }

            DJO_INLINE bool DoesSupportInstanceLayers(const VulkanContext& _ctx, const Buffer<const char*>& _layerNames)
            {
                return _Internal::CheckAvailability<::VkLayerProperties>(_ctx.layerProperties, _layerNames);
            }

            DJO_INLINE void LogStringsPerLine(String8OStream* const _logger, const Buffer<const char*>& _strs)
            {
                const sz strCount = _strs.count;
                for (sz i = 0; i < strCount; ++i)
                {
                    Push(_logger, ToStringView(_strs[i]));
                    Push(_logger, '\n');
                }
                Flush(_logger);
            }

            VKAPI_ATTR ::VkBool32 VKAPI_CALL DebugCallback(
                const ::VkDebugUtilsMessageSeverityFlagBitsEXT _severity,
                const ::VkDebugUtilsMessageTypeFlagsEXT _type,
                const ::VkDebugUtilsMessengerCallbackDataEXT* const _data,
                void* const _userData)
            {
                VulkanContext* const ctx = reinterpret_cast<VulkanContext*>(_userData);

                Format::ELogLevel logLevel;
                switch (_severity)
                {
                    case ::VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
                    case ::VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
                    {
                        logLevel = Format::ELogLevel::Info;
                        break;
                    }
                    case ::VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
                    {
                        logLevel = Format::ELogLevel::Warning;
                        break;
                    }
                    case ::VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
                    {
                        logLevel = Format::ELogLevel::Error;
                        break;
                    }
                    default:
                    {
                        DJO_ASSERT_MSG(false, "Unknown severity level, VkDebugUtilsMessageSeverityFlagBitsEXT enum might have been updated with new value(s)");
                        logLevel = Format::ELogLevel::Error;
                        break;
                    }
                }

                if (logLevel == Format::ELogLevel::Warning || logLevel == Format::ELogLevel::Error)
                {
                    DJO_FORMAT(&ctx->logger, "% #################### VULKAN ERROR #####################\n", logLevel);
                    DJO_FORMAT(&ctx->logger, "% %\n", logLevel, _data->pMessage);

                #define DJO_LOG_VK_TYPE_ENUM(enum_) case enum_: { DJO_FORMAT(&ctx->logger, "% %\n", logLevel, #enum_); break; }

                    switch (_type)
                    {
                        DJO_LOG_VK_TYPE_ENUM(VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT);
                        DJO_LOG_VK_TYPE_ENUM(VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT);
                        DJO_LOG_VK_TYPE_ENUM(VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT);
                        default:
                        {
                            DJO_FORMAT(&ctx->logger, "% %\n", logLevel, _type);
                            DJO_ASSERT_MSG(false, "Unknown message type");
                            break;
                        }
                    }

                #undef DJO_LOG_VK_TYPE_ENUM

                    DJO_FORMAT_LF(&ctx->logger, "% #######################################################\n", logLevel);

                    // Break here, after message print, if error
                    DJO_ASSERT(logLevel != Format::ELogLevel::Error);
                }

                /*
                    The callback returns a boolean that indicates if the Vulkan call that triggered the validation layer message should be aborted.
                    If the callback returns true, then the call is aborted with the VK_ERROR_VALIDATION_FAILED_EXT error.
                    This is normally only used to test the validation layers themselves, so you should always return VK_FALSE.
                */
                return VK_FALSE;
            }
        }

        VulkanContext MoldContext() noexcept
        {
            VulkanContext ctx{};
            ctx.arena = DJO_MEMORY_ARENA_ALLOCATE("VulkanContext arena");
            ctx.errorMessage = PushEmptyString8(ctx.arena, 2048);
            return ctx;
        }

        bool InitContext(VulkanContext* const _ctx, void* const _windowHandle) noexcept
        {
            DJO_ASSERT(_ctx->arena != nullptr);
            DJO_ASSERT(!_ctx->hasError);
            DJO_ASSERT(GetData(_ctx->errorMessage));

            const Memory::ArenaMark arenaMark = GetThreadLocalArenaMark();

        #define DJO_VK_CONTEXT_INIT_LOG_STEP(msg_, ...)  DJO_VK_CONTEXT_LOG("Init: " msg_, __VA_ARGS__)
        #define DJO_VK_CONTEXT_INIT_ERROR_RETURN(msg_)   { DJO_VK_CONTEXT_ERROR_RETURN("[ERROR] Init: " msg_); }

            // Load library
            {
                DJO_VK_CONTEXT_INIT_LOG_STEP("vulkan-1.dll loading...");

                const ::HMODULE libHandle = ::LoadLibraryA("vulkan-1.dll");
                if (!libHandle)
                    DJO_VK_CONTEXT_INIT_ERROR_RETURN("Can not load vulkan-1.dll");

                _ctx->libHandle = libHandle;

                const auto vkGetInstanceProcAddr = reinterpret_cast<::PFN_vkGetInstanceProcAddr>(
                    ::GetProcAddress(reinterpret_cast<::HMODULE>(_ctx->libHandle), "vkGetInstanceProcAddr"));
                if (!vkGetInstanceProcAddr)
                    DJO_VK_CONTEXT_INIT_ERROR_RETURN("Vulkan lib does not export vkGetInstanceProcAddr");

                _ctx->vkGetInstanceProcAddr = vkGetInstanceProcAddr;

                const auto vkGetDeviceProcAddr = reinterpret_cast<::PFN_vkGetDeviceProcAddr>(
                    ::GetProcAddress(reinterpret_cast<::HMODULE>(_ctx->libHandle), "vkGetDeviceProcAddr"));
                if (!vkGetDeviceProcAddr)
                    DJO_VK_CONTEXT_INIT_ERROR_RETURN("Vulkan lib does not export vkGetDeviceProcAddr");

                _ctx->vkGetDeviceProcAddr = vkGetDeviceProcAddr;

                DJO_VK_CONTEXT_INIT_LOG_STEP("vulkan-1.dll loaded");
            }

            // Enumerate extensions and layers
            {
                DJO_VK_CONTEXT_INIT_LOG_STEP("Instance extensions and layers check...");

                u32 extensionCount = 0;
                if (::vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr) != ::VK_SUCCESS)
                    DJO_VK_CONTEXT_INIT_ERROR_RETURN("Failed to count instance extension properties");

                Array<::VkExtensionProperties> extensionProperties = PushArray<::VkExtensionProperties>(_ctx->arena, extensionCount);
                if (::vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, GetData(extensionProperties)) != ::VK_SUCCESS)
                    DJO_VK_CONTEXT_INIT_ERROR_RETURN("Failed to enumerate instance extension properties");

                _ctx->extensionProperties = extensionProperties;

                u32 layerCount = 0;
                if (::vkEnumerateInstanceLayerProperties(&layerCount, nullptr) != ::VK_SUCCESS)
                    DJO_VK_CONTEXT_INIT_ERROR_RETURN("Failed to count instance layer properties");

                Array<::VkLayerProperties> layerProperties = PushArray<::VkLayerProperties>(_ctx->arena, layerCount);
                if (::vkEnumerateInstanceLayerProperties(&layerCount, GetData(layerProperties)) != ::VK_SUCCESS)
                    DJO_VK_CONTEXT_INIT_ERROR_RETURN("Failed to enumerate instance layer properties");

                _ctx->layerProperties = layerProperties;

                constexpr sz c_allocCount = 20; // arbitrary large enough to fit
                Array<const char*> extensionNames = PushEmptyArray<const char*>(_ctx->arena, c_allocCount);
                Array<const char*> layerNames = PushEmptyArray<const char*>(_ctx->arena, c_allocCount);

                if (!_Internal::DoesSupportInstanceExtensions(*_ctx, ToBuffer(_Internal::s_requiredExtensionsNamesForWindow)))
                    DJO_VK_CONTEXT_INIT_ERROR_RETURN("Not all required extensions are available for window usage");
                InsertBack(&extensionNames, ToBufferConst(_Internal::s_requiredExtensionsNamesForWindow));

            #if DJO_VK_ENABLE_VALIDATION_LAYERS

                DJO_ASSERT(_Internal::DoesSupportInstanceLayers(*_ctx, ToBuffer(_Internal::s_requiredValidationLayers)));
                InsertBack(&layerNames, ToBufferConst(_Internal::s_requiredValidationLayers));

                DJO_ASSERT(_Internal::DoesSupportInstanceExtensions(*_ctx, ToBuffer(_Internal::s_requiredExtensionsNamesForDev)));
                InsertBack(&extensionNames, ToBufferConst(_Internal::s_requiredExtensionsNamesForDev));

            #endif

                DJO_FORMAT_LF(&_ctx->logger, "Required instance extensions (%):", extensionNames.count);
                _Internal::LogStringsPerLine(&_ctx->logger, extensionNames);
                DJO_FORMAT_LF(&_ctx->logger, "Required instance layers (%):", layerNames.count);
                _Internal::LogStringsPerLine(&_ctx->logger, layerNames);

                _ctx->extensionNames = extensionNames;
                _ctx->layerNames = layerNames;

                DJO_VK_CONTEXT_INIT_LOG_STEP("Instance extensions and layers checked");
            }

            // Init instance
            {
                DJO_VK_CONTEXT_INIT_LOG_STEP("Instance init...");

                DJO_TODO("Pass VkApplicationInfo by caller");

                ::VkApplicationInfo applicationInfo{};
                applicationInfo.sType = ::VK_STRUCTURE_TYPE_APPLICATION_INFO;
                applicationInfo.pNext = nullptr;
                applicationInfo.pApplicationName = "TODO";
                applicationInfo.applicationVersion = VK_MAKE_API_VERSION(0, 0, 0, 0);
                applicationInfo.pEngineName = "TODO";
                applicationInfo.engineVersion = VK_MAKE_API_VERSION(0, 0, 0, 0);
                applicationInfo.apiVersion = DJO_VK_API_VERSION;

            #if DJO_VK_ENABLE_VALIDATION_LAYERS
                // Add debug messenger for vkCreateInstance/vkDestroyInstance functions
                ::VkDebugUtilsMessengerCreateInfoEXT debugUtilsMessengerCreateInfo{};
                debugUtilsMessengerCreateInfo.sType = ::VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
                debugUtilsMessengerCreateInfo.pNext = nullptr;
                debugUtilsMessengerCreateInfo.flags = 0;
                debugUtilsMessengerCreateInfo.messageSeverity = ::VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | ::VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT | ::VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | ::VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
                debugUtilsMessengerCreateInfo.messageType = ::VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | ::VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | ::VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
                debugUtilsMessengerCreateInfo.pfnUserCallback = _Internal::DebugCallback;
                debugUtilsMessengerCreateInfo.pUserData = _ctx;
            #endif

                ::VkInstanceCreateInfo instanceCreateInfo{};
                instanceCreateInfo.sType = ::VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
            #if DJO_VK_ENABLE_VALIDATION_LAYERS
                instanceCreateInfo.pNext = &debugUtilsMessengerCreateInfo;
            #else
                instanceCreateInfo.pNext = nullptr;
            #endif
                instanceCreateInfo.flags = 0;
                instanceCreateInfo.pApplicationInfo = &applicationInfo;
            #if DJO_VK_ENABLE_VALIDATION_LAYERS
                instanceCreateInfo.enabledLayerCount = (u32)_ctx->layerNames.count;
                instanceCreateInfo.ppEnabledLayerNames = GetData(_ctx->layerNames);
            #else
                instanceCreateInfo.enabledLayerCount = 0;
                instanceCreateInfo.ppEnabledLayerNames = nullptr;
            #endif
                instanceCreateInfo.enabledExtensionCount = (u32)_ctx->extensionNames.count;
                instanceCreateInfo.ppEnabledExtensionNames = GetData(_ctx->extensionNames);

                ::VkInstance instance;
                if (::vkCreateInstance(&instanceCreateInfo, nullptr, &instance) != ::VK_SUCCESS)
                    DJO_VK_CONTEXT_INIT_ERROR_RETURN("Failed to create instance");

                _ctx->instance = instance;

            #if DJO_VK_ENABLE_VALIDATION_LAYERS
                const auto vkCreateDebugUtilsMessengerEXT = reinterpret_cast<::PFN_vkCreateDebugUtilsMessengerEXT>(
                    ::vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT"));
                DJO_ASSERT(vkCreateDebugUtilsMessengerEXT != nullptr);

                const auto vkDestroyDebugUtilsMessengerEXT = reinterpret_cast<::PFN_vkDestroyDebugUtilsMessengerEXT>(
                    ::vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT"));
                DJO_ASSERT(vkCreateDebugUtilsMessengerEXT != nullptr);

                _ctx->vkCreateDebugUtilsMessengerEXT = vkCreateDebugUtilsMessengerEXT;
                _ctx->vkDestroyDebugUtilsMessengerEXT = vkDestroyDebugUtilsMessengerEXT;
            #endif

                DJO_VK_CONTEXT_INIT_LOG_STEP("Instance initialized");
            }

        #if DJO_VK_ENABLE_VALIDATION_LAYERS

            // Init debug utils messenger
            {
                DJO_VK_CONTEXT_INIT_LOG_STEP("Debug utils messenger init...");

                ::VkDebugUtilsMessengerCreateInfoEXT debugCreateInfo{};
                debugCreateInfo.sType = ::VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
                debugCreateInfo.pNext = nullptr;
                debugCreateInfo.flags = 0;
                debugCreateInfo.messageSeverity = ::VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | ::VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT | ::VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | ::VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
                debugCreateInfo.messageType = ::VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | ::VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | ::VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
                debugCreateInfo.pfnUserCallback = _Internal::DebugCallback;
                debugCreateInfo.pUserData = _ctx;

                ::VkDebugUtilsMessengerEXT debugMessenger;
                if (_ctx->vkCreateDebugUtilsMessengerEXT(_ctx->instance, &debugCreateInfo, nullptr, &debugMessenger) != ::VK_SUCCESS)
                    DJO_VK_CONTEXT_INIT_ERROR_RETURN("Failed to create debug utils messenger");

                _ctx->debugMessenger = debugMessenger;

                DJO_VK_CONTEXT_INIT_LOG_STEP("Debug utils messenger initialized");
            }
        #endif

            // Init surface
            {
                DJO_VK_CONTEXT_INIT_LOG_STEP("Win32 surface init...");

                const ::HWND windowHandle = reinterpret_cast<::HWND>(_windowHandle);
                const ::HMODULE moduleHandle = ::GetModuleHandleA(NULL);

                ::VkWin32SurfaceCreateInfoKHR win32SurfaceCreateInfo{};
                win32SurfaceCreateInfo.sType = ::VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
                win32SurfaceCreateInfo.pNext = nullptr;
                win32SurfaceCreateInfo.flags = 0;
                win32SurfaceCreateInfo.hinstance = moduleHandle;
                win32SurfaceCreateInfo.hwnd = windowHandle;

                ::VkSurfaceKHR surface;
                if (::vkCreateWin32SurfaceKHR(_ctx->instance, &win32SurfaceCreateInfo, nullptr, &surface) != ::VK_SUCCESS)
                    DJO_VK_CONTEXT_INIT_ERROR_RETURN("Failed to create Win32 surface");

                _ctx->windowHandle = windowHandle;
                _ctx->surface = surface;

                DJO_VK_CONTEXT_INIT_LOG_STEP("Win32 surface initialized");
            }

            // Pick physical device
            {
                DJO_VK_CONTEXT_INIT_LOG_STEP("Physical device picking...");

                ::VkPhysicalDevice chosenPhysicalDevice = VK_NULL_HANDLE;
                ::VkPhysicalDeviceProperties chosenPhysicalDeviceProperties{};
                ::VkPhysicalDeviceFeatures chosenPhysicalDeviceFeatures{};
                u32 chosenGraphicsQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
                u32 chosenPresentQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;

                u32 physicalDevicesCount = 0;
                if (::vkEnumeratePhysicalDevices(_ctx->instance, &physicalDevicesCount, nullptr) != ::VK_SUCCESS || physicalDevicesCount == 0)
                    DJO_VK_CONTEXT_INIT_ERROR_RETURN("Failed to count physical devices");

                Array<::VkPhysicalDevice> physicalDevices = PushArray<::VkPhysicalDevice>(_ctx->arena, physicalDevicesCount);
                if (::vkEnumeratePhysicalDevices(_ctx->instance, &physicalDevicesCount, GetData(physicalDevices)) != ::VK_SUCCESS || physicalDevicesCount == 0)
                    DJO_VK_CONTEXT_INIT_ERROR_RETURN("Failed to enumerate physical devices");

                for (u32 d = 0; d < physicalDevicesCount; ++d)
                {
                    const ::VkPhysicalDevice currentPhysicalDevice = physicalDevices[d];

                    // Filter type
                    ::VkPhysicalDeviceProperties currentPhysicalDeviceProperties;
                    ::VkPhysicalDeviceFeatures currentPhysicalDeviceFeatures;
                    ::vkGetPhysicalDeviceProperties(currentPhysicalDevice, &currentPhysicalDeviceProperties);
                    ::vkGetPhysicalDeviceFeatures(currentPhysicalDevice, &currentPhysicalDeviceFeatures);
                    if (currentPhysicalDeviceProperties.deviceType != ::VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
                        continue;

                    // Filter by extensions
                    u32 extensionCount = 0;
                    if (::vkEnumerateDeviceExtensionProperties(currentPhysicalDevice, nullptr, &extensionCount, nullptr) != ::VK_SUCCESS)
                        continue;

                    Array<::VkExtensionProperties> extensionProperties = PushArray<::VkExtensionProperties>(arenaMark.arena, extensionCount);
                    if (::vkEnumerateDeviceExtensionProperties(currentPhysicalDevice, nullptr, &extensionCount, GetData(extensionProperties)) != ::VK_SUCCESS)
                        continue;

                    if (!_Internal::CheckAvailability<::VkExtensionProperties>(extensionProperties, ToBuffer(_Internal::s_requiredDeviceExtensionsNamesForWindow)))
                        continue;

                    // Filter by available queue families
                    u32 graphicsQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
                    u32 presentQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
                    u32 queueFamilyCount = 0;
                    ::vkGetPhysicalDeviceQueueFamilyProperties(currentPhysicalDevice, &queueFamilyCount, nullptr);
                    Array<::VkQueueFamilyProperties> queueFamilyProperties = PushArray<::VkQueueFamilyProperties>(arenaMark.arena, queueFamilyCount);
                    ::vkGetPhysicalDeviceQueueFamilyProperties(currentPhysicalDevice, &queueFamilyCount, GetData(queueFamilyProperties));

                    for (u32 i = 0; i < queueFamilyCount; ++i)
                    {
                        // Check if graphics queue
                        if (TestFlag<::VkQueueFlags>(queueFamilyProperties[i].queueFlags, ::VK_QUEUE_GRAPHICS_BIT))
                            graphicsQueueFamilyIndex = i;

                        // Check if present queue
                        ::VkBool32 presentSupport = ::vkGetPhysicalDeviceWin32PresentationSupportKHR(currentPhysicalDevice, i);
                        if (presentSupport == VK_TRUE)
                            presentQueueFamilyIndex = i;
                    }

                    if (graphicsQueueFamilyIndex == VK_QUEUE_FAMILY_IGNORED || presentQueueFamilyIndex == VK_QUEUE_FAMILY_IGNORED)
                        continue;

                    // Filter by swap chain support
                    u32 swapchainFormatCount = 0;
                    if (::vkGetPhysicalDeviceSurfaceFormatsKHR(currentPhysicalDevice, _ctx->surface, &swapchainFormatCount, nullptr) != ::VK_SUCCESS)
                        continue;
                    u32 swapchainPresentModeCount = 0;
                    if (::vkGetPhysicalDeviceSurfacePresentModesKHR(currentPhysicalDevice, _ctx->surface, &swapchainPresentModeCount, nullptr) != ::VK_SUCCESS)
                        continue;
                    if (swapchainFormatCount == 0 || swapchainPresentModeCount == 0)
                        continue;

                    // OK get this one
                    chosenPhysicalDevice = currentPhysicalDevice;
                    chosenPhysicalDeviceProperties = currentPhysicalDeviceProperties;
                    chosenPhysicalDeviceFeatures = currentPhysicalDeviceFeatures;
                    chosenGraphicsQueueFamilyIndex = graphicsQueueFamilyIndex;
                    chosenPresentQueueFamilyIndex = presentQueueFamilyIndex;
                    break;
                }

                if (chosenPhysicalDevice == VK_NULL_HANDLE)
                    DJO_VK_CONTEXT_INIT_ERROR_RETURN("Could not choose physical device");

                _ctx->physicalDevice = chosenPhysicalDevice;
                _ctx->physicalDeviceProperties = chosenPhysicalDeviceProperties;
                _ctx->physicalDeviceFeatures = chosenPhysicalDeviceFeatures;
                _ctx->graphicsQueueFamilyIndex = chosenGraphicsQueueFamilyIndex;
                _ctx->presentQueueFamilyIndex = chosenPresentQueueFamilyIndex;

                DJO_VK_CONTEXT_INIT_LOG_STEP("Physical device chosen");
            }

            // Init logical device
            {
                DJO_VK_CONTEXT_INIT_LOG_STEP("Logical device init...");

                ::VkDeviceQueueCreateInfo queueCreateInfosBuffer[2]{};
                Array<::VkDeviceQueueCreateInfo> queueCreateInfos;
                queueCreateInfos.buffer = ToBuffer(queueCreateInfosBuffer);
                queueCreateInfos.count = 0;
                const f32 queuePriority = 1.f;

                ::VkDeviceQueueCreateInfo queueCreateInfo{};
                queueCreateInfo.sType = ::VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
                queueCreateInfo.pNext = nullptr;
                queueCreateInfo.flags = 0;
                queueCreateInfo.queueFamilyIndex = _ctx->graphicsQueueFamilyIndex;
                queueCreateInfo.queueCount = 1;
                queueCreateInfo.pQueuePriorities = &queuePriority;

                InsertBack(&queueCreateInfos, queueCreateInfo);
                if (_ctx->presentQueueFamilyIndex != _ctx->graphicsQueueFamilyIndex)
                {
                    queueCreateInfo.queueFamilyIndex = _ctx->presentQueueFamilyIndex;
                    InsertBack(&queueCreateInfos, queueCreateInfo);
                }

                ::VkPhysicalDeviceFeatures physicalDeviceFeatures;
                ::vkGetPhysicalDeviceFeatures(_ctx->physicalDevice, &physicalDeviceFeatures);

                ::VkPhysicalDeviceShaderDrawParametersFeatures shaderDrawParametersFeatures{};
                shaderDrawParametersFeatures.sType = ::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_DRAW_PARAMETERS_FEATURES;
                shaderDrawParametersFeatures.pNext = nullptr;
                shaderDrawParametersFeatures.shaderDrawParameters = VK_TRUE;

                /*
                    Previous implementations of Vulkan made a distinction between instance and device specific validation layers,
                    but this is no longer the case. That means that the enabledLayerCount and ppEnabledLayerNames fields of
                    VkDeviceCreateInfo are ignored by up-to-date implementations.
                    However, it is still a good idea to set them anyway to be compatible with older implementations.
                */
                ::VkDeviceCreateInfo createInfo{};
                createInfo.sType = ::VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
                createInfo.pNext = &shaderDrawParametersFeatures;
                createInfo.flags = 0;
                createInfo.queueCreateInfoCount = (u32)queueCreateInfos.count;
                createInfo.pQueueCreateInfos = GetData(queueCreateInfos);
                createInfo.enabledLayerCount = 0;
                createInfo.ppEnabledLayerNames = nullptr;
                createInfo.enabledExtensionCount = ArrayCount(_Internal::s_requiredDeviceExtensionsNamesForWindow);
                createInfo.ppEnabledExtensionNames = _Internal::s_requiredDeviceExtensionsNamesForWindow;
                createInfo.pEnabledFeatures = &physicalDeviceFeatures;

                ::VkDevice logicalDevice;
                if (::vkCreateDevice(_ctx->physicalDevice, &createInfo, nullptr, &logicalDevice) != ::VK_SUCCESS)
                    DJO_VK_CONTEXT_INIT_ERROR_RETURN("Failed to create logical device");

                ::VkQueue graphicsQueue, presentQueue;
                ::vkGetDeviceQueue(logicalDevice, _ctx->graphicsQueueFamilyIndex, 0, &graphicsQueue);
                ::vkGetDeviceQueue(logicalDevice, _ctx->presentQueueFamilyIndex, 0, &presentQueue);

                _ctx->device = logicalDevice;
                _ctx->graphicsQueue = graphicsQueue;
                _ctx->presentQueue = presentQueue;

                DJO_VK_CONTEXT_INIT_LOG_STEP("Logical device initialized");
            }

            // Init allocator
            {
                DJO_VK_CONTEXT_INIT_LOG_STEP("AMD's Vulkan memory allocator init...");

                ::VmaVulkanFunctions vulkanFunctions{};
                vulkanFunctions.vkGetInstanceProcAddr = _ctx->vkGetInstanceProcAddr;
                vulkanFunctions.vkGetDeviceProcAddr = _ctx->vkGetDeviceProcAddr;

                ::VmaAllocatorCreateInfo allocatorInfo{};
                allocatorInfo.flags = 0;
                allocatorInfo.physicalDevice = _ctx->physicalDevice;
                allocatorInfo.device = _ctx->device;
                allocatorInfo.preferredLargeHeapBlockSize = 0;
                allocatorInfo.pAllocationCallbacks = nullptr;
                allocatorInfo.pDeviceMemoryCallbacks = nullptr;
                allocatorInfo.pHeapSizeLimit = nullptr;
                allocatorInfo.pVulkanFunctions = &vulkanFunctions;
                allocatorInfo.instance = _ctx->instance;
                allocatorInfo.vulkanApiVersion = DJO_VK_API_VERSION;
            #if VMA_EXTERNAL_MEMORY
                allocatorInfo.pTypeExternalMemoryHandleTypes = nullptr;
            #endif

                ::VmaAllocator allocator;
                if (::vmaCreateAllocator(&allocatorInfo, &allocator) != ::VK_SUCCESS)
                    DJO_VK_CONTEXT_INIT_ERROR_RETURN("Failed to create allocator");

                _ctx->allocator = allocator;

                DJO_VK_CONTEXT_INIT_LOG_STEP("AMD's Vulkan memory allocator initialized");
            }

            // Init swapchain
            {
                DJO_VK_CONTEXT_INIT_LOG_STEP("Swapchain init...");

                if (!CreateSwapchain(_ctx))
                    return false;

                DJO_VK_CONTEXT_INIT_LOG_STEP("Swapchain initialized");
            }

        #undef DJO_VK_CONTEXT_INIT_ERROR_RETURN
        #undef DJO_VK_CONTEXT_INIT_LOG_STEP

            DJO_ASSERT(!_ctx->hasError);
            return true;
        }

        bool CreateSwapchain(VulkanContext* const _ctx) noexcept
        {
            DJO_ASSERT(_ctx->windowHandle != nullptr);
            DJO_ASSERT(_ctx->physicalDevice != nullptr);
            DJO_ASSERT(_ctx->surface != nullptr);

            const Memory::ArenaMark arenaMark = GetThreadLocalArenaMark();

            DJO_VK_CONTEXT_LOG("Swapchain creation...");

            // Choose extent

            ::VkExtent2D chosenExtent{};
            ::VkSurfaceCapabilitiesKHR capabilities;
            if (::vkGetPhysicalDeviceSurfaceCapabilitiesKHR(_ctx->physicalDevice, _ctx->surface, &capabilities) != ::VK_SUCCESS)
                DJO_VK_CONTEXT_ERROR_RETURN("Failed to retrieve physical device capabilities");

            // The range of the possible resolutions is defined in the VkSurfaceCapabilitiesKHR structure.Vulkan tells us to match
            // the resolution of the window by setting the width and height in the currentExtent member.
            // However, some window managers do allow us to differ here and this is indicated by setting
            // the width and height in currentExtent to a special value : the maximum value of uint32_t.
            if (capabilities.currentExtent.width != (~0u))
            {
                chosenExtent = capabilities.currentExtent;
            }
            else
            {
                const ::HWND windowHandle = reinterpret_cast<::HWND>(_ctx->windowHandle);
                ::RECT area;
                ::GetClientRect(windowHandle, &area);
                chosenExtent.width = static_cast<u32>(area.right);
                chosenExtent.height = static_cast<u32>(area.bottom);
            }
            if (chosenExtent.width == 0 || chosenExtent.height == 0)
                DJO_VK_CONTEXT_ERROR_RETURN("Impossible to choose extent");

            chosenExtent.width = Clamp<u32>(chosenExtent.width, capabilities.minImageExtent.width, capabilities.maxImageExtent.width);
            chosenExtent.height = Clamp<u32>(chosenExtent.height, capabilities.minImageExtent.height, capabilities.maxImageExtent.height);

            // Choose format

            ::VkSurfaceFormatKHR chosenFormat{};
            {
                u32 formatCount;
                if (::vkGetPhysicalDeviceSurfaceFormatsKHR(_ctx->physicalDevice, _ctx->surface, &formatCount, nullptr) != ::VK_SUCCESS)
                    DJO_VK_CONTEXT_ERROR_RETURN("Failed to retrieve physical device format count");

                Array<::VkSurfaceFormatKHR> formats = PushArray<::VkSurfaceFormatKHR>(arenaMark.arena, formatCount);
                if (::vkGetPhysicalDeviceSurfaceFormatsKHR(_ctx->physicalDevice, _ctx->surface, &formatCount, GetData(formats)) != ::VK_SUCCESS)
                    DJO_VK_CONTEXT_ERROR_RETURN("Failed to retrieve physical device formats");

                bool fullySuitable = false;
                chosenFormat = formats[0]; // Kinda OK default if not fully suitable is found
                for (u32 i = 0; i < formatCount; ++i)
                {
                    const ::VkSurfaceFormatKHR format = formats[i];
                    if (format.format == ::VK_FORMAT_B8G8R8A8_UNORM && format.colorSpace == ::VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
                    {
                        chosenFormat = format;
                        fullySuitable = true;
                        break;
                    }
                }
                DJO_ASSERT(fullySuitable); // when does this happend?
            }

            // Choose present mode

            ::VkPresentModeKHR chosenPresentMode = ::VK_PRESENT_MODE_FIFO_KHR; // This mode is guaranteed to be available
            {
                u32 presentModeCount;
                if (::vkGetPhysicalDeviceSurfacePresentModesKHR(_ctx->physicalDevice, _ctx->surface, &presentModeCount, nullptr))
                    DJO_VK_CONTEXT_ERROR_RETURN("Failed to retrieve physical device surface present mode count");

                Array<::VkPresentModeKHR> presentModes = PushArray<::VkPresentModeKHR>(arenaMark.arena, presentModeCount);
                if (::vkGetPhysicalDeviceSurfacePresentModesKHR(_ctx->physicalDevice, _ctx->surface, &presentModeCount, GetData(presentModes)))
                    DJO_VK_CONTEXT_ERROR_RETURN("Failed to retrieve physical device surface present modes");

                for (u32 i = 0; i < presentModeCount; ++i)
                {
                    const ::VkPresentModeKHR presentMode = presentModes[i];
                    if (presentMode == ::VK_PRESENT_MODE_MAILBOX_KHR)
                    {
                        chosenPresentMode = presentMode;
                        break;
                    }
                }
            }

            // Choose depth/stencil format

            // TODO : https://vulkan-tutorial.com/Depth_buffering
            const ::VkFormat depthFormat = ::VK_FORMAT_D24_UNORM_S8_UINT;
            const ::VkImageTiling depthTiling = ::VK_IMAGE_TILING_OPTIMAL;

            // Setup queue family indices

            const u32 queueFamilyIndices[]{ _ctx->graphicsQueueFamilyIndex, _ctx->presentQueueFamilyIndex };

            // Create swapchain

            ::VkSwapchainCreateInfoKHR createInfo{};
            createInfo.sType = ::VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
            createInfo.pNext = nullptr;
            createInfo.flags = 0;
            createInfo.surface = _ctx->surface;
            createInfo.minImageCount = Clamp<u32>(capabilities.minImageCount + 1, 0, capabilities.maxImageCount);
            createInfo.imageFormat = chosenFormat.format;
            createInfo.imageColorSpace = chosenFormat.colorSpace;
            createInfo.imageExtent = chosenExtent;
            createInfo.imageArrayLayers = 1;
            createInfo.imageUsage = ::VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT; // It is also possible that you'll render images to a separate image first to perform operations like post-processing. In that case you may use a value like VK_IMAGE_USAGE_TRANSFER_DST_BIT instead and use a memory operation to transfer the rendered image to a swap chain image.
            if (_ctx->graphicsQueueFamilyIndex != _ctx->presentQueueFamilyIndex)
            {
                createInfo.imageSharingMode = ::VK_SHARING_MODE_CONCURRENT;
                createInfo.queueFamilyIndexCount = ArrayCount(queueFamilyIndices);
                createInfo.pQueueFamilyIndices = queueFamilyIndices;
            }
            else
            {
                createInfo.imageSharingMode = ::VK_SHARING_MODE_EXCLUSIVE;
                createInfo.queueFamilyIndexCount = 0;
                createInfo.pQueueFamilyIndices = nullptr;
            }
            createInfo.preTransform = capabilities.currentTransform;
            createInfo.compositeAlpha = ::VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
            createInfo.presentMode = chosenPresentMode;
            createInfo.clipped = VK_TRUE;
            createInfo.oldSwapchain = VK_NULL_HANDLE;

            ::VkSwapchainKHR swapchain;
            if (::vkCreateSwapchainKHR(_ctx->device, &createInfo, nullptr, &swapchain) != ::VK_SUCCESS)
                DJO_VK_CONTEXT_ERROR_RETURN("Failed to create swapchain");

            _ctx->swapchain = swapchain;
            _ctx->swapchainFormat = chosenFormat;
            _ctx->swapchainExtent = chosenExtent;

            // Retrieve images

            u32 imageCount;
            if (::vkGetSwapchainImagesKHR(_ctx->device, swapchain, &imageCount, nullptr) != ::VK_SUCCESS)
                DJO_VK_CONTEXT_ERROR_RETURN("Failed to retrieve swapchain image count");

            Array<::VkImage> swapchainImages = (_ctx->swapchainImages.count == imageCount)
                ? _ctx->swapchainImages
                : PushArray<::VkImage>(_ctx->arena, imageCount);
            if (::vkGetSwapchainImagesKHR(_ctx->device, swapchain, &imageCount, GetData(swapchainImages)) != ::VK_SUCCESS)
                DJO_VK_CONTEXT_ERROR_RETURN("Failed to retrieve swapchain images");

            // Init image views

            Array<::VkImageView> swapchainImageViews = (_ctx->swapchainImageViews.count == imageCount)
                ? _ctx->swapchainImageViews
                : PushArray<::VkImageView>(_ctx->arena, imageCount);

            ::VkImageViewCreateInfo imageViewCreateInfo = ImageViewCreateInfo();
            imageViewCreateInfo.image = VK_NULL_HANDLE; // to fill
            imageViewCreateInfo.viewType = ::VK_IMAGE_VIEW_TYPE_2D;
            imageViewCreateInfo.format = chosenFormat.format;
            imageViewCreateInfo.components.r = ::VK_COMPONENT_SWIZZLE_IDENTITY;
            imageViewCreateInfo.components.g = ::VK_COMPONENT_SWIZZLE_IDENTITY;
            imageViewCreateInfo.components.b = ::VK_COMPONENT_SWIZZLE_IDENTITY;
            imageViewCreateInfo.components.a = ::VK_COMPONENT_SWIZZLE_IDENTITY;
            imageViewCreateInfo.subresourceRange.aspectMask = ::VK_IMAGE_ASPECT_COLOR_BIT;
            imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
            imageViewCreateInfo.subresourceRange.levelCount = 1;
            imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
            imageViewCreateInfo.subresourceRange.layerCount = 1;

            for (u32 i = 0; i < imageCount; ++i)
            {
                imageViewCreateInfo.image = swapchainImages[i];

                if (::vkCreateImageView(_ctx->device, &imageViewCreateInfo, nullptr, &swapchainImageViews[i]) != ::VK_SUCCESS)
                    DJO_VK_CONTEXT_ERROR_RETURN("Failed to create swapchain image views");
            }

            _ctx->swapchainImages = swapchainImages;
            _ctx->swapchainImageViews = swapchainImageViews;

            // Depth image

            const ::VkExtent3D depthImageExtent{ chosenExtent.width, chosenExtent.height, 1 };

            ::VkImageCreateInfo depthImageInfo = ImageCreateInfo();
            depthImageInfo.imageType = ::VK_IMAGE_TYPE_2D;
            depthImageInfo.format = depthFormat;
            depthImageInfo.extent = depthImageExtent;
            depthImageInfo.mipLevels = 1;
            depthImageInfo.arrayLayers = 1;
            depthImageInfo.samples = ::VK_SAMPLE_COUNT_1_BIT;
            depthImageInfo.tiling = depthTiling;
            depthImageInfo.usage = ::VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
            depthImageInfo.sharingMode = ::VK_SHARING_MODE_EXCLUSIVE;
            depthImageInfo.queueFamilyIndexCount = 0;
            depthImageInfo.pQueueFamilyIndices = nullptr;
            depthImageInfo.initialLayout = ::VK_IMAGE_LAYOUT_UNDEFINED;

            ::VmaAllocationCreateInfo depthImageAllocInfo{};
            depthImageAllocInfo.flags = 0;
            depthImageAllocInfo.usage = ::VMA_MEMORY_USAGE_UNKNOWN;
            depthImageAllocInfo.requiredFlags = ::VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
            depthImageAllocInfo.preferredFlags = 0;
            depthImageAllocInfo.memoryTypeBits = 0;
            depthImageAllocInfo.pool = VK_NULL_HANDLE;
            depthImageAllocInfo.pUserData = nullptr;
            depthImageAllocInfo.priority = 0;

            AllocatedVkImage depthImage;
            if (::vmaCreateImage(_ctx->allocator, &depthImageInfo, &depthImageAllocInfo, &depthImage.image, &depthImage.alloc, nullptr) != ::VK_SUCCESS)
                DJO_VK_CONTEXT_ERROR_RETURN("Failed to allocate depth image");

            _ctx->depthFormat = depthFormat;
            _ctx->depthImage = depthImage;

            ::VkImageViewCreateInfo depthImageViewCreateInfo = ImageViewCreateInfo();
            depthImageViewCreateInfo.image = depthImage.image;
            depthImageViewCreateInfo.viewType = ::VK_IMAGE_VIEW_TYPE_2D;
            depthImageViewCreateInfo.format = depthFormat;
            depthImageViewCreateInfo.components.r = ::VK_COMPONENT_SWIZZLE_IDENTITY;
            depthImageViewCreateInfo.components.g = ::VK_COMPONENT_SWIZZLE_IDENTITY;
            depthImageViewCreateInfo.components.b = ::VK_COMPONENT_SWIZZLE_IDENTITY;
            depthImageViewCreateInfo.components.a = ::VK_COMPONENT_SWIZZLE_IDENTITY;
            depthImageViewCreateInfo.subresourceRange.aspectMask = ::VK_IMAGE_ASPECT_DEPTH_BIT | ::VK_IMAGE_ASPECT_STENCIL_BIT;
            depthImageViewCreateInfo.subresourceRange.baseMipLevel = 0;
            depthImageViewCreateInfo.subresourceRange.levelCount = 1;
            depthImageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
            depthImageViewCreateInfo.subresourceRange.layerCount = 1;

            ::VkImageView depthImageView;
            if (::vkCreateImageView(_ctx->device, &depthImageViewCreateInfo, nullptr, &depthImageView) != ::VK_SUCCESS)
                DJO_VK_CONTEXT_ERROR_RETURN("Failed to create swapchain depth image view");

            _ctx->depthImageView = depthImageView;

            DJO_VK_CONTEXT_LOG("Swapchain created");

            DJO_ASSERT(!_ctx->hasError);
            return !_ctx->hasError;
        }

        void DestroySwapchain(VulkanContext* const _ctx) noexcept
        {
            DJO_VK_CONTEXT_LOG("Swapchain destruction...");

            if (_ctx->device != VK_NULL_HANDLE)
            {
                ::vkDestroyImageView(_ctx->device, _ctx->depthImageView, nullptr);
                DestroyAllocatedVkImage(_ctx->allocator, &_ctx->depthImage);

                const sz imageViewCount = _ctx->swapchainImageViews.count;
                for (sz i = 0; i < imageViewCount; ++i)
                {
                    ::vkDestroyImageView(_ctx->device, _ctx->swapchainImageViews[i], nullptr);
                    _ctx->swapchainImageViews[i] = VK_NULL_HANDLE;
                }

                ::vkDestroySwapchainKHR(_ctx->device, _ctx->swapchain, nullptr);
                _ctx->swapchain = VK_NULL_HANDLE;
            }

            DJO_VK_CONTEXT_LOG("Swapchain destroyed");
        }

        void ShutdownContext(VulkanContext* const _ctx) noexcept
        {
            DJO_VK_CONTEXT_LOG("Shutdown...");

            DestroySwapchain(_ctx);
            
            ::vmaDestroyAllocator(_ctx->allocator);
            ::vkDestroyDevice(_ctx->device, nullptr);

            if (_ctx->instance != VK_NULL_HANDLE)
            {
                ::vkDestroySurfaceKHR(_ctx->instance, _ctx->surface, nullptr);

            #if DJO_VK_ENABLE_VALIDATION_LAYERS
                _ctx->vkDestroyDebugUtilsMessengerEXT(_ctx->instance, _ctx->debugMessenger, nullptr);
            #endif

                ::vkDestroyInstance(_ctx->instance, nullptr);
            }

            if (_ctx->libHandle != nullptr)
                ::FreeLibrary(reinterpret_cast<::HMODULE>(_ctx->libHandle));

            DJO_VK_CONTEXT_LOG("Shutdown");

            FreeArena(_ctx->arena);
            ZeroMem(_ctx);
        }

        void SetError(VulkanContext* const _ctx, const StringView& _message) noexcept
        {
            DJO_ASSERT(false);

            _ctx->hasError = true;
            Set(&_ctx->errorMessage, 0, _message);
        }

    #undef DJO_VK_CONTEXT_ERROR_RETURN
    #undef DJO_VK_CONTEXT_LOG
    }
}
