#include "Window_pch.h"

#include "File.hpp"


namespace Djo
{
    MappedFile MapFileReadOnly(const char* const _filePath)
    {
        MappedFile mappedFile{};

        const ::HANDLE hFile = ::CreateFileA(
            _filePath,
            GENERIC_READ,
            FILE_SHARE_READ,
            NULL,
            OPEN_EXISTING,
            FILE_ATTRIBUTE_READONLY | FILE_FLAG_RANDOM_ACCESS,
            NULL);
        if (hFile == INVALID_HANDLE_VALUE)
        {
            DJO_ASSERT(false);
            goto Error_early;
        }

        ::LARGE_INTEGER fileSize;
        if (!::GetFileSizeEx(hFile, &fileSize))
        {
            DJO_ASSERT(false);
            goto Error_close;
        }
        if (fileSize.QuadPart == 0)
        {
            // empty, error?
            goto Error_close;
        }

        const ::HANDLE hMap = ::CreateFileMappingA(
            hFile, NULL,
            PAGE_READONLY,
            0, 0, NULL);
        if (hMap == NULL)
        {
            DJO_ASSERT(false);
            goto Error_close;
        }

        const ::LPVOID mappedPtr = ::MapViewOfFile(hMap, FILE_MAP_READ, 0, 0, 0);
        if (mappedPtr == NULL)
        {
            DJO_ASSERT(false);
            goto Error_unmap;
        }

        mappedFile.fileHandle = hFile;
        mappedFile.mapHandle = hMap;
        mappedFile.mappedBuffer = Memory::Area{ reinterpret_cast<u8*>(mappedPtr), static_cast<sz>(fileSize.QuadPart) };

        return mappedFile;

    Error_unmap:
        ::CloseHandle(hMap);
    Error_close:
        ::CloseHandle(hFile);
    Error_early:
        return mappedFile;
    }

    void UnmapFile(MappedFile* const _mappedFile)
    {
        if (_mappedFile->mappedBuffer.data != nullptr)
            ::UnmapViewOfFile(_mappedFile->mappedBuffer.data);

        if (_mappedFile->mapHandle != nullptr)
            ::CloseHandle(_mappedFile->mapHandle);

        if (_mappedFile->fileHandle != nullptr)
            ::CloseHandle(_mappedFile->fileHandle);

        ZeroMem(_mappedFile);
    }
}
