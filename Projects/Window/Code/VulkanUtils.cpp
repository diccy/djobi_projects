#include "Window_pch.h"

#include "VulkanUtils.hpp"

#include <DjobiCore/Core/Utils.hpp>


namespace Djo
{
    namespace Vk
    {
        ::VkResult CreateAllocatedVkBuffer(
            const ::VmaAllocator _allocator,
            const ::VkDeviceSize _size,
            const ::VkBufferUsageFlags _usage,
            const ::VkMemoryPropertyFlags _required,
            const ::VkMemoryPropertyFlags _preferred,
            AllocatedVkBuffer* const _buffer) noexcept
        {
            ::VkBufferCreateInfo bufferCreateInfo = BufferCreateInfo();
            bufferCreateInfo.size = _size;
            bufferCreateInfo.usage = _usage;
            bufferCreateInfo.sharingMode = ::VK_SHARING_MODE_EXCLUSIVE;
            bufferCreateInfo.queueFamilyIndexCount = 0;
            bufferCreateInfo.pQueueFamilyIndices = nullptr;

            ::VmaAllocationCreateInfo vmaAllocInfo{};
            vmaAllocInfo.flags = 0;
            vmaAllocInfo.usage = ::VMA_MEMORY_USAGE_UNKNOWN;
            vmaAllocInfo.requiredFlags = _required;
            vmaAllocInfo.preferredFlags = _preferred;
            vmaAllocInfo.memoryTypeBits = 0;
            vmaAllocInfo.pool = VK_NULL_HANDLE;
            vmaAllocInfo.pUserData = nullptr;
            vmaAllocInfo.priority = 0;

            ZeroMem(_buffer);
            return ::vmaCreateBuffer(
                _allocator,
                &bufferCreateInfo, &vmaAllocInfo,
                &_buffer->buffer, &_buffer->alloc,
                nullptr);
        }

        void DestroyAllocatedVkBuffer(const ::VmaAllocator _allocator, AllocatedVkBuffer* const _buffer) noexcept
        {
            ::vmaDestroyBuffer(_allocator, _buffer->buffer, _buffer->alloc);
        }

        void DestroyAllocatedVkImage(const ::VmaAllocator _allocator, AllocatedVkImage* const _image) noexcept
        {
            ::vmaDestroyImage(_allocator, _image->image, _image->alloc);
        }

        ::VkResult BeginOnceCommands(
            const ::VkDevice _device,
            const ::VkCommandPool _commandPool,
            ::VkCommandBuffer* const _commandBuffer) noexcept
        {
            ::VkCommandBufferAllocateInfo commandBufferAllocateInfo = CommandBufferAllocateInfo();
            commandBufferAllocateInfo.commandPool = _commandPool;
            commandBufferAllocateInfo.level = ::VK_COMMAND_BUFFER_LEVEL_PRIMARY;
            commandBufferAllocateInfo.commandBufferCount = 1;

            ::VkCommandBuffer commandBuffer;
            const ::VkResult allocResult = ::vkAllocateCommandBuffers(_device, &commandBufferAllocateInfo, &commandBuffer);
            if (allocResult != ::VK_SUCCESS)
                return allocResult;

            ::VkCommandBufferBeginInfo beginInfo = CommandBufferBeginInfo();
            beginInfo.flags = ::VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
            beginInfo.pInheritanceInfo = nullptr;
            ::vkBeginCommandBuffer(commandBuffer, &beginInfo);

            *_commandBuffer = commandBuffer;
            return ::VK_SUCCESS;
        }

        ::VkResult EndSubmitFreeOnceCommands(
            const ::VkDevice _device,
            const ::VkCommandPool _commandPool,
            const ::VkCommandBuffer _commandBuffer,
            const ::VkQueue _queue) noexcept
        {
            ::VkResult result = ::vkEndCommandBuffer(_commandBuffer);
            if (result != ::VK_SUCCESS)
                goto EndSubmitFreeOnceCommands_end;

            {
                ::VkSubmitInfo submitInfo = SubmitInfo();
                submitInfo.waitSemaphoreCount = 0;
                submitInfo.pWaitSemaphores = nullptr;
                submitInfo.pWaitDstStageMask = nullptr;
                submitInfo.commandBufferCount = 1;
                submitInfo.pCommandBuffers = &_commandBuffer;
                submitInfo.signalSemaphoreCount = 0;
                submitInfo.pSignalSemaphores = nullptr;
                result = ::vkQueueSubmit(_queue, 1, &submitInfo, VK_NULL_HANDLE);
                if (result != ::VK_SUCCESS)
                    goto EndSubmitFreeOnceCommands_end;
            }

            result = ::vkQueueWaitIdle(_queue);

        EndSubmitFreeOnceCommands_end:
            ::vkFreeCommandBuffers(_device, _commandPool, 1, &_commandBuffer);
            return result;
        }

        ::VkResult InitBufferFromMemory(
            const ::VkDevice _device,
            const ::VmaAllocator _allocator,
            const void* const _mem,
            const sz _size,
            const ::VkBufferUsageFlagBits _usage,
            const ::VkCommandPool _stagingCommandPool,
            const ::VkQueue _transferQueue,
            AllocatedVkBuffer* const _buffer) noexcept
        {
            ::VkResult result;

            AllocatedVkBuffer stagingBuffer{};
            result = CreateAllocatedVkBuffer(
                _allocator, _size,
                ::VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                ::VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | ::VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                0,
                &stagingBuffer);
            if (result != ::VK_SUCCESS)
                return result;

            DJO_DEFER_LAMBDA([_allocator, &stagingBuffer]() { DestroyAllocatedVkBuffer(_allocator, &stagingBuffer); });

            void* mappedData;
            result = ::vmaMapMemory(_allocator, stagingBuffer.alloc, &mappedData);
            if (result != ::VK_SUCCESS)
                return result;
            ::memcpy(mappedData, _mem, _size);
            ::vmaUnmapMemory(_allocator, stagingBuffer.alloc);

            AllocatedVkBuffer buffer{};
            result = CreateAllocatedVkBuffer(
                _allocator, _size,
                ::VK_BUFFER_USAGE_TRANSFER_DST_BIT | _usage,
                ::VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                0,
                &buffer);
            if (result != ::VK_SUCCESS)
                return result;

            DJO_DEFER_LAMBDA([_allocator, &buffer, &result]() { if (result != ::VK_SUCCESS) DestroyAllocatedVkBuffer(_allocator, &buffer); });

            ::VkCommandBuffer commandBuffer;
            result = BeginOnceCommands(_device, _stagingCommandPool, &commandBuffer);
            if (result != ::VK_SUCCESS)
                return result;

            ::VkBufferCopy region{};
            region.srcOffset = 0;
            region.dstOffset = 0;
            region.size = _size;
            ::vkCmdCopyBuffer(commandBuffer, stagingBuffer.buffer, buffer.buffer, 1, &region);

            result = EndSubmitFreeOnceCommands(_device, _stagingCommandPool, commandBuffer, _transferQueue);
            if (result != ::VK_SUCCESS)
                return result;

            *_buffer = buffer;
            return ::VK_SUCCESS;
        }

        ::VkResult CreateAndPrepareImage2DForFragmentShaderRead(
            const ::VkDevice _device,
            const ::VmaAllocator _allocator,
            const void* const _imageData,
            const ::VkDeviceSize _imageSize,
            const ::VkExtent3D _imageExtent,
            const ::VkFormat _imageFormat,
            const ::VkCommandPool _stagingCommandPool,
            const ::VkQueue _transferQueue,
            AllocatedVkImage* const _image,
            ::VkImageView* const _imageView) noexcept
        {
            ::VkResult result;

            AllocatedVkBuffer stagingBuffer{};
            ::VkImageCreateInfo imageCreateInfo = ImageCreateInfo();
            ::VmaAllocationCreateInfo imageAllocInfo{};
            AllocatedVkImage image{};
            ::VkCommandBuffer commandBuffer;
            ::VkBufferImageCopy region{};
            ::VkImageViewCreateInfo imageViewCreateInfo = ImageViewCreateInfo();
            ::VkImageView imageView;

            result = CreateAllocatedVkBuffer(
                _allocator, _imageSize,
                ::VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                ::VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | ::VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                0,
                &stagingBuffer);
            if (result != ::VK_SUCCESS)
                goto CreateAndPrepareImage2DForFragmentShaderRead_Error;

            void* mappedData;
            result = ::vmaMapMemory(_allocator, stagingBuffer.alloc, &mappedData);
            if (result != ::VK_SUCCESS)
                goto CreateAndPrepareImage2DForFragmentShaderRead_Error;
            ::memcpy(mappedData, _imageData, _imageSize);
            ::vmaUnmapMemory(_allocator, stagingBuffer.alloc);

            imageCreateInfo.imageType = ::VK_IMAGE_TYPE_2D;
            imageCreateInfo.format = _imageFormat;
            imageCreateInfo.extent = _imageExtent;
            imageCreateInfo.mipLevels = 1;
            imageCreateInfo.arrayLayers = 1;
            imageCreateInfo.samples = ::VK_SAMPLE_COUNT_1_BIT;
            imageCreateInfo.tiling = ::VK_IMAGE_TILING_OPTIMAL;
            imageCreateInfo.usage = ::VK_IMAGE_USAGE_TRANSFER_DST_BIT | ::VK_IMAGE_USAGE_SAMPLED_BIT;
            imageCreateInfo.sharingMode = ::VK_SHARING_MODE_EXCLUSIVE;
            imageCreateInfo.queueFamilyIndexCount = 0;
            imageCreateInfo.pQueueFamilyIndices = nullptr;
            imageCreateInfo.initialLayout = ::VK_IMAGE_LAYOUT_UNDEFINED;

            imageAllocInfo.flags = 0;
            imageAllocInfo.usage = ::VMA_MEMORY_USAGE_UNKNOWN;
            imageAllocInfo.requiredFlags = ::VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
            imageAllocInfo.preferredFlags = 0;
            imageAllocInfo.memoryTypeBits = 0;
            imageAllocInfo.pool = VK_NULL_HANDLE;
            imageAllocInfo.pUserData = nullptr;
            imageAllocInfo.priority = 0;

            result = ::vmaCreateImage(_allocator, &imageCreateInfo, &imageAllocInfo, &image.image, &image.alloc, nullptr);
            if (result != ::VK_SUCCESS)
                goto CreateAndPrepareImage2DForFragmentShaderRead_Error;

            result = ChangeImageLayout(
                _device,
                image.image, _imageFormat,
                ::VK_IMAGE_LAYOUT_UNDEFINED, ::VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                _stagingCommandPool, _transferQueue);
            if (result != ::VK_SUCCESS)
                goto CreateAndPrepareImage2DForFragmentShaderRead_Error_DestroyImage;

            result = BeginOnceCommands(_device, _stagingCommandPool, &commandBuffer);
            if (result != ::VK_SUCCESS)
                goto CreateAndPrepareImage2DForFragmentShaderRead_Error_DestroyImage;

            region.bufferOffset = 0;
            region.bufferRowLength = 0;
            region.bufferImageHeight = 0;
            region.imageSubresource.aspectMask = ::VK_IMAGE_ASPECT_COLOR_BIT;
            region.imageSubresource.mipLevel = 0;
            region.imageSubresource.baseArrayLayer = 0;
            region.imageSubresource.layerCount = 1;
            region.imageOffset = { 0, 0, 0 };
            region.imageExtent = _imageExtent;

            ::vkCmdCopyBufferToImage(
                commandBuffer,
                stagingBuffer.buffer, image.image,
                ::VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                1, &region);

            result = EndSubmitFreeOnceCommands(_device, _stagingCommandPool, commandBuffer, _transferQueue);
            if (result != ::VK_SUCCESS)
                goto CreateAndPrepareImage2DForFragmentShaderRead_Error_DestroyImage;

            result = ChangeImageLayout(
                _device,
                image.image, _imageFormat,
                ::VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, ::VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                _stagingCommandPool, _transferQueue);
            if (result != ::VK_SUCCESS)
                goto CreateAndPrepareImage2DForFragmentShaderRead_Error_DestroyImage;

            imageViewCreateInfo.image = image.image;
            imageViewCreateInfo.viewType = ::VK_IMAGE_VIEW_TYPE_2D;
            imageViewCreateInfo.format = _imageFormat;
            imageViewCreateInfo.components.r = ::VK_COMPONENT_SWIZZLE_IDENTITY;
            imageViewCreateInfo.components.g = ::VK_COMPONENT_SWIZZLE_IDENTITY;
            imageViewCreateInfo.components.b = ::VK_COMPONENT_SWIZZLE_IDENTITY;
            imageViewCreateInfo.components.a = ::VK_COMPONENT_SWIZZLE_IDENTITY;
            imageViewCreateInfo.subresourceRange.aspectMask = ::VK_IMAGE_ASPECT_COLOR_BIT;
            imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
            imageViewCreateInfo.subresourceRange.levelCount = 1;
            imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
            imageViewCreateInfo.subresourceRange.layerCount = 1;

            result = ::vkCreateImageView(_device, &imageViewCreateInfo, nullptr, &imageView);
            if (result != ::VK_SUCCESS)
                goto CreateAndPrepareImage2DForFragmentShaderRead_Error_DestroyImage;

            *_image = image;
            *_imageView = imageView;

            DestroyAllocatedVkBuffer(_allocator, &stagingBuffer);
            return ::VK_SUCCESS;

        CreateAndPrepareImage2DForFragmentShaderRead_Error_DestroyImage:
            DestroyAllocatedVkImage(_allocator, &image);

        CreateAndPrepareImage2DForFragmentShaderRead_Error:
            DestroyAllocatedVkBuffer(_allocator, &stagingBuffer);
            return result;
        }

        namespace _Internal
        {
            // those two functions may be conceptually quite wrong

            DJO_INLINE ::VkAccessFlags ToAccessFlags(const ::VkImageLayout _layout)
            {
                switch (_layout)
                {
                    case ::VK_IMAGE_LAYOUT_UNDEFINED:                       return ::VK_ACCESS_NONE;
                    case ::VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:            return ::VK_ACCESS_TRANSFER_WRITE_BIT;
                    case ::VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:        return ::VK_ACCESS_SHADER_READ_BIT;
                    default:
                    {
                        // undefined case (yet)
                        DJO_ASSERT(false);
                        return ::VK_ACCESS_NONE;
                    }
                }
            }
            DJO_INLINE ::VkPipelineStageFlags ToPipelineStageFlags(const ::VkImageLayout _layout)
            {
                switch (_layout)
                {
                    case ::VK_IMAGE_LAYOUT_UNDEFINED:                       return ::VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
                    case ::VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:            return ::VK_PIPELINE_STAGE_TRANSFER_BIT;
                    case ::VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:        return ::VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
                    default:
                    {
                        // undefined case (yet)
                        DJO_ASSERT(false);
                        return ::VK_ACCESS_NONE;
                    }
                }
            }
        }

        ::VkResult ChangeImageLayout(
            const ::VkDevice _device,
            const ::VkImage _image,
            const ::VkFormat /*_format*/,
            const ::VkImageLayout _oldLayout,
            const ::VkImageLayout _newLayout,
            const ::VkCommandPool _stagingCommandPool,
            const ::VkQueue _transferQueue) noexcept
        {
            ::VkResult result;

            ::VkAccessFlags srcAccessMask = _Internal::ToAccessFlags(_oldLayout);
            ::VkAccessFlags dstAccessMask = _Internal::ToAccessFlags(_newLayout);
            ::VkPipelineStageFlags srcStage = _Internal::ToPipelineStageFlags(_oldLayout);
            ::VkPipelineStageFlags dstStage = _Internal::ToPipelineStageFlags(_newLayout);

            ::VkCommandBuffer commandBuffer;
            result = BeginOnceCommands(_device, _stagingCommandPool, &commandBuffer);
            if (result != ::VK_SUCCESS)
                return result;

            ::VkImageMemoryBarrier barrier = ImageMemoryBarrier();
            barrier.srcAccessMask = srcAccessMask;
            barrier.dstAccessMask = dstAccessMask;
            barrier.oldLayout = _oldLayout;
            barrier.newLayout = _newLayout;
            barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            barrier.image = _image;
            barrier.subresourceRange.aspectMask = ::VK_IMAGE_ASPECT_COLOR_BIT;
            barrier.subresourceRange.baseMipLevel = 0;
            barrier.subresourceRange.levelCount = 1;
            barrier.subresourceRange.baseArrayLayer = 0;
            barrier.subresourceRange.layerCount = 1;
            ::vkCmdPipelineBarrier(
                commandBuffer,
                srcStage, dstStage,
                0,
                0, nullptr,
                0, nullptr,
                1, &barrier);

            result = EndSubmitFreeOnceCommands(_device, _stagingCommandPool, commandBuffer, _transferQueue);
            if (result != ::VK_SUCCESS)
                return result;

            return ::VK_SUCCESS;
        }
    }
}
