#pragma once

#include "Vulkan.hpp"


namespace Djo
{
    namespace Vk
    {
    #define DJO_VK_STRUCT_INITIALIZER(struct_, func_, structEnum_)\
        constexpr struct_ func_() noexcept\
            {\
                struct_ s{};\
                s.sType = structEnum_;\
                return s;\
            }   

        DJO_VK_STRUCT_INITIALIZER(::VkImageCreateInfo,              ImageCreateInfo,                ::VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO);
        DJO_VK_STRUCT_INITIALIZER(::VkImageViewCreateInfo,          ImageViewCreateInfo,            ::VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO);
        DJO_VK_STRUCT_INITIALIZER(::VkImageMemoryBarrier,           ImageMemoryBarrier,             ::VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER);
        DJO_VK_STRUCT_INITIALIZER(::VkSamplerCreateInfo,            SamplerCreateInfo,              ::VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO);

        DJO_VK_STRUCT_INITIALIZER(::VkRenderPassCreateInfo,         RenderPassCreateInfo,           ::VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO);
        DJO_VK_STRUCT_INITIALIZER(::VkRenderPassBeginInfo,          RenderPassBeginInfo,            ::VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO);

        DJO_VK_STRUCT_INITIALIZER(::VkPipelineCacheCreateInfo,                  PipelineCacheCreateInfo,                    ::VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO);
        DJO_VK_STRUCT_INITIALIZER(::VkPipelineDynamicStateCreateInfo,           PipelineDynamicStateCreateInfo,             ::VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO);
        DJO_VK_STRUCT_INITIALIZER(::VkPipelineVertexInputStateCreateInfo,       PipelineVertexInputStateCreateInfo,         ::VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO);
        DJO_VK_STRUCT_INITIALIZER(::VkPipelineInputAssemblyStateCreateInfo,     PipelineInputAssemblyStateCreateInfo,       ::VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO);
        DJO_VK_STRUCT_INITIALIZER(::VkPipelineViewportStateCreateInfo,          PipelineViewportStateCreateInfo,            ::VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO);
        DJO_VK_STRUCT_INITIALIZER(::VkPipelineRasterizationStateCreateInfo,     PipelineRasterizationStateCreateInfo,       ::VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO);
        DJO_VK_STRUCT_INITIALIZER(::VkPipelineMultisampleStateCreateInfo,       PipelineMultisampleStateCreateInfo,         ::VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO);
        DJO_VK_STRUCT_INITIALIZER(::VkPipelineDepthStencilStateCreateInfo,      PipelineDepthStencilStateCreateInfo,        ::VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO);
        DJO_VK_STRUCT_INITIALIZER(::VkPipelineColorBlendStateCreateInfo,        PipelineColorBlendStateCreateInfo,          ::VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO);
        DJO_VK_STRUCT_INITIALIZER(::VkPipelineLayoutCreateInfo,                 PipelineLayoutCreateInfo,                   ::VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO);
        DJO_VK_STRUCT_INITIALIZER(::VkPipelineShaderStageCreateInfo,            PipelineShaderStageCreateInfo,              ::VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO);
        DJO_VK_STRUCT_INITIALIZER(::VkGraphicsPipelineCreateInfo,               GraphicsPipelineCreateInfo,                 ::VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO);
        
        DJO_VK_STRUCT_INITIALIZER(::VkDescriptorPoolCreateInfo,         DescriptorPoolCreateInfo,           ::VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO);
        DJO_VK_STRUCT_INITIALIZER(::VkDescriptorSetLayoutCreateInfo,    DescriptorSetLayoutCreateInfo,      ::VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO);
        DJO_VK_STRUCT_INITIALIZER(::VkDescriptorSetAllocateInfo,        DescriptorSetAllocateInfo,          ::VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO);
        DJO_VK_STRUCT_INITIALIZER(::VkWriteDescriptorSet,               DescriptorSetWriteInfo,             ::VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET);

        DJO_VK_STRUCT_INITIALIZER(::VkBufferCreateInfo,                 BufferCreateInfo,                   ::VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO);
        DJO_VK_STRUCT_INITIALIZER(::VkFramebufferCreateInfo,            FramebufferCreateInfo,              ::VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO);

        DJO_VK_STRUCT_INITIALIZER(::VkShaderModuleCreateInfo,       ShaderModuleCreateInfo,         ::VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO);

        DJO_VK_STRUCT_INITIALIZER(::VkCommandPoolCreateInfo,        CommandPoolCreateInfo,          ::VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO);
        DJO_VK_STRUCT_INITIALIZER(::VkCommandBufferAllocateInfo,    CommandBufferAllocateInfo,      ::VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO);
        DJO_VK_STRUCT_INITIALIZER(::VkCommandBufferBeginInfo,       CommandBufferBeginInfo,         ::VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO);

        DJO_VK_STRUCT_INITIALIZER(::VkSemaphoreCreateInfo,          SemaphoreCreateInfo,            ::VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO);
        DJO_VK_STRUCT_INITIALIZER(::VkFenceCreateInfo,              FenceCreateInfo,                ::VK_STRUCTURE_TYPE_FENCE_CREATE_INFO);

        DJO_VK_STRUCT_INITIALIZER(::VkSubmitInfo,                   SubmitInfo,                     ::VK_STRUCTURE_TYPE_SUBMIT_INFO);

        DJO_VK_STRUCT_INITIALIZER(::VkPresentInfoKHR,               PresentInfoKHR,                 ::VK_STRUCTURE_TYPE_PRESENT_INFO_KHR);

        DJO_VK_STRUCT_INITIALIZER(::VkMappedMemoryRange,            MappedMemoryRange,              ::VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE);

    #undef DJO_VK_STRUCT_INITIALIZER
    }
}
