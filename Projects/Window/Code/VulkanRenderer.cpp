#include "Window_pch.h"

#include "VulkanRenderer.hpp"

#include "File.hpp"
#include "StbImage.hpp"

#include <DjobiCore/Math/Matrix44.hpp>
#include <DjobiCore/Format/Format.hpp>
#include <DjobiCore/OS/Time.hpp>
#include <DjobiCore/Containers/ArrayAlloc.hpp>
#include <DjobiCore/Thread/ThreadContext.hpp>


namespace Djo
{
    namespace Vk
    {
        // tmp
        struct Vertex
        {
            f32 pos[3];
            f32 color[3];
            f32 uv[2];
        };
        struct UniformBufferObject
        {
            Mat44f model;
            Mat44f view;
            Mat44f proj;
        };

        static const Vertex s_quadVertices[]
        {
            { { -1.f,  1.f, 0.f, }, { 1.f, 0.f, 0.f }, { 0.f, 1.f } },
            { { -1.f, -1.f, 0.f, }, { 0.f, 1.f, 0.f }, { 0.f, 0.f } },
            { {  1.f, -1.f, 0.f, }, { 0.f, 0.f, 1.f }, { 1.f, 0.f } },
            { {  1.f,  1.f, 0.f, }, { 1.f, 1.f, 1.f }, { 1.f, 1.f } },
        };
        static const u16 s_quadIndices[]
        {
            0, 1, 2, 0, 2, 3,
        };

        static const Vertex s_cubeVertices[]
        {
           { {  0.5f, -0.5f, -0.5f, }, { 1.f, 1.f, 0.f }, { 0.f, 0.f } },
           { {  0.5f,  0.5f, -0.5f, }, { 1.f, 0.f, 1.f }, { 1.f, 0.f } },
           { {  0.5f,  0.5f,  0.5f, }, { 0.f, 0.f, 0.f }, { 1.f, 1.f } },
           { {  0.5f, -0.5f,  0.5f, }, { 0.f, 1.f, 0.f }, { 0.f, 1.f } },

           { {  0.5f,  0.5f,  0.5f, }, { 0.f, 0.f, 0.f }, { 1.f, 1.f } },
           { {  0.5f,  0.5f, -0.5f, }, { 1.f, 0.f, 1.f }, { 1.f, 0.f } },
           { { -0.5f,  0.5f, -0.5f, }, { 0.f, 1.f, 1.f }, { 0.f, 0.f } },
           { { -0.5f,  0.5f,  0.5f, }, { 0.f, 0.f, 1.f }, { 0.f, 1.f } },

           { { -0.5f, -0.5f,  0.5f, }, { 1.f, 1.f, 1.f }, { 0.f, 0.f } },
           { {  0.5f, -0.5f,  0.5f, }, { 0.f, 1.f, 0.f }, { 1.f, 0.f } },
           { {  0.5f,  0.5f,  0.5f, }, { 0.f, 0.f, 0.f }, { 1.f, 1.f } },
           { { -0.5f,  0.5f,  0.5f, }, { 0.f, 0.f, 1.f }, { 0.f, 1.f } },

           { { -0.5f,  0.5f,  0.5f, }, { 0.f, 0.f, 1.f }, { 1.f, 1.f } },
           { { -0.5f,  0.5f, -0.5f, }, { 0.f, 1.f, 1.f }, { 1.f, 0.f } },
           { { -0.5f, -0.5f, -0.5f, }, { 1.f, 0.f, 0.f }, { 0.f, 0.f } },
           { { -0.5f, -0.5f,  0.5f, }, { 1.f, 1.f, 1.f }, { 0.f, 1.f } },

           { { -0.5f, -0.5f, -0.5f, }, { 1.f, 0.f, 0.f }, { 0.f, 0.f } },
           { {  0.5f, -0.5f, -0.5f, }, { 1.f, 1.f, 0.f }, { 1.f, 0.f } },
           { {  0.5f, -0.5f,  0.5f, }, { 0.f, 1.f, 0.f }, { 1.f, 1.f } },
           { { -0.5f, -0.5f,  0.5f, }, { 1.f, 1.f, 1.f }, { 0.f, 1.f } },

           { {  0.5f,  0.5f, -0.5f, }, { 1.f, 0.f, 1.f }, { 1.f, 1.f } },
           { {  0.5f, -0.5f, -0.5f, }, { 1.f, 1.f, 0.f }, { 1.f, 0.f } },
           { { -0.5f, -0.5f, -0.5f, }, { 1.f, 0.f, 0.f }, { 0.f, 0.f } },
           { { -0.5f,  0.5f, -0.5f, }, { 0.f, 1.f, 1.f }, { 0.f, 1.f } },
        };
        static const u16 s_cubeIndices[]
        {
             0,  1,  2,  0,  2,  3,
             4,  5,  6,  4,  6,  7,
             8,  9, 10,  8, 10, 11,
            12, 13, 14, 12, 14, 15,
            16, 17, 18, 16, 18, 19,
            20, 21, 22, 20, 22, 23,
        };

        #define DJO_VK_RENDERER_LOG(msg_, ...)\
            DJO_FORMAT_LF(&_renderer->ctx->logger, "% [Vulkan Renderer] " msg_, Format::ELogLevel::Info, __VA_ARGS__)

        #define DJO_VK_RENDERER_ERROR_RETURN(msg_)\
            {\
                SetError(_renderer->ctx, ToStringView("[Vulkan Renderer] " msg_ "\n"));\
                return false;\
            }

        namespace _Internal
        {
            void DrawMainRenderPass(VulkanRenderer* _renderer, VulkanRenderer::PerFrameData* const _frameData) noexcept;
            bool RecreateSwapchain(VulkanRenderer* _renderer) noexcept;
            bool CreateSwapchainFrameBuffers(VulkanRenderer* _renderer) noexcept;
            void DestroySwapchainFrameBuffers(VulkanRenderer* _renderer) noexcept;
        }

        VulkanRenderer MoldRenderer(VulkanContext* _ctx) noexcept
        {
            VulkanRenderer renderer{};
            renderer.ctx = _ctx;
            return renderer;
        }

        bool InitRenderer(VulkanRenderer* const _renderer) noexcept
        {
            DJO_ASSERT(_renderer->ctx->arena != nullptr);
            DJO_ASSERT(!_renderer->ctx->hasError);
            DJO_ASSERT(GetData(_renderer->ctx->errorMessage));
            DJO_ASSERT(_renderer->ctx->device != VK_NULL_HANDLE);
            DJO_ASSERT(_renderer->ctx->swapchain != VK_NULL_HANDLE);

        #define DJO_VK_RENDERER_INIT_LOG_STEP(msg_, ...)  DJO_VK_RENDERER_LOG("Init: " msg_, __VA_ARGS__)
        #define DJO_VK_RENDERER_INIT_ERROR_RETURN(msg_)   DJO_VK_RENDERER_ERROR_RETURN("[ERROR] Init: " msg_)

            VulkanContext* const ctx = _renderer->ctx;
            const ::VkDevice ctxDevice = ctx->device;

            // Graphics pipeline
            {
                // Main render pass

                DJO_VK_RENDERER_INIT_LOG_STEP("Main render pass init...");

                ::VkAttachmentDescription colorAttachment{};
                colorAttachment.flags = 0;
                colorAttachment.format = ctx->swapchainFormat.format;
                colorAttachment.samples = ::VK_SAMPLE_COUNT_1_BIT;
                colorAttachment.loadOp = ::VK_ATTACHMENT_LOAD_OP_CLEAR;
                colorAttachment.storeOp = ::VK_ATTACHMENT_STORE_OP_STORE;
                colorAttachment.stencilLoadOp = ::VK_ATTACHMENT_LOAD_OP_DONT_CARE;
                colorAttachment.stencilStoreOp = ::VK_ATTACHMENT_STORE_OP_DONT_CARE;
                colorAttachment.initialLayout = ::VK_IMAGE_LAYOUT_UNDEFINED;
                colorAttachment.finalLayout = ::VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

                ::VkAttachmentReference colorAttachmentRef{};
                colorAttachmentRef.attachment = 0;
                colorAttachmentRef.layout = ::VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

                ::VkAttachmentDescription depthAttachment{};
                depthAttachment.flags = 0;
                depthAttachment.format = ctx->depthFormat;
                depthAttachment.samples = ::VK_SAMPLE_COUNT_1_BIT;
                depthAttachment.loadOp = ::VK_ATTACHMENT_LOAD_OP_CLEAR;
                depthAttachment.storeOp = ::VK_ATTACHMENT_STORE_OP_DONT_CARE;
                depthAttachment.stencilLoadOp = ::VK_ATTACHMENT_LOAD_OP_CLEAR;
                depthAttachment.stencilStoreOp = ::VK_ATTACHMENT_STORE_OP_DONT_CARE;
                depthAttachment.initialLayout = ::VK_IMAGE_LAYOUT_UNDEFINED;
                depthAttachment.finalLayout = ::VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

                ::VkAttachmentReference depthAttachmentRef{};
                depthAttachmentRef.attachment = 1;
                depthAttachmentRef.layout = ::VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

                ::VkSubpassDescription subpassDesc{};
                subpassDesc.flags = 0;
                subpassDesc.pipelineBindPoint = ::VK_PIPELINE_BIND_POINT_GRAPHICS;
                subpassDesc.inputAttachmentCount = 0;
                subpassDesc.pInputAttachments = nullptr;
                subpassDesc.colorAttachmentCount = 1;
                subpassDesc.pColorAttachments = &colorAttachmentRef;
                subpassDesc.pResolveAttachments = nullptr;
                subpassDesc.pDepthStencilAttachment = &depthAttachmentRef;
                subpassDesc.preserveAttachmentCount = 0;
                subpassDesc.pPreserveAttachments = nullptr;

                ::VkSubpassDependency subpassDependency{};
                subpassDependency.srcSubpass = VK_SUBPASS_EXTERNAL;
                subpassDependency.dstSubpass = 0;
                subpassDependency.srcStageMask = ::VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | ::VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
                subpassDependency.dstStageMask = ::VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | ::VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
                subpassDependency.srcAccessMask = 0;
                subpassDependency.dstAccessMask = ::VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | ::VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
                subpassDependency.dependencyFlags = 0;

                const ::VkAttachmentDescription attachments[]{ colorAttachment, depthAttachment };
                const ::VkSubpassDependency dependencies[]{ subpassDependency };

                ::VkRenderPassCreateInfo renderPassCreateInfo = RenderPassCreateInfo();
                renderPassCreateInfo.attachmentCount = ArrayCount(attachments);
                renderPassCreateInfo.pAttachments = attachments;
                renderPassCreateInfo.subpassCount = 1;
                renderPassCreateInfo.pSubpasses = &subpassDesc;
                renderPassCreateInfo.dependencyCount = ArrayCount(dependencies);
                renderPassCreateInfo.pDependencies = dependencies;

                ::VkRenderPass renderPass;
                if (::vkCreateRenderPass(ctxDevice, &renderPassCreateInfo, nullptr, &renderPass) != ::VK_SUCCESS)
                    DJO_VK_RENDERER_INIT_ERROR_RETURN("Failed to create render pass");

                _renderer->mainRenderPass = renderPass;

                DJO_VK_RENDERER_INIT_LOG_STEP("Main render pass initialized");

                // Init pipeline layout

                DJO_VK_RENDERER_INIT_LOG_STEP("Pipeline layout init...");

                // Dynamic states

                ::VkDynamicState dynamicStates[] =
                {
                    ::VK_DYNAMIC_STATE_VIEWPORT,
                    ::VK_DYNAMIC_STATE_SCISSOR,
                };
                ::VkPipelineDynamicStateCreateInfo dynamicStateCreateInfo = PipelineDynamicStateCreateInfo();
                dynamicStateCreateInfo.dynamicStateCount = ArrayCount(dynamicStates);
                dynamicStateCreateInfo.pDynamicStates = dynamicStates;

                // Vertex input

                static const ::VkVertexInputBindingDescription vertexInputBindingDesc
                {
                    0,                              // binding
                    sizeof(Vertex),                 // stride
                    ::VK_VERTEX_INPUT_RATE_VERTEX,  // inputRate
                };
                static const ::VkVertexInputAttributeDescription vertexInputAttributeDescription[3]
                {
                    {
                         0,                             // location
                         0,                             // binding
                         ::VK_FORMAT_R32G32B32_SFLOAT,  // format
                         offsetof(Vertex, pos),         // offset
                    },
                    {
                        1,
                        0,
                        ::VK_FORMAT_R32G32B32_SFLOAT,
                        offsetof(Vertex, color),
                    },
                    {
                        2,
                        0,
                        ::VK_FORMAT_R32G32_SFLOAT,
                        offsetof(Vertex, uv),
                    },
                };

                ::VkPipelineVertexInputStateCreateInfo vertexInputStateCreateInfo = PipelineVertexInputStateCreateInfo();
                vertexInputStateCreateInfo.vertexBindingDescriptionCount = 1;
                vertexInputStateCreateInfo.pVertexBindingDescriptions = &vertexInputBindingDesc;
                vertexInputStateCreateInfo.vertexAttributeDescriptionCount = ArrayCount(vertexInputAttributeDescription);
                vertexInputStateCreateInfo.pVertexAttributeDescriptions = vertexInputAttributeDescription;

                // Input assembly

                ::VkPipelineInputAssemblyStateCreateInfo inputAssemblyStateCreateInfo = PipelineInputAssemblyStateCreateInfo();
                inputAssemblyStateCreateInfo.topology = ::VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
                inputAssemblyStateCreateInfo.primitiveRestartEnable = VK_FALSE;

                // Viewport and scissor

                ::VkPipelineViewportStateCreateInfo viewportStateCreateInfo = PipelineViewportStateCreateInfo();
                viewportStateCreateInfo.viewportCount = 1;
                viewportStateCreateInfo.pViewports = nullptr; // dynamic
                viewportStateCreateInfo.scissorCount = 1;
                viewportStateCreateInfo.pScissors = nullptr; // dynamic

                // Rasterizer

                ::VkPipelineRasterizationStateCreateInfo rasterizationStateCreateInfo = PipelineRasterizationStateCreateInfo();
                rasterizationStateCreateInfo.depthClampEnable = VK_FALSE;
                rasterizationStateCreateInfo.rasterizerDiscardEnable = VK_FALSE;
                rasterizationStateCreateInfo.polygonMode = ::VK_POLYGON_MODE_FILL;
                rasterizationStateCreateInfo.cullMode = ::VK_CULL_MODE_BACK_BIT;
                rasterizationStateCreateInfo.frontFace = ::VK_FRONT_FACE_CLOCKWISE;
                rasterizationStateCreateInfo.depthBiasEnable = VK_FALSE;
                rasterizationStateCreateInfo.depthBiasConstantFactor = 0.f;
                rasterizationStateCreateInfo.depthBiasClamp = 0.f;
                rasterizationStateCreateInfo.depthBiasSlopeFactor = 0.f;
                rasterizationStateCreateInfo.lineWidth = 1.f;

                // Multisampling

                ::VkPipelineMultisampleStateCreateInfo multisampleStateCreateInfo = PipelineMultisampleStateCreateInfo();
                multisampleStateCreateInfo.rasterizationSamples = ::VK_SAMPLE_COUNT_1_BIT;
                multisampleStateCreateInfo.sampleShadingEnable = VK_FALSE;
                multisampleStateCreateInfo.minSampleShading = 0.f;
                multisampleStateCreateInfo.pSampleMask = nullptr;
                multisampleStateCreateInfo.alphaToCoverageEnable = VK_FALSE;
                multisampleStateCreateInfo.alphaToOneEnable = VK_FALSE;

                // Depth and stencil

                ::VkPipelineDepthStencilStateCreateInfo depthStencilStateCreateInfo = PipelineDepthStencilStateCreateInfo();
                depthStencilStateCreateInfo.depthTestEnable = VK_TRUE;
                depthStencilStateCreateInfo.depthWriteEnable = VK_TRUE;
                depthStencilStateCreateInfo.depthCompareOp = ::VK_COMPARE_OP_LESS;
                depthStencilStateCreateInfo.depthBoundsTestEnable = VK_FALSE;
                depthStencilStateCreateInfo.stencilTestEnable = VK_FALSE;
                depthStencilStateCreateInfo.front = {};
                depthStencilStateCreateInfo.back = {};
                depthStencilStateCreateInfo.minDepthBounds = 0.f;
                depthStencilStateCreateInfo.maxDepthBounds = 1.f;

                // Color blending

                ::VkPipelineColorBlendAttachmentState colorBlendAttachmentState{};
                colorBlendAttachmentState.blendEnable = VK_FALSE;
                colorBlendAttachmentState.srcColorBlendFactor = ::VK_BLEND_FACTOR_SRC_ALPHA;
                colorBlendAttachmentState.dstColorBlendFactor = ::VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
                colorBlendAttachmentState.colorBlendOp = ::VK_BLEND_OP_ADD;
                colorBlendAttachmentState.srcAlphaBlendFactor = ::VK_BLEND_FACTOR_ONE;
                colorBlendAttachmentState.dstAlphaBlendFactor = ::VK_BLEND_FACTOR_ZERO;
                colorBlendAttachmentState.alphaBlendOp = ::VK_BLEND_OP_ADD;
                colorBlendAttachmentState.colorWriteMask = ::VK_COLOR_COMPONENT_R_BIT | ::VK_COLOR_COMPONENT_G_BIT | ::VK_COLOR_COMPONENT_B_BIT | ::VK_COLOR_COMPONENT_A_BIT;

                ::VkPipelineColorBlendStateCreateInfo colorBlendStateCreateInfo = PipelineColorBlendStateCreateInfo();
                colorBlendStateCreateInfo.logicOpEnable = VK_TRUE;
                colorBlendStateCreateInfo.logicOp = ::VK_LOGIC_OP_COPY;
                colorBlendStateCreateInfo.attachmentCount = 1;
                colorBlendStateCreateInfo.pAttachments = &colorBlendAttachmentState;
                colorBlendStateCreateInfo.blendConstants[0] = 0.f;
                colorBlendStateCreateInfo.blendConstants[1] = 0.f;
                colorBlendStateCreateInfo.blendConstants[2] = 0.f;
                colorBlendStateCreateInfo.blendConstants[3] = 0.f;

                // Descriptor set layout

                ::VkDescriptorSetLayoutBinding uboDescriptorSetLayoutBinding{};
                uboDescriptorSetLayoutBinding.binding = 0;
                uboDescriptorSetLayoutBinding.descriptorType = ::VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
                uboDescriptorSetLayoutBinding.descriptorCount = 1;
                uboDescriptorSetLayoutBinding.stageFlags = ::VK_SHADER_STAGE_VERTEX_BIT;
                uboDescriptorSetLayoutBinding.pImmutableSamplers = nullptr;

                ::VkDescriptorSetLayoutBinding samplerDescriptorSetLayoutBinding{};
                samplerDescriptorSetLayoutBinding.binding = 1;
                samplerDescriptorSetLayoutBinding.descriptorType = ::VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
                samplerDescriptorSetLayoutBinding.descriptorCount = 1;
                samplerDescriptorSetLayoutBinding.stageFlags = ::VK_SHADER_STAGE_FRAGMENT_BIT;
                samplerDescriptorSetLayoutBinding.pImmutableSamplers = nullptr;

                ::VkDescriptorSetLayoutBinding descriptorSetLayoutBindings[]{ uboDescriptorSetLayoutBinding, samplerDescriptorSetLayoutBinding };
                ::VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo = DescriptorSetLayoutCreateInfo();
                descriptorSetLayoutCreateInfo.bindingCount = ArrayCount(descriptorSetLayoutBindings);
                descriptorSetLayoutCreateInfo.pBindings = descriptorSetLayoutBindings;

                ::VkDescriptorSetLayout descriptorSetLayout;
                if (::vkCreateDescriptorSetLayout(ctxDevice, &descriptorSetLayoutCreateInfo, nullptr, &descriptorSetLayout) != ::VK_SUCCESS)
                    DJO_VK_RENDERER_INIT_ERROR_RETURN("Failed to create descriptor set layout");

                _renderer->descriptorSetLayout = descriptorSetLayout;

                // Pipeline layout

                ::VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo = PipelineLayoutCreateInfo();
                pipelineLayoutCreateInfo.setLayoutCount = 1;
                pipelineLayoutCreateInfo.pSetLayouts = &descriptorSetLayout;
                pipelineLayoutCreateInfo.pushConstantRangeCount = 0;
                pipelineLayoutCreateInfo.pPushConstantRanges = nullptr;

                ::VkPipelineLayout pipelineLayout;
                if (::vkCreatePipelineLayout(ctxDevice, &pipelineLayoutCreateInfo, nullptr, &pipelineLayout) != ::VK_SUCCESS)
                    DJO_VK_RENDERER_INIT_ERROR_RETURN("Failed to create pipeline layout");

                _renderer->pipelineLayout = pipelineLayout;

                DJO_VK_RENDERER_INIT_LOG_STEP("Pipeline layout initialized");

                // Shader modules

                DJO_VK_RENDERER_INIT_LOG_STEP("Shader modules init...");

                MappedFile meshShaderFiles[2]{};

                meshShaderFiles[0] = MapFileReadOnly("Assets/Shaders/mesh.vert.glsl.spv");
                if (meshShaderFiles[0].mapHandle == nullptr)
                    DJO_VK_RENDERER_INIT_ERROR_RETURN("Failed to load vertex shader file");

                DJO_DEFER(&meshShaderFiles[0], f, UnmapFile(f));

                meshShaderFiles[1] = MapFileReadOnly("Assets/Shaders/mesh.frag.glsl.spv");
                if (meshShaderFiles[1].mapHandle == nullptr)
                    DJO_VK_RENDERER_INIT_ERROR_RETURN("Failed to load fragment shader file");

                DJO_DEFER(&meshShaderFiles[1], f, UnmapFile(f));

                ::VkShaderModuleCreateInfo meshShaderModuleCreateInfos[2]{};

                meshShaderModuleCreateInfos[0] = ShaderModuleCreateInfo();
                meshShaderModuleCreateInfos[0].codeSize = meshShaderFiles[0].mappedBuffer.count;
                meshShaderModuleCreateInfos[0].pCode = reinterpret_cast<const uint32_t*>(meshShaderFiles[0].mappedBuffer.data);

                meshShaderModuleCreateInfos[1] = ShaderModuleCreateInfo();
                meshShaderModuleCreateInfos[1].codeSize = meshShaderFiles[1].mappedBuffer.count;
                meshShaderModuleCreateInfos[1].pCode = reinterpret_cast<const uint32_t*>(meshShaderFiles[1].mappedBuffer.data);

                ::VkShaderModule meshShaderModules[2]{};

                if (::vkCreateShaderModule(ctxDevice, &meshShaderModuleCreateInfos[0], nullptr, &meshShaderModules[0]) != ::VK_SUCCESS)
                    DJO_VK_RENDERER_INIT_ERROR_RETURN("Failed to create vertex shader module");

                DJO_DEFER_LAMBDA([ctxDevice, shaderModule = meshShaderModules[0]]() { ::vkDestroyShaderModule(ctxDevice, shaderModule, nullptr); });

                if (::vkCreateShaderModule(ctxDevice, &meshShaderModuleCreateInfos[1], nullptr, &meshShaderModules[1]) != ::VK_SUCCESS)
                    DJO_VK_RENDERER_INIT_ERROR_RETURN("Failed to create fragment shader module");

                DJO_DEFER_LAMBDA([ctxDevice, shaderModule = meshShaderModules[1]]() { ::vkDestroyShaderModule(ctxDevice, shaderModule, nullptr); });

                ::VkPipelineShaderStageCreateInfo pipelineShaderStageCreateInfo[2]{};

                pipelineShaderStageCreateInfo[0] = PipelineShaderStageCreateInfo();
                pipelineShaderStageCreateInfo[0].stage = ::VK_SHADER_STAGE_VERTEX_BIT;
                pipelineShaderStageCreateInfo[0].module = meshShaderModules[0];
                pipelineShaderStageCreateInfo[0].pName = "main"; // entry point
                pipelineShaderStageCreateInfo[0].pSpecializationInfo = nullptr; // constant values initialization

                pipelineShaderStageCreateInfo[1] = PipelineShaderStageCreateInfo();
                pipelineShaderStageCreateInfo[1].stage = ::VK_SHADER_STAGE_FRAGMENT_BIT;
                pipelineShaderStageCreateInfo[1].module = meshShaderModules[1];
                pipelineShaderStageCreateInfo[1].pName = "main"; // entry point
                pipelineShaderStageCreateInfo[1].pSpecializationInfo = nullptr; // constant values initialization

                DJO_VK_RENDERER_INIT_LOG_STEP("Shader modules initialized");

                // Graphics pipeline

                DJO_VK_RENDERER_INIT_LOG_STEP("Graphics pipeline init...");

                ::VkGraphicsPipelineCreateInfo graphicsPipelineCreateInfo = GraphicsPipelineCreateInfo();
                graphicsPipelineCreateInfo.stageCount = ArrayCount(pipelineShaderStageCreateInfo);
                graphicsPipelineCreateInfo.pStages = pipelineShaderStageCreateInfo;
                graphicsPipelineCreateInfo.pVertexInputState = &vertexInputStateCreateInfo;
                graphicsPipelineCreateInfo.pInputAssemblyState = &inputAssemblyStateCreateInfo;
                graphicsPipelineCreateInfo.pTessellationState = nullptr;
                graphicsPipelineCreateInfo.pViewportState = &viewportStateCreateInfo;
                graphicsPipelineCreateInfo.pRasterizationState = &rasterizationStateCreateInfo;
                graphicsPipelineCreateInfo.pMultisampleState = &multisampleStateCreateInfo;
                graphicsPipelineCreateInfo.pDepthStencilState = &depthStencilStateCreateInfo;
                graphicsPipelineCreateInfo.pColorBlendState = &colorBlendStateCreateInfo;
                graphicsPipelineCreateInfo.pDynamicState = &dynamicStateCreateInfo;
                graphicsPipelineCreateInfo.layout = pipelineLayout;
                graphicsPipelineCreateInfo.renderPass = renderPass;
                graphicsPipelineCreateInfo.subpass = 0; // index
                graphicsPipelineCreateInfo.basePipelineHandle = VK_NULL_HANDLE;
                graphicsPipelineCreateInfo.basePipelineIndex = -1;

                ::VkPipeline graphicsPipeline;
                if (::vkCreateGraphicsPipelines(ctxDevice, VK_NULL_HANDLE, 1, &graphicsPipelineCreateInfo, nullptr, &graphicsPipeline) != ::VK_SUCCESS)
                    DJO_VK_RENDERER_INIT_ERROR_RETURN("Failed to create graphics pipeline");

                _renderer->graphicsPipeline = graphicsPipeline;

                DJO_VK_RENDERER_INIT_LOG_STEP("Graphics pipeline initialized");
            }

            // Descriptor pool
            {
                ::VkDescriptorPoolSize uboDescriptorPoolSize{};
                uboDescriptorPoolSize.type = ::VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
                uboDescriptorPoolSize.descriptorCount = VulkanRenderer::c_concurrentFrameCount;

                ::VkDescriptorPoolSize samplerDescriptorPoolSize{};
                samplerDescriptorPoolSize.type = ::VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
                samplerDescriptorPoolSize.descriptorCount = VulkanRenderer::c_concurrentFrameCount;

                ::VkDescriptorPoolSize descriptorPoolSizes[]{ uboDescriptorPoolSize, samplerDescriptorPoolSize };
                ::VkDescriptorPoolCreateInfo descriptorPoolCreateInfo = DescriptorPoolCreateInfo();
                descriptorPoolCreateInfo.flags = 0; // VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT // can free individual descriptor set
                descriptorPoolCreateInfo.maxSets = VulkanRenderer::c_concurrentFrameCount;
                descriptorPoolCreateInfo.poolSizeCount = ArrayCount(descriptorPoolSizes);
                descriptorPoolCreateInfo.pPoolSizes = descriptorPoolSizes;

                VkDescriptorPool descriptorPool;
                if (::vkCreateDescriptorPool(ctxDevice, &descriptorPoolCreateInfo, nullptr, &descriptorPool) != ::VK_SUCCESS)
                    DJO_VK_RENDERER_INIT_ERROR_RETURN("Failed to create descriptor pool");

                _renderer->descriptorPool = descriptorPool;
            }

            // Swapchain frame buffers
            {
                DJO_VK_RENDERER_INIT_LOG_STEP("Swapchain frame buffers init...");

                if (!_Internal::CreateSwapchainFrameBuffers(_renderer))
                    return false;

                DJO_VK_RENDERER_INIT_LOG_STEP("Swapchain frame buffers initialized");
            }

            // Command pool
            {
                DJO_VK_RENDERER_INIT_LOG_STEP("Command pools init...");

                ::VkCommandPoolCreateInfo commandPoolCreateInfo = CommandPoolCreateInfo();

                ::VkCommandPool stagingCommandPool;
                commandPoolCreateInfo.flags = ::VK_COMMAND_POOL_CREATE_TRANSIENT_BIT; // for short lived command buffers
                commandPoolCreateInfo.queueFamilyIndex = ctx->graphicsQueueFamilyIndex;
                if (::vkCreateCommandPool(ctxDevice, &commandPoolCreateInfo, nullptr, &stagingCommandPool) != ::VK_SUCCESS)
                    DJO_VK_RENDERER_INIT_ERROR_RETURN("Failed to create command pool");

                _renderer->stagingCommandPool = stagingCommandPool;

                ::VkCommandPool drawCommandPool;
                commandPoolCreateInfo.flags = ::VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
                commandPoolCreateInfo.queueFamilyIndex = ctx->graphicsQueueFamilyIndex;
                if (::vkCreateCommandPool(ctxDevice, &commandPoolCreateInfo, nullptr, &drawCommandPool) != ::VK_SUCCESS)
                    DJO_VK_RENDERER_INIT_ERROR_RETURN("Failed to create command pool");

                _renderer->drawCommandPool = drawCommandPool;

                DJO_VK_RENDERER_INIT_LOG_STEP("Command pools initialized");
            }

            // tmp vertex buffer
            {
                AllocatedVkBuffer vertexBuffer{};
                if (InitBufferFromMemory(
                    ctxDevice, ctx->allocator,
                    s_cubeVertices, sizeof(s_cubeVertices),
                    ::VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
                    _renderer->stagingCommandPool, ctx->graphicsQueue,
                    &vertexBuffer) != ::VK_SUCCESS)
                {
                    DJO_VK_RENDERER_INIT_ERROR_RETURN("Failed to init vertex buffer");
                }

                _renderer->vertexBuffer = vertexBuffer;
            }
            //
            // tmp index buffer
            {
                AllocatedVkBuffer indexBuffer{};
                if (InitBufferFromMemory(
                    ctxDevice, ctx->allocator,
                    s_cubeIndices, sizeof(s_cubeIndices),
                    ::VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
                    _renderer->stagingCommandPool, ctx->graphicsQueue,
                    &indexBuffer) != ::VK_SUCCESS)
                {
                    DJO_VK_RENDERER_INIT_ERROR_RETURN("Failed to init index buffer");
                }

                _renderer->indexBuffer = indexBuffer;
            }
            //
            // tmp image
            {
                MappedFile imageFile = MapFileReadOnly("Assets/Textures/test01.png");
                if (imageFile.mapHandle == nullptr)
                    DJO_VK_RENDERER_INIT_ERROR_RETURN("Failed to load test01.png file");

                DJO_DEFER(&imageFile, file, UnmapFile(file));

                int w, h, c;
                stbi_uc* const imageContent = ::stbi_load_from_memory(
                    reinterpret_cast<const stbi_uc*>(imageFile.mappedBuffer.data),
                    static_cast<int>(imageFile.mappedBuffer.count),
                    &w, &h, &c, ::STBI_rgb_alpha);
                if (imageContent == nullptr)
                    DJO_VK_RENDERER_INIT_ERROR_RETURN("Failed to load test01.png content");

                DJO_DEFER(imageContent, content, ::stbi_image_free(content));

                AllocatedVkImage image{};
                ::VkImageView imageView;
                if (CreateAndPrepareImage2DForFragmentShaderRead(
                    ctx->device, ctx->allocator,
                    imageContent, w * h * c, ::VkExtent3D{ (u32)w, (u32)h, 1 },
                    ::VK_FORMAT_R8G8B8A8_SRGB,
                    _renderer->stagingCommandPool, ctx->graphicsQueue,
                    &image, &imageView) != ::VK_SUCCESS)
                {
                    DJO_VK_RENDERER_INIT_ERROR_RETURN("Failed to create test01.png image and image view for fragment shader usage");
                }

                _renderer->test01Image = image;
                _renderer->test01ImageView = imageView;

                ::VkSamplerCreateInfo samplerCreateInfo = SamplerCreateInfo();
                samplerCreateInfo.magFilter = ::VK_FILTER_LINEAR;
                samplerCreateInfo.minFilter = ::VK_FILTER_LINEAR;
                samplerCreateInfo.mipmapMode = ::VK_SAMPLER_MIPMAP_MODE_LINEAR;
                samplerCreateInfo.addressModeU = ::VK_SAMPLER_ADDRESS_MODE_REPEAT;
                samplerCreateInfo.addressModeV = ::VK_SAMPLER_ADDRESS_MODE_REPEAT;
                samplerCreateInfo.addressModeW = ::VK_SAMPLER_ADDRESS_MODE_REPEAT;
                samplerCreateInfo.mipLodBias = 0.f;
                samplerCreateInfo.anisotropyEnable = VK_FALSE; // check VkPhysicalDeviceFeatures.samplerAnisotropy, then activate VkPhysicalDeviceFeatures.samplerAnisotropy to use
                samplerCreateInfo.maxAnisotropy = 0.f; // use VkPhysicalDeviceProperties.limits.maxSamplerAnisotropy
                samplerCreateInfo.compareEnable = VK_FALSE;
                samplerCreateInfo.compareOp = ::VK_COMPARE_OP_ALWAYS;
                samplerCreateInfo.minLod = 0.f;
                samplerCreateInfo.maxLod = 0.f;
                samplerCreateInfo.borderColor = ::VK_BORDER_COLOR_INT_OPAQUE_BLACK;
                samplerCreateInfo.unnormalizedCoordinates = VK_FALSE;

                ::VkSampler sampler;
                if (::vkCreateSampler(ctxDevice, &samplerCreateInfo, nullptr, &sampler) != ::VK_SUCCESS)
                    DJO_VK_RENDERER_INIT_ERROR_RETURN("Failed to create test01.png texture sampler");

                _renderer->textureSampler = sampler;
            }

            // Per frame data
            {
                DJO_VK_RENDERER_INIT_LOG_STEP("Per frame data init...");

                ::VkCommandBufferAllocateInfo commandBufferAllocateInfo = CommandBufferAllocateInfo();
                commandBufferAllocateInfo.commandPool = _renderer->drawCommandPool;
                commandBufferAllocateInfo.level = ::VK_COMMAND_BUFFER_LEVEL_PRIMARY;
                commandBufferAllocateInfo.commandBufferCount = 1;

                ::VkSemaphoreCreateInfo semaphoreCreateInfo = SemaphoreCreateInfo();

                ::VkFenceCreateInfo fenceCreateInfo = FenceCreateInfo();
                fenceCreateInfo.flags = ::VK_FENCE_CREATE_SIGNALED_BIT;

                ::VkDescriptorSetLayout uboDescriptorSetLayouts[VulkanRenderer::c_concurrentFrameCount]{ 0 };
                for (u32 f = 0; f < VulkanRenderer::c_concurrentFrameCount; ++f)
                    uboDescriptorSetLayouts[f] = _renderer->descriptorSetLayout;

                ::VkDescriptorSet descriptorSets[VulkanRenderer::c_concurrentFrameCount]{ 0 };
                ::VkDescriptorSetAllocateInfo uboDescriptorSetAllocateInfo = DescriptorSetAllocateInfo();
                uboDescriptorSetAllocateInfo.descriptorPool = _renderer->descriptorPool;
                uboDescriptorSetAllocateInfo.descriptorSetCount = VulkanRenderer::c_concurrentFrameCount;
                uboDescriptorSetAllocateInfo.pSetLayouts = uboDescriptorSetLayouts;
                if (::vkAllocateDescriptorSets(ctxDevice, &uboDescriptorSetAllocateInfo, descriptorSets) != ::VK_SUCCESS)
                    DJO_VK_RENDERER_INIT_ERROR_RETURN("Failed to allocate UBO descriptor sets");

                for (u32 f = 0; f < VulkanRenderer::c_concurrentFrameCount; ++f)
                {
                    // Command buffer

                    ::VkCommandBuffer commandBuffer;
                    if (::vkAllocateCommandBuffers(ctxDevice, &commandBufferAllocateInfo, &commandBuffer) != ::VK_SUCCESS)
                        DJO_VK_RENDERER_INIT_ERROR_RETURN("Failed to allocate command buffer");

                    _renderer->perFrameData[f].commandBuffer = commandBuffer;

                    // Synchro structures

                    ::VkSemaphore presentSemaphore;
                    if (::vkCreateSemaphore(ctxDevice, &semaphoreCreateInfo, nullptr, &presentSemaphore) != ::VK_SUCCESS)
                        DJO_VK_RENDERER_INIT_ERROR_RETURN("Failed to create present semaphore");

                    _renderer->perFrameData[f].presentSemaphore = presentSemaphore;

                    ::VkSemaphore renderSemaphore;
                    if (::vkCreateSemaphore(ctxDevice, &semaphoreCreateInfo, nullptr, &renderSemaphore) != ::VK_SUCCESS)
                        DJO_VK_RENDERER_INIT_ERROR_RETURN("Failed to create render semaphore");

                    _renderer->perFrameData[f].renderSemaphore = renderSemaphore;

                    ::VkFence renderFence;
                    if (::vkCreateFence(ctxDevice, &fenceCreateInfo, nullptr, &renderFence) != ::VK_SUCCESS)
                        DJO_VK_RENDERER_INIT_ERROR_RETURN("Failed to create render fence");

                    _renderer->perFrameData[f].renderFence = renderFence;

                    // Uniform buffer

                    AllocatedVkBuffer uboBuffer{};
                    if (CreateAllocatedVkBuffer(
                        ctx->allocator, sizeof(UniformBufferObject),
                        ::VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                        ::VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | ::VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                        0,
                        &uboBuffer) != ::VK_SUCCESS)
                    {
                        DJO_VK_RENDERER_INIT_ERROR_RETURN("Failed to create staging index buffer");
                    }

                    _renderer->perFrameData[f].uboBuffer = uboBuffer;

                    void* mappedData;
                    if (::vmaMapMemory(ctx->allocator, uboBuffer.alloc, &mappedData) != ::VK_SUCCESS)
                        DJO_VK_RENDERER_INIT_ERROR_RETURN("Failed to map staging index buffer");

                    _renderer->perFrameData[f].uboBufferMappedPtr = mappedData;

                    // Descriptor set

                    ::VkDescriptorSet descriptorSet = descriptorSets[f];
                    ::VkDescriptorBufferInfo uboDescriptorBufferInfo{};
                    uboDescriptorBufferInfo.buffer = _renderer->perFrameData[f].uboBuffer.buffer;
                    uboDescriptorBufferInfo.offset = 0;
                    uboDescriptorBufferInfo.range = sizeof(UniformBufferObject);

                    ::VkDescriptorImageInfo descriptorImageInfo{};
                    descriptorImageInfo.sampler = _renderer->textureSampler;
                    descriptorImageInfo.imageView = _renderer->test01ImageView;
                    descriptorImageInfo.imageLayout = ::VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

                    ::VkWriteDescriptorSet descriptorSetWrites[2]{};
                    descriptorSetWrites[0] = DescriptorSetWriteInfo();
                    descriptorSetWrites[0].dstSet = descriptorSet;
                    descriptorSetWrites[0].dstBinding = 0;
                    descriptorSetWrites[0].dstArrayElement = 0;
                    descriptorSetWrites[0].descriptorCount = 1;
                    descriptorSetWrites[0].descriptorType = ::VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
                    descriptorSetWrites[0].pImageInfo = nullptr;
                    descriptorSetWrites[0].pBufferInfo = &uboDescriptorBufferInfo;
                    descriptorSetWrites[0].pTexelBufferView = nullptr;

                    descriptorSetWrites[1] = DescriptorSetWriteInfo();
                    descriptorSetWrites[1].dstSet = descriptorSet;
                    descriptorSetWrites[1].dstBinding = 1;
                    descriptorSetWrites[1].dstArrayElement = 0;
                    descriptorSetWrites[1].descriptorCount = 1;
                    descriptorSetWrites[1].descriptorType = ::VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
                    descriptorSetWrites[1].pImageInfo = &descriptorImageInfo;
                    descriptorSetWrites[1].pBufferInfo = nullptr;
                    descriptorSetWrites[1].pTexelBufferView = nullptr;

                    ::vkUpdateDescriptorSets(ctxDevice, ArrayCount(descriptorSetWrites), descriptorSetWrites, 0, nullptr);

                    _renderer->perFrameData[f].descriptorSet = descriptorSet;

                }

                DJO_VK_RENDERER_INIT_LOG_STEP("Per frame data initialized");
            }

        #undef DJO_VK_RENDERER_INIT_ERROR_RETURN
        #undef DJO_VK_RENDERER_INIT_LOG_STEP

            DJO_ASSERT(!ctx->hasError);
            return true;
        }

        void ShutdownRenderer(VulkanRenderer* const _renderer) noexcept
        {
            DJO_ASSERT(_renderer->ctx != nullptr);

            const ::VkDevice ctxDevice = _renderer->ctx->device;
            if (ctxDevice == VK_NULL_HANDLE)
                return;

            DJO_VK_RENDERER_LOG("Shutdown...");

            ::vkDeviceWaitIdle(ctxDevice);

            for (u32 f = 0; f < VulkanRenderer::c_concurrentFrameCount; ++f)
            {
                ::vmaUnmapMemory(_renderer->ctx->allocator, _renderer->perFrameData[f].uboBuffer.alloc);
                DestroyAllocatedVkBuffer(_renderer->ctx->allocator, &_renderer->perFrameData[f].uboBuffer);
                ::vkDestroyFence(ctxDevice, _renderer->perFrameData[f].renderFence, nullptr);
                ::vkDestroySemaphore(ctxDevice, _renderer->perFrameData[f].renderSemaphore, nullptr);
                ::vkDestroySemaphore(ctxDevice, _renderer->perFrameData[f].presentSemaphore, nullptr);
            }

            ::vkDestroyCommandPool(ctxDevice, _renderer->drawCommandPool, nullptr);
            ::vkDestroyCommandPool(ctxDevice, _renderer->stagingCommandPool, nullptr);

            ::vkDestroySampler(ctxDevice, _renderer->textureSampler, nullptr);
            ::vkDestroyImageView(ctxDevice, _renderer->test01ImageView, nullptr);
            DestroyAllocatedVkImage(_renderer->ctx->allocator, &_renderer->test01Image);
            DestroyAllocatedVkBuffer(_renderer->ctx->allocator, &_renderer->indexBuffer);
            DestroyAllocatedVkBuffer(_renderer->ctx->allocator, &_renderer->vertexBuffer);

            _Internal::DestroySwapchainFrameBuffers(_renderer);

            ::vkDestroyDescriptorPool(ctxDevice, _renderer->descriptorPool, nullptr);
            ::vkDestroyPipeline(ctxDevice, _renderer->graphicsPipeline, nullptr);
            ::vkDestroyRenderPass(ctxDevice, _renderer->mainRenderPass, nullptr);
            ::vkDestroyPipelineLayout(ctxDevice, _renderer->pipelineLayout, nullptr);
            ::vkDestroyDescriptorSetLayout(ctxDevice, _renderer->descriptorSetLayout, nullptr);

            DJO_VK_RENDERER_LOG("Shutdown");

            ZeroMem(_renderer);
        }

        bool BeginFrame(VulkanRenderer* const _renderer) noexcept
        {
            DJO_ASSERT(_renderer->ctx->device != VK_NULL_HANDLE);

            VulkanContext* const ctx = _renderer->ctx;
            const ::VkDevice ctxDevice = ctx->device;

            const u32 currentFrameIndex = _renderer->currentFrameIndex % VulkanRenderer::c_concurrentFrameCount;
            _renderer->currentFrameIndex = currentFrameIndex;

            VulkanRenderer::PerFrameData* const currentFrameData = &_renderer->perFrameData[_renderer->currentFrameIndex];

            // wait until the GPU has finished rendering the last frame
            ::vkWaitForFences(ctxDevice, 1, &currentFrameData->renderFence, true, UINT64_MAX);

            // request image from the swapchain
            u32 swapchainImageIndex;
            {
                ::VkResult acquireNextImageResult;
                u32 vkAcquireNextImageKHRCount = 0;
                do
                {
                    acquireNextImageResult = ::vkAcquireNextImageKHR(ctxDevice, ctx->swapchain, UINT64_MAX, currentFrameData->presentSemaphore, nullptr, &swapchainImageIndex);
                    ++vkAcquireNextImageKHRCount;
                    if (acquireNextImageResult != ::VK_SUCCESS) DJO_UNLIKELY
                    {
                        if (acquireNextImageResult == ::VK_ERROR_OUT_OF_DATE_KHR)
                        {
                            // probably due to a window resize
                            if (!_Internal::RecreateSwapchain(_renderer))
                            {
                                // ragequit, probably minimized window
                                DJO_ASSERT(false);
                                return false;
                            }
                        }
                        else if (acquireNextImageResult == ::VK_SUBOPTIMAL_KHR)
                        {
                            // not an issue
                            break;
                        }
                        else if (acquireNextImageResult == ::VK_ERROR_SURFACE_LOST_KHR)
                        {
                            // when does this happen?
                            DJO_ASSERT(false);
                            return false;
                        }
                        else
                        {
                            // untreated case
                            DJO_ASSERT(false);
                            return false;
                        }
                    }
                }
                while (acquireNextImageResult != ::VK_SUCCESS);

                if (vkAcquireNextImageKHRCount > 1)
                {
                    DJO_FORMAT_LF(&ctx->logger, "% vkAcquireNextImageKHR count: %",
                        Format::ELogLevel::Warning,
                        vkAcquireNextImageKHRCount);
                }

                if (swapchainImageIndex >= _renderer->swapChainFramebuffers.count) DJO_UNLIKELY
                {
                    DJO_ASSERT(false);
                    return false;
                }
            }

            _renderer->currentSwapchainImageIndex = swapchainImageIndex;

            // Only reset the fence if we are submitting work
            ::vkResetFences(ctxDevice, 1, &currentFrameData->renderFence);

            // now that we are sure that the commands finished executing, we can safely reset the command buffer to begin recording again
            ::vkResetCommandBuffer(currentFrameData->commandBuffer, 0);

            return !ctx->hasError;
        }

        void BeginMainRenderPass(VulkanRenderer* const _renderer) noexcept
        {
            DJO_ASSERT(_renderer->ctx->device != VK_NULL_HANDLE);

            VulkanRenderer::PerFrameData* const currentFrameData = &_renderer->perFrameData[_renderer->currentFrameIndex];
            const ::VkCommandBuffer cmd = currentFrameData->commandBuffer;

            // begin the command buffer recording
            // we will use this command buffer exactly once, so we want to let Vulkan know that
            ::VkCommandBufferBeginInfo commandBufferBeginInfo = CommandBufferBeginInfo();
            commandBufferBeginInfo.flags = ::VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
            commandBufferBeginInfo.pInheritanceInfo = nullptr;
            ::vkBeginCommandBuffer(cmd, &commandBufferBeginInfo);

            // start the main renderpass

            f32 blue = (f32)::sin(ConvertTime<Seconds>(OS::Time::Now()).t * 0.5) * 0.5f;
            ::VkClearValue clearValues[2]{};
            clearValues[0].color = { { 0.1f, 0.2f, 0.5f + blue, 1.f } };
            clearValues[1].depthStencil = { 1.f, 0 };

            ::VkRenderPassBeginInfo renderPassBeginInfo = RenderPassBeginInfo();
            renderPassBeginInfo.renderPass = _renderer->mainRenderPass;
            renderPassBeginInfo.framebuffer = _renderer->swapChainFramebuffers[_renderer->currentSwapchainImageIndex];
            renderPassBeginInfo.renderArea = { VkOffset2D{ 0, 0 }, _renderer->ctx->swapchainExtent };
            renderPassBeginInfo.clearValueCount = ArrayCount(clearValues);
            renderPassBeginInfo.pClearValues = clearValues;
            ::vkCmdBeginRenderPass(cmd, &renderPassBeginInfo, ::VK_SUBPASS_CONTENTS_INLINE);
        }

        void Draw(VulkanRenderer* const _renderer) noexcept
        {
            DJO_ASSERT(_renderer->ctx->device != VK_NULL_HANDLE);

            VulkanRenderer::PerFrameData* const currentFrameData = &_renderer->perFrameData[_renderer->currentFrameIndex];

            _Internal::DrawMainRenderPass(_renderer, currentFrameData);
        }

        void EndMainRenderPass(VulkanRenderer* const _renderer) noexcept
        {
            DJO_ASSERT(_renderer->ctx->device != VK_NULL_HANDLE);

            VulkanRenderer::PerFrameData* const currentFrameData = &_renderer->perFrameData[_renderer->currentFrameIndex];
            const ::VkCommandBuffer cmd = currentFrameData->commandBuffer;

            // finalize the render pass
            ::vkCmdEndRenderPass(cmd);

            // finalize the command buffer (we can no longer add commands, but it can now be executed)
            ::vkEndCommandBuffer(cmd);
        }

        bool PresentFrame(VulkanRenderer* const _renderer) noexcept
        {
            VulkanContext* const ctx = _renderer->ctx;
            VulkanRenderer::PerFrameData* const currentFrameData = &_renderer->perFrameData[_renderer->currentFrameIndex];
            const ::VkCommandBuffer cmd = currentFrameData->commandBuffer;

            // prepare the submission to the queue
            // we want to wait on the presentSemaphore, as that semaphore is signaled when the swapchain is ready
            // we will signal the renderSemaphore, to signal that rendering has finished
            const ::VkPipelineStageFlags waitStage = ::VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
            ::VkSubmitInfo submitInfo = SubmitInfo();
            submitInfo.waitSemaphoreCount = 1;
            submitInfo.pWaitSemaphores = &currentFrameData->presentSemaphore;
            submitInfo.pWaitDstStageMask = &waitStage;
            submitInfo.commandBufferCount = 1;
            submitInfo.pCommandBuffers = &cmd;
            submitInfo.signalSemaphoreCount = 1;
            submitInfo.pSignalSemaphores = &currentFrameData->renderSemaphore;
            ::vkQueueSubmit(_renderer->ctx->graphicsQueue, 1, &submitInfo, currentFrameData->renderFence);
            // see vkQueueSubmit2 since VK 1.3

            // this will put the image we just rendered into the visible window
            // we want to wait on the m_renderSemaphore for that,
            // as it's necessary that drawing commands have finished before the image is displayed to the user
            ::VkPresentInfoKHR presentInfo = PresentInfoKHR();
            presentInfo.pSwapchains = &_renderer->ctx->swapchain;
            presentInfo.swapchainCount = 1;
            presentInfo.pWaitSemaphores = &currentFrameData->renderSemaphore;
            presentInfo.waitSemaphoreCount = 1;
            presentInfo.pImageIndices = &_renderer->currentSwapchainImageIndex;
            presentInfo.pResults = nullptr;
            ::VkResult presentResult = ::vkQueuePresentKHR(_renderer->ctx->graphicsQueue, &presentInfo);
            if (presentResult != ::VK_SUCCESS)
            {
                DJO_FORMAT(&ctx->errorMessage, "% [Vulkan Renderer] vkQueuePresentKHR failed: % (%)",
                    Format::ELogLevel::Error,
                    VkResultToString(presentResult),
                    static_cast<int>(presentResult));
                ctx->hasError = true;
            }

            _renderer->currentFrameIndex = (_renderer->currentFrameIndex + 1) % VulkanRenderer::c_concurrentFrameCount;

            return !ctx->hasError;
        }

        namespace _Internal
        {
            void DrawMainRenderPass(VulkanRenderer* const _renderer, VulkanRenderer::PerFrameData* const _frameData) noexcept
            {
                const ::VkCommandBuffer cmd = _frameData->commandBuffer;

                static Seconds start = ConvertTime<Seconds>(OS::Time::Now());
                Seconds elapsed{ ConvertTime<Seconds>(OS::Time::Now()).t - start.t };

                UniformBufferObject ubo{};
                ubo.model = glm::rotate(Mat44f{ 1.f }, glm::radians((f32)elapsed.t), Vec3f{ 0.f, 1.f, 0.f });
                ubo.view = _renderer->camera.view;
                ubo.proj = glm::perspective(
                    _renderer->camera.fov,
                    static_cast<f32>(_renderer->ctx->swapchainExtent.width) / static_cast<f32>(_renderer->ctx->swapchainExtent.height),
                    0.01f, 1000.f);
                ubo.proj[1][1] *= -1.f; // TODO : find a way to setup glm to not have to do this?
                ::memcpy(_frameData->uboBufferMappedPtr, &ubo, sizeof(ubo));

                ::vkCmdBindPipeline(cmd, ::VK_PIPELINE_BIND_POINT_GRAPHICS, _renderer->graphicsPipeline);

                ::VkViewport viewport{};
                viewport.x = 0.0f;
                viewport.y = 0.0f;
                viewport.width = static_cast<f32>(_renderer->ctx->swapchainExtent.width);
                viewport.height = static_cast<f32>(_renderer->ctx->swapchainExtent.height);
                viewport.minDepth = 0.f;
                viewport.maxDepth = 1.f;
                ::vkCmdSetViewport(cmd, 0, 1, &viewport);

                ::VkRect2D scissor{};
                scissor.offset = { 0, 0 };
                scissor.extent = _renderer->ctx->swapchainExtent;
                ::vkCmdSetScissor(cmd, 0, 1, &scissor);

                const ::VkDeviceSize offsets[]{ 0 };
                const ::VkBuffer vertexBuffers[]{ _renderer->vertexBuffer.buffer };
                DJO_ASSERT(ArrayCount(vertexBuffers) == ArrayCount(offsets));
                ::vkCmdBindVertexBuffers(cmd, 0, ArrayCount(vertexBuffers), vertexBuffers, offsets);
                ::vkCmdBindIndexBuffer(cmd, _renderer->indexBuffer.buffer, 0, ::VK_INDEX_TYPE_UINT16);

                ::vkCmdBindDescriptorSets(cmd, ::VK_PIPELINE_BIND_POINT_GRAPHICS, _renderer->pipelineLayout, 0, 1, &_frameData->descriptorSet, 0, nullptr);

                ::vkCmdDrawIndexed(cmd, ArrayCount(s_cubeIndices), 1, 0, 0, 0);
            }

            bool RecreateSwapchain(VulkanRenderer* const _renderer) noexcept
            {
                DJO_VK_RENDERER_LOG("Swapchain recreation...");

                ::vkDeviceWaitIdle(_renderer->ctx->device);

                DestroySwapchainFrameBuffers(_renderer);
                DestroySwapchain(_renderer->ctx);

                return CreateSwapchain(_renderer->ctx)
                    && CreateSwapchainFrameBuffers(_renderer);
            }

            bool CreateSwapchainFrameBuffers(VulkanRenderer* const _renderer) noexcept
            {
                DJO_VK_RENDERER_LOG("Swapchain framebuffers creation...");

                const Memory::ArenaMark arenaMark = GetThreadLocalArenaMark();

                VulkanContext* const ctx = _renderer->ctx;
                const ::VkDevice ctxDevice = ctx->device;

                const sz swapchainImageCount = ctx->swapchainImageViews.count;
                Array<::VkFramebuffer> swapChainFramebuffers = (_renderer->swapChainFramebuffers.count == swapchainImageCount)
                    ? _renderer->swapChainFramebuffers
                    : PushArray<::VkFramebuffer>(arenaMark.arena, swapchainImageCount);

                for (sz i = 0; i < swapchainImageCount; ++i)
                {
                    const ::VkImageView attachments[]{ ctx->swapchainImageViews[i], ctx->depthImageView };

                    ::VkFramebufferCreateInfo framebufferCreateInfo = FramebufferCreateInfo();
                    framebufferCreateInfo.renderPass = _renderer->mainRenderPass;
                    framebufferCreateInfo.attachmentCount = ArrayCount(attachments);
                    framebufferCreateInfo.pAttachments = attachments;
                    framebufferCreateInfo.width = ctx->swapchainExtent.width;
                    framebufferCreateInfo.height = ctx->swapchainExtent.height;
                    framebufferCreateInfo.layers = 1;

                    ::VkFramebuffer frameBuffer;
                    if (::vkCreateFramebuffer(ctxDevice, &framebufferCreateInfo, nullptr, &frameBuffer) != ::VK_SUCCESS)
                        DJO_VK_RENDERER_ERROR_RETURN("Failed to create swapchain frame buffer");

                    swapChainFramebuffers[i] = frameBuffer;
                }

                _renderer->swapChainFramebuffers = swapChainFramebuffers;

                DJO_VK_RENDERER_LOG("Swapchain framebuffers created");

                DJO_ASSERT(!ctx->hasError);
                return true;
            }

            void DestroySwapchainFrameBuffers(VulkanRenderer* const _renderer) noexcept
            {
                DJO_VK_RENDERER_LOG("Swapchain framebuffers destruction...");

                for (sz i = 0; i < _renderer->swapChainFramebuffers.count; ++i)
                {
                    ::vkDestroyFramebuffer(_renderer->ctx->device, _renderer->swapChainFramebuffers[i], nullptr);
                    _renderer->swapChainFramebuffers[i] = VK_NULL_HANDLE;
                }

                DJO_VK_RENDERER_LOG("Swapchain framebuffers destroyed");
            }
        }

    #undef DJO_VK_RENDERER_LOG_STEP
    #undef DJO_VK_RENDERER_ERROR_RETURN
    }
}
