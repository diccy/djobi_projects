#pragma once

#include <DjobiCore/Memory/Arena.hpp>
#include <DjobiCore/Containers/String8OStream.hpp>

#include "VulkanUtils.hpp"


namespace Djo
{
    namespace Vk
    {
        struct VulkanContext
        {
            Memory::Arena* arena;
            String8 errorMessage;
            bool hasError;
            String8OStream logger;

            // Library
            void* libHandle;
            PFN_vkGetInstanceProcAddr vkGetInstanceProcAddr;

            // Instance
            Array<::VkExtensionProperties> extensionProperties;
            Array<::VkLayerProperties> layerProperties;
            Array<const char*> extensionNames;
            Array<const char*> layerNames;
            ::VkInstance instance;
        #if DJO_VK_ENABLE_VALIDATION_LAYERS
            ::PFN_vkCreateDebugUtilsMessengerEXT vkCreateDebugUtilsMessengerEXT;
            ::PFN_vkDestroyDebugUtilsMessengerEXT vkDestroyDebugUtilsMessengerEXT;
            ::VkDebugUtilsMessengerEXT debugMessenger;
        #endif

            // Surface
            void* windowHandle;
            ::VkSurfaceKHR surface;

            // Physical device
            ::VkPhysicalDevice physicalDevice;
            ::VkPhysicalDeviceProperties physicalDeviceProperties;
            ::VkPhysicalDeviceFeatures physicalDeviceFeatures;
            u32 graphicsQueueFamilyIndex;
            u32 presentQueueFamilyIndex;
            PFN_vkGetDeviceProcAddr vkGetDeviceProcAddr;

            // Logical device
            ::VkDevice device;
            ::VkQueue graphicsQueue;
            ::VkQueue presentQueue;

            // Allocator
            ::VmaAllocator allocator;

            // Swapchain
            ::VkSwapchainKHR swapchain;
            ::VkSurfaceFormatKHR swapchainFormat;
            ::VkExtent2D swapchainExtent;
            Array<::VkImage> swapchainImages;
            Array<::VkImageView> swapchainImageViews;
            ::VkFormat depthFormat;
            ::VkImageView depthImageView;
            AllocatedVkImage depthImage;
        };

        VulkanContext MoldContext() noexcept;
        bool InitContext(VulkanContext* _ctx, void* _windowHandle) noexcept;
        void ShutdownContext(VulkanContext* _ctx) noexcept;

        void SetError(VulkanContext* _ctx, const StringView& _message) noexcept;

        bool CreateSwapchain(VulkanContext* _ctx) noexcept;
        void DestroySwapchain(VulkanContext* _ctx) noexcept;
    }
}
