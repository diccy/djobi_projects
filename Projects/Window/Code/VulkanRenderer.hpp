#pragma once

#include "VulkanContext.hpp"
#include "VulkanUtils.hpp"
#include "CameraDesc.hpp"

// https://developer.nvidia.com/sites/default/files/akamai/gameworks/blog/Vulkan/vulkan_intro_management.png
// https://developer.nvidia.com/sites/default/files/akamai/gameworks/blog/munich/mschott_vulkan_multi_threading.pdf
// https://developer.nvidia.com/sites/default/files/akamai/gameworks/blog/Vulkan_Memory_Management/vulkan_memory_strategy.png

namespace Djo
{
    namespace Vk
    {
        struct VulkanRenderer
        {
            VulkanContext* ctx;

            ::VkRenderPass mainRenderPass;
            ::VkDescriptorSetLayout descriptorSetLayout;
            ::VkDescriptorPool descriptorPool;
            ::VkPipelineLayout pipelineLayout;
            ::VkPipeline graphicsPipeline;

            Array<::VkFramebuffer> swapChainFramebuffers;
            ::VkCommandPool stagingCommandPool;
            ::VkCommandPool drawCommandPool;

            AllocatedVkBuffer vertexBuffer;
            AllocatedVkBuffer indexBuffer;
            AllocatedVkImage test01Image;
            ::VkImageView test01ImageView;
            ::VkSampler textureSampler;

            struct PerFrameData
            {
                ::VkCommandBuffer commandBuffer;
                ::VkSemaphore presentSemaphore;
                ::VkSemaphore renderSemaphore;
                ::VkFence renderFence;
                AllocatedVkBuffer uboBuffer;
                void* uboBufferMappedPtr;
                ::VkDescriptorSet descriptorSet;
            };
            static constexpr u32 c_concurrentFrameCount{ 2 };
            PerFrameData perFrameData[c_concurrentFrameCount];
            u32 currentFrameIndex;
            u32 currentSwapchainImageIndex;

            CameraDesc camera;
        };

        VulkanRenderer MoldRenderer(VulkanContext* _ctx) noexcept;
        bool InitRenderer(VulkanRenderer* _renderer) noexcept;
        bool BeginFrame(VulkanRenderer* _renderer) noexcept;
        void BeginMainRenderPass(VulkanRenderer* _renderer) noexcept;
        void Draw(VulkanRenderer* _renderer) noexcept;
        void EndMainRenderPass(VulkanRenderer* _renderer) noexcept;
        bool PresentFrame(VulkanRenderer* _renderer) noexcept;
        void ShutdownRenderer(VulkanRenderer* _renderer) noexcept;
    }
}
