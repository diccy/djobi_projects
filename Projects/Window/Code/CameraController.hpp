#pragma once

#include "CameraDesc.hpp"
#include "InputState.hpp"

#include <DjobiCore/Core/Time.hpp>


namespace Djo
{
    struct CameraController
    {
        Mat44f cameraWorld;
        Vec3f targetWorldPos;
        Vec3f cameraGoalWorldPos;
        Vec3f targetGoalWorldPos;
        f32 moveSpeed;
    };

#if (GLM_COORDINATE_SYSTEM == GLM_RIGHT_HANDED)

    static constexpr Vec3f c_cameraRight{ 1.f, 0.f, 0.f };
    static constexpr Vec3f c_cameraUp{ 0.f, 1.f, 0.f };
    static constexpr Vec3f c_cameraForward{ 0.f, 0.f, -1.f };
    const Vec3f c_cameraUpClose{ glm::normalize(Vec3f{ 0.01f, 1.f, 0.f }) }; // glm::normalize not constexpr

#else
#error TODO
#endif

    DJO_INLINE Mat44f GetView(const CameraController& _cameraCtrl) noexcept
    {
        return glm::inverse(_cameraCtrl.cameraWorld);
    }

    DJO_INLINE void SetCameraWorldPos(CameraController* const _cameraCtrl, const Vec3f& _pos) noexcept
    {
        Mat44Pos(_cameraCtrl->cameraWorld) = _pos;
        _cameraCtrl->cameraGoalWorldPos = _pos;
    }
    DJO_INLINE void SetTargetWorldPos(CameraController* _cameraCtrl, const Vec3f& _pos) noexcept
    {
        _cameraCtrl->targetWorldPos = _pos;
        _cameraCtrl->targetGoalWorldPos = _pos;
    }

    void UpdateFreeCamera(CameraController* _cameraCtrl, const Input::State& _input, Milliseconds _dt) noexcept;
}
