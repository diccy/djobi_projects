#pragma once

#include <DjobiCore/Core/Types.hpp>


// see : https://learn.microsoft.com/en-us/windows/win32/inputdev/about-keyboard-input#scan-codes
//       https://learn.microsoft.com/en-us/windows/win32/inputdev/virtual-key-codes

namespace Djo
{
    namespace Input
    {
        struct KbScanCode
        {
            enum EValue : u8
            {
                None = 0,

                Escape                      = 0x01,
                k1                          = 0x02,
                k2                          = 0x03,
                k3                          = 0x04,
                k4                          = 0x05,
                k5                          = 0x06,
                k6                          = 0x07,
                k7                          = 0x08,
                k8                          = 0x09,
                k9                          = 0x0A,
                k0                          = 0x0B,
                Dash                        = 0x0C,
                Equals                      = 0x0D,
                Delete                      = 0x0E,
                Tab                         = 0x0F,
                Q                           = 0x10,
                W                           = 0x11,
                E                           = 0x12,
                R                           = 0x13,
                T                           = 0x14,
                Y                           = 0x15,
                U                           = 0x16,
                I                           = 0x17,
                O                           = 0x18,
                P                           = 0x19,
                LeftBrace                   = 0x1A,
                RightBrace                  = 0x1B,
                Enter                       = 0x1C,
                LeftControl                 = 0x1D,
                A                           = 0x1E,
                S                           = 0x1F,
                D                           = 0x20,
                F                           = 0x21,
                G                           = 0x22,
                H                           = 0x23,
                J                           = 0x24,
                K                           = 0x25,
                L                           = 0x26,
                SemiColon                   = 0x27,
                Apostrophe                  = 0x28,
                GraveAccent                 = 0x29,
                LeftShift                   = 0x2A,
                Pipe                        = 0x2B,
                Z                           = 0x2C,
                X                           = 0x2D,
                C                           = 0x2E,
                V                           = 0x2F,
                B                           = 0x30,
                N                           = 0x31,
                M                           = 0x32,
                Comma                       = 0x33,
                Period                      = 0x34,
                QuestionMark                = 0x35,
                RightShift                  = 0x36,
                Pad_Star                    = 0x37,
                LeftAlt                     = 0x38,
                Spacebar                    = 0x39,
                CapsLock                    = 0x3A,
                F1                          = 0x3B,
                F2                          = 0x3C,
                F3                          = 0x3D,
                F4                          = 0x3E,
                F5                          = 0x3F,
                F6                          = 0x40,
                F7                          = 0x41,
                F8                          = 0x42,
                F9                          = 0x43,
                F10                         = 0x44,
                Pad_NumLock                 = 0x45,
                ScrollLock                  = 0x46,
                Pad_7                       = 0x47,
                Pad_8                       = 0x48,
                Pad_9                       = 0x49,
                Pad_Dash                    = 0x4A,
                Pad_4                       = 0x4B,
                Pad_5                       = 0x4C,
                Pad_6                       = 0x4D,
                Pad_Plus                    = 0x4E,
                Pad_1                       = 0x4F,
                Pad_2                       = 0x50,
                Pad_3                       = 0x51,
                Pad_0                       = 0x52,
                Pad_Period                  = 0x53,
                PrintScreen                 = 0x54,
                //??                        = 0x55,
                SlashBar                    = 0x56,
                F11                         = 0x57,
                F12                         = 0x58,
                Pad_Equals                  = 0x59,
                //??                        = 0x5A,
                //??                        = 0x5B,
                International6              = 0x5C,
                //??                        = 0x5D,
                //??                        = 0x5E,
                //??                        = 0x5F,
                //??                        = 0x60,
                //??                        = 0x61,
                //??                        = 0x62,
                //??                        = 0x63,
                F13                         = 0x64,
                F14                         = 0x65,
                F15                         = 0x66,
                F16                         = 0x67,
                F17                         = 0x68,
                F18                         = 0x69,
                F19                         = 0x6A,
                F20                         = 0x6B,
                F21                         = 0x6C,
                F22                         = 0x6D,
                F23                         = 0x6E,
                International2              = 0x70,
                LANG2                       = 0x71,
                LANG1                       = 0x72,
                International1              = 0x73,
                //??                        = 0x74,
                //??                        = 0x75,
                F24                         = 0x76,
                LANG4                       = 0x77,
                LANG3                       = 0x78,
                International4              = 0x79,
                //??                        = 0x7A,
                International5              = 0x7B,
                //??                        = 0x7C,
                International3              = 0x7D,
                Pad_Comma                   = 0x7E,

                Count = 0xFF,

                LeftArrow   = Pad_4,
                UpArrow     = Pad_8,
                RightArrow  = Pad_6,
                DownArrow   = Pad_2,
            };
        };

        constexpr const char* KeyboardScanCodeToString(const KbScanCode::EValue _kbScanCode) noexcept
        {
        #define DJO_SCAN_CODE_STRING_CASE(enum_)     case enum_: return DJO_STR(enum_)

            switch (_kbScanCode)
            {
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::None);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::Escape);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::k1);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::k2);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::k3);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::k4);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::k5);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::k6);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::k7);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::k8);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::k9);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::k0);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::Dash);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::Equals);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::Delete);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::Tab);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::Q);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::W);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::E);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::R);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::T);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::Y);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::U);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::I);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::O);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::P);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::LeftBrace);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::RightBrace);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::Enter);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::LeftControl);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::A);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::S);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::D);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::F);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::G);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::H);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::J);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::K);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::L);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::SemiColon);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::Apostrophe);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::GraveAccent);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::LeftShift);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::Pipe);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::Z);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::X);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::C);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::V);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::B);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::N);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::M);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::Comma);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::Period);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::QuestionMark);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::RightShift);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::Pad_Star);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::LeftAlt);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::Spacebar);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::CapsLock);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::F1);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::F2);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::F3);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::F4);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::F5);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::F6);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::F7);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::F8);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::F9);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::F10);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::Pad_NumLock);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::ScrollLock);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::Pad_7);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::Pad_8);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::Pad_9);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::Pad_Dash);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::Pad_4);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::Pad_5);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::Pad_6);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::Pad_Plus);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::Pad_1);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::Pad_2);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::Pad_3);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::Pad_0);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::Pad_Period);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::PrintScreen);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::SlashBar);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::F11);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::F12);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::Pad_Equals);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::International6);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::F13);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::F14);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::F15);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::F16);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::F17);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::F18);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::F19);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::F20);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::F21);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::F22);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::F23);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::International2);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::LANG2);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::LANG1);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::International1);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::F24);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::LANG4);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::LANG3);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::International4);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::International5);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::International3);
                DJO_SCAN_CODE_STRING_CASE(KbScanCode::Pad_Comma);
                default: return "Unknown KbScanCode";
            }

        #undef DJO_SCAN_CODE_STRING_CASE
        }
    }
}
