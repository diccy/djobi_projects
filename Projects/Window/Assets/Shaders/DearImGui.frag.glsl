#version 460
#pragma shader_stage(fragment)

layout(binding = 0) uniform sampler2D fontSampler;

layout(location = 0) in vec2 inTexCoord;
layout(location = 1) in vec4 inColor;

layout(location = 0) out vec4 outColor;

void main() 
{
    outColor = inColor * texture(fontSampler, inTexCoord);
}
