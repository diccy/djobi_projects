#version 460
#pragma shader_stage(vertex)

layout(location = 0) in vec2 inPos;
layout(location = 1) in vec2 inTexCoord;
layout(location = 2) in vec4 inColor;

layout(push_constant) uniform PushConstants
{
    vec2 scale;
    vec2 translate;
}
pushConstants;

layout(location = 0) out vec2 outTexCoord;
layout(location = 1) out vec4 outColor;

void main()
{
    outTexCoord = inTexCoord;
    outColor = inColor;
    gl_Position = vec4(inPos * pushConstants.scale + pushConstants.translate, 0.0, 1.0);
}
