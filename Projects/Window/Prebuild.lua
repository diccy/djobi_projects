#!lua
require("ProjectCommon")

require("DjobiCore")
require("External/DearImGui")
require("External/cgltf")
require("External/stb")
require("External/Vulkan")


function Project_Window_SetupPCH(_project)

    ForEachSupportedPlatformAndConfig( function(_p, _c)
        pchheader (_project.name.."_pch.h")
        pchsource (_project.sourcesDir..CodeDir.._project.name.."_pch.cpp")
    end )
    filter {}
end

Project_Window = NewProjectCommon({
    name = "Window",
    dependencies = {
        Project_DjobiCore,
        Dependency_VulkanSDK, Dependency_Vma,
        Dependency_Stb,
        Dependency_DearImGui,
        Dependency_CGltf
    },
    SetupPCH = Project_Window_SetupPCH,
    DefineProject = CommonProjectDefinition_Exe,
})
