#!lua
require("ProjectCommon")

require("DjobiCore")
require("DjobiTests")


function Project_Tests_DjobiCore_SetupPCH(_project)

    ForEachSupportedPlatformAndConfig( function(_p, _c)
        pchheader (_project.name.."_pch.h")
        pchsource (_project.sourcesDir..CodeDir.._project.name.."_pch.cpp")
    end )
    filter {}
end

function Project_Tests_DjobiCore_SetupAdditional(_project)

    filter {}
    cppdialect  "C++20"
end

Project_Tests_DjobiCore = NewProjectCommon({
    name = "Tests_DjobiCore",
    dependencies = {Project_DjobiCore, Project_DjobiTests},
    SetupPCH = Project_Tests_DjobiCore_SetupPCH,
    SetupAdditional = Project_Tests_DjobiCore_SetupAdditional,
    DefineProject = CommonProjectDefinition_Exe,
})
