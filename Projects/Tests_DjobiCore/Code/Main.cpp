#include "Tests_DjobiCore_pch.h"

#include "Core/TestList.hpp"
#include "OS/TestList.hpp"
#include "Memory/TestList.hpp"
#include "Containers/TestList.hpp"
#include "Format/TestList.hpp"

#include <DjobiTests/Context.hpp>

#include <DjobiCore/Memory/Arena.hpp>
#include <DjobiCore/Format/Format.hpp>


int main()
{
    using namespace ::Djo;

    SetupSystemMemoryLeakWatcher();

    Memory::Arena* const ctxInternalArena = DJO_MEMORY_ARENA_ALLOCATE("Tests internal arena");
    Memory::Arena* const ctxUserArena = DJO_MEMORY_ARENA_ALLOCATE("Tests user arena");

    Tests::Context ctx = Tests::MoldContext(ctxInternalArena, ctxUserArena, Memory::MoldMallocAllocator());
    Tests::Init(&ctx);

    DJO_FORMAT_LF(&ctx.logger, "% -- DjobiCore tests start ----------------------------------------------------------------------------------",
        Format::ELogLevel::Info);

    {
        Tests::OpenScope(&ctx, __FILE__, "DjobiCore overall tests");
        const Tests::_Internal::ScopeInfo* const firstScope = ctx.currentScopeInfo;

        Tests::Tests(&ctx);
        Tests::Tests_OS(&ctx);
        Tests::Tests_Memory(&ctx);
        Tests::Tests_Containers(&ctx);
        Tests::Tests_Format(&ctx);

        DJO_ASSERT(ctx.currentScopeInfo == firstScope); DJO_UNUSED(firstScope);
        Tests::CloseScope(&ctx);
    }

    DJO_FORMAT_LF(&ctx.logger, "% -- DjobiCore tests summary --------------------------------------------------------------------------------",
        Format::ELogLevel::Info);

    Tests::LogSummary(&ctx);

    DJO_FORMAT_LF(&ctx.logger, "% -- DjobiCore tests end ------------------------------------------------------------------------------------",
        Format::ELogLevel::Info);

    Memory::FreeArena(ctxInternalArena);
    Memory::FreeArena(ctxUserArena);

    return 0;
}
