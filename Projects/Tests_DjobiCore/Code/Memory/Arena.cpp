#include "Tests_DjobiCore_pch.h"

#include "Utils.hpp"

#include <DjobiTests/Context.hpp>
#include <DjobiTests/Tests.hpp>

#include <DjobiCore/Memory/Arena.hpp>


namespace Djo
{
    namespace Tests
    {
        void Tests_Arena(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            const sz reserveSize = 100;
            const sz commitSize = 20;
            Memory::Arena* const _arena = DJO_MEMORY_ARENA_ALLOCATE_SIZES("test", reserveSize, commitSize);
            DJO_UTEST(_arena != nullptr);
            DJO_UTEST(_arena->configCommitSize >= commitSize);
            DJO_UTEST(_arena->commitedSize == _arena->configCommitSize);
            DJO_UTEST(_arena->reservedSize >= reserveSize);
            DJO_UTEST(_arena->reservedSize >= _arena->commitedSize);
            DJO_UTEST(_arena->state.localCursor > 0);
            DJO_UTEST(_arena->state.lastAllocStart <= _arena->state.localCursor);

            // TODO

            FreeArena(_arena);

            DJO_UTESTS_CLOSE_SCOPE;
        }
    }
}
