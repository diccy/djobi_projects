#pragma once

#include <DjobiTests/Context.hpp>
#include <DjobiTests/Tests.hpp>


namespace Djo
{
    namespace Tests
    {
        void Tests_Buffer(DJO_UTESTS_ARGS) noexcept;
        void Tests_Array(DJO_UTESTS_ARGS) noexcept;
        void Tests_LinkedList(DJO_UTESTS_ARGS) noexcept;
        void Tests_String8(DJO_UTESTS_ARGS) noexcept;
        void Tests_String8OStream(DJO_UTESTS_ARGS) noexcept;
        void Tests_PageArray(DJO_UTESTS_ARGS) noexcept;
        void Tests_ListArray(DJO_UTESTS_ARGS) noexcept;
        void Tests_SparseSet(DJO_UTESTS_ARGS) noexcept;

        DJO_INLINE void Tests_Containers(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            Tests_Buffer(DJO_UTESTS_PARAMS);
            Tests_Array(DJO_UTESTS_PARAMS);
            Tests_LinkedList(DJO_UTESTS_PARAMS);
            Tests_String8(DJO_UTESTS_PARAMS);
            Tests_String8OStream(DJO_UTESTS_PARAMS);
            Tests_PageArray(DJO_UTESTS_PARAMS);
            Tests_ListArray(DJO_UTESTS_PARAMS);
            Tests_SparseSet(DJO_UTESTS_PARAMS);

            DJO_UTESTS_CLOSE_SCOPE;
        }
    }
}
