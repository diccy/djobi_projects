#include "Tests_DjobiCore_pch.h"

#include "ContainersContracts.hpp"
#include "Utils.hpp"

#include <DjobiTests/Context.hpp>
#include <DjobiTests/Tests.hpp>
#include <DjobiTests/Utils.hpp>

#include <DjobiCore/Containers/Array.hpp>
#include <DjobiCore/Containers/ArrayAlloc.hpp>


namespace Djo
{
    namespace Tests
    {
        namespace
        {
            bool IsFromZeroToN(const Array<int>& _array)
            {
                const sz count = GetCount(_array);
                for (sz i = 0; i < count; ++i)
                    if (*GetAt(_array, i) != i)
                        return false;
                return true;
            }

            bool IsFromStartToN(const Array<int>& _array, const int _start)
            {
                const sz count = GetCount(_array);
                for (sz i = 0; i < count; ++i)
                    if (*GetAt(_array, i) != _start + i)
                        return false;
                return true;
            }
        }

        void Tests_Array_MemoryContiguous(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            DJO_STATIC_UTEST(MemoryContiguous<Array<int>>);

            {
                Array<int> nullArray = c_nullArray<int>;
                DJO_UTEST(GetCapacity(nullArray) == 0);
                DJO_UTEST(GetCount(nullArray) == 0);
                DJO_UTEST(IsEmpty(nullArray) == true);
                DJO_UTEST(GetData(nullArray) == nullptr);
                DJO_UTEST(GetSize(nullArray) == 0);
            }
            {
                constexpr sz arrayCount = 25;
                int a[arrayCount];
                Array<int> array = { { a, arrayCount }, arrayCount };
                DJO_UTEST(GetCapacity(array) == arrayCount);
                DJO_UTEST(GetCount(array) == arrayCount);
                DJO_UTEST(IsEmpty(array) == false);
                DJO_UTEST(GetData(array) == &a[0]);
                DJO_UTEST(GetSize(array) == (arrayCount * sizeof(int)));
                DJO_UTEST(GetEnd(array) == GetData(array) + arrayCount);
            }
            {
                constexpr sz arrayCount = 25;
                int a[arrayCount];
                FillIntBufferFromZeroToN(a, arrayCount);
                Array<int> array = { ToBuffer(a), arrayCount };

                DJO_UTEST(GetFirst(array) != nullptr);
                DJO_UTEST(*GetFirst(array) == 0);
                DJO_UTEST(GetLast(array) != nullptr);
                DJO_UTEST(*GetLast(array) == (arrayCount - 1));
                DJO_UTEST(IsFromZeroToN(array));

                {
                    bool isSameAsSourceArray = true;
                    sz i = 0;
                    for (int* p = GetData(array); p != GetEnd(array); ++p, ++i)
                        if (GetAt(array, i) != p)
                            isSameAsSourceArray = false;
                    DJO_UTEST(isSameAsSourceArray);
                }
            }

            DJO_UTESTS_CLOSE_SCOPE;
        }

        void Tests_Array_IndexVisitable(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            DJO_STATIC_UTEST(IndexVisitable<Array<int>>);

            {
                constexpr sz arrayCount = 25;
                int a[arrayCount];
                FillIntBufferFromZeroToN(a, arrayCount);
                Array<int> array = { ToBuffer(a), arrayCount };

                Visit(array, [DJO_UTESTS_LAMBDA_CAPTURE](const int _value, const sz _index)
                {
                    DJO_UTEST(_value == _index);
                    return EVisitorResult::Continue;
                });
            }
            {
                constexpr sz arrayCount = 25;
                int a[arrayCount];
                Array<int> array = { ToBuffer(a), arrayCount };

                constexpr sz stopAtIndex = 10;
                sz lastIndexVisited = 0;
                Visit(array, [&lastIndexVisited](const int, const sz _index)
                {
                    lastIndexVisited = _index;
                    return (_index == stopAtIndex) ? EVisitorResult::Break : EVisitorResult::Continue;
                });
                DJO_UTEST(lastIndexVisited == stopAtIndex);
            }

            DJO_UTESTS_CLOSE_SCOPE;
        }

        void Tests_Array_IndexAccessGrowable(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            DJO_STATIC_UTEST(IndexAccessGrowable<Array<int>>);

            {
                constexpr sz arrayCount = 25;
                int a[arrayCount];
                Array<int> array = { ToBuffer(a), 0 };

                DJO_UTEST(GetCount(array) == 0);
                DJO_UTEST(IsEmpty(array) == true);

                int value1 = 111;
                InsertBack(&array, value1);
                DJO_UTEST(GetCount(array) == 1);
                DJO_UTEST(IsEmpty(array) == false);
                DJO_UTEST(*GetAt(array, 0) == value1);

                int value2 = 222;
                InsertBack(&array, value2);
                DJO_UTEST(GetCount(array) == 2);
                DJO_UTEST(*GetAt(array, 0) == value1);
                DJO_UTEST(*GetAt(array, 1) == value2);
            }
            {
                constexpr sz chunkElemCount = 5;
                constexpr sz chunkCount = 3;
                constexpr sz arrayCount = chunkElemCount * chunkCount;
                int base[arrayCount];
                int added[arrayCount];
                ZeroMem(&base);
                FillIntBufferFromZeroToN(added, arrayCount);
                Array<int> array = { ToBuffer(base), 0 };

                DJO_UTEST(GetCount(array) == 0);
                DJO_UTEST(IsEmpty(array) == true);

                for (sz chunk = 0; chunk < chunkCount; ++chunk)
                {
                    InsertBack(&array, &added[chunk * chunkElemCount], chunkElemCount);
                    DJO_UTEST(GetCount(array) == (chunk + 1) * chunkElemCount);
                    DJO_UTEST(IsEmpty(array) == false);
                    for (sz i = 0; i < chunkElemCount; ++i)
                    {
                        const sz index = (chunk * chunkElemCount) + i;
                        DJO_UTEST(*GetAt(array, index) == index);
                    }
                }
            }
            {
                constexpr sz arrayCount = 25;
                int a[arrayCount];
                Array<int> array = { ToBuffer(a), 0 };

                for (sz i = 0; i < arrayCount; ++i)
                    InsertBack(&array, static_cast<int>(i));

                DJO_UTEST(IsFromZeroToN(array));
            }

            {
                constexpr sz arrayCount = 25;
                int a[arrayCount];
                Array<int> array = { ToBuffer(a), 0 };

                DJO_UTEST(GetCount(array) == 0);
                DJO_UTEST(IsEmpty(array) == true);

                int value1 = 111;
                InsertFront(&array, value1);
                DJO_UTEST(GetCount(array) == 1);
                DJO_UTEST(IsEmpty(array) == false);
                DJO_UTEST(*GetAt(array, 0) == value1);

                int value2 = 222;
                InsertFront(&array, value2);
                DJO_UTEST(GetCount(array) == 2);
                DJO_UTEST(*GetAt(array, 0) == value2);
                DJO_UTEST(*GetAt(array, 1) == value1);
            }
            {
                constexpr sz chunkElemCount = 5;
                constexpr sz chunkCount = 3;
                constexpr sz arrayCount = chunkElemCount * chunkCount;
                int base[arrayCount];
                int added[arrayCount];
                ZeroMem(&base);
                FillIntBufferFromZeroToN(added, arrayCount);
                Array<int> array = { ToBuffer(base), 0 };

                DJO_UTEST(GetCount(array) == 0);
                DJO_UTEST(IsEmpty(array) == true);

                for (sz chunk = 0; chunk < chunkCount; ++chunk)
                {
                    InsertFront(&array, &added[chunk * chunkElemCount], chunkElemCount);
                    DJO_UTEST(GetCount(array) == (chunk + 1) * chunkElemCount);
                    DJO_UTEST(IsEmpty(array) == false);
                    for (sz i = 0; i < chunkElemCount; ++i)
                    {
                        const sz index = (chunk * chunkElemCount) + i;
                        DJO_UTEST(*GetAt(array, i) == index);
                    }
                }
            }
            {
                constexpr sz arrayCount = 25;
                int a[arrayCount];
                Array<int> array = { ToBuffer(a), 0 };

                for (sz i = 0; i < arrayCount; ++i)
                    InsertFront(&array, static_cast<int>(i));

                bool isSameAsSourceArray = true;
                for (sz i = arrayCount, n = 0; i > 0; --i, ++n)
                    if (*GetAt(array, i - 1) != n)
                        isSameAsSourceArray = false;
                DJO_UTEST(isSameAsSourceArray);
            }

            {
                constexpr sz arrayCount = 25;
                int a[arrayCount];
                Array<int> array = { ToBuffer(a), 0 };

                DJO_UTEST(GetCount(array) == 0);
                DJO_UTEST(IsEmpty(array) == true);

                int value1 = 111;
                Insert(&array, 0, value1);
                DJO_UTEST(GetCount(array) == 1);
                DJO_UTEST(IsEmpty(array) == false);
                DJO_UTEST(*GetAt(array, 0) == value1);

                int value2 = 222;
                Insert(&array, 0, value2);
                DJO_UTEST(GetCount(array) == 2);
                DJO_UTEST(*GetAt(array, 0) == value2);
                DJO_UTEST(*GetAt(array, 1) == value1);

                int value3 = 333;
                Insert(&array, 1, value3);
                DJO_UTEST(GetCount(array) == 3);
                DJO_UTEST(*GetAt(array, 0) == value2);
                DJO_UTEST(*GetAt(array, 1) == value3);
                DJO_UTEST(*GetAt(array, 2) == value1);
            }
            {
                constexpr sz chunkElemCount = 5;
                constexpr sz arrayCount = chunkElemCount * 3;
                int base[arrayCount];
                int added[arrayCount];
                ZeroMem(&base);
                FillIntBufferFromZeroToN(added, arrayCount);
                Array<int> array = { ToBuffer(base), 0 };

                DJO_UTEST(GetCount(array) == 0);
                DJO_UTEST(IsEmpty(array) == true);

                Insert(&array, 0, &added[0], chunkElemCount);
                DJO_UTEST(GetCount(array) == chunkElemCount);
                DJO_UTEST(IsEmpty(array) == false);
                for (sz i = 0; i < chunkElemCount; ++i)
                    DJO_UTEST(*GetAt(array, i) == i);

                Insert(&array, 0, &added[chunkElemCount], chunkElemCount);
                DJO_UTEST(GetCount(array) == 2 * chunkElemCount);
                for (sz i = 0; i < chunkElemCount; ++i)
                    DJO_UTEST(*GetAt(array, i) == chunkElemCount + i);

                Insert(&array, chunkElemCount, &added[2 * chunkElemCount], chunkElemCount);
                DJO_UTEST(GetCount(array) == 3 * chunkElemCount);
                for (sz i = 0; i < chunkElemCount; ++i)
                    DJO_UTEST(*GetAt(array, chunkElemCount + i) == (2 * chunkElemCount) + i);
            }

            DJO_UTESTS_CLOSE_SCOPE;
        }

        void Tests_Array_IndexAccessShrinkable(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            DJO_STATIC_UTEST(IndexAccessShrinkable<Array<int>>);

            {
                constexpr sz arrayCount = 25;
                int a[arrayCount];
                FillIntBufferFromZeroToN(a, arrayCount);
                Array<int> array = { ToBuffer(a), arrayCount };

                DJO_UTEST(GetCapacity(array) == arrayCount);
                DJO_UTEST(GetCount(array) == arrayCount);
                DJO_UTEST(IsEmpty(array) == false);

                EraseBack(&array, 1);
                DJO_UTEST(GetCapacity(array) == arrayCount);
                DJO_UTEST(GetCount(array) == arrayCount - 1);
                DJO_UTEST(IsFromZeroToN(array));

                EraseBack(&array, 4);
                DJO_UTEST(GetCapacity(array) == arrayCount);
                DJO_UTEST(GetCount(array) == arrayCount - 5);
                DJO_UTEST(IsFromZeroToN(array));

                EraseBack(&array, 10);
                DJO_UTEST(GetCapacity(array) == arrayCount);
                DJO_UTEST(GetCount(array) == arrayCount - 15);
                DJO_UTEST(IsFromZeroToN(array));

                EraseBack(&array, GetCount(array));
                DJO_UTEST(GetCapacity(array) == arrayCount);
                DJO_UTEST(GetCount(array) == 0);
                DJO_UTEST(IsEmpty(array) == true);
            }

            {
                constexpr sz arrayCount = 25;
                int a[arrayCount];
                FillIntBufferFromZeroToN(a, arrayCount);
                Array<int> array = { ToBuffer(a), arrayCount };

                DJO_UTEST(GetCapacity(array) == arrayCount);
                DJO_UTEST(GetCount(array) == arrayCount);
                DJO_UTEST(IsEmpty(array) == false);

                EraseFront(&array, 1);
                DJO_UTEST(GetCapacity(array) == arrayCount);
                DJO_UTEST(GetCount(array) == arrayCount - 1);
                DJO_UTEST(IsFromStartToN(array, 1));

                EraseFront(&array, 4);
                DJO_UTEST(GetCapacity(array) == arrayCount);
                DJO_UTEST(GetCount(array) == arrayCount - 5);
                DJO_UTEST(IsFromStartToN(array, 5));

                EraseFront(&array, 10);
                DJO_UTEST(GetCapacity(array) == arrayCount);
                DJO_UTEST(GetCount(array) == arrayCount - 15);
                DJO_UTEST(IsFromStartToN(array, 15));

                EraseFront(&array, GetCount(array));
                DJO_UTEST(GetCapacity(array) == arrayCount);
                DJO_UTEST(GetCount(array) == 0);
                DJO_UTEST(IsEmpty(array) == true);
            }

            {
                constexpr sz arrayCount = 25;
                int a[arrayCount];
                FillIntBufferFromZeroToN(a, arrayCount);
                Array<int> array = { ToBuffer(a), arrayCount };

                DJO_UTEST(GetCapacity(array) == arrayCount);
                DJO_UTEST(GetCount(array) == arrayCount);
                DJO_UTEST(IsEmpty(array) == false);

                EraseAt(&array, 10, 1);
                DJO_UTEST(GetCapacity(array) == arrayCount);
                DJO_UTEST(GetCount(array) == arrayCount - 1);
                DJO_UTEST(*GetAt(array, 9) == 9);
                DJO_UTEST(*GetAt(array, 10) == 10 + 1);

                EraseAt(&array, 10, 4);
                DJO_UTEST(GetCapacity(array) == arrayCount);
                DJO_UTEST(GetCount(array) == arrayCount - 5);
                DJO_UTEST(*GetAt(array, 9) == 9);
                DJO_UTEST(*GetAt(array, 10) == 10 + 5);

                EraseAt(&array, 5, 10);
                DJO_UTEST(GetCapacity(array) == arrayCount);
                DJO_UTEST(GetCount(array) == arrayCount - 15);
                DJO_UTEST(*GetAt(array, 4) == 4);
                DJO_UTEST(*GetAt(array, 5) == 5 + 15);

                EraseAt(&array, 0, GetCount(array));
                DJO_UTEST(GetCapacity(array) == arrayCount);
                DJO_UTEST(GetCount(array) == 0);
                DJO_UTEST(IsEmpty(array) == true);
            }

            DJO_UTESTS_CLOSE_SCOPE;
        }

        void Tests_Array_IndexAccessGrowableArena(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            DJO_STATIC_UTEST(IndexAccessGrowableArena<Array<int>>);

            Memory::Arena* const arena = DJO_UTESTS_CTX->userArena;

            {
                Array<int> array = c_nullArray<int>;

                DJO_UTEST(GetCapacity(array) == 0);
                DJO_UTEST(GetCount(array) == 0);
                DJO_UTEST(IsEmpty(array) == true);

                int value1 = 111;
                InsertBack(&array, arena, value1);
                DJO_UTEST(GetCount(array) == 1);
                DJO_UTEST(GetCapacity(array) >= GetCount(array));
                DJO_UTEST(IsEmpty(array) == false);
                DJO_UTEST(*GetAt(array, 0) == value1);

                int value2 = 222;
                InsertBack(&array, arena, value2);
                DJO_UTEST(GetCount(array) == 2);
                DJO_UTEST(GetCapacity(array) >= GetCount(array));
                DJO_UTEST(*GetAt(array, 0) == value1);
                DJO_UTEST(*GetAt(array, 1) == value2);
            }
            {
                constexpr sz chunkElemCount = 5;
                constexpr sz chunkCount = 3;
                constexpr sz arrayCount = chunkElemCount * chunkCount;
                int added[arrayCount];
                FillIntBufferFromZeroToN(added, arrayCount);
                Array<int> array = c_nullArray<int>;

                for (sz chunk = 0; chunk < chunkCount; ++chunk)
                {
                    InsertBack(&array, arena, &added[chunk * chunkElemCount], chunkElemCount);
                    DJO_UTEST(GetCount(array) == (chunk + 1) * chunkElemCount);
                    DJO_UTEST(GetCapacity(array) >= GetCount(array));
                    DJO_UTEST(IsEmpty(array) == false);
                    for (sz i = 0; i < chunkElemCount; ++i)
                    {
                        const sz index = (chunk * chunkElemCount) + i;
                        DJO_UTEST(*GetAt(array, index) == index);
                    }
                }
            }
            {
                constexpr sz arrayCount = 25;
                Array<int> array = c_nullArray<int>;

                for (sz i = 0; i < arrayCount; ++i)
                    InsertBack(&array, arena, static_cast<int>(i));

                DJO_UTEST(GetCapacity(array) >= GetCount(array));
                bool isSameAsSourceArray = true;
                for (sz i = 0; i < arrayCount; ++i)
                    if (*GetAt(array, i) != i)
                        isSameAsSourceArray = false;
                DJO_UTEST(isSameAsSourceArray);
            }

            {
                Array<int> array = c_nullArray<int>;

                DJO_UTEST(GetCapacity(array) == 0);
                DJO_UTEST(GetCount(array) == 0);
                DJO_UTEST(IsEmpty(array) == true);

                int value1 = 111;
                InsertFront(&array, arena, value1);
                DJO_UTEST(GetCount(array) == 1);
                DJO_UTEST(GetCapacity(array) >= GetCount(array));
                DJO_UTEST(IsEmpty(array) == false);
                DJO_UTEST(*GetAt(array, 0) == value1);

                int value2 = 222;
                InsertFront(&array, arena, value2);
                DJO_UTEST(GetCount(array) == 2);
                DJO_UTEST(GetCapacity(array) >= GetCount(array));
                DJO_UTEST(*GetAt(array, 0) == value2);
                DJO_UTEST(*GetAt(array, 1) == value1);
            }
            {
                constexpr sz chunkElemCount = 5;
                constexpr sz chunkCount = 3;
                constexpr sz arrayCount = chunkElemCount * chunkCount;
                int added[arrayCount];
                FillIntBufferFromZeroToN(added, arrayCount);
                Array<int> array = c_nullArray<int>;

                for (sz chunk = 0; chunk < chunkCount; ++chunk)
                {
                    InsertFront(&array, arena, &added[chunk * chunkElemCount], chunkElemCount);
                    DJO_UTEST(GetCount(array) == (chunk + 1) * chunkElemCount);
                    DJO_UTEST(IsEmpty(array) == false);
                    for (sz i = 0; i < chunkElemCount; ++i)
                    {
                        const sz index = (chunk * chunkElemCount) + i;
                        DJO_UTEST(*GetAt(array, i) == index);
                    }
                }
            }
            {
                constexpr sz arrayCount = 25;
                Array<int> array = c_nullArray<int>;

                for (sz i = 0; i < arrayCount; ++i)
                    InsertFront(&array, arena, static_cast<int>(i));

                DJO_UTEST(GetCapacity(array) >= GetCount(array));
                bool isSameAsSourceArray = true;
                for (sz i = arrayCount, n = 0; i > 0; --i, ++n)
                    if (*GetAt(array, i - 1) != n)
                        isSameAsSourceArray = false;
                DJO_UTEST(isSameAsSourceArray);
            }

            {
                Array<int> array = c_nullArray<int>;

                DJO_UTEST(GetCapacity(array) == 0);
                DJO_UTEST(GetCount(array) == 0);
                DJO_UTEST(IsEmpty(array) == true);

                int value1 = 111;
                Insert(&array, arena, 0, value1);
                DJO_UTEST(GetCount(array) == 1);
                DJO_UTEST(GetCapacity(array) >= GetCount(array));
                DJO_UTEST(IsEmpty(array) == false);
                DJO_UTEST(*GetAt(array, 0) == value1);

                int value2 = 222;
                Insert(&array, arena, 0, value2);
                DJO_UTEST(GetCount(array) == 2);
                DJO_UTEST(GetCapacity(array) >= GetCount(array));
                DJO_UTEST(*GetAt(array, 0) == value2);
                DJO_UTEST(*GetAt(array, 1) == value1);

                int value3 = 333;
                Insert(&array, arena, 1, value3);
                DJO_UTEST(GetCount(array) == 3);
                DJO_UTEST(GetCapacity(array) >= GetCount(array));
                DJO_UTEST(*GetAt(array, 0) == value2);
                DJO_UTEST(*GetAt(array, 1) == value3);
                DJO_UTEST(*GetAt(array, 2) == value1);
            }
            {
                constexpr sz chunkElemCount = 5;
                constexpr sz chunkCount = 3;
                constexpr sz arrayCount = chunkElemCount * chunkCount;
                int added[arrayCount];
                FillIntBufferFromZeroToN(added, arrayCount);
                Array<int> array = c_nullArray<int>;

                Insert(&array, arena, 0, &added[0], chunkElemCount);
                DJO_UTEST(GetCount(array) == chunkElemCount);
                DJO_UTEST(IsEmpty(array) == false);
                for (sz i = 0; i < chunkElemCount; ++i)
                    DJO_UTEST(*GetAt(array, i) == i);

                Insert(&array, arena, 0, &added[chunkElemCount], chunkElemCount);
                DJO_UTEST(GetCount(array) == 2 * chunkElemCount);
                for (sz i = 0; i < chunkElemCount; ++i)
                    DJO_UTEST(*GetAt(array, i) == chunkElemCount + i);

                Insert(&array, arena, chunkElemCount, &added[2 * chunkElemCount], chunkElemCount);
                DJO_UTEST(GetCount(array) == 3 * chunkElemCount);
                for (sz i = 0; i < chunkElemCount; ++i)
                    DJO_UTEST(*GetAt(array, chunkElemCount + i) == (2 * chunkElemCount) + i);
            }

            DJO_UTESTS_CLOSE_SCOPE;
        }

        void Tests_Array_IndexAccessGrowableAlloc(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            DJO_STATIC_UTEST(IndexAccessGrowableAlloc<Array<int>>);

            const Memory::Allocator& allocator = DJO_UTESTS_CTX->userAllocator;

            {
                Array<int> array = c_nullArray<int>;

                DJO_UTEST(GetCapacity(array) == 0);
                DJO_UTEST(GetCount(array) == 0);
                DJO_UTEST(IsEmpty(array) == true);

                int value1 = 111;
                InsertBack(&array, allocator, value1);
                DJO_UTEST(GetCount(array) == 1);
                DJO_UTEST(GetCapacity(array) >= GetCount(array));
                DJO_UTEST(IsEmpty(array) == false);
                DJO_UTEST(*GetAt(array, 0) == value1);

                int value2 = 222;
                InsertBack(&array, allocator, value2);
                DJO_UTEST(GetCount(array) == 2);
                DJO_UTEST(GetCapacity(array) >= GetCount(array));
                DJO_UTEST(*GetAt(array, 0) == value1);
                DJO_UTEST(*GetAt(array, 1) == value2);

                FreeArray(&array, allocator);
            }
            {
                constexpr sz chunkElemCount = 5;
                constexpr sz chunkCount = 3;
                constexpr sz arrayCount = chunkElemCount * chunkCount;
                int added[arrayCount];
                FillIntBufferFromZeroToN(added, arrayCount);
                Array<int> array = c_nullArray<int>;

                for (sz chunk = 0; chunk < chunkCount; ++chunk)
                {
                    InsertBack(&array, allocator, &added[chunk * chunkElemCount], chunkElemCount);
                    DJO_UTEST(GetCount(array) == (chunk + 1) * chunkElemCount);
                    DJO_UTEST(GetCapacity(array) >= GetCount(array));
                    DJO_UTEST(IsEmpty(array) == false);
                    for (sz i = 0; i < chunkElemCount; ++i)
                    {
                        const sz index = (chunk * chunkElemCount) + i;
                        DJO_UTEST(*GetAt(array, index) == index);
                    }
                }

                FreeArray(&array, allocator);
            }
            {
                constexpr sz arrayCount = 25;
                Array<int> array = c_nullArray<int>;

                for (sz i = 0; i < arrayCount; ++i)
                    InsertBack(&array, allocator, static_cast<int>(i));

                DJO_UTEST(GetCapacity(array) >= GetCount(array));
                for (sz i = 0; i < arrayCount; ++i)
                    DJO_UTEST(*GetAt(array, i) == i);

                FreeArray(&array, allocator);
            }

            {
                Array<int> array = c_nullArray<int>;

                DJO_UTEST(GetCapacity(array) == 0);
                DJO_UTEST(GetCount(array) == 0);
                DJO_UTEST(IsEmpty(array) == true);

                int value1 = 111;
                InsertFront(&array, allocator, value1);
                DJO_UTEST(GetCount(array) == 1);
                DJO_UTEST(GetCapacity(array) >= GetCount(array));
                DJO_UTEST(IsEmpty(array) == false);
                DJO_UTEST(*GetAt(array, 0) == value1);

                int value2 = 222;
                InsertFront(&array, allocator, value2);
                DJO_UTEST(GetCount(array) == 2);
                DJO_UTEST(GetCapacity(array) >= GetCount(array));
                DJO_UTEST(*GetAt(array, 0) == value2);
                DJO_UTEST(*GetAt(array, 1) == value1);

                FreeArray(&array, allocator);
            }
            {
                constexpr sz chunkElemCount = 5;
                constexpr sz chunkCount = 3;
                constexpr sz arrayCount = chunkElemCount * chunkCount;
                int added[arrayCount];
                FillIntBufferFromZeroToN(added, arrayCount);
                Array<int> array = c_nullArray<int>;

                for (sz chunk = 0; chunk < chunkCount; ++chunk)
                {
                    InsertFront(&array, allocator, &added[chunk * chunkElemCount], chunkElemCount);
                    DJO_UTEST(GetCount(array) == (chunk + 1) * chunkElemCount);
                    DJO_UTEST(IsEmpty(array) == false);
                    for (sz i = 0; i < chunkElemCount; ++i)
                    {
                        const sz index = (chunk * chunkElemCount) + i;
                        DJO_UTEST(*GetAt(array, i) == index);
                    }
                }

                FreeArray(&array, allocator);
            }
            {
                constexpr sz arrayCount = 25;
                Array<int> array = c_nullArray<int>;

                for (sz i = 0; i < arrayCount; ++i)
                    InsertFront(&array, allocator, static_cast<int>(i));

                DJO_UTEST(GetCapacity(array) >= GetCount(array));
                for (sz i = arrayCount, n = 0; i > 0; --i, ++n)
                    DJO_UTEST(*GetAt(array, i - 1) == n);

                FreeArray(&array, allocator);
            }

            {
                Array<int> array = c_nullArray<int>;

                DJO_UTEST(GetCapacity(array) == 0);
                DJO_UTEST(GetCount(array) == 0);
                DJO_UTEST(IsEmpty(array) == true);

                int value1 = 111;
                Insert(&array, allocator, 0, value1);
                DJO_UTEST(GetCount(array) == 1);
                DJO_UTEST(GetCapacity(array) >= GetCount(array));
                DJO_UTEST(IsEmpty(array) == false);
                DJO_UTEST(*GetAt(array, 0) == value1);

                int value2 = 222;
                Insert(&array, allocator, 0, value2);
                DJO_UTEST(GetCount(array) == 2);
                DJO_UTEST(GetCapacity(array) >= GetCount(array));
                DJO_UTEST(*GetAt(array, 0) == value2);
                DJO_UTEST(*GetAt(array, 1) == value1);

                int value3 = 333;
                Insert(&array, allocator, 1, value3);
                DJO_UTEST(GetCount(array) == 3);
                DJO_UTEST(GetCapacity(array) >= GetCount(array));
                DJO_UTEST(*GetAt(array, 0) == value2);
                DJO_UTEST(*GetAt(array, 1) == value3);
                DJO_UTEST(*GetAt(array, 2) == value1);

                FreeArray(&array, allocator);
            }
            {
                constexpr sz chunkElemCount = 5;
                constexpr sz chunkCount = 3;
                constexpr sz arrayCount = chunkElemCount * chunkCount;
                int added[arrayCount];
                FillIntBufferFromZeroToN(added, arrayCount);
                Array<int> array = c_nullArray<int>;

                Insert(&array, allocator, 0, &added[0], chunkElemCount);
                DJO_UTEST(GetCount(array) == chunkElemCount);
                DJO_UTEST(IsEmpty(array) == false);
                for (sz i = 0; i < chunkElemCount; ++i)
                    DJO_UTEST(*GetAt(array, i) == i);

                Insert(&array, allocator, 0, &added[chunkElemCount], chunkElemCount);
                DJO_UTEST(GetCount(array) == 2 * chunkElemCount);
                for (sz i = 0; i < chunkElemCount; ++i)
                    DJO_UTEST(*GetAt(array, i) == chunkElemCount + i);

                Insert(&array, allocator, chunkElemCount, &added[2 * chunkElemCount], chunkElemCount);
                DJO_UTEST(GetCount(array) == 3 * chunkElemCount);
                for (sz i = 0; i < chunkElemCount; ++i)
                    DJO_UTEST(*GetAt(array, chunkElemCount + i) == (2 * chunkElemCount) + i);

                FreeArray(&array, allocator);
            }

            DJO_UTESTS_CLOSE_SCOPE;
        }

        void Tests_Array(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            Tests_Array_MemoryContiguous(DJO_UTESTS_PARAMS);
            Tests_Array_IndexVisitable(DJO_UTESTS_PARAMS);
            Tests_Array_IndexAccessGrowable(DJO_UTESTS_PARAMS);
            Tests_Array_IndexAccessShrinkable(DJO_UTESTS_PARAMS);
            Tests_Array_IndexAccessGrowableArena(DJO_UTESTS_PARAMS);
            Tests_Array_IndexAccessGrowableAlloc(DJO_UTESTS_PARAMS);

            DJO_UTESTS_CLOSE_SCOPE;
        }
    }
}
