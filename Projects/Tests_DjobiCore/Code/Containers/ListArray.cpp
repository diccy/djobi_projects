#include "Tests_DjobiCore_pch.h"

#include "ContainersContracts.hpp"
#include "Utils.hpp"

#include <DjobiTests/Context.hpp>
#include <DjobiTests/Tests.hpp>
#include <DjobiTests/Utils.hpp>

#include <DjobiCore/Containers/ListArray.hpp>
#include <DjobiCore/Containers/ListArrayAlloc.hpp>


namespace Djo
{
    namespace Tests
    {
        namespace
        {
            bool IsFromZeroToN(const Array<int>& _array)
            {
                const sz count = GetCount(_array);
                for (sz i = 0; i < count; ++i)
                    if (*GetAt(_array, i) != i)
                        return false;
                return true;
            }
        }

        void Tests_ListArray_Accountable(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            DJO_STATIC_UTEST(Accountable<ListArray<int>>);

            {
                ListArray<int> nullListArray = c_nullListArray<int>;
                DJO_UTEST(GetCapacity(nullListArray) == 0);
                DJO_UTEST(GetCount(nullListArray) == 0);
                DJO_UTEST(IsEmpty(nullListArray) == true);
            }
            {
                constexpr sz halfArrayCount = 15;
                constexpr sz arrayCount = halfArrayCount * 2;
                int a[arrayCount];
                ListArray<int> la = c_nullListArray<int>;
                ListArray<int>::NodeType n1 = { nullptr, c_nullArray<int> };
                ListArray<int>::NodeType n2 = { nullptr, c_nullArray<int> };
                SLList::InsertBack(&la, &n1);
                SLList::InsertBack(&la, &n2);
                DJO_UTEST(la.head == &n1);
                DJO_UTEST(la.tail == &n2);
                DJO_UTEST(n1.next == &n2);
                DJO_UTEST(n2.next == nullptr);

                n1.array = c_nullArray<int>;
                n2.array = c_nullArray<int>;
                DJO_UTEST(GetCapacity(la) == 0);
                DJO_UTEST(GetCount(la) == 0);
                DJO_UTEST(IsEmpty(la) == true);

                n1.array = { ToBuffer(a), arrayCount };
                n2.array = c_nullArray<int>;
                DJO_UTEST(GetCapacity(la) == arrayCount);
                DJO_UTEST(GetCount(la) == arrayCount);
                DJO_UTEST(IsEmpty(la) == false);

                n1.array = c_nullArray<int>;
                n2.array = { ToBuffer(a), arrayCount };
                DJO_UTEST(GetCapacity(la) == arrayCount);
                DJO_UTEST(GetCount(la) == arrayCount);
                DJO_UTEST(IsEmpty(la) == false);

                n1.array = { { &a[0],              halfArrayCount }, halfArrayCount };
                n2.array = { { &a[halfArrayCount], halfArrayCount }, halfArrayCount };
                DJO_UTEST(GetCapacity(la) == arrayCount);
                DJO_UTEST(GetCount(la) == arrayCount);
                DJO_UTEST(IsEmpty(la) == false);

                n1.array = { { &a[0],              halfArrayCount }, 1 };
                n2.array = { { &a[halfArrayCount], halfArrayCount }, 2 };
                DJO_UTEST(GetCapacity(la) == arrayCount);
                DJO_UTEST(GetCount(la) == 3);
                DJO_UTEST(IsEmpty(la) == false);

                n1.array = { { &a[0],              halfArrayCount }, 0 };
                n2.array = { { &a[halfArrayCount], halfArrayCount }, 0 };
                DJO_UTEST(GetCapacity(la) == arrayCount);
                DJO_UTEST(GetCount(la) == 0);
                DJO_UTEST(IsEmpty(la) == true);
            }

            DJO_UTESTS_CLOSE_SCOPE;
        }

        void Tests_ListArray_BackGrowableArena(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            DJO_STATIC_UTEST(BackGrowableArena<ListArray<int>>);

            Memory::Arena* const arena = DJO_UTESTS_CTX->userArena;

            {
                ListArray<int> la = c_nullListArray<int>;

                DJO_UTEST(GetCapacity(la) == 0);
                DJO_UTEST(GetCount(la) == 0);
                DJO_UTEST(IsEmpty(la) == true);

                int value1 = 111;
                InsertBack(&la, arena, value1);
                DJO_UTEST(GetCount(la) == 1);
                DJO_UTEST(GetCapacity(la) >= GetCount(la));
                DJO_UTEST(IsEmpty(la) == false);

                int value2 = 222;
                InsertBack(&la, arena, value2);
                DJO_UTEST(GetCount(la) == 2);
                DJO_UTEST(GetCapacity(la) >= GetCount(la));

                Array<int> mergedArray = PushMergedArray(la, arena);
                DJO_UTEST(GetCount(mergedArray) == GetCount(la));
                DJO_UTEST(IsEmpty(mergedArray) == false);
                DJO_UTEST(*GetAt(mergedArray, 0) == value1);
                DJO_UTEST(*GetAt(mergedArray, 1) == value2);
            }
            {
                constexpr sz arrayCount = 100;
                int a[arrayCount];
                FillIntBufferFromZeroToN(a, arrayCount);
                ListArray<int> la = c_nullListArray<int>;

                InsertBack(&la, arena, a, arrayCount);
                DJO_UTEST(GetCapacity(la) >= arrayCount);
                DJO_UTEST(GetCount(la) == arrayCount);

                Array<int> mergedArray = PushMergedArray(la, arena);
                DJO_UTEST(GetCount(mergedArray) == GetCount(la));
                DJO_UTEST(IsEmpty(mergedArray) == false);
                DJO_UTEST(IsFromZeroToN(mergedArray));
            }
            {
                constexpr sz chunkElemCount = 50;
                constexpr sz chunkCount = 3;
                constexpr sz arrayCount = chunkElemCount * chunkCount;
                int a[arrayCount];
                FillIntBufferFromZeroToN(a, arrayCount);
                ListArray<int> la = c_nullListArray<int>;

                for (sz chunk = 0; chunk < chunkCount; ++chunk)
                {
                    InsertBack(&la, arena, &a[chunk * chunkElemCount], chunkElemCount);
                    DJO_UTEST(GetCount(la) == (chunk + 1) * chunkElemCount);
                    DJO_UTEST(GetCapacity(la) >= GetCount(la));
                    DJO_UTEST(IsEmpty(la) == false);
                }

                Array<int> mergedArray = PushMergedArray(la, arena);
                DJO_UTEST(GetCount(mergedArray) == GetCount(la));
                DJO_UTEST(IsEmpty(mergedArray) == false);
                DJO_UTEST(IsFromZeroToN(mergedArray));
            }
            {
                constexpr sz arrayCount = 50;
                ListArray<int> la = c_nullListArray<int>;

                for (sz i = 0; i < arrayCount; ++i)
                    InsertBack(&la, arena, static_cast<int>(i));

                Array<int> mergedArray = PushMergedArray(la, arena);
                DJO_UTEST(GetCount(mergedArray) == GetCount(la));
                DJO_UTEST(IsEmpty(mergedArray) == false);
                DJO_UTEST(IsFromZeroToN(mergedArray));
            }

            DJO_UTESTS_CLOSE_SCOPE;
        }

        void Tests_ListArray(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            Tests_ListArray_Accountable(DJO_UTESTS_PARAMS);
            Tests_ListArray_BackGrowableArena(DJO_UTESTS_PARAMS);

            DJO_UTESTS_CLOSE_SCOPE;
        }
    }
}
