#include "Tests_DjobiCore_pch.h"

#include "ContainersContracts.hpp"
#include "Utils.hpp"

#include <DjobiTests/Context.hpp>
#include <DjobiTests/Tests.hpp>
#include <DjobiTests/Utils.hpp>

#include <DjobiCore/Core/Buffer.hpp>


namespace Djo
{
    namespace Tests
    {
        void Tests_Buffer_MemoryContiguous(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            DJO_STATIC_UTEST(MemoryContiguous<Buffer<int>>);

            {
                Buffer<int> nullBuffer = c_nullBuffer<int>;
                DJO_UTEST(GetCapacity(nullBuffer) == 0);
                DJO_UTEST(GetCount(nullBuffer) == 0);
                DJO_UTEST(IsEmpty(nullBuffer) == true);
                DJO_UTEST(GetData(nullBuffer) == nullptr);
                DJO_UTEST(GetSize(nullBuffer) == 0);
            }
            {
                constexpr sz arrayCount = 25;
                int a[arrayCount];
                Buffer<int> buffer = { a, arrayCount };
                DJO_UTEST(GetCapacity(buffer) == arrayCount);
                DJO_UTEST(GetCount(buffer) == arrayCount);
                DJO_UTEST(IsEmpty(buffer) == false);
                DJO_UTEST(GetData(buffer) == &a[0]);
                DJO_UTEST(GetSize(buffer) == (arrayCount * sizeof(int)));
                DJO_UTEST(GetEnd(buffer) == GetData(buffer) + arrayCount);
            }
            {
                constexpr sz arrayCount = 25;
                int a[arrayCount];
                FillIntBufferFromZeroToN(a, arrayCount);
                Buffer<int> buffer = ToBuffer(a);

                DJO_UTEST(GetFirst(buffer) != nullptr);
                DJO_UTEST(*GetFirst(buffer) == 0);
                DJO_UTEST(GetLast(buffer) != nullptr);
                DJO_UTEST(*GetLast(buffer) == (arrayCount - 1));

                for (sz i = 0; i < arrayCount; ++i)
                    DJO_UTEST(*GetAt(buffer, i) == i);

                {
                    sz i = 0;
                    for (int* p = GetData(buffer); p != GetEnd(buffer); ++p, ++i)
                        DJO_UTEST(GetAt(buffer, i) == p);
                }
            }

            DJO_UTESTS_CLOSE_SCOPE;
        }

        void Tests_Buffer(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            Tests_Buffer_MemoryContiguous(DJO_UTESTS_PARAMS);

            DJO_UTESTS_CLOSE_SCOPE;
        }
    }
}
