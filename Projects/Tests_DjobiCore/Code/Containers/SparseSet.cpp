#include "Tests_DjobiCore_pch.h"

#include "Utils.hpp"

#include <DjobiTests/Context.hpp>
#include <DjobiTests/Tests.hpp>
#include <DjobiTests/Utils.hpp>

#include <DjobiCore/Containers/SparseSet.hpp>
#include <DjobiCore/Containers/SparseSetAlloc.hpp>


namespace Djo
{
    namespace Tests
    {
        namespace
        {
            template <typename T>
            bool IsSparseSetValid(const SparseSet<T>& _ss) noexcept
            {
                for (u32 i = 0; i < GetCount(_ss); ++i)
                    if (*Pages::GetAt(_ss.sparseDenseIdsPages, _ss.sparsePageElemCountPow2, *Pages::GetAt(_ss.denseSparseIdsPages, _ss.densePageElemCountPow2, i)) != i)
                        return false;

                for (u32 i = 0; i < GetSparseCapacity(_ss); ++i)
                {
                    const u32 denseId = *Pages::GetAt(_ss.sparseDenseIdsPages, _ss.sparsePageElemCountPow2, i);
                    if (denseId == SparseSetConst::c_tombstone)
                        continue;
                    if (denseId >= GetCount(_ss) || *Pages::GetAt(_ss.denseSparseIdsPages, _ss.densePageElemCountPow2, denseId) != i)
                        return false;
                }

                return true;
            }
        }

        void Tests_SparseSet(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            {
                SparseSet<int> ss = c_nullSparseSet<int>;
                DJO_UTEST(GetDenseBudget(ss) == 0);
                DJO_UTEST(GetSparseBudget(ss) == 0);
                DJO_UTEST(GetDenseCapacity(ss) == 0);
                DJO_UTEST(GetSparseCapacity(ss) == 0);
                DJO_UTEST(GetCount(ss) == 0);
                DJO_UTEST(IsEmpty(ss) == true);
                DJO_UTEST(IsSparseSetValid(ss));
            }
            {
                SparseSet<int> ss = PushEmptySparseSet<int>(DJO_UTESTS_CTX->userArena, 10, 10, 0, 20, 20, 0);
                DJO_UTEST(GetDenseBudget(ss) >= 10 * 10);
                DJO_UTEST(GetSparseBudget(ss) >= 20 * 20);
                DJO_UTEST(GetDenseCapacity(ss) == 0);
                DJO_UTEST(GetSparseCapacity(ss) == 0);
                DJO_UTEST(GetCount(ss) == 0);
                DJO_UTEST(IsEmpty(ss) == true);
                DJO_UTEST(IsSparseSetValid(ss));
            }
            {
                SparseSet<int> ss = PushEmptySparseSet<int>(DJO_UTESTS_CTX->userArena, 10, 10, 100, 20, 20, 400);
                DJO_UTEST(GetDenseBudget(ss) >= 10 * 10);
                DJO_UTEST(GetSparseBudget(ss) >= 20 * 20);
                DJO_UTEST(GetDenseCapacity(ss) >= 100);
                DJO_UTEST(GetDenseCapacity(ss) <= GetDenseBudget(ss));
                DJO_UTEST(GetSparseCapacity(ss) >= 400);
                DJO_UTEST(GetSparseCapacity(ss) <= GetSparseBudget(ss));
                DJO_UTEST(GetCount(ss) == 0);
                DJO_UTEST(IsEmpty(ss) == true);
                DJO_UTEST(IsSparseSetValid(ss));

                bool allNullptr = true;
                for (u32 i = 0; i < GetSparseCapacity(ss); ++i)
                {
                    if (GetAtSparse(ss, i) != nullptr)
                    {
                        allNullptr = false;
                        break;
                    }
                }
                DJO_UTEST(allNullptr);
            }

            {
                SparseSet<int> ss = PushEmptySparseSet<int>(DJO_UTESTS_CTX->userArena, 10, 10, 100, 20, 20, 400);
                int* zero = SetAtSparse(&ss, 0);
                DJO_UTEST(zero != nullptr);
                int* one = SetAtSparse(&ss, 1);
                DJO_UTEST(one != nullptr);
                int* two = SetAtSparse(&ss, 2);
                DJO_UTEST(two != nullptr);
                DJO_UTEST(zero != one && one != two && zero != two);

                DJO_UTEST(GetAtSparse(ss, 0) == zero);
                DJO_UTEST(GetAtSparse(ss, 1) == one);
                DJO_UTEST(GetAtSparse(ss, 2) == two);

                int* zero2 = SetAtSparse(&ss, 0);
                DJO_UTEST(zero2 != nullptr);
                int* one2 = SetAtSparse(&ss, 1);
                DJO_UTEST(one2 != nullptr);
                int* two2 = SetAtSparse(&ss, 2);
                DJO_UTEST(two2 != nullptr);
                DJO_UTEST(zero2 != one2 && one2 != two2 && zero2 != two2);
                DJO_UTEST(zero == zero2 && one == one2 && two2 == two2);

                DJO_UTEST(IsSparseSetValid(ss));

                bool allNullptr = true;
                for (u32 i = 3; i < GetSparseCapacity(ss); ++i)
                {
                    if (GetAtSparse(ss, i) != nullptr)
                    {
                        allNullptr = false;
                        break;
                    }
                }
                DJO_UTEST(allNullptr);
            }

            {
                SparseSet<int> ss = PushEmptySparseSet<int>(DJO_UTESTS_CTX->userArena, 5, 5, 25, 5, 5, 25);

                SetAtSparse(&ss, 0);
                DJO_UTEST(GetAtSparse(ss, 0) != nullptr);

                bool erased = EraseAt(&ss, 0);
                DJO_UTEST(erased);
                DJO_UTEST(GetAtSparse(ss, 0) == nullptr);
                DJO_UTEST(IsSparseSetValid(ss));

                bool erasedTwice = EraseAt(&ss, 0);
                DJO_UTEST(!erasedTwice);
                DJO_UTEST(GetAtSparse(ss, 0) == nullptr);
                DJO_UTEST(IsSparseSetValid(ss));

                SetAtSparse(&ss, 0);
                DJO_UTEST(GetAtSparse(ss, 0) != nullptr);
                DJO_UTEST(IsSparseSetValid(ss));
            }

            {
                SparseSet<int> ss = PushEmptySparseSet<int>(DJO_UTESTS_CTX->userArena, 5, 5, 25, 5, 5, 25);

                DJO_UTEST(GetSparseCapacity(ss) >= GetDenseCapacity(ss));

                const u32 denseCapacity = GetDenseCapacity(ss);

                for (u32 i = 0; i < denseCapacity; ++i)
                    *SetAtSparse(&ss, i) = (int)i;

                bool noNullptr = true;
                for (u32 i = 0; i < denseCapacity; ++i)
                {
                    if (GetAtSparse(ss, i) == nullptr)
                    {
                        noNullptr = false;
                        break;
                    }
                }
                DJO_UTEST(noNullptr);
                DJO_UTEST(IsSparseSetValid(ss));

                const u32 previousCount = GetCount(ss);
                bool allErased = true;
                u32 erasedCount = 0;
                for (u32 i = denseCapacity; i > 0; --i)
                {
                    if ((i & 1) == 0) // even entries
                    {
                        if (!EraseAt(&ss, i - 1))
                        {
                            allErased = false;
                            break;
                        }
                        ++erasedCount;
                        DJO_UTEST(IsSparseSetValid(ss));
                    }
                }
                DJO_UTEST(allErased);
                DJO_UTEST(GetCount(ss) == (previousCount - erasedCount));
                DJO_UTEST(IsSparseSetValid(ss));

                u32 nullCount = 0;
                for (u32 i = 0; i < denseCapacity; ++i)
                {
                    if (GetAtSparse(ss, i) == nullptr)
                        ++nullCount;
                }
                DJO_UTEST(erasedCount == nullCount);

                allErased = true;
                for (u32 i = denseCapacity; i > 0; --i)
                {
                    if ((i & 1) != 0) // odd entries
                    {
                        if (!EraseAt(&ss, i - 1))
                        {
                            allErased = false;
                            break;
                        }
                        ++erasedCount;
                        DJO_UTEST(IsSparseSetValid(ss));
                    }
                }
                DJO_UTEST(allErased);
                DJO_UTEST(erasedCount == previousCount);
                DJO_UTEST(GetCount(ss) == 0);
                DJO_UTEST(IsSparseSetValid(ss));
            }

            DJO_UTESTS_CLOSE_SCOPE;
        }
    }
}
