#include "Tests_DjobiCore_pch.h"

#include <DjobiTests/Context.hpp>
#include <DjobiTests/Tests.hpp>
#include <DjobiTests/Utils.hpp>

#include <DjobiCore/Containers/String8Alloc.hpp>


namespace Djo
{
    namespace Tests
    {
        void Tests_String8(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            String8 str = PushEmptyString8(DJO_UTESTS_CTX->userArena, 100);

            DJO_UTEST(GetData(str) != nullptr);
            DJO_UTEST(GetCapacity(str) >= 100);
            DJO_UTEST(GetLength(str) == 0);
            DJO_UTEST(IsEmpty(str));

            constexpr sz c_addCount = 5;
            {
                for (sz i = 0; i < c_addCount; ++i)
                {
                    const char value = '0' + (char)(i + 1);
                    InsertBack(&str, value);
                    DJO_UTEST(GetData(str) != nullptr);
                    DJO_UTEST(GetLength(str) == (i + 1));
                    DJO_UTEST(GetCapacity(str) >= GetLength(str));
                    DJO_UTEST(!IsEmpty(str));
                    DJO_UTEST(GetLast(str) == GetAt(str, i));
                    DJO_UTEST(*GetLast(str) == value);
                    DJO_UTEST(*GetAt(str, i) == value);
                }
            }

            constexpr sz c_addRangeCount = 5;
            {
                char range[c_addRangeCount];
                for (sz i = 0; i < c_addRangeCount; ++i)
                    range[i] = 'a' + (char)(i + 1);

                const sz previousCount = GetLength(str);
                InsertBack(&str, ToBufferConst(range));
                DJO_UTEST(GetLength(str) == previousCount + c_addRangeCount);
                DJO_UTEST(GetCapacity(str) >= GetLength(str));

                for (sz i = 0; i < c_addRangeCount; ++i)
                    DJO_UTEST(*GetAt(str, previousCount + i) == range[i]);
            }

            DJO_UTESTS_CLOSE_SCOPE;
        }
    }
}
