#include "Tests_DjobiCore_pch.h"

#include <DjobiTests/Context.hpp>
#include <DjobiTests/Tests.hpp>
#include <DjobiTests/Utils.hpp>

#include <DjobiCore/Containers/LinkedList.hpp>


namespace Djo
{
    namespace Tests
    {
        static constexpr sz c_listNodeCount{ 20 };

        struct SLLNode
        {
            SLLNode* next;
            int i;
        };

        void Tests_SingleLinkedList(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            {
                SLLNode* const nullNode = nullptr;
                DJO_UTEST(SLList::GetTail(nullNode) == nullptr);
            }

            SLLNode nodes[c_listNodeCount]{};

            DJO_UTEST(SLList::GetTail(&nodes[0]) == &nodes[0]);

            SLList::InsertAfter(&nodes[0], &nodes[1]);
            DJO_UTEST(nodes[0].next == &nodes[1]);
            DJO_UTEST(SLList::GetTail(&nodes[0]) == &nodes[1]);

            SLList::InsertAfter(&nodes[1], &nodes[2]);
            DJO_UTEST(nodes[0].next == &nodes[1]);
            DJO_UTEST(nodes[1].next == &nodes[2]);
            DJO_UTEST(SLList::GetTail(&nodes[0]) == &nodes[2]);
            DJO_UTEST(SLList::GetTail(&nodes[1]) == &nodes[2]);

            SLList::RemoveNext(&nodes[0]);
            DJO_UTEST(nodes[0].next == &nodes[2]);
            DJO_UTEST(nodes[1].next == nullptr);
            DJO_UTEST(SLList::GetTail(&nodes[0]) == &nodes[2]);
            DJO_UTEST(SLList::GetTail(&nodes[1]) == &nodes[1]);

            SLList::RemoveNext(&nodes[0]);
            DJO_UTEST(nodes[0].next == nullptr);
            DJO_UTEST(SLList::GetTail(&nodes[0]) == &nodes[0]);

            ZeroMem(&nodes);

            LinkedList<SLLNode> linkedList{};

            DJO_UTEST(SLList::GetTail(linkedList.head) == linkedList.tail);

            SLList::InsertBack(&linkedList, &nodes[2]);
            DJO_UTEST(SLList::GetTail(linkedList.head) == linkedList.tail);
            DJO_UTEST(linkedList.head == &nodes[2]);
            DJO_UTEST(linkedList.tail == &nodes[2]);

            SLList::InsertBack(&linkedList, &nodes[3]);
            DJO_UTEST(SLList::GetTail(linkedList.head) == linkedList.tail);
            DJO_UTEST(linkedList.head == &nodes[2]);
            DJO_UTEST(linkedList.tail == &nodes[3]);
            DJO_UTEST(nodes[2].next == &nodes[3]);

            SLList::InsertBack(&linkedList, &nodes[4]);
            DJO_UTEST(SLList::GetTail(linkedList.head) == linkedList.tail);
            DJO_UTEST(linkedList.head == &nodes[2]);
            DJO_UTEST(linkedList.tail == &nodes[4]);
            DJO_UTEST(nodes[2].next == &nodes[3]);
            DJO_UTEST(nodes[3].next == &nodes[4]);

            SLList::AddFront(&linkedList, &nodes[1]);
            DJO_UTEST(SLList::GetTail(linkedList.head) == linkedList.tail);
            DJO_UTEST(linkedList.head == &nodes[1]);
            DJO_UTEST(linkedList.tail == &nodes[4]);
            DJO_UTEST(nodes[1].next == &nodes[2]);
            DJO_UTEST(nodes[2].next == &nodes[3]);
            DJO_UTEST(nodes[3].next == &nodes[4]);

            SLList::AddFront(&linkedList, &nodes[0]);
            DJO_UTEST(SLList::GetTail(linkedList.head) == linkedList.tail);
            DJO_UTEST(linkedList.head == &nodes[0]);
            DJO_UTEST(linkedList.tail == &nodes[4]);
            DJO_UTEST(nodes[0].next == &nodes[1]);
            DJO_UTEST(nodes[1].next == &nodes[2]);
            DJO_UTEST(nodes[2].next == &nodes[3]);
            DJO_UTEST(nodes[3].next == &nodes[4]);

            SLLNode* popedNode;

            popedNode = SLList::RemoveFront(&linkedList);
            DJO_UTEST(SLList::GetTail(linkedList.head) == linkedList.tail);
            DJO_UTEST(popedNode == &nodes[0]);
            DJO_UTEST(popedNode->next == nullptr);
            DJO_UTEST(linkedList.head == &nodes[1]);
            DJO_UTEST(linkedList.tail == &nodes[4]);
            DJO_UTEST(nodes[1].next == &nodes[2]);
            DJO_UTEST(nodes[2].next == &nodes[3]);
            DJO_UTEST(nodes[3].next == &nodes[4]);

            popedNode = SLList::RemoveFront(&linkedList);
            DJO_UTEST(SLList::GetTail(linkedList.head) == linkedList.tail);
            DJO_UTEST(popedNode == &nodes[1]);
            DJO_UTEST(popedNode->next == nullptr);
            DJO_UTEST(linkedList.head == &nodes[2]);
            DJO_UTEST(linkedList.tail == &nodes[4]);
            DJO_UTEST(nodes[2].next == &nodes[3]);
            DJO_UTEST(nodes[3].next == &nodes[4]);

            popedNode = SLList::RemoveFront(&linkedList);
            DJO_UTEST(SLList::GetTail(linkedList.head) == linkedList.tail);
            DJO_UTEST(popedNode == &nodes[2]);
            DJO_UTEST(popedNode->next == nullptr);
            DJO_UTEST(linkedList.head == &nodes[3]);
            DJO_UTEST(linkedList.tail == &nodes[4]);
            DJO_UTEST(nodes[3].next == &nodes[4]);

            popedNode = SLList::RemoveFront(&linkedList);
            DJO_UTEST(SLList::GetTail(linkedList.head) == linkedList.tail);
            DJO_UTEST(popedNode == &nodes[3]);
            DJO_UTEST(popedNode->next == nullptr);
            DJO_UTEST(linkedList.head == &nodes[4]);
            DJO_UTEST(linkedList.tail == &nodes[4]);
            DJO_UTEST(nodes[4].next == nullptr);

            popedNode = SLList::RemoveFront(&linkedList);
            DJO_UTEST(SLList::GetTail(linkedList.head) == linkedList.tail);
            DJO_UTEST(popedNode == &nodes[4]);
            DJO_UTEST(popedNode->next == nullptr);
            DJO_UTEST(linkedList.head == nullptr);
            DJO_UTEST(linkedList.tail == nullptr);

            popedNode = SLList::RemoveFront(&linkedList);
            DJO_UTEST(SLList::GetTail(linkedList.head) == linkedList.tail);
            DJO_UTEST(popedNode == nullptr);
            DJO_UTEST(linkedList.head == nullptr);
            DJO_UTEST(linkedList.tail == nullptr);

            DJO_UTESTS_CLOSE_SCOPE;
        }

        struct DLLNode
        {
            DLLNode* previous;
            DLLNode* next;
            int i;
        };

        void Tests_DoubleLinkedList(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            {
                DLLNode* const nullNode = nullptr;
                DJO_UTEST(DLList::GetHead(nullNode) == nullptr);
                DJO_UTEST(DLList::GetTail(nullNode) == nullptr);
            }

            DLLNode nodes[c_listNodeCount]{};

            DJO_UTEST(DLList::GetHead(&nodes[0]) == &nodes[0]);
            DJO_UTEST(DLList::GetTail(&nodes[0]) == &nodes[0]);

            DLList::InsertAfter(&nodes[0], &nodes[1]);
            DJO_UTEST(nodes[0].previous == nullptr);
            DJO_UTEST(nodes[0].next == &nodes[1]);
            DJO_UTEST(nodes[1].previous == &nodes[0]);
            DJO_UTEST(nodes[1].next == nullptr);
            DJO_UTEST(DLList::GetHead(&nodes[0]) == &nodes[0]);
            DJO_UTEST(DLList::GetTail(&nodes[0]) == &nodes[1]);
            DJO_UTEST(DLList::GetHead(&nodes[1]) == &nodes[0]);
            DJO_UTEST(DLList::GetTail(&nodes[1]) == &nodes[1]);

            DLList::InsertAfter(&nodes[1], &nodes[2]);
            DJO_UTEST(nodes[0].previous == nullptr);
            DJO_UTEST(nodes[0].next == &nodes[1]);
            DJO_UTEST(nodes[1].previous == &nodes[0]);
            DJO_UTEST(nodes[1].next == &nodes[2]);
            DJO_UTEST(nodes[2].previous == &nodes[1]);
            DJO_UTEST(nodes[2].next == nullptr);
            DJO_UTEST(DLList::GetHead(&nodes[0]) == &nodes[0]);
            DJO_UTEST(DLList::GetTail(&nodes[0]) == &nodes[2]);
            DJO_UTEST(DLList::GetHead(&nodes[1]) == &nodes[0]);
            DJO_UTEST(DLList::GetTail(&nodes[1]) == &nodes[2]);
            DJO_UTEST(DLList::GetHead(&nodes[2]) == &nodes[0]);
            DJO_UTEST(DLList::GetTail(&nodes[2]) == &nodes[2]);

            DLList::InsertBefore(&nodes[3], &nodes[2]);
            DJO_UTEST(nodes[0].previous == nullptr);
            DJO_UTEST(nodes[0].next == &nodes[1]);
            DJO_UTEST(nodes[1].previous == &nodes[0]);
            DJO_UTEST(nodes[1].next == &nodes[2]);
            DJO_UTEST(nodes[2].previous == &nodes[1]);
            DJO_UTEST(nodes[2].next == &nodes[3]);
            DJO_UTEST(nodes[3].previous == &nodes[2]);
            DJO_UTEST(nodes[3].next == nullptr);
            DJO_UTEST(DLList::GetHead(&nodes[0]) == &nodes[0]);
            DJO_UTEST(DLList::GetTail(&nodes[0]) == &nodes[3]);
            DJO_UTEST(DLList::GetHead(&nodes[1]) == &nodes[0]);
            DJO_UTEST(DLList::GetTail(&nodes[1]) == &nodes[3]);
            DJO_UTEST(DLList::GetHead(&nodes[2]) == &nodes[0]);
            DJO_UTEST(DLList::GetTail(&nodes[2]) == &nodes[3]);
            DJO_UTEST(DLList::GetHead(&nodes[3]) == &nodes[0]);
            DJO_UTEST(DLList::GetTail(&nodes[3]) == &nodes[3]);

            DLList::InsertBefore(&nodes[1], &nodes[4]);
            DJO_UTEST(nodes[0].previous == nullptr);
            DJO_UTEST(nodes[0].next == &nodes[4]);
            DJO_UTEST(nodes[4].previous == &nodes[0]);
            DJO_UTEST(nodes[4].next == &nodes[1]);
            DJO_UTEST(nodes[1].previous == &nodes[4]);
            DJO_UTEST(nodes[1].next == &nodes[2]);
            DJO_UTEST(nodes[2].previous == &nodes[1]);
            DJO_UTEST(nodes[2].next == &nodes[3]);
            DJO_UTEST(nodes[3].previous == &nodes[2]);
            DJO_UTEST(nodes[3].next == nullptr);
            DJO_UTEST(DLList::GetHead(&nodes[0]) == &nodes[0]);
            DJO_UTEST(DLList::GetTail(&nodes[0]) == &nodes[3]);
            DJO_UTEST(DLList::GetHead(&nodes[4]) == &nodes[0]);
            DJO_UTEST(DLList::GetTail(&nodes[4]) == &nodes[3]);
            DJO_UTEST(DLList::GetHead(&nodes[1]) == &nodes[0]);
            DJO_UTEST(DLList::GetTail(&nodes[1]) == &nodes[3]);
            DJO_UTEST(DLList::GetHead(&nodes[2]) == &nodes[0]);
            DJO_UTEST(DLList::GetTail(&nodes[2]) == &nodes[3]);
            DJO_UTEST(DLList::GetHead(&nodes[3]) == &nodes[0]);
            DJO_UTEST(DLList::GetTail(&nodes[3]) == &nodes[3]);

            DLList::Remove(&nodes[4]);
            DJO_UTEST(nodes[0].previous == nullptr);
            DJO_UTEST(nodes[0].next == &nodes[1]);
            DJO_UTEST(nodes[1].previous == &nodes[0]);
            DJO_UTEST(nodes[1].next == &nodes[2]);
            DJO_UTEST(nodes[2].previous == &nodes[1]);
            DJO_UTEST(nodes[2].next == &nodes[3]);
            DJO_UTEST(nodes[3].previous == &nodes[2]);
            DJO_UTEST(nodes[3].next == nullptr);
            DJO_UTEST(DLList::GetHead(&nodes[0]) == &nodes[0]);
            DJO_UTEST(DLList::GetTail(&nodes[0]) == &nodes[3]);
            DJO_UTEST(DLList::GetHead(&nodes[1]) == &nodes[0]);
            DJO_UTEST(DLList::GetTail(&nodes[1]) == &nodes[3]);
            DJO_UTEST(DLList::GetHead(&nodes[2]) == &nodes[0]);
            DJO_UTEST(DLList::GetTail(&nodes[2]) == &nodes[3]);
            DJO_UTEST(DLList::GetHead(&nodes[3]) == &nodes[0]);
            DJO_UTEST(DLList::GetTail(&nodes[3]) == &nodes[3]);

            DLList::Remove(&nodes[0]);
            DJO_UTEST(nodes[1].previous == nullptr);
            DJO_UTEST(nodes[1].next == &nodes[2]);
            DJO_UTEST(nodes[2].previous == &nodes[1]);
            DJO_UTEST(nodes[2].next == &nodes[3]);
            DJO_UTEST(nodes[3].previous == &nodes[2]);
            DJO_UTEST(nodes[3].next == nullptr);
            DJO_UTEST(DLList::GetHead(&nodes[1]) == &nodes[1]);
            DJO_UTEST(DLList::GetTail(&nodes[1]) == &nodes[3]);
            DJO_UTEST(DLList::GetHead(&nodes[2]) == &nodes[1]);
            DJO_UTEST(DLList::GetTail(&nodes[2]) == &nodes[3]);
            DJO_UTEST(DLList::GetHead(&nodes[3]) == &nodes[1]);
            DJO_UTEST(DLList::GetTail(&nodes[3]) == &nodes[3]);

            DLList::Remove(&nodes[3]);
            DJO_UTEST(nodes[1].previous == nullptr);
            DJO_UTEST(nodes[1].next == &nodes[2]);
            DJO_UTEST(nodes[2].previous == &nodes[1]);
            DJO_UTEST(nodes[2].next == nullptr);
            DJO_UTEST(DLList::GetHead(&nodes[1]) == &nodes[1]);
            DJO_UTEST(DLList::GetTail(&nodes[1]) == &nodes[2]);
            DJO_UTEST(DLList::GetHead(&nodes[2]) == &nodes[1]);
            DJO_UTEST(DLList::GetTail(&nodes[2]) == &nodes[2]);

            ZeroMem(&nodes);

            LinkedList<DLLNode> linkedList{};

            DJO_UTEST(DLList::GetHead(linkedList.tail) == linkedList.head);
            DJO_UTEST(DLList::GetTail(linkedList.head) == linkedList.tail);

            DLList::InsertBack(&linkedList, &nodes[2]);
            DJO_UTEST(DLList::GetHead(linkedList.tail) == linkedList.head);
            DJO_UTEST(DLList::GetTail(linkedList.head) == linkedList.tail);
            DJO_UTEST(linkedList.head == &nodes[2]);
            DJO_UTEST(linkedList.tail == &nodes[2]);

            DLList::InsertBack(&linkedList, &nodes[3]);
            DJO_UTEST(DLList::GetHead(linkedList.tail) == linkedList.head);
            DJO_UTEST(DLList::GetTail(linkedList.head) == linkedList.tail);
            DJO_UTEST(linkedList.head == &nodes[2]);
            DJO_UTEST(linkedList.tail == &nodes[3]);
            DJO_UTEST(nodes[2].next == &nodes[3]);
            DJO_UTEST(nodes[3].previous == &nodes[2]);

            DLList::InsertBack(&linkedList, &nodes[4]);
            DJO_UTEST(DLList::GetHead(linkedList.tail) == linkedList.head);
            DJO_UTEST(DLList::GetTail(linkedList.head) == linkedList.tail);
            DJO_UTEST(linkedList.head == &nodes[2]);
            DJO_UTEST(linkedList.tail == &nodes[4]);
            DJO_UTEST(nodes[2].next == &nodes[3]);
            DJO_UTEST(nodes[3].next == &nodes[4]);
            DJO_UTEST(nodes[4].previous == &nodes[3]);
            DJO_UTEST(nodes[3].previous == &nodes[2]);

            DLList::AddFront(&linkedList, &nodes[1]);
            DJO_UTEST(DLList::GetHead(linkedList.tail) == linkedList.head);
            DJO_UTEST(DLList::GetTail(linkedList.head) == linkedList.tail);
            DJO_UTEST(linkedList.head == &nodes[1]);
            DJO_UTEST(linkedList.tail == &nodes[4]);
            DJO_UTEST(nodes[1].next == &nodes[2]);
            DJO_UTEST(nodes[2].next == &nodes[3]);
            DJO_UTEST(nodes[3].next == &nodes[4]);
            DJO_UTEST(nodes[4].previous == &nodes[3]);
            DJO_UTEST(nodes[3].previous == &nodes[2]);
            DJO_UTEST(nodes[2].previous == &nodes[1]);

            DLList::AddFront(&linkedList, &nodes[0]);
            DJO_UTEST(DLList::GetHead(linkedList.tail) == linkedList.head);
            DJO_UTEST(DLList::GetTail(linkedList.head) == linkedList.tail);
            DJO_UTEST(linkedList.head == &nodes[0]);
            DJO_UTEST(linkedList.tail == &nodes[4]);
            DJO_UTEST(nodes[0].next == &nodes[1]);
            DJO_UTEST(nodes[1].next == &nodes[2]);
            DJO_UTEST(nodes[2].next == &nodes[3]);
            DJO_UTEST(nodes[3].next == &nodes[4]);
            DJO_UTEST(nodes[4].previous == &nodes[3]);
            DJO_UTEST(nodes[3].previous == &nodes[2]);
            DJO_UTEST(nodes[2].previous == &nodes[1]);
            DJO_UTEST(nodes[1].previous == &nodes[0]);

            DJO_UTESTS_CLOSE_SCOPE;
        }

        void Tests_LinkedList(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            Tests_SingleLinkedList(DJO_UTESTS_PARAMS);
            Tests_DoubleLinkedList(DJO_UTESTS_PARAMS);

            DJO_UTESTS_CLOSE_SCOPE;
        }
    }
}
