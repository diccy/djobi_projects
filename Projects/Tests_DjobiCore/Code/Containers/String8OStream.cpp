#include "Tests_DjobiCore_pch.h"

#include <DjobiTests/Context.hpp>
#include <DjobiTests/Tests.hpp>
#include <DjobiTests/Utils.hpp>

#include <DjobiCore/Containers/String8OStream.hpp>
#include <DjobiCore/Containers/String8Alloc.hpp>
#include <DjobiCore/Core/Utils.hpp>


namespace Djo
{
    namespace Tests
    {
        static constexpr sz c_stringStreamBufferSize = 20;
        static thread_local char s_callbackBuffer[c_stringStreamBufferSize * 2];
        static thread_local sz s_outputCount = 0;

        void StringStreamOutputCallback(const char* const _data, const sz _count, void* const _userData) noexcept
        {
            DJO_ASSERT(_count < (ArrayCount(s_callbackBuffer)));

            Context* const DJO_UTESTS_CTX = reinterpret_cast<Context*>(_userData);
            DJO_UTEST(true); // checks _userData is forwarded by string stream setup

            ++s_outputCount;
            ::memcpy(s_callbackBuffer, _data, _count);
            s_callbackBuffer[_count] = '\0';
        }

        void Tests_String8OStream_Basics(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            char stringStreamBuffer[c_stringStreamBufferSize];
            sz n = 0;

            String8OStream stream;
            ZeroMem(&stream);

            // no effect if pushed with null initialized stream struct
            s_outputCount = 0;
            n = Push(&stream, 'a');
            DJO_UTEST(n == 0);
            n = Push(&stream, ToStringView("coucou"));
            DJO_UTEST(n == 0);
            n = PushPrintf(&stream, "%d %u", 2, 3u);
            DJO_UTEST(n == 0);
            DJO_UTEST(stream.array.buffer.data == nullptr);
            DJO_UTEST(stream.array.buffer.count == 0);
            DJO_UTEST(stream.array.count == 0);
            DJO_UTEST(s_outputCount == 0);

            stream.output.callback = StringStreamOutputCallback;
            stream.output.userData = DJO_UTESTS_CTX;

            // no array in stream means direct output
            s_outputCount = 0;
            n = Push(&stream, 'a');
            DJO_UTEST(n == 1);
            DJO_UTEST(s_outputCount == 1);
            DJO_UTEST(ToStringView("a") == s_callbackBuffer);
            n = Push(&stream, ToStringView("coucou"));
            DJO_UTEST(n == 6);
            DJO_UTEST(s_outputCount == 2);
            DJO_UTEST(ToStringView("coucou") == s_callbackBuffer);
            n = PushPrintf(&stream, "%d %u", 2, 3u);
            DJO_UTEST(n == 3);
            DJO_UTEST(s_outputCount == 3);
            DJO_UTEST(ToStringView("2 3") == s_callbackBuffer);
            n = PushPrintf(&stream, "aaa");
            DJO_UTEST(n == 3);
            DJO_UTEST(s_outputCount == 4);
            DJO_UTEST(ToStringView("aaa") == s_callbackBuffer);

            stream.array.buffer = ToBuffer(stringStreamBuffer);
            stream.array.count = 0;

            s_outputCount = 0;
            n = Push(&stream, 'a');
            DJO_UTEST(n == 1);
            DJO_UTEST(s_outputCount == 0);
            DJO_UTEST(stream.array.count == 1);
            n = Flush(&stream);
            DJO_UTEST(n == 1);
            DJO_UTEST(s_outputCount == 1);
            DJO_UTEST(stream.array.count == 0);
            DJO_UTEST(ToStringView("a") == s_callbackBuffer);

            s_outputCount = 0;
            n = Push(&stream, ToStringView("coucou"));
            DJO_UTEST(n == 6);
            DJO_UTEST(s_outputCount == 0);
            DJO_UTEST(stream.array.count == 6);
            n = Flush(&stream);
            DJO_UTEST(n == 6);
            DJO_UTEST(s_outputCount == 1);
            DJO_UTEST(stream.array.count == 0);
            DJO_UTEST(ToStringView("coucou") == s_callbackBuffer);

            s_outputCount = 0;
            n = PushPrintf(&stream, "%d %u", 2, 3u);
            DJO_UTEST(n == 3);
            DJO_UTEST(s_outputCount == 0);
            DJO_UTEST(stream.array.count == 3);
            n = Flush(&stream);
            DJO_UTEST(n == 3);
            DJO_UTEST(s_outputCount == 1);
            DJO_UTEST(stream.array.count == 0);
            DJO_UTEST(ToStringView("2 3") == s_callbackBuffer);

            // input too big for internal buffer means direct output
            s_outputCount = 0;
            n = PushPrintf(&stream, "%0*d", ArrayCount(s_callbackBuffer) - 1, 1);
            DJO_UTEST(n == ArrayCount(s_callbackBuffer) - 1);
            DJO_UTEST(s_outputCount == 1);
            DJO_UTEST(stream.array.count == 0);

            DJO_UTESTS_CLOSE_SCOPE;
        }

        void Tests_String8OStream_BufferFillerOStream(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            constexpr sz c_bufferSize = 10;
            char stringStreamBuffer[c_bufferSize + 1]; // make a room for null char
            ZeroMem(&stringStreamBuffer);
            sz n = 0;

            Buffer<char> buffer = ToBuffer(stringStreamBuffer);
            buffer.count = c_bufferSize; // does not allow to write over terminating null char
            String8OStream stream = MoldBufferFillerOStream(buffer);
            s_outputCount = 0;

            n = Push(&stream, 'a');
            DJO_UTEST(n == 1);
            DJO_UTEST(stream.array.count <= GetCapacity(stream.array));
            DJO_UTEST(stream.array.count == 1);
            DJO_UTEST(StrNCmp(stringStreamBuffer, "a", c_bufferSize) == 0);

            n = Push(&stream, 'a');
            DJO_UTEST(n == 1);
            DJO_UTEST(stream.array.count <= GetCapacity(stream.array));
            DJO_UTEST(stream.array.count == 2);
            DJO_UTEST(StrNCmp(stringStreamBuffer, "aa", c_bufferSize) == 0);

            n = Push(&stream, ToStringView("aaaaa"));
            DJO_UTEST(n == 5);
            DJO_UTEST(stream.array.count == 7);
            DJO_UTEST(StrNCmp(stringStreamBuffer, "aaaaaaa", c_bufferSize) == 0);

            n = PushPrintf(&stream, "a%ca", 'a');
            DJO_UTEST(n == 3);
            DJO_UTEST(stream.array.count <= GetCapacity(stream.array));
            DJO_UTEST(stream.array.count == c_bufferSize);
            DJO_UTEST(StrNCmp(stringStreamBuffer, "aaaaaaaaaa", c_bufferSize) == 0);

            n = Push(&stream, 'b');
            DJO_UTEST(n == 0);
            DJO_UTEST(stream.array.count <= GetCapacity(stream.array));
            DJO_UTEST(stream.array.count == c_bufferSize);
            DJO_UTEST(StrNCmp(stringStreamBuffer, "aaaaaaaaaa", c_bufferSize) == 0);

            n = Push(&stream, ToStringView("bbb"));
            DJO_UTEST(n == 0);
            DJO_UTEST(stream.array.count <= GetCapacity(stream.array));
            DJO_UTEST(stream.array.count == c_bufferSize);
            DJO_UTEST(StrNCmp(stringStreamBuffer, "aaaaaaaaaa", c_bufferSize) == 0);

            n = PushPrintf(&stream, "bbb");
            DJO_UTEST(n == 0);
            DJO_UTEST(stream.array.count <= GetCapacity(stream.array));
            DJO_UTEST(stream.array.count == c_bufferSize);
            DJO_UTEST(StrNCmp(stringStreamBuffer, "aaaaaaaaaa", c_bufferSize) == 0);

            n = Flush(&stream);
            DJO_UTEST(n == 0);
            DJO_UTEST(stream.array.count == 0);
            DJO_UTEST(StrNCmp(stringStreamBuffer, "aaaaaaaaaa", c_bufferSize) == 0);

            n = Push(&stream, 'b');
            DJO_UTEST(n == 1);
            DJO_UTEST(stream.array.count <= GetCapacity(stream.array));
            DJO_UTEST(stream.array.count == 1);
            DJO_UTEST(StrNCmp(stringStreamBuffer, "baaaaaaaaa", c_bufferSize) == 0);

            DJO_UTESTS_CLOSE_SCOPE;
        }

        void Tests_String8OStream_String8FillerOStream(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            sz n = 0;

            String8 str = PushEmptyString8(DJO_UTESTS_CTX->userArena, 150);
            String8OStream stream = MoldString8FillerOStream(&str);

            n = Push(&stream, 'a');
            DJO_UTEST(n == 1);
            DJO_UTEST(GetLength(str) == 1);
            DJO_UTEST(str == "a");

            n = Push(&stream, ToStringView("bbbbbb"));
            DJO_UTEST(n == 6);
            DJO_UTEST(GetLength(str) == 7);
            DJO_UTEST(str == "abbbbbb");

            n = PushPrintf(&stream, "%d%d%d%d", 1, 2, 3, 4);
            DJO_UTEST(n == 4);
            DJO_UTEST(GetLength(str) == 11);
            DJO_UTEST(str == "abbbbbb1234");

            n = PushPrintf(&stream, "%0*d", 100, 1);
            DJO_UTEST(n == 100);
            DJO_UTEST(GetLength(str) == 111);
            DJO_UTEST(*GetLast(str) == '1');
            DJO_UTEST(*(GetLast(str) - 1) == '0');

            DJO_UTESTS_CLOSE_SCOPE;
        }

        void Tests_String8OStream(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            Tests_String8OStream_Basics(DJO_UTESTS_PARAMS);
            Tests_String8OStream_BufferFillerOStream(DJO_UTESTS_PARAMS);
            Tests_String8OStream_String8FillerOStream(DJO_UTESTS_PARAMS);

            DJO_UTESTS_CLOSE_SCOPE;
        }
    }
}
