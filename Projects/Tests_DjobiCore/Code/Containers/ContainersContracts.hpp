#pragma once

#include <DjobiCore/Memory/Allocator.hpp>
#include <DjobiCore/Memory/Arena.hpp>
#include <DjobiCore/Containers/Policies.hpp>
#include <DjobiCore/Core/VisitorResultEnum.hpp>

#include <concepts>


namespace Djo
{
    namespace Tests
    {
        template <typename TContainer>
        concept Accountable = requires(const TContainer& _c)
        {
            { GetCapacity(_c) } noexcept -> std::same_as<sz>;
            { GetCount(_c) } noexcept -> std::same_as<sz>;
            { IsEmpty(_c) } noexcept -> std::same_as<bool>;
        };

        template <typename TContainer>
        concept BackAccessible = requires(const TContainer& _c)
        {
            { GetLast(_c) } noexcept -> std::same_as<typename TContainer::DataType*>;
        };

        template <typename TContainer>
        concept FrontAccessible = requires(const TContainer& _c)
        {
            { GetFirst(_c) } noexcept -> std::same_as<typename TContainer::DataType*>;
        };

        template <typename TContainer>
        concept Indexable = Accountable<TContainer> && BackAccessible<TContainer> && FrontAccessible<TContainer> && requires(TContainer _c, sz _i, typename TContainer::DataType* _values)
        {
            { GetAt(_c, _i) } noexcept -> std::same_as<typename TContainer::DataType*>;
            { Set(&_c, _i, _values, _i) } noexcept -> std::same_as<void>;
        };

        template <typename TContainer>
        concept MemoryContiguous = Indexable<TContainer> && requires(const TContainer & _c)
        {
            { GetData(_c) } noexcept -> std::same_as<typename TContainer::DataType*>;
            { GetSize(_c) } noexcept -> std::same_as<sz>;
            { GetEnd(_c) } noexcept -> std::same_as<typename TContainer::DataType*>;
        };

        template <typename TContainer>
        concept BackGrowable = Accountable<TContainer> && requires(TContainer _c, sz _i, typename TContainer::DataType _value, typename TContainer::DataType* _values)
        {
            { InsertBack(&_c, _value) } noexcept -> std::same_as<void>;
            { InsertBack(&_c, _values, _i) } noexcept -> std::same_as<void>;
        };

        template <typename TContainer>
        concept FrontGrowable = Accountable<TContainer> && requires(TContainer _c, sz _i, typename TContainer::DataType _value, typename TContainer::DataType* _values)
        {
            { InsertFront(&_c, _value) } noexcept -> std::same_as<void>;
            { InsertFront(&_c, _values, _i) } noexcept -> std::same_as<void>;
        };

        template <typename TContainer>
        concept IndexAccessGrowable = FrontGrowable<TContainer> && BackGrowable<TContainer> && Indexable<TContainer> && requires(TContainer _c, sz _i, typename TContainer::DataType _value, typename TContainer::DataType* _values)
        {
            { Insert(&_c, _i, _value) } noexcept -> std::same_as<void>;
            { Insert(&_c, _i, _values, _i) } noexcept -> std::same_as<void>;
        };

        template <typename TContainer>
        concept BackGrowableAlloc = BackGrowable<TContainer> && requires(TContainer _c, Memory::Allocator _alloc, sz _i, typename TContainer::DataType _value, typename TContainer::DataType* _values)
        {
            { InsertBack(&_c, _alloc, _value) } noexcept -> std::same_as<void>;
            { InsertBack(&_c, _alloc, _values, _i) } noexcept -> std::same_as<void>;
        };

        template <typename TContainer>
        concept BackGrowableArena = BackGrowable<TContainer> && requires(TContainer _c, Memory::Arena* _arena, sz _i, typename TContainer::DataType _value, typename TContainer::DataType* _values)
        {
            { InsertBack(&_c, _arena, _value) } noexcept -> std::same_as<void>;
            { InsertBack(&_c, _arena, _values, _i) } noexcept -> std::same_as<void>;
        };

        template <typename TContainer>
        concept FrontGrowableAlloc = FrontGrowable<TContainer> && requires(TContainer _c, Memory::Allocator _alloc, sz _i, typename TContainer::DataType _value, typename TContainer::DataType* _values)
        {
            { InsertFront(&_c, _alloc, _value) } noexcept -> std::same_as<void>;
            { InsertFront(&_c, _alloc, _values, _i) } noexcept -> std::same_as<void>;
        };

        template <typename TContainer>
        concept FrontGrowableArena = FrontGrowable<TContainer> && requires(TContainer _c, Memory::Arena* _arena, sz _i, typename TContainer::DataType _value, typename TContainer::DataType* _values)
        {
            { InsertFront(&_c, _arena, _value) } noexcept -> std::same_as<void>;
            { InsertFront(&_c, _arena, _values, _i) } noexcept -> std::same_as<void>;
        };

        template <typename TContainer>
        concept IndexAccessGrowableAlloc = BackGrowableAlloc<TContainer> && FrontGrowableAlloc<TContainer> && requires(TContainer _c, Memory::Allocator _alloc, sz _i, typename TContainer::DataType _value, typename TContainer::DataType* _values)
        {
            { Insert(&_c, _alloc, _i, _value) } noexcept -> std::same_as<void>;
            { Insert(&_c, _alloc, _i, _values, _i) } noexcept -> std::same_as<void>;
        };

        template <typename TContainer>
        concept IndexAccessGrowableArena = BackGrowableArena<TContainer> && FrontGrowableArena<TContainer> && requires(TContainer _c, Memory::Arena* _arena, sz _i, typename TContainer::DataType _value, typename TContainer::DataType* _values)
        {
            { Insert(&_c, _arena, _i, _value) } noexcept -> std::same_as<void>;
            { Insert(&_c, _arena, _i, _values, _i) } noexcept -> std::same_as<void>;
        };

        template <typename TContainer>
        concept BackShrinkable = Accountable<TContainer> && requires(TContainer _c, sz _i)
        {
            { EraseBack(&_c, _i) } noexcept -> std::same_as<void>;
        };

        template <typename TContainer>
        concept FrontShrinkable = Accountable<TContainer> && requires(TContainer _c, sz _i)
        {
            { EraseFront(&_c, _i) } noexcept -> std::same_as<void>;
        };

        template <typename TContainer>
        concept IndexAccessShrinkable = FrontShrinkable<TContainer> && BackShrinkable<TContainer> && Indexable<TContainer> && requires(TContainer _c, sz _i)
        {
            { EraseAt(&_c, _i, _i) } noexcept -> std::same_as<void>;
        };

        template <typename TContainer, typename TVisitor>
        concept Visitable = requires(TContainer _c, TVisitor _visitor)
        {
            { Visit(_c, _visitor) } noexcept -> std::same_as<void>;
        };

        template <typename TContainer>
        using IndexVisitor = EVisitorResult(*)(typename TContainer::DataType, sz);

        template <typename TContainer>
        concept IndexVisitable = requires(TContainer _c, IndexVisitor<TContainer> _visitor)
        {
            { Visit(_c, _visitor) } noexcept -> std::same_as<void>;
        };
    }
}
