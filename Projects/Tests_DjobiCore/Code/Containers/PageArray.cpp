#include "Tests_DjobiCore_pch.h"

#include "ContainersContracts.hpp"
#include "Utils.hpp"

#include <DjobiTests/Context.hpp>
#include <DjobiTests/Tests.hpp>
#include <DjobiTests/Utils.hpp>

#include <DjobiCore/Containers/PageArray.hpp>
#include <DjobiCore/Containers/PageArrayAlloc.hpp>


namespace Djo
{
    namespace Tests
    {
        namespace
        {
            void InitPagesBuffer(int* const _valuesBuffer, const sz _valuesBufferCount, int** const _pagesBuffer, const sz _pagesBufferCount)
            {
                const sz pageElemCount = _valuesBufferCount / _pagesBufferCount;
                for (sz p = 0; p < _pagesBufferCount; ++p)
                    _pagesBuffer[p] = _valuesBuffer + (p * pageElemCount);
            }

            PageArray<int> InitPageArrayIntFromBuffers(const sz _valuesBufferCount, int** const _pagesBuffer, const sz _pagesBufferCount)
            {
                const sz pageElemCount = _valuesBufferCount / _pagesBufferCount;
                DJO_ASSERT(IsPow2(pageElemCount));
                PageArray<int> pageArray = c_nullPageArray<int>;
                pageArray.pages = { { _pagesBuffer, _pagesBufferCount }, _pagesBufferCount };
                pageArray.pageElemCountPow2 = GetFirstBitIndex(pageElemCount);
                return pageArray;
            }

            bool IsFromZeroToN(const PageArray<int>& _pa)
            {
                const sz count = GetCount(_pa);
                for (sz i = 0; i < count; ++i)
                    if (*GetAt(_pa, i) != i)
                        return false;
                return true;
            }
        }

        void Tests_PageArray_Indexable(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            DJO_STATIC_UTEST(Indexable<PageArray<int>>);

            {
                PageArray<int> nullPageArray = c_nullPageArray<int>;
                DJO_UTEST(GetCapacity(nullPageArray) == 0);
                DJO_UTEST(GetCount(nullPageArray) == 0);
                DJO_UTEST(IsEmpty(nullPageArray) == true);
            }
            {
                constexpr sz valuesCount = 32;
                constexpr sz pagesCount = 4;
                int v[valuesCount];
                int* p[pagesCount];
                FillIntBufferFromZeroToN(v, valuesCount);
                InitPagesBuffer(v, valuesCount, p, pagesCount);
                PageArray<int> pa = InitPageArrayIntFromBuffers(valuesCount, p, pagesCount);
                pa.count = valuesCount;
                DJO_UTEST(GetCapacity(pa) == valuesCount);
                DJO_UTEST(GetCount(pa) == valuesCount);
                DJO_UTEST(IsEmpty(pa) == false);
            }
            {
                constexpr sz valuesCount = 32;
                constexpr sz pagesCount = 4;
                int v[valuesCount];
                int* p[pagesCount];
                FillIntBufferFromZeroToN(v, valuesCount);
                InitPagesBuffer(v, valuesCount, p, pagesCount);
                PageArray<int> pa = InitPageArrayIntFromBuffers(valuesCount, p, pagesCount);
                pa.count = valuesCount;

                DJO_UTEST(GetFirst(pa) != nullptr);
                DJO_UTEST(*GetFirst(pa) == 0);
                DJO_UTEST(GetLast(pa) != nullptr);
                DJO_UTEST(*GetLast(pa) == (valuesCount - 1));
                DJO_UTEST(IsFromZeroToN(pa));
            }

            DJO_UTESTS_CLOSE_SCOPE;
        }

        void Tests_PageArray_IndexVisitable(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            DJO_STATIC_UTEST(IndexVisitable<PageArray<int>>);

            {
                constexpr sz valuesCount = 32;
                constexpr sz pagesCount = 4;
                int v[valuesCount];
                int* p[pagesCount];
                FillIntBufferFromZeroToN(v, valuesCount);
                InitPagesBuffer(v, valuesCount, p, pagesCount);
                PageArray<int> pa = InitPageArrayIntFromBuffers(valuesCount, p, pagesCount);
                pa.count = valuesCount;

                Visit(pa, [DJO_UTESTS_LAMBDA_CAPTURE](const int _value, const sz _index)
                {
                    DJO_UTEST(_value == _index);
                    return EVisitorResult::Continue;
                });
            }
            {
                constexpr sz valuesCount = 32;
                constexpr sz pagesCount = 4;
                int v[valuesCount];
                int* p[pagesCount];
                InitPagesBuffer(v, valuesCount, p, pagesCount);
                PageArray<int> pa = InitPageArrayIntFromBuffers(valuesCount, p, pagesCount);
                pa.count = valuesCount;

                constexpr sz stopAtIndex = 10;
                sz lastIndexVisited = 0;
                Visit(pa, [&lastIndexVisited](const int, const sz _index)
                {
                    lastIndexVisited = _index;
                    return (_index == stopAtIndex) ? EVisitorResult::Break : EVisitorResult::Continue;
                });
                DJO_UTEST(lastIndexVisited == stopAtIndex);
            }

            DJO_UTESTS_CLOSE_SCOPE;
        }

        void Tests_PageArray_BackGrowable(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            DJO_STATIC_UTEST(BackGrowable<PageArray<int>>);

            {
                constexpr sz valuesCount = 32;
                constexpr sz pagesCount = 4;
                int v[valuesCount];
                int* p[pagesCount];
                InitPagesBuffer(v, valuesCount, p, pagesCount);
                PageArray<int> pa = InitPageArrayIntFromBuffers(valuesCount, p, pagesCount);

                DJO_UTEST(GetCount(pa) == 0);
                DJO_UTEST(IsEmpty(pa) == true);

                int value1 = 111;
                InsertBack(&pa, value1);
                DJO_UTEST(GetCount(pa) == 1);
                DJO_UTEST(IsEmpty(pa) == false);
                DJO_UTEST(*GetAt(pa, 0) == value1);

                int value2 = 222;
                InsertBack(&pa, value2);
                DJO_UTEST(GetCount(pa) == 2);
                DJO_UTEST(*GetAt(pa, 0) == value1);
                DJO_UTEST(*GetAt(pa, 1) == value2);
            }
            {
                constexpr sz chunkElemCount = 10;
                constexpr sz chunkCount = 3;
                constexpr sz valuesCount = 32;
                constexpr sz pagesCount = 4;
                int v[valuesCount];
                int added[valuesCount];
                int* p[pagesCount];
                ZeroMem(&v);
                FillIntBufferFromZeroToN(added, valuesCount);
                InitPagesBuffer(v, valuesCount, p, pagesCount);
                PageArray<int> pa = InitPageArrayIntFromBuffers(valuesCount, p, pagesCount);

                DJO_UTEST(GetCount(pa) == 0);
                DJO_UTEST(IsEmpty(pa) == true);

                for (sz chunk = 0; chunk < chunkCount; ++chunk)
                {
                    InsertBack(&pa, &added[chunk * chunkElemCount], chunkElemCount);
                    DJO_UTEST(GetCount(pa) == (chunk + 1) * chunkElemCount);
                    DJO_UTEST(IsEmpty(pa) == false);
                    for (sz i = 0; i < chunkElemCount; ++i)
                    {
                        const sz index = (chunk * chunkElemCount) + i;
                        DJO_UTEST(*GetAt(pa, index) == index);
                    }
                }
            }
            {
                constexpr sz valuesCount = 32;
                constexpr sz pagesCount = 4;
                int v[valuesCount];
                int* p[pagesCount];
                InitPagesBuffer(v, valuesCount, p, pagesCount);
                PageArray<int> pa = InitPageArrayIntFromBuffers(valuesCount, p, pagesCount);

                for (sz i = 0; i < valuesCount; ++i)
                    InsertBack(&pa, static_cast<int>(i));

                DJO_UTEST(IsFromZeroToN(pa));
            }

            DJO_UTESTS_CLOSE_SCOPE;
        }

        void Tests_PageArray_BackShrinkable(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            DJO_STATIC_UTEST(BackShrinkable<PageArray<int>>);

            {
                constexpr sz valuesCount = 32;
                constexpr sz pagesCount = 4;
                int v[valuesCount];
                int* p[pagesCount];
                FillIntBufferFromZeroToN(v, valuesCount);
                InitPagesBuffer(v, valuesCount, p, pagesCount);
                PageArray<int> pa = InitPageArrayIntFromBuffers(valuesCount, p, pagesCount);
                pa.count = valuesCount;

                DJO_UTEST(GetCapacity(pa) == valuesCount);
                DJO_UTEST(GetCount(pa) == valuesCount);
                DJO_UTEST(IsEmpty(pa) == false);

                EraseBack(&pa, 1);
                DJO_UTEST(GetCapacity(pa) == valuesCount);
                DJO_UTEST(GetCount(pa) == valuesCount - 1);
                DJO_UTEST(IsFromZeroToN(pa));

                EraseBack(&pa, 4);
                DJO_UTEST(GetCapacity(pa) == valuesCount);
                DJO_UTEST(GetCount(pa) == valuesCount - 5);
                DJO_UTEST(IsFromZeroToN(pa));

                EraseBack(&pa, 10);
                DJO_UTEST(GetCapacity(pa) == valuesCount);
                DJO_UTEST(GetCount(pa) == valuesCount - 15);
                DJO_UTEST(IsFromZeroToN(pa));

                EraseBack(&pa, GetCount(pa));
                DJO_UTEST(GetCapacity(pa) == valuesCount);
                DJO_UTEST(GetCount(pa) == 0);
                DJO_UTEST(IsEmpty(pa) == true);
            }

            DJO_UTESTS_CLOSE_SCOPE;
        }

        void Tests_PageArray_BackGrowableArena(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            DJO_STATIC_UTEST(BackGrowableArena<PageArray<int>>);

            Memory::Arena* const arena = DJO_UTESTS_CTX->userArena;

            {
                PageArray<int> pa = PushEmptyPageArray<int>(arena, 10, 10, 0);

                DJO_UTEST(GetCapacity(pa) == 0);
                DJO_UTEST(GetCount(pa) == 0);
                DJO_UTEST(IsEmpty(pa) == true);

                int value1 = 111;
                InsertBack(&pa, arena, value1);
                DJO_UTEST(GetCount(pa) == 1);
                DJO_UTEST(GetCapacity(pa) >= GetCount(pa));
                DJO_UTEST(IsEmpty(pa) == false);
                DJO_UTEST(*GetAt(pa, 0) == value1);

                int value2 = 222;
                InsertBack(&pa, arena, value2);
                DJO_UTEST(GetCount(pa) == 2);
                DJO_UTEST(GetCapacity(pa) >= GetCount(pa));
                DJO_UTEST(*GetAt(pa, 0) == value1);
                DJO_UTEST(*GetAt(pa, 1) == value2);
            }
            {
                constexpr sz chunkElemCount = 5;
                constexpr sz chunkCount = 3;
                constexpr sz arrayCount = chunkElemCount * chunkCount;
                int added[arrayCount];
                FillIntBufferFromZeroToN(added, arrayCount);
                PageArray<int> pa = PushEmptyPageArray<int>(arena, 10, 10, 0);

                for (sz chunk = 0; chunk < chunkCount; ++chunk)
                {
                    InsertBack(&pa, arena, &added[chunk * chunkElemCount], chunkElemCount);
                    DJO_UTEST(GetCount(pa) == (chunk + 1) * chunkElemCount);
                    DJO_UTEST(GetCapacity(pa) >= GetCount(pa));
                    DJO_UTEST(IsEmpty(pa) == false);
                    for (sz i = 0; i < chunkElemCount; ++i)
                    {
                        const sz index = (chunk * chunkElemCount) + i;
                        DJO_UTEST(*GetAt(pa, index) == index);
                    }
                }
            }
            {
                constexpr sz arrayCount = 25;
                PageArray<int> pa = PushEmptyPageArray<int>(arena, 10, 10, 0);

                for (sz i = 0; i < arrayCount; ++i)
                    InsertBack(&pa, arena, static_cast<int>(i));

                DJO_UTEST(GetCapacity(pa) >= GetCount(pa));
                DJO_UTEST(IsFromZeroToN(pa));
            }

            DJO_UTESTS_CLOSE_SCOPE;
        }

        void Tests_PageArray(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            Tests_PageArray_Indexable(DJO_UTESTS_PARAMS);
            Tests_PageArray_IndexVisitable(DJO_UTESTS_PARAMS);
            Tests_PageArray_BackGrowable(DJO_UTESTS_PARAMS);
            Tests_PageArray_BackShrinkable(DJO_UTESTS_PARAMS);
            Tests_PageArray_BackGrowableArena(DJO_UTESTS_PARAMS);

            DJO_UTESTS_CLOSE_SCOPE;
        }
    }
}
