#pragma once

#include <DjobiCore/Core/Core.hpp>
#include <DjobiCore/Core/Types.hpp>


namespace Djo
{
    namespace Tests
    {
        DJO_INLINE void FillIntBufferFromZeroToN(int* const _buffer, const sz _bufferCount) noexcept
        {
            for (sz i = 0; i < _bufferCount; ++i)
                _buffer[i] = static_cast<int>(i);
        }
    }
}
