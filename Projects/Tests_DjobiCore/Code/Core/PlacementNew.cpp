#include "Tests_DjobiCore_pch.h"

#include <DjobiTests/Context.hpp>
#include <DjobiTests/Tests.hpp>
#include <DjobiTests/Utils.hpp>

#include <DjobiCore/Core/PlacementNew.hpp>


namespace Djo
{
    namespace Tests
    {
        struct NumWrapper
        {
            DJO_NO_DEFAULT_CTOR(NumWrapper);
            DJO_NO_COPY_NO_MOVE(NumWrapper);

        public:
            NumWrapper(const i32 _i, const f32 _f) noexcept : i{ _i }, f{ _f } {}
            ~NumWrapper() noexcept { i = 0; f = 0.f; }

            i32 i = 0;
            f32 f = 0;
        };

        void Tests_PlacementNew(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            constexpr u32 c_range{ 10 };
            char buffer[256]{ 0 };
            void* ptr = (char*)buffer;

            StaticCount::ZeroCounts();
            ::Djo::_Internal::ConstructInPlace<StaticCount>(ptr);
            DJO_UTEST_STATIC_COUNTS(1, 0, 0, 0, 0, 0, 0);
            ::Djo::_Internal::Destroy<StaticCount>(ptr);
            DJO_UTEST_STATIC_COUNTS(1, 0, 0, 0, 0, 0, 1);

            StaticCount::ZeroCounts();
            ::Djo::_Internal::ConstructRangeInPlace<StaticCount>(ptr, c_range);
            DJO_UTEST_STATIC_COUNTS(c_range, 0, 0, 0, 0, 0, 0);
            ::Djo::_Internal::DestroyRange<StaticCount>(ptr, c_range);
            DJO_UTEST_STATIC_COUNTS(c_range, 0, 0, 0, 0, 0, c_range);

            constexpr i32 c_i{ 11 };
            constexpr f32 c_f{ 12.f };
            NumWrapper* const p = static_cast<NumWrapper*>(ptr);

            ::memset(buffer, 0, ArrayCount(buffer));
            DJO_UTEST(p->i == 0 && p->f == 0.f);
            ::Djo::_Internal::ConstructInPlace<NumWrapper>(ptr, c_i, c_f);
            DJO_UTEST(p->i == c_i && p->f == c_f);
            ::Djo::_Internal::Destroy<NumWrapper>(ptr);
            DJO_UTEST(p->i == 0 && p->f == 0.f);

            ::memset(buffer, 0, ArrayCount(buffer));
            for (u32 i = 0; i < c_range; ++i)
                DJO_UTEST(p[i].i == 0 && p[i].f == 0.f);
            ::Djo::_Internal::ConstructRangeInPlace<NumWrapper>(ptr, c_range, c_i, c_f);
            for (u32 i = 0; i < c_range; ++i)
                DJO_UTEST(p[i].i == c_i && p[i].f == c_f);
            ::Djo::_Internal::DestroyRange<NumWrapper>(ptr, c_range);
            for (u32 i = 0; i < c_range; ++i)
                DJO_UTEST(p[i].i == 0 && p[i].f == 0.f);

            DJO_UTESTS_CLOSE_SCOPE;
        }
    }
}
