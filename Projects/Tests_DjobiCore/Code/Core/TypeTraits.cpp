#include "Tests_DjobiCore_pch.h"

#include <DjobiTests/Context.hpp>
#include <DjobiTests/Tests.hpp>

#include <DjobiCore/Core/TypeTraits.hpp>


namespace Djo
{
    namespace Tests
    {
        struct POD1 {};
        struct POD2 { int i; };
        struct POD3 { POD1 p; };
        struct POD4 : public POD3 { float f; };
        struct POD5 { static float f; };
        struct POD6 { static POD3 pp; };
        struct POD7 { DJO_ALL_DEFAULT(POD7); int i; };
        struct POD8 { ~POD8() { i = 12; } static int i; };

        struct NonPOD1 { virtual ~NonPOD1() = 0; };
        struct NonPOD2 : NonPOD1 {};

        void Tests_TypeTraits_Metaprogramming(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            DJO_STATIC_UTEST( c_isTrivial<POD1>);
            DJO_STATIC_UTEST( c_isStdLayout<POD1>);
            DJO_STATIC_UTEST( c_isTrivial<POD2>);
            DJO_STATIC_UTEST( c_isStdLayout<POD2>);
            DJO_STATIC_UTEST( c_isTrivial<POD3>);
            DJO_STATIC_UTEST( c_isStdLayout<POD3>);
            DJO_STATIC_UTEST( c_isTrivial<POD4>);
            DJO_STATIC_UTEST(!c_isStdLayout<POD4>);
            DJO_STATIC_UTEST( c_isTrivial<POD5>);
            DJO_STATIC_UTEST( c_isStdLayout<POD5>);
            DJO_STATIC_UTEST( c_isTrivial<POD6>);
            DJO_STATIC_UTEST( c_isStdLayout<POD6>);
            DJO_STATIC_UTEST( c_isTrivial<POD7>);
            DJO_STATIC_UTEST( c_isStdLayout<POD7>);
            DJO_STATIC_UTEST(!c_isTrivial<POD8>);
            DJO_STATIC_UTEST( c_isStdLayout<POD8>);

            DJO_STATIC_UTEST(!c_isTrivial<NonPOD1>);
            DJO_STATIC_UTEST(!c_isStdLayout<NonPOD1>);
            DJO_STATIC_UTEST(!c_isTrivial<NonPOD2>);
            DJO_STATIC_UTEST(!c_isStdLayout<NonPOD2>);

            DJO_STATIC_ASSERT(DJO_NOOP( c_isAnyOf<bool, bool>));
            DJO_STATIC_ASSERT(DJO_NOOP(!c_isAnyOf<bool, int>));
            DJO_STATIC_ASSERT(DJO_NOOP( c_isAnyOf<bool, bool, int>));
            DJO_STATIC_ASSERT(DJO_NOOP( c_isAnyOf<bool, int, bool>));
            DJO_STATIC_ASSERT(DJO_NOOP(!c_isAnyOf<bool, int, int>));
            DJO_STATIC_ASSERT(DJO_NOOP(!c_isAnyOf<bool, int, bool*>));

            DJO_STATIC_ASSERT(DJO_NOOP( std::is_same_v<bool, PlainType_t<bool>>));
            DJO_STATIC_ASSERT(DJO_NOOP( std::is_same_v<bool, PlainType_t<bool&>>));
            DJO_STATIC_ASSERT(DJO_NOOP( std::is_same_v<bool, PlainType_t<bool&&>>));
            DJO_STATIC_ASSERT(DJO_NOOP( std::is_same_v<bool, PlainType_t<bool*>>));
            DJO_STATIC_ASSERT(DJO_NOOP( std::is_same_v<bool, PlainType_t<bool*>>));
            DJO_STATIC_ASSERT(DJO_NOOP( std::is_same_v<bool, PlainType_t<bool[]>>));
            DJO_STATIC_ASSERT(DJO_NOOP(!std::is_same_v<bool, PlainType_t<const bool>>));
            DJO_STATIC_ASSERT(DJO_NOOP(!std::is_same_v<bool, PlainType_t<int>>));

            DJO_STATIC_ASSERT(DJO_NOOP( std::is_same_v<bool, PureType_t<const bool>>));
            DJO_STATIC_ASSERT(DJO_NOOP( std::is_same_v<bool, PureType_t<volatile bool>>));
            DJO_STATIC_ASSERT(DJO_NOOP( std::is_same_v<bool, PureType_t<const bool&>>));
            DJO_STATIC_ASSERT(DJO_NOOP( std::is_same_v<bool, PureType_t<const bool*>>));
            DJO_STATIC_ASSERT(DJO_NOOP(!std::is_same_v<bool, PureType_t<int>>));

            DJO_STATIC_ASSERT(c_canUseMemcpy<bool>);

            DJO_UTESTS_CLOSE_SCOPE;
        }

        enum class E1 { a, DJO_ENUM_COUNT_INVALID };
        enum class E2 { a, b, DJO_ENUM_COUNT_INVALID };
        enum class F1 : u32 { a = Flag(0), DJO_ENUM_FLAG_COUNT };
        enum class F2 : u32 { a = Flag(0), b = Flag(1), DJO_ENUM_FLAG_COUNT };

        void Tests_TypeTraits_Enum(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            DJO_STATIC_UTEST(ECount<E1> == 1);
            DJO_STATIC_UTEST(ECount<E1> == EInvalid<E1>);
            DJO_STATIC_UTEST(ECount<E2> == 2);
            DJO_STATIC_UTEST(ECount<E2> == EInvalid<E2>);
            DJO_STATIC_UTEST(EFlagCount<F1> == 1);
            DJO_STATIC_UTEST(EFlagCount<F2> == 2);

            DJO_UTESTS_CLOSE_SCOPE;
        }

        void Tests_TypeTraits(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            Tests_TypeTraits_Metaprogramming(DJO_UTESTS_PARAMS);
            Tests_TypeTraits_Enum(DJO_UTESTS_PARAMS);

            DJO_UTESTS_CLOSE_SCOPE;
        }
    }
}
