#include "Tests_DjobiCore_pch.h"

#include <DjobiTests/Context.hpp>
#include <DjobiTests/Tests.hpp>

#include <DjobiCore/Core/Random.hpp>
#include <DjobiCore/Core/Buffer.hpp>


namespace Djo
{
    namespace Tests
    {
        DJO_INLINE u32 PositiveDiff(const u32 _a, const u32 _b) noexcept
        {
            return (_a > _b) ? (_a - _b) : (_b - _a);
        }

        u32 GetMaxPositiveDiff(const Buffer<const u32>& _gen, const u32 _average) noexcept
        {
            u32 maxPositiveDiff = 0;
            const sz count = _gen.count;
            for (u32 i = 0; i < count; ++i)
                maxPositiveDiff = Max(PositiveDiff(_gen.data[i], _average), maxPositiveDiff);
            return maxPositiveDiff;
        }

        using PartIdGenerator = u32(*)(FastRandom::State*, u32);
        template <u32 NPartCount>
        void GenerateAndTest(DJO_UTESTS_ARGS, FastRandom::State* const _r, PartIdGenerator _partIdGenerator) noexcept
        {
            constexpr u32 c_genCount{ NPartCount * 10000 };
            constexpr u32 c_average{ c_genCount / NPartCount };
            constexpr u32 c_expectedMaxDiff = { (c_average * 5) / 100 }; // 5%, arbitrary

            u32 genParts[NPartCount]{ 0 };
            for (u32 i = 0; i < c_genCount; ++i)
                ++genParts[_partIdGenerator(_r, NPartCount)];

            const u32 maxDiff = GetMaxPositiveDiff(ToBuffer(genParts), c_average);
            DJO_UTEST(maxDiff <= c_expectedMaxDiff);
        }

        void Tests_FastRandom(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            FastRandom::State r = FastRandom::MoldState();
            GenerateAndTest<4>(DJO_UTESTS_PARAMS, &r,   [](FastRandom::State* const _r, const u32 _partCount) { return Random::U8(_r) % _partCount; });
            GenerateAndTest<16>(DJO_UTESTS_PARAMS, &r,  [](FastRandom::State* const _r, const u32 _partCount) { return Random::U16(_r) % _partCount; });
            GenerateAndTest<64>(DJO_UTESTS_PARAMS, &r,  [](FastRandom::State* const _r, const u32 _partCount) { return Random::U32(_r) % _partCount; });
            GenerateAndTest<256>(DJO_UTESTS_PARAMS, &r, [](FastRandom::State* const _r, const u32 _partCount) { return (u32)(Random::U64(_r) % _partCount); });
            GenerateAndTest<64>(DJO_UTESTS_PARAMS, &r,  [](FastRandom::State* const _r, const u32 _partCount) { return (u32)(Random::F32(_r) * _partCount); });
            GenerateAndTest<256>(DJO_UTESTS_PARAMS, &r, [](FastRandom::State* const _r, const u32 _partCount) { return (u32)(Random::F64(_r) * _partCount); });

            DJO_UTESTS_CLOSE_SCOPE;
        }

        void Tests_Random(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            //Tests_FastRandom(DJO_UTESTS_PARAMS); // quite long to process

            DJO_UTESTS_CLOSE_SCOPE;
        }
    }
}
