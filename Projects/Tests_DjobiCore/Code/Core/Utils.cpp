#include "Tests_DjobiCore_pch.h"

#include <DjobiTests/Context.hpp>
#include <DjobiTests/Tests.hpp>

#include <math.h>


namespace Djo
{
    namespace Tests
    {
        void Tests_Utils_Numeric(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            DJO_STATIC_UTEST(GetFirstBitIndex(1u) == 0u);
            DJO_STATIC_UTEST(GetFirstBitIndex(2u) == 1u);
            DJO_STATIC_UTEST(GetFirstBitIndex(3u) == 0u);
            DJO_STATIC_UTEST(GetFirstBitIndex(4u) == 2u);
            DJO_STATIC_UTEST(GetFirstBitIndex(5u) == 0u);

            //DJO_STATIC_UTEST(Align(0u, 1u) == 1u); // Issues with base value == 0
            DJO_STATIC_UTEST(Align(1u, 1u) == 1u);
            DJO_STATIC_UTEST(Align(2u, 1u) == 2u);
            //DJO_STATIC_UTEST(Align(0u, 2u) == 2u);
            DJO_STATIC_UTEST(Align(1u, 2u) == 2u);
            DJO_STATIC_UTEST(Align(2u, 2u) == 2u);
            DJO_STATIC_UTEST(Align(3u, 2u) == 4u);
            //DJO_STATIC_UTEST(Align(0u, 3u) == 3u);
            DJO_STATIC_UTEST(Align(1u, 3u) == 3u);
            DJO_STATIC_UTEST(Align(2u, 3u) == 3u);
            DJO_STATIC_UTEST(Align(3u, 3u) == 3u);
            DJO_STATIC_UTEST(Align(4u, 3u) == 6u);
            DJO_STATIC_UTEST(Align(5u, 3u) == 6u);

            DJO_STATIC_UTEST( IsPow2(0u));
            DJO_STATIC_UTEST( IsPow2(1u));
            DJO_STATIC_UTEST( IsPow2(2u));
            DJO_STATIC_UTEST(!IsPow2(3u));
            DJO_STATIC_UTEST( IsPow2(4u));
            DJO_STATIC_UTEST(!IsPow2(5u));

            //DJO_STATIC_UTEST(AlignPow2(0u, 1u) == Align(0u, 1u)); // Issues with base value == 0
            DJO_STATIC_UTEST(AlignPow2(1u, 1u) == Align(1u, 1u));
            DJO_STATIC_UTEST(AlignPow2(2u, 1u) == Align(2u, 1u));
            //DJO_STATIC_UTEST(AlignPow2(0u, 2u) == Align(0u, 2u));
            DJO_STATIC_UTEST(AlignPow2(1u, 2u) == Align(1u, 2u));
            DJO_STATIC_UTEST(AlignPow2(2u, 2u) == Align(2u, 2u));
            DJO_STATIC_UTEST(AlignPow2(3u, 2u) == Align(3u, 2u));
            //DJO_STATIC_UTEST(AlignPow2(0u, 4u) == Align(0u, 4u));
            DJO_STATIC_UTEST(AlignPow2(1u, 4u) == Align(1u, 4u));
            DJO_STATIC_UTEST(AlignPow2(2u, 4u) == Align(2u, 4u));
            DJO_STATIC_UTEST(AlignPow2(3u, 4u) == Align(3u, 4u));
            DJO_STATIC_UTEST(AlignPow2(4u, 4u) == Align(4u, 4u));
            DJO_STATIC_UTEST(AlignPow2(5u, 4u) == Align(5u, 4u));

            DJO_STATIC_UTEST(AlignFloor(0u, 1u) == 0u);
            DJO_STATIC_UTEST(AlignFloor(1u, 1u) == 1u);
            DJO_STATIC_UTEST(AlignFloor(2u, 1u) == 2u);
            DJO_STATIC_UTEST(AlignFloor(0u, 2u) == 0u);
            DJO_STATIC_UTEST(AlignFloor(1u, 2u) == 0u);
            DJO_STATIC_UTEST(AlignFloor(2u, 2u) == 2u);
            DJO_STATIC_UTEST(AlignFloor(3u, 2u) == 2u);
            DJO_STATIC_UTEST(AlignFloor(0u, 3u) == 0u);
            DJO_STATIC_UTEST(AlignFloor(1u, 3u) == 0u);
            DJO_STATIC_UTEST(AlignFloor(2u, 3u) == 0u);
            DJO_STATIC_UTEST(AlignFloor(3u, 3u) == 3u);
            DJO_STATIC_UTEST(AlignFloor(4u, 3u) == 3u);
            DJO_STATIC_UTEST(AlignFloor(5u, 3u) == 3u);
            DJO_STATIC_UTEST(AlignFloor(6u, 3u) == 6u);

            DJO_STATIC_UTEST(AlignFloorPow2(0u, 1u) == AlignFloor(0u, 1u));
            DJO_STATIC_UTEST(AlignFloorPow2(1u, 1u) == AlignFloor(1u, 1u));
            DJO_STATIC_UTEST(AlignFloorPow2(2u, 1u) == AlignFloor(2u, 1u));
            DJO_STATIC_UTEST(AlignFloorPow2(0u, 2u) == AlignFloor(0u, 2u));
            DJO_STATIC_UTEST(AlignFloorPow2(1u, 2u) == AlignFloor(1u, 2u));
            DJO_STATIC_UTEST(AlignFloorPow2(2u, 2u) == AlignFloor(2u, 2u));
            DJO_STATIC_UTEST(AlignFloorPow2(3u, 2u) == AlignFloor(3u, 2u));
            DJO_STATIC_UTEST(AlignFloorPow2(0u, 4u) == AlignFloor(0u, 4u));
            DJO_STATIC_UTEST(AlignFloorPow2(1u, 4u) == AlignFloor(1u, 4u));
            DJO_STATIC_UTEST(AlignFloorPow2(2u, 4u) == AlignFloor(2u, 4u));
            DJO_STATIC_UTEST(AlignFloorPow2(3u, 4u) == AlignFloor(3u, 4u));
            DJO_STATIC_UTEST(AlignFloorPow2(4u, 4u) == AlignFloor(4u, 4u));
            DJO_STATIC_UTEST(AlignFloorPow2(5u, 4u) == AlignFloor(5u, 4u));

            //DJO_STATIC_UTEST(CeilPow2(0u) == 0u); // Issues with base value == 0
            DJO_STATIC_UTEST(CeilPow2(1u) == 1u);
            DJO_STATIC_UTEST(CeilPow2(2u) == 2u);
            DJO_STATIC_UTEST(CeilPow2(3u) == 4u);
            DJO_STATIC_UTEST(CeilPow2(4u) == 4u);
            DJO_STATIC_UTEST(CeilPow2(5u) == 8u);

            DJO_STATIC_UTEST(SignOf(0)     ==  0);
            DJO_STATIC_UTEST(SignOf(1)     ==  1);
            DJO_STATIC_UTEST(SignOf(-1)    == -1);
            DJO_STATIC_UTEST(SignOf(3)     ==  1);
            DJO_STATIC_UTEST(SignOf(-3)    == -1);
            DJO_STATIC_UTEST(SignOf(0.3f)  ==  1.f);
            DJO_STATIC_UTEST(SignOf(-0.3f) == -1.f);
            DJO_STATIC_UTEST(SignOf(3.)    ==  1.);
            DJO_STATIC_UTEST(SignOf(-3.)   == -1.);

            // from https://en.cppreference.com/w/cpp/types/numeric_limits/epsilon
            const f64 point2 = 0.2;
            const f64 sqrt5 = 1.0 / ::sqrt(5.0) / ::sqrt(5.0);
            DJO_UTEST(point2 != sqrt5);
            DJO_UTEST(Equals(point2, sqrt5));

            DJO_UTESTS_CLOSE_SCOPE;
        }

        void Tests_Utils_Algorthms(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            DJO_STATIC_UTEST(Find<char>("",       0, 'c') == c_npos);
            DJO_STATIC_UTEST(Find<char>("coucou", 6, 'c') == 0);
            DJO_STATIC_UTEST(Find<char>("coucou", 6, 'o') == 1);
            DJO_STATIC_UTEST(Find<char>("coucou", 6, 'u') == 2);
            DJO_STATIC_UTEST(Find<char>("coucou", 6, 'z') == c_npos);

            DJO_STATIC_UTEST(FindLast<char>("",      0, 'h') == c_npos);
            DJO_STATIC_UTEST(FindLast<char>("hello", 5, 'h') == 0);
            DJO_STATIC_UTEST(FindLast<char>("hello", 5, 'e') == 1);
            DJO_STATIC_UTEST(FindLast<char>("hello", 5, 'l') == 3);
            DJO_STATIC_UTEST(FindLast<char>("hello", 5, 'o') == 4);
            DJO_STATIC_UTEST(FindLast<char>("hello", 5, 'z') == c_npos);

            DJO_UTESTS_CLOSE_SCOPE;
        }

        void Tests_Utils_String(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            DJO_STATIC_UTEST(StrLen("")      == 0);
            DJO_STATIC_UTEST(StrLen("hello") == 5);

            DJO_STATIC_UTEST(StrNCmp("",      "", 0)       == 0);
            DJO_STATIC_UTEST(StrNCmp("",      "", 10)      == 0);
            DJO_STATIC_UTEST(StrNCmp("hello", "", 0)       == 0);
            DJO_STATIC_UTEST(StrNCmp("",      "hello", 0)  == 0);
            DJO_STATIC_UTEST(StrNCmp("hello", "hello", 5)  == 0);
            DJO_STATIC_UTEST(StrNCmp("hello", "hello", 10) == 0);
            DJO_STATIC_UTEST(StrNCmp("hello", "hello", 5)  == 0);
            DJO_STATIC_UTEST(StrNCmp("helli", "hello", 5)  == -1);
            DJO_STATIC_UTEST(StrNCmp("hello", "helli", 5)  == 1);
            DJO_STATIC_UTEST(StrNCmp("hello", "helli", 4)  == 0);

            DJO_STATIC_UTEST(TStrNChrI("",      0,  'a', StrNChr)  == c_npos);
            DJO_STATIC_UTEST(TStrNChrI("",      10, 'a', StrNChr)  == c_npos);
            DJO_STATIC_UTEST(TStrNChrI("hello", 0,  'a', StrNChr)  == c_npos);
            DJO_STATIC_UTEST(TStrNChrI("hello", 10, 'a', StrNChr)  == c_npos);
            DJO_STATIC_UTEST(TStrNChrI("hello", 1,  'l', StrNChr)  == c_npos);
            DJO_STATIC_UTEST(TStrNChrI("hello", 10, 'l', StrNChr)  == 2);
            DJO_STATIC_UTEST(TStrNChrI("",      0,  'a', StrNRChr) == c_npos);
            DJO_STATIC_UTEST(TStrNChrI("",      10, 'a', StrNRChr) == c_npos);
            DJO_STATIC_UTEST(TStrNChrI("hello", 0,  'a', StrNRChr) == c_npos);
            DJO_STATIC_UTEST(TStrNChrI("hello", 10, 'a', StrNRChr) == c_npos);
            DJO_STATIC_UTEST(TStrNChrI("hello", 1,  'l', StrNRChr) == c_npos);
            DJO_STATIC_UTEST(TStrNChrI("hello", 10, 'l', StrNRChr) == 3);

            DJO_UTESTS_CLOSE_SCOPE;
        }

        void Tests_Utils(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            Tests_Utils_Numeric(DJO_UTESTS_PARAMS);
            Tests_Utils_Algorthms(DJO_UTESTS_PARAMS);
            Tests_Utils_String(DJO_UTESTS_PARAMS);

            DJO_UTESTS_CLOSE_SCOPE;
        }
    }
}
