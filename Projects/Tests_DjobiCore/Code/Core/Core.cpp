#include "Tests_DjobiCore_pch.h"

#include <DjobiTests/Context.hpp>
#include <DjobiTests/Tests.hpp>


namespace Djo
{
    namespace Tests
    {
        void Tests_Core_Macros(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            const char* s = DJO_STR(coucou);
            DJO_UTEST(::strncmp(s, "coucou", 7) == 0);
            s = DJO_STR(cou   cou);
            DJO_UTEST(::strncmp(s, "cou cou", 8) == 0);
            s = DJO_STR(   coucou);
            DJO_UTEST(::strncmp(s, "coucou", 7) == 0);
            s = DJO_STR(coucou   );
            DJO_UTEST(::strncmp(s, "coucou", 7) == 0);

            DJO_CAT(unsig, ned int) u0;
            DJO_CAT(DJO_CAT(u, DJO_CAT(ns, ign)), DJO_CAT(e, d int)) u1;
            DJO_UNUSED(u0); DJO_UNUSED(u1);

            u32 n;

            DJO_TODO("Fix DJO_ARG_COUNT(), does not work as intended");
            n = DJO_ARG_COUNT();
            //DJO_UTEST(n == 0);

            n = DJO_ARG_COUNT(n);
            DJO_UTEST(n == 1);

            n = DJO_ARG_COUNT(a, b);
            DJO_UTEST(n == 2);

            n = DJO_ARG_COUNT(a, b, c, d, e, f);
            DJO_UTEST(n == 6);

            #define DJO_PLUS(a_) a_ +
            n = DJO_APPLY(DJO_PLUS, 1, 2, 3, 4) 5;
            DJO_UTEST(n == 1 + 2 + 3 + 4 + 5);
            #undef DJO_PLUS

            DJO_UTESTS_CLOSE_SCOPE;
        }

        void Tests_Core_Utils(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            DJO_STATIC_UTEST(Mask(0u)  == 0u);
            DJO_STATIC_UTEST(Mask(1u)  == 0x1u);
            DJO_STATIC_UTEST(Mask(4u)  == 0xfu);
            DJO_STATIC_UTEST(Mask(31u) == 0x7fffffffu);

            DJO_STATIC_UTEST(!TestFlag(0u, 0u));
            DJO_STATIC_UTEST(!TestFlag(0u, 1u));
            DJO_STATIC_UTEST(!TestFlag(1u, 0u));
            DJO_STATIC_UTEST( TestFlag(1u, 1u));
            DJO_STATIC_UTEST(!TestFlag(1u, 3u));
            DJO_STATIC_UTEST( TestFlag(3u, 1u));
            DJO_STATIC_UTEST( TestFlag(3u, 3u));

            DJO_STATIC_UTEST(!TestAnyFlag(0u, 0u));
            DJO_STATIC_UTEST(!TestAnyFlag(0u, 1u));
            DJO_STATIC_UTEST(!TestAnyFlag(1u, 0u));
            DJO_STATIC_UTEST( TestAnyFlag(1u, 1u));
            DJO_STATIC_UTEST( TestAnyFlag(1u, 3u));
            DJO_STATIC_UTEST( TestAnyFlag(3u, 1u));
            DJO_STATIC_UTEST( TestAnyFlag(3u, 3u));

            DJO_STATIC_UTEST((Version{ 1, 0, 0 } > Version{ 0, 1, 0 }));
            DJO_STATIC_UTEST((Version{ 0, 1, 0 } > Version{ 0, 0, 1 }));
            DJO_STATIC_UTEST((Version{ 0, 0, 1 } > Version{ 0, 0, 0 }));
            DJO_STATIC_UTEST((Version{ 2, 0, 0 } > Version{ 1, 9, 9 }));
            DJO_STATIC_UTEST((Version{ 0, 2, 0 } > Version{ 0, 1, 9 }));
            DJO_STATIC_UTEST((Version{ 0, 0, 2 } > Version{ 0, 0, 1 }));

            DJO_UTESTS_CLOSE_SCOPE;
        }

        void Tests_Core(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            Tests_Core_Macros(DJO_UTESTS_PARAMS);
            Tests_Core_Utils(DJO_UTESTS_PARAMS);

            DJO_UTESTS_CLOSE_SCOPE;
        }
    }
}
