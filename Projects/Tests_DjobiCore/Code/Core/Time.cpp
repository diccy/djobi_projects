#include "Tests_DjobiCore_pch.h"

#include <DjobiTests/Context.hpp>
#include <DjobiTests/Tests.hpp>

#include <DjobiCore/Core/Time.hpp>
#include <DjobiCore/Core/Utils.hpp>


namespace Djo
{
    namespace Tests
    {
        void Tests_Time_Convert(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            const auto Test_1ms = [DJO_UTESTS_LAMBDA_CAPTURE](const auto _t)
            {
                auto s = ConvertTime<SecondsT<f64>>(_t);
                auto ms = ConvertTime<MillisecondsT<f32>>(_t);
                auto us = ConvertTime<MicrosecondsT<i32>>(_t);
                auto ns = ConvertTime<NanosecondsT<u64>>(_t);
                DJO_UTEST(Equals(s.t, 0.001));
                DJO_UTEST(Equals(ms.t, 1.f));
                DJO_UTEST(us.t == 1000);
                DJO_UTEST(ns.t == 1000000);
            };

            Test_1ms(SecondsT<f64>{ 0.001 });
            Test_1ms(MillisecondsT<f32>{ 1.f });
            Test_1ms(MicrosecondsT<i32>{ 1000 });
            Test_1ms(NanosecondsT<u64>{ 1000000 });

            const auto Test_02ms = [DJO_UTESTS_LAMBDA_CAPTURE](const auto _t)
            {
                auto s = ConvertTime<SecondsT<f64>>(_t);
                auto ms = ConvertTime<MillisecondsT<f32>>(_t);
                auto us = ConvertTime<MicrosecondsT<i32>>(_t);
                auto ns = ConvertTime<NanosecondsT<u64>>(_t);
                DJO_UTEST(Equals(s.t, 0.0002, 0.000001));
                DJO_UTEST(Equals(ms.t, 0.2f));
                DJO_UTEST(us.t == 200);
                DJO_UTEST(ns.t == 200000);
            };

            Test_02ms(SecondsT<f64>{ 0.0002 });
            Test_02ms(MillisecondsT<f32>{ 0.2f });
            Test_02ms(MicrosecondsT<i32>{ 200 });
            Test_02ms(NanosecondsT<u64>{ 200000 });

            const auto Test_5555ms = [DJO_UTESTS_LAMBDA_CAPTURE](const auto _t)
            {
                auto s = ConvertTime<SecondsT<f64>>(_t);
                auto ms = ConvertTime<MillisecondsT<f32>>(_t);
                auto us = ConvertTime<MicrosecondsT<i32>>(_t);
                auto ns = ConvertTime<NanosecondsT<u64>>(_t);
                DJO_UTEST(Equals(s.t, 5.555));
                DJO_UTEST(Equals(ms.t, 5555.f));
                DJO_UTEST(us.t == 5555000);
                DJO_UTEST(ns.t == 5555000000);
            };

            Test_5555ms(SecondsT<f64>{ 5.555 });
            Test_5555ms(MillisecondsT<f32>{ 5555.f });
            Test_5555ms(MicrosecondsT<i32>{ 5555000 });
            Test_5555ms(NanosecondsT<u64>{ 5555000000 });

            DJO_UTESTS_CLOSE_SCOPE;
        }

        void Tests_Time_MainStructs(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            Seconds s{ 0. };
            Milliseconds ms{ 0. };
            Microseconds us{ 0. };

            DJO_UTEST(s.t == 0.);
            DJO_UTEST(s.t == ms.t);
            DJO_UTEST(s.t == us.t);

            s.t = 1.;
            ms.t = 1000.;
            us.t = 1000000.;

            DJO_UTEST(Equals(Sec(s).t, 1.));
            DJO_UTEST(Equals(Sec(ms).t, 1.));
            DJO_UTEST(Equals(Sec(us).t, 1.));
            DJO_UTEST(Equals(Ms(s).t, 1000.));
            DJO_UTEST(Equals(Ms(ms).t, 1000.));
            DJO_UTEST(Equals(Ms(us).t, 1000.));
            DJO_UTEST(Equals(Us(s).t, 1000000.));
            DJO_UTEST(Equals(Us(ms).t, 1000000.));
            DJO_UTEST(Equals(Us(us).t, 1000000.));

            s.t = 0.003;
            ms.t = 3.;
            us.t = 3000.;

            DJO_UTEST(Equals(Sec(s).t, 0.003));
            DJO_UTEST(Equals(Sec(ms).t, 0.003));
            DJO_UTEST(Equals(Sec(us).t, 0.003));
            DJO_UTEST(Equals(Ms(s).t, 3.));
            DJO_UTEST(Equals(Ms(ms).t, 3.));
            DJO_UTEST(Equals(Ms(us).t, 3.));
            DJO_UTEST(Equals(Us(s).t, 3000.));
            DJO_UTEST(Equals(Us(ms).t, 3000.));
            DJO_UTEST(Equals(Us(us).t, 3000.));

            s.t = 0.000005;
            ms.t = 0.005;
            us.t = 5.;

            DJO_UTEST(Equals(Sec(s).t, 0.000005));
            DJO_UTEST(Equals(Sec(ms).t, 0.000005));
            DJO_UTEST(Equals(Sec(us).t, 0.000005));
            DJO_UTEST(Equals(Ms(s).t, 0.005));
            DJO_UTEST(Equals(Ms(ms).t, 0.005));
            DJO_UTEST(Equals(Ms(us).t, 0.005));
            DJO_UTEST(Equals(Us(s).t, 5.));
            DJO_UTEST(Equals(Us(ms).t, 5.));
            DJO_UTEST(Equals(Us(us).t, 5.));

            s.t = 50000.;

            DJO_UTEST(Equals(Sec(s).t, 50000.));
            DJO_UTEST(Equals(Ms(s).t, 50000000.));
            DJO_UTEST(Equals(Us(s).t, 50000000000.));

            DJO_UTESTS_CLOSE_SCOPE;
        }

        void Tests_Time(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            Tests_Time_Convert(DJO_UTESTS_PARAMS);
            Tests_Time_MainStructs(DJO_UTESTS_PARAMS);

            DJO_UTESTS_CLOSE_SCOPE;
        }
    }
}
