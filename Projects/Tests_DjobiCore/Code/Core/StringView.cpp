#include "Tests_DjobiCore_pch.h"

#include <DjobiTests/Context.hpp>
#include <DjobiTests/Tests.hpp>

#include <DjobiCore/Core/StringView.hpp>


namespace Djo
{
    namespace Tests
    {
        #define DJO_TEST_LITTERAL_S1      "abcd"
        #define DJO_TEST_LITTERAL_S2      "abcdefgh"
        #define DJO_TEST_LITTERAL_SEMPTY  ""

        static const char s_s1[]{ DJO_TEST_LITTERAL_S1 };
        static const sz s_s1Len{ ArrayCount(s_s1) - 1 };
        static const char s_s2[]{ DJO_TEST_LITTERAL_S2 };
        static const sz s_s2Len{ ArrayCount(s_s2) - 1 };
        static const char s_sEmpty[]{ DJO_TEST_LITTERAL_SEMPTY };
        static const sz s_sEmptyLen{ ArrayCount(s_sEmpty) - 1 };

        void Tests_StringView_Constructors(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            StringView sv1;
            StringView sv2;
            StringView svEmpty;
            const auto TestSvs = [&]()
            {
                DJO_UTEST(sv1.count == s_s1Len);
                DJO_UTEST(sv1.data[0] == s_s1[0]);
                DJO_UTEST(sv2.count == s_s2Len);
                DJO_UTEST(sv2.data[0] == s_s2[0]);
                DJO_UTEST(svEmpty.count == 0);
                DJO_UTEST(svEmpty.data != nullptr);
            };

            sv1 = ToStringView(DJO_TEST_LITTERAL_S1);
            sv2 = ToStringView(DJO_TEST_LITTERAL_S2);
            svEmpty = ToStringView(DJO_TEST_LITTERAL_SEMPTY);
            TestSvs();

            sv1 = ToStringView((const char*)s_s1);
            sv2 = ToStringView((const char*)s_s2);
            svEmpty = ToStringView((const char*)s_sEmpty);
            TestSvs();

            sv1 = StringView{ (const char*)s_s1, s_s1Len };
            sv2 = StringView{ (const char*)s_s2, s_s2Len };
            svEmpty = StringView{ (const char*)s_sEmpty, s_sEmptyLen };
            TestSvs();

            DJO_UTESTS_CLOSE_SCOPE;
        }

        void Tests_StringView_Basics(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            StringView sv1 = ToStringView(DJO_TEST_LITTERAL_S1);
            StringView sv2 = ToStringView(DJO_TEST_LITTERAL_S2);
            StringView svEmpty = ToStringView(DJO_TEST_LITTERAL_SEMPTY);

            DJO_UTEST(IsEmpty(sv1) == false);
            DJO_UTEST(IsEmpty(sv2) == false);
            DJO_UTEST(IsEmpty(svEmpty) == true);

            DJO_UTEST(sv1.data[0] == 'a');
            DJO_UTEST(sv1.data[2] == 'c');
            DJO_UTEST(*GetLast(sv1) == 'd');
            DJO_UTEST(sv2.data[0] == 'a');
            DJO_UTEST(sv2.data[2] == 'c');
            DJO_UTEST(*GetLast(sv2) == 'h');

            DJO_UTEST(sv1 == sv1);
            DJO_UTEST(sv1 != sv2);
            DJO_UTEST(sv1 != svEmpty);
            DJO_UTEST(sv2 != sv1);
            DJO_UTEST(sv2 == sv2);
            DJO_UTEST(sv2 != svEmpty);
            DJO_UTEST(svEmpty != sv1);
            DJO_UTEST(svEmpty != sv2);
            DJO_UTEST(svEmpty == svEmpty);

            DJO_UTEST(Find(sv1, 'a') == 0);
            DJO_UTEST(Find(sv1, 'c') == 2);
            DJO_UTEST(Find(sv1, 'z') == c_npos);
            DJO_UTEST(FindLast(sv1, 'a') == Find(sv1, 'a'));
            DJO_UTEST(FindLast(sv1, 'c') == Find(sv1, 'c'));
            DJO_UTEST(FindLast(sv1, 'z') == Find(sv1, 'z'));
            DJO_UTEST(Find(sv2, 'a') == 0);
            DJO_UTEST(Find(sv2, 'c') == 2);
            DJO_UTEST(Find(sv2, 'g') == 6);
            DJO_UTEST(Find(sv2, 'z') == c_npos);
            DJO_UTEST(FindLast(sv2, 'a') == Find(sv2, 'a'));
            DJO_UTEST(FindLast(sv2, 'c') == Find(sv2, 'c'));
            DJO_UTEST(FindLast(sv2, 'g') == Find(sv2, 'g'));
            DJO_UTEST(FindLast(sv2, 'z') == Find(sv2, 'z'));
            DJO_UTEST(Find(svEmpty, 'a') == c_npos);
            DJO_UTEST(Find(svEmpty, 'z') == c_npos);
            DJO_UTEST(FindLast(svEmpty, 'a') == Find(svEmpty, 'a'));
            DJO_UTEST(FindLast(svEmpty, 'z') == Find(svEmpty, 'z'));

            DJO_UTESTS_CLOSE_SCOPE;
        }

        void Tests_StringView_Shrink(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            StringView sv1 = ToStringView(DJO_TEST_LITTERAL_S1);
            StringView sv2 = ToStringView(DJO_TEST_LITTERAL_S2);
            StringView svEmpty = ToStringView(DJO_TEST_LITTERAL_SEMPTY);

            DJO_UTEST(ShrinkFront(sv1, c_npos) == svEmpty);
            DJO_UTEST(ShrinkFront(sv1, 123456) == svEmpty);
            DJO_UTEST(ShrinkFront(sv1, 2) != sv1);
            DJO_UTEST(ShrinkFront(sv2, c_npos) == svEmpty);
            DJO_UTEST(ShrinkFront(sv2, 123456) == svEmpty);
            DJO_UTEST(ShrinkFront(sv2, 2) != sv2);
            DJO_UTEST(ShrinkFront(svEmpty, c_npos) == svEmpty);
            DJO_UTEST(ShrinkFront(svEmpty, 123456) == svEmpty);
            DJO_UTEST(ShrinkFront(svEmpty, 2) == svEmpty);
            DJO_UTEST(ShrinkBack(sv1, c_npos) == sv1);
            DJO_UTEST(ShrinkBack(sv1, 123456) == sv1);
            DJO_UTEST(ShrinkBack(sv1, 2) != sv1);
            DJO_UTEST(ShrinkBack(sv2, c_npos) == sv2);
            DJO_UTEST(ShrinkBack(sv2, 123456) == sv2);
            DJO_UTEST(ShrinkBack(sv2, 2) != sv2);
            DJO_UTEST(ShrinkBack(svEmpty, c_npos) == svEmpty);
            DJO_UTEST(ShrinkBack(svEmpty, 123456) == svEmpty);
            DJO_UTEST(ShrinkBack(svEmpty, 2) == svEmpty);

            DJO_UTEST(Shrink(sv1, 0, c_npos) == sv1);
            DJO_UTEST(Shrink(sv1, 1, 3) == "bc");
            DJO_UTEST(Shrink(sv1, 1, 1) == svEmpty);
            DJO_UTEST(Shrink(sv1, 8, 10) == svEmpty);

            DJO_UTEST(Shrink(sv2, 0, c_npos) == sv2);
            DJO_UTEST(Shrink(sv2, 1, 3) == "bc");
            DJO_UTEST(Shrink(sv2, 1, 1) == svEmpty);
            DJO_UTEST(Shrink(sv2, 8, 10) == svEmpty);

            DJO_UTEST(Shrink(svEmpty, 0, c_npos) == svEmpty);
            DJO_UTEST(Shrink(svEmpty, 1, 3) == svEmpty);
            DJO_UTEST(Shrink(svEmpty, 0, 0) == svEmpty);

            DJO_UTESTS_CLOSE_SCOPE;
        }

        #undef DJO_TEST_LITTERAL_S1
        #undef DJO_TEST_LITTERAL_S2
        #undef DJO_TEST_LITTERAL_SEMPTY

        void Tests_StringView(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            Tests_StringView_Constructors(DJO_UTESTS_PARAMS);
            Tests_StringView_Basics(DJO_UTESTS_PARAMS);
            Tests_StringView_Shrink(DJO_UTESTS_PARAMS);

            DJO_UTESTS_CLOSE_SCOPE;
        }
    }
}
