#pragma once

#include <DjobiTests/Context.hpp>
#include <DjobiTests/Tests.hpp>


namespace Djo
{
    namespace Tests
    {
        void Tests_Core(DJO_UTESTS_ARGS) noexcept;
        void Tests_TypeTraits(DJO_UTESTS_ARGS) noexcept;
        void Tests_Utils(DJO_UTESTS_ARGS) noexcept;
        void Tests_StringView(DJO_UTESTS_ARGS) noexcept;
        void Tests_Time(DJO_UTESTS_ARGS) noexcept;
        void Tests_PlacementNew(DJO_UTESTS_ARGS) noexcept;
        void Tests_Random(DJO_UTESTS_ARGS) noexcept;

        DJO_INLINE void Tests(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            Tests_Core(DJO_UTESTS_PARAMS);
            Tests_TypeTraits(DJO_UTESTS_PARAMS);
            Tests_Utils(DJO_UTESTS_PARAMS);
            Tests_StringView(DJO_UTESTS_PARAMS);
            Tests_Time(DJO_UTESTS_PARAMS);
            Tests_PlacementNew(DJO_UTESTS_PARAMS);
            Tests_Random(DJO_UTESTS_PARAMS);

            DJO_UTESTS_CLOSE_SCOPE;
        }
    }
}
