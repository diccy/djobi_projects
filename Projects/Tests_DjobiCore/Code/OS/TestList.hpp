#pragma once

#include <DjobiTests/Context.hpp>
#include <DjobiTests/Tests.hpp>


namespace Djo
{
    namespace Tests
    {
        void Tests_VirtualMemory(DJO_UTESTS_ARGS) noexcept;
        void Tests_OS_Time(DJO_UTESTS_ARGS) noexcept;

        DJO_INLINE void Tests_OS(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            Tests_VirtualMemory(DJO_UTESTS_PARAMS);
            Tests_OS_Time(DJO_UTESTS_PARAMS);

            DJO_UTESTS_CLOSE_SCOPE;
        }
    }
}
