#include "Tests_DjobiCore_pch.h"

#include <DjobiTests/Context.hpp>
#include <DjobiTests/Tests.hpp>

#include <DjobiCore/OS/Time.hpp>


namespace Djo
{
    namespace Tests
    {
        void Tests_OS_Time(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            OS::Time::TimePoint t1 = OS::Time::Now();
            OS::Time::ThreadSleep(OS::Time::TimePoint{ 1 });
            OS::Time::TimePoint t2 = OS::Time::Now();
            DJO_UTEST(t2.t > t1.t);

            DJO_UTESTS_CLOSE_SCOPE;
        }
    }
}
