#include "Tests_DjobiCore_pch.h"

#include <DjobiTests/Context.hpp>
#include <DjobiTests/Tests.hpp>

#include <DjobiCore/OS/VirtualMemory.hpp>

#include <signal.h>


namespace Djo
{
    namespace Tests
    {
        struct SegFaultException { int signal; };

        void SegFaultSignalHandler(const int _signal)
        {
            throw SegFaultException{ _signal };
        }

        void Tests_VirtualMemory(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            DJO_UTEST(OS::GetPageSize() == OS::GetPageSize());

            using SignalHandlerPointer = void (*)(int);
            const SignalHandlerPointer previousSegFaultSignalHandler = ::signal(SIGSEGV, SegFaultSignalHandler);

            {
                const auto [p1, size1] = OS::ReserveAddressSpace(10);
                DJO_UTEST(p1 != nullptr);
                DJO_UTEST(size1 == OS::GetPageSize());
                DJO_TODO("Uncomment this test, at least once, then re-comment it, if annoying");
                /*
                try
                {
                    *((int*)p1) = 0; // VS might freak out here, just continue execution
                    DJO_UTEST(false);
                }
                catch (const SegFaultException& e)
                {
                    DJO_UTEST(e.signal == SIGSEGV);
                }
                */

                const auto [p2, size2] = OS::ReserveAddressSpace(10);
                DJO_UTEST(p2 != nullptr);
                DJO_UTEST(p1 != p2);
                DJO_UTEST(size2 == OS::GetPageSize());

                const bool release1 = OS::ReleaseAddressSpace(p1, 10);
                DJO_UTEST(release1 == true);
                const bool twiceRelease1 = OS::ReleaseAddressSpace(p1, 10);
                DJO_UTEST(twiceRelease1 == false);
                const bool release2 = OS::ReleaseAddressSpace(p2, 10);
                DJO_UTEST(release2 == true);
            }
            {
                const auto [p, size] = OS::ReserveAddressSpace(10);
                DJO_UTEST(p != nullptr);
                DJO_UTEST(size == OS::GetPageSize());
                const auto [pp, sizep] = OS::AllocatePhysicalMemory(p, 10);
                DJO_UTEST(pp != nullptr);
                DJO_UTEST(sizep >= 10);

                *((int*)pp) = 0; // allowed now
                DJO_UTEST(true);

                const bool free = OS::FreePhysicalMemory(pp, sizep);
                DJO_UTEST(free == true);
                const bool release = OS::ReleaseAddressSpace(p, 10);
                DJO_UTEST(release == true);
            }

            ::signal(SIGSEGV, previousSegFaultSignalHandler);

            DJO_UTESTS_CLOSE_SCOPE;
        }
    }
}
