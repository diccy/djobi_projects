#include "Tests_DjobiCore_pch.h"

#include <DjobiTests/Context.hpp>
#include <DjobiTests/Tests.hpp>
#include <DjobiTests/Utils.hpp>

#include <DjobiCore/Format/Format.hpp>


namespace Djo
{
    namespace Tests
    {
        void Tests_Format_Basics(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            constexpr sz c_bufferSize = 10;
            char stringStreamBuffer[c_bufferSize + 1]{}; // make a room for null char
            sz n = 0;

            Buffer<char> buffer = ToBuffer(stringStreamBuffer);
            buffer.count = c_bufferSize; // does not allow to write over terminating null char

            DJO_UTEST(StrLen(stringStreamBuffer) == 0);

            ZeroMem(&stringStreamBuffer);
            n = DJO_FORMAT(&buffer, "");
            DJO_UTEST(n == 0);
            DJO_UTEST(StrLen(stringStreamBuffer) == n);

            ZeroMem(&stringStreamBuffer);
            n = DJO_FORMAT(&buffer, "hello");
            DJO_UTEST(n == 5);
            DJO_UTEST(StrLen(stringStreamBuffer) == n);
            DJO_UTEST(StrNCmp(stringStreamBuffer, "hello", c_bufferSize) == 0);

            ZeroMem(&stringStreamBuffer);
            n = DJO_FORMAT(&buffer, "%");
            DJO_UTEST(n == 1);
            DJO_UTEST(StrLen(stringStreamBuffer) == n);
            DJO_UTEST(StrNCmp(stringStreamBuffer, "%", c_bufferSize) == 0);

            ZeroMem(&stringStreamBuffer);
            n = DJO_FORMAT(&buffer, "%", 12);
            DJO_UTEST(n == 2);
            DJO_UTEST(StrLen(stringStreamBuffer) == n);
            DJO_UTEST(StrNCmp(stringStreamBuffer, "12", c_bufferSize) == 0);

            ZeroMem(&stringStreamBuffer);
            n = DJO_FORMAT(&buffer, "%", -12);
            DJO_UTEST(n == 3);
            DJO_UTEST(StrLen(stringStreamBuffer) == n);
            DJO_UTEST(StrNCmp(stringStreamBuffer, "-12", c_bufferSize) == 0);

            ZeroMem(&stringStreamBuffer);
            n = DJO_FORMAT(&buffer, "%", 'c');
            DJO_UTEST(n == 1);
            DJO_UTEST(StrLen(stringStreamBuffer) == n);
            DJO_UTEST(StrNCmp(stringStreamBuffer, "c", c_bufferSize) == 0);

            ZeroMem(&stringStreamBuffer);
            n = DJO_FORMAT(&buffer, "%", '%');
            DJO_UTEST(n == 1);
            DJO_UTEST(StrLen(stringStreamBuffer) == n);
            DJO_UTEST(StrNCmp(stringStreamBuffer, "%", c_bufferSize) == 0);

            ZeroMem(&stringStreamBuffer);
            n = DJO_FORMAT(&buffer, "%", "coucou");
            DJO_UTEST(n == 6);
            DJO_UTEST(StrLen(stringStreamBuffer) == n);
            DJO_UTEST(StrNCmp(stringStreamBuffer, "coucou", c_bufferSize) == 0);

            ZeroMem(&stringStreamBuffer);
            n = DJO_FORMAT(&buffer, "%", ToStringView("hello"));
            DJO_UTEST(n == 5);
            DJO_UTEST(StrLen(stringStreamBuffer) == n);
            DJO_UTEST(StrNCmp(stringStreamBuffer, "hello", c_bufferSize) == 0);

            {
                char strBuffer[10]{};
                ::memcpy(strBuffer, "coucou", 7);
                String8 str{};
                str.buffer = ToBuffer(strBuffer);
                str.count = 6;

                ZeroMem(&stringStreamBuffer);
                n = DJO_FORMAT(&buffer, "%", str);
                DJO_UTEST(n == 6);
                DJO_UTEST(StrLen(stringStreamBuffer) == n);
                DJO_UTEST(StrNCmp(stringStreamBuffer, "coucou", c_bufferSize) == 0);
            }

            ZeroMem(&stringStreamBuffer);
            n = DJO_FORMAT(&buffer, "%%", 2, '%');
            DJO_UTEST(n == 2);
            DJO_UTEST(StrLen(stringStreamBuffer) == n);
            DJO_UTEST(StrNCmp(stringStreamBuffer, "2%", c_bufferSize) == 0);

            DJO_UTESTS_CLOSE_SCOPE;
        }

        void Tests_Formatting(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            Tests_Format_Basics(DJO_UTESTS_PARAMS);

            DJO_UTESTS_CLOSE_SCOPE;
        }
    }
}
