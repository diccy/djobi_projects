#pragma once

#include <DjobiTests/Context.hpp>
#include <DjobiTests/Tests.hpp>


namespace Djo
{
    namespace Tests
    {
        void Tests_Formatting(DJO_UTESTS_ARGS) noexcept;

        DJO_INLINE void Tests_Format(DJO_UTESTS_ARGS) noexcept
        {
            DJO_UTESTS_OPEN_SCOPE;

            Tests_Formatting(DJO_UTESTS_PARAMS);

            DJO_UTESTS_CLOSE_SCOPE;
        }
    }
}
