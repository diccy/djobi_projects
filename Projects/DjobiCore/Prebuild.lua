#!lua
require("ProjectCommon")

require("External/glm")


Project_DjobiCore = NewProjectCommon({
    name = "DjobiCore",
    dependencies = {Dependency_GLM},
    DefineProject = CommonProjectDefinition_Lib,
})
