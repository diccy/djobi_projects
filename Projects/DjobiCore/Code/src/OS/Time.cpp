#include "DjobiCore_pch.h"

#include <DjobiCore/OS/Time.hpp>


namespace Djo::OS::Time
{
    TimePoint Now() noexcept
    {
        using namespace std::chrono;

        return TimePoint
        {
            static_cast<TimePoint::Type>(
                time_point_cast<nanoseconds>(high_resolution_clock::now())
                .time_since_epoch()
                .count())
        };
    }

    void ThreadSleep(TimePoint _time) noexcept
    {
        ::std::this_thread::sleep_for(std::chrono::nanoseconds{ _time.t });
    }
}
