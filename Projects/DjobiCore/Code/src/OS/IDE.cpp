#include "DjobiCore_pch.h"

#include <DjobiCore/OS/IDE.hpp>

#include <DjobiCore/OS/OSHeader.hpp>


namespace Djo::IDE
{
#if __DJO_DEBUG__
    String8OStream g_debugLogger = MoldOutputStringStream();
#endif

    void Output(const char* const _str)
    {
        ::OutputDebugStringA(_str);
    }

    void Output(const StringView& _sv)
    {
        constexpr sz c_chunkLength = 512 - 1;
        char chunk[c_chunkLength + 1]{ 0 };

        const char* p = _sv.data;
        sz remainingLength = _sv.count;
        while (remainingLength > 0)
        {
            const sz outputLength = Min(remainingLength, c_chunkLength);
            ::memcpy(chunk, p, outputLength);
            chunk[outputLength] = '\0';
            Output((const char*)chunk);
            p += outputLength;
            remainingLength -= outputLength;
        }
    }

    void Output(const String8& _str)
    {
        Output(GetData(_str));
    }

    namespace _Internal
    {
        void OutputCallback(const char* const _data, const sz _count, void* const) noexcept
        {
            Output(StringView{ _data, _count });
        }
    }

    String8OStream MoldOutputStringStream() noexcept
    {
        String8OStream stream{};
        stream.output.callback = _Internal::OutputCallback;
        return stream;
    }
}
