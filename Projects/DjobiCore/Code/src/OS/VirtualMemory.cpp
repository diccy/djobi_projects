#include "DjobiCore_pch.h"

#include <DjobiCore/OS/VirtualMemory.hpp>

#include <DjobiCore/OS/OSHeader.hpp>
#include <DjobiCore/Core/Utils.hpp>


// see
// https://learn.microsoft.com/en-us/windows/win32/Core/reserving-and-committing-memory
// https://blog.molecular-matters.com/2012/10/02/memory-allocation-strategies-interlude-virtual-memory/

namespace Djo::OS
{
    namespace _Internal
    {
        sz GetPageSize() noexcept
        {
            SYSTEM_INFO sysInfo;
            ::GetSystemInfo(&sysInfo);
            return sysInfo.dwPageSize;
        }
    }

    sz GetPageSize() noexcept
    {
        static thread_local const sz s_pageSize{ _Internal::GetPageSize() };
        return s_pageSize;
    }

    Memory::Area ReserveAddressSpace(const sz _size) noexcept
    {
        const sz allocSize = Align(_size, GetPageSize());
        return Memory::Area
        {
            reinterpret_cast<u8*>(::VirtualAlloc(NULL, allocSize, MEM_RESERVE, PAGE_NOACCESS)),
            allocSize,
        };
    }

    Memory::Area AllocatePhysicalMemory(void* const _p, const sz _size) noexcept
    {
        const sz allocSize = Align(_size, GetPageSize());
        return Memory::Area
        {
            reinterpret_cast<u8*>(::VirtualAlloc(_p, allocSize, MEM_COMMIT, PAGE_READWRITE)),
            allocSize,
        };
    }

    bool FreePhysicalMemory(void* const _p, const sz _size) noexcept
    {
        return ::VirtualFree(_p, _size, MEM_DECOMMIT);
    }

    bool ReleaseAddressSpace(void* const _p, const sz) noexcept
    {
        return ::VirtualFree(_p, 0, MEM_RELEASE);
    }
}
