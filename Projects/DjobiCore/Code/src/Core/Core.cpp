#include "DjobiCore_pch.h"

#include <DjobiCore/Core/Core.hpp>


namespace Djo
{
    namespace _Internal
    {
        void Break()
        {
            __debugbreak();
        }
    }

    void SetupSystemMemoryLeakWatcher()
    {
    #if __DJO_DEBUG__
        ::_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
        //::_CrtSetBreakAlloc(441); // uncomment to use when memory leak is found
    #endif
    }
}
