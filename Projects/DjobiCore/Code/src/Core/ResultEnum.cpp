#include "DjobiCore_pch.h"

#include <DjobiCore/Core/ResultEnum.hpp>


namespace Djo
{
#if DJO_IS_ASSERT_AVAILABLE

    EResult MakeResult(const EResult _result, const char* const _errorMsg /*= nullptr*/)
    {
        if (_errorMsg != nullptr)
        {
            DJO_ASSERT_MSG(_result == EResult::OK, _errorMsg);
        }
        else
        {
            DJO_ASSERT(_result == EResult::OK);
        }
        return _result;
    }

#endif
}
