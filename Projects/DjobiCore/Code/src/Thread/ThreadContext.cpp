#include "DjobiCore_pch.h"

#include <DjobiCore/Thread/ThreadContext.hpp>
#include <DjobiCore/Core/StructUtils.hpp>
#include <DjobiCore/Core/Utils.hpp>


namespace Djo
{
    namespace _Internal
    {
        DJO_PER_THREAD_STATIC ThreadContext* ts_threadContext{ nullptr };
    }

    void InitAndSetThreadContext(ThreadContext* const _ctx) noexcept
    {
        DJO_ASSERT(_Internal::ts_threadContext == nullptr);

        ZeroMem(_ctx);
        _ctx->arena = DJO_MEMORY_ARENA_ALLOCATE("Per thread arena");
        _Internal::ts_threadContext = _ctx;
    }

    ThreadContext* GetThreadContext() noexcept
    {
        return _Internal::ts_threadContext;
    }

    Memory::ArenaMark GetThreadLocalArenaMark() noexcept
    {
        ThreadContext* const ctx = GetThreadContext();
        return Memory::ArenaMark{ ctx->arena };
    }
}
