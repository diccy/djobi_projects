#include "DjobiCore_pch.h"

#include <DjobiCore/Format/Format.hpp>


namespace Djo
{
    namespace _Internal
    {
        constexpr char c_valueChar{ '%' };
        constexpr char c_escapeChar{ '\\' };

        sz ConsumeText(StringView* const _format, String8OStream* const _sink) noexcept
        {
            if (_format->count == 0)
                return 0;

            const char* const start = _format->data;
            const char* p = start;
            const sz maxOffset = _format->count;
            sz offset = 0;
            while (offset < maxOffset && !(*p == c_valueChar && (p == start || *(p - 1) != c_escapeChar)))
            {
                ++p;
                ++offset;
            }

            sz wrote = 0;
            if (offset > 0)
            {
                wrote = Push(_sink, StringView{ _format->data, offset });
                _format->data = p;
                _format->count -= offset;
            }
            return wrote;
        }

        bool ConsumeParams(StringView* const _format, StringView* const _params) noexcept
        {
            DJO_ASSERT(!IsEmpty(*_format));
            DJO_ASSERT(_format->data[0] == c_valueChar);

            const char* const start = _format->data;
            const char* p = start + 1;
            sz offset = 1;

            // TODO

            _format->data = p;
            _format->count -= offset;
            *_params = StringView{ start, offset };
            return true;
        }
    }
}
