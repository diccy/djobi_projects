#include "DjobiCore_pch.h"

#include <DjobiCore/Containers/String8OStream.hpp>


namespace Djo
{
    namespace _Internal
    {
        static constexpr sz c_formatBufferSize = 2048;
        DJO_PER_THREAD_STATIC char s_formatBuffer[c_formatBufferSize];
    }

    sz PushPrintf(String8OStream* const _stream, const char* _format, ...) noexcept
    {
        va_list args;
        va_start(args, _format);
        int length = ::vsnprintf(NULL, 0, _format, args);
        sz wrote = 0;
        if (length > 0)
        {
            const sz freeSize = GetCapacity(_stream->array) - _stream->array.count;
            if (length < freeSize)
            {
                length = ::vsnprintf(GetEnd(_stream->array), freeSize, _format, args);
                DJO_ASSERT(length > 0);
                _stream->array.count += length;
                wrote = (sz)length;
            }
            else
            {
                DJO_ASSERT(length <= _Internal::c_formatBufferSize);

                length = ::vsnprintf(_Internal::s_formatBuffer, _Internal::c_formatBufferSize, _format, args);
                if (length > 0)
                    wrote = Push(_stream, StringView{ _Internal::s_formatBuffer, (sz)length });
            }
        }
        va_end(args);
        return wrote;
    }

    String8OStream MoldBufferFillerOStream(const Buffer<char>& _buffer) noexcept
    {
        String8OStream stream{};
        stream.array.buffer = _buffer;
        return stream;
    }

    namespace _Internal
    {
        void String8FillerOStreamCallBack(const char* const _data, const sz _count, void* const _userData) noexcept
        {
            InsertBack(reinterpret_cast<String8*>(_userData), StringView{ _data, _count });
        }
    }

    String8OStream MoldString8FillerOStream(String8* const _str) noexcept
    {
        String8OStream stream{};
        stream.output.callback = _Internal::String8FillerOStreamCallBack;
        stream.output.userData = _str;
        return stream;
    }
}
