#include "DjobiCore_pch.h"

#include <DjobiCore/Containers/String8Alloc.hpp>

#include <stdarg.h>
#include <stdio.h>


namespace Djo
{
    constexpr sz c_string8MinAllocLength = 32;

    String8 PushEmptyString8(Memory::Arena* const _arena, const sz _length) noexcept
    {
        if (_length == 0)
            return c_nullString8;

        const sz allocSize = Max(c_string8MinAllocLength, _length);
        return String8{ PushEmptyArray<char>(_arena, allocSize) };
    }

    String8 PushString8(Memory::Arena* const _arena, const StringView& _sv, const sz _length /*= 0*/) noexcept
    {
        if ((_sv.count + _length) == 0)
            return c_nullString8;

        String8 str = PushEmptyString8(_arena, Max(_sv.count, _length));
        Set(&str, 0, _sv);
        return str;
    }

    String8 PushString8Formatted(Memory::Arena* const _arena, const char* const _format, ...) noexcept
    {
        va_list args;
        va_start(args, _format);
        const int length = ::vsnprintf(NULL, 0, _format, args); // does not count trailing zero
        String8 str = PushEmptyString8(_arena, length + 1);
        if (length > 0)
        {
            ::vsnprintf(GetData(str), length + 1, _format, args); // trailing zero done by vsnprintf
        }
        va_end(args);
        return str;
    }

    String8 AllocEmptyString8(const Memory::Allocator& _allocator, const sz _length) noexcept
    {
        if (_length == 0)
            return c_nullString8;

        const sz allocSize = Max(c_string8MinAllocLength, _length);
        return String8{ AllocEmptyArray<char>(_allocator, allocSize) };
    }

    String8 AllocString8(const Memory::Allocator& _allocator, const StringView& _sv, const sz _length /*= 0*/) noexcept
    {
        if ((_sv.count + _length) == 0)
            return c_nullString8;

        String8 str = AllocEmptyString8(_allocator, Max(_sv.count, _length));
        Set(&str, 0, _sv);
        return str;
    }

    String8 AllocString8Formatted(const Memory::Allocator& _allocator, const char* const _format, ...) noexcept
    {
        va_list args;
        va_start(args, _format);
        const int length = ::vsnprintf(NULL, 0, _format, args); // does not count trailing zero
        String8 str = AllocEmptyString8(_allocator, length + 1);
        if (length > 0)
        {
            ::vsnprintf(GetData(str), length + 1, _format, args); // trailing zero done by vsnprintf
        }
        va_end(args);
        return str;
    }
}
