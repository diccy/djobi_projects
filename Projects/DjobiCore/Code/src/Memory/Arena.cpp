#include "DjobiCore_pch.h"

#include <DjobiCore/Memory/Arena.hpp>

#include <DjobiCore/OS/VirtualMemory.hpp>


namespace Djo
{
    namespace Memory
    {
        constexpr sz c_arenaHeaderSize = AlignPow2(sizeof(Arena), c_baseAlign);

        namespace _Internal
        {
            Arena* AllocateArena(const sz _reserveSize, const sz _commitSize) noexcept
            {
                DJO_ASSERT(_reserveSize >= _commitSize);

                const Memory::Area reservedArea = OS::ReserveAddressSpace(_reserveSize);
                DJO_ASSERT(reservedArea.data != nullptr && reservedArea.count >= _reserveSize);
                const Memory::Area commitedArea = OS::AllocatePhysicalMemory(reservedArea.data, _commitSize);
                DJO_ASSERT(commitedArea.data != nullptr && commitedArea.count >= _commitSize);

                Arena* const arena = reinterpret_cast<Arena*>(commitedArea.data);
                if (arena)
                {
                    arena->state.localCursor = c_arenaHeaderSize;
                    arena->state.lastAllocStart = c_arenaHeaderSize;
                    arena->reservedSize = reservedArea.count;
                    arena->commitedSize = commitedArea.count;
                    arena->configCommitSize = commitedArea.count;
                }

                return arena;
            }

            Area Push(Arena* const _arena, const sz _size) noexcept
            {
                sz start = AlignPow2(_arena->state.localCursor, c_baseAlign);
                sz end = start + _size;

                if (_arena->reservedSize < end)
                {
                    DJO_ASSERT_MSG(false, "Memory Arena allocation failed");
                    return c_nullArea;
                }

                if (_arena->commitedSize < end)
                {
                    // commit new memory
                    const sz commitEnd = Min(Align(end, _arena->configCommitSize), _arena->reservedSize);
                    const sz commitSize = commitEnd - _arena->commitedSize;
                    const Memory::Area commitedArea = OS::AllocatePhysicalMemory(
                        reinterpret_cast<u8*>(_arena) + _arena->commitedSize,
                        commitSize);

                    DJO_ASSERT(_arena->reservedSize >= (_arena->commitedSize + commitedArea.count));
                    DJO_ASSERT((_arena->commitedSize + commitedArea.count) >= end);
                    _arena->commitedSize += commitedArea.count;
                }

                _arena->state.lastAllocStart = start;
                _arena->state.localCursor = end;
                return Area{ reinterpret_cast<u8*>(_arena) + start, _size };
            }

            DJO_INLINE bool IsLastAlloc(const Arena& _arena, void* const _p) noexcept
            {
                return _p == (reinterpret_cast<const u8*>(&_arena) + _arena.state.lastAllocStart);
            }

            Area Reuse(Arena* const _arena, void* const _p, const sz _size) noexcept
            {
                if (_p == nullptr)
                    return DJO_MEMORY_ARENA_PUSH(_arena, _size);

                else if (IsLastAlloc(*_arena, _p))
                {
                    SetArenaState(_arena, ArenaState{ _arena->state.lastAllocStart , _arena->state.lastAllocStart });
                    return DJO_MEMORY_ARENA_PUSH(_arena, _size);
                }

                return c_nullArea;
            }
        }

        void FreeArena(Arena* const _arena) noexcept
        {
            const bool released = OS::ReleaseAddressSpace(_arena, _arena->reservedSize);
            DJO_ASSERT(released); DJO_UNUSED(released);
        }

        void SetArenaState(Arena* const _arena, const ArenaState _state) noexcept
        {
            DJO_ASSERT(_state.lastAllocStart <= _state.localCursor);
            DJO_ASSERT(_state.lastAllocStart <= _arena->commitedSize);
            DJO_ASSERT(_state.localCursor <= _arena->commitedSize);

            _arena->state = _state;
        }

        namespace _Internal
        {
            Area ArenaAlloc(const sz _size, void* const _context) noexcept
            {
                Memory::Arena* const arena = reinterpret_cast<Memory::Arena*>(_context);
                return DJO_MEMORY_ARENA_PUSH(arena, _size);
            }

            Area ArenaRealloc(void* const _p, const sz _size, void* const _context) noexcept
            {
                Memory::Arena* const arena = reinterpret_cast<Memory::Arena*>(_context);
                if (!IsLastAlloc(*arena, _p))
                {
                    DJO_ASSERT_MSG(false, "Realloc with arena, maybe change your allocation strategy");
                    return c_nullArea;
                }

                return DJO_MEMORY_ARENA_REUSE(arena, _p, _size);
            }

            void ArenaFree(void* const /*_p*/, void* const /*_context*/) noexcept
            {
                DJO_ASSERT_MSG(false, "Not a good idea to use free with arena, maybe change your allocation strategy");
            }
        }

        Allocator MoldArenaAllocator(Arena* const _arena) noexcept
        {
            return Allocator{ _arena, _Internal::ArenaAlloc, _Internal::ArenaRealloc, _Internal::ArenaFree };
        }
    }
}
