#include "DjobiCore_pch.h"

#include <DjobiCore/Memory/Allocator.hpp>

#include <stdlib.h>


namespace Djo
{
    namespace Memory
    {
        namespace _Internal
        {
            Area Malloc(const sz _size, void* /*_context*/) noexcept
            {
                return Area{ reinterpret_cast<u8*>(::_aligned_malloc(_size, c_baseAlign)), _size };
            }

            Area Realloc(void* const _p, const sz _size, void* /*_context*/) noexcept
            {
                return Area{ reinterpret_cast<u8*>(::_aligned_realloc(_p, _size, c_baseAlign)), _size };
            }

            void Free(void* const _p, void* /*_context*/) noexcept
            {
                return ::_aligned_free(_p);
            }
        }

        Allocator MoldMallocAllocator() noexcept
        {
            return Allocator{ nullptr, _Internal::Malloc, _Internal::Realloc, _Internal::Free };
        }
    }
}
