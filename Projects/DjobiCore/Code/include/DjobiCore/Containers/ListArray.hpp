#pragma once

#include <DjobiCore/Containers/Array.hpp>
#include <DjobiCore/Containers/LinkedList.hpp>


namespace Djo
{
    template <typename T>
    struct ListArrayNode
    {
        using DataType = T;

        ListArrayNode<T>* next;
        Array<T> array;

        constexpr operator const ListArrayNode<const T>& () const noexcept  { return *AsConst(this); }
        constexpr operator ListArrayNode<const T>& () noexcept              { return *AsConst(this); }
    };

    template <typename T>
    struct ListArray
        : public LinkedList<ListArrayNode<T>>
    {
        using DataType = T;

        constexpr operator const ListArray<const T>& () const noexcept  { return *AsConst(this); }
        constexpr operator ListArray<const T>& () noexcept              { return *AsConst(this); }
    };

    DJO_DEFINE_NULL_CONSTEXPR_STATIC_T(typename T, ListArray<T>, ListArray, (nullptr, nullptr));


    template <typename T>
    constexpr sz GetCapacity(const ListArray<T>& _la) noexcept
    {
        sz capacity = 0;
        const ListArray<T>::NodeType* node = _la.head;
        while (node != nullptr)
        {
            capacity += GetCapacity(node->array);
            node = node->next;
        }
        return capacity;
    }

    template <typename T>
    constexpr sz GetCount(const ListArray<T>& _la) noexcept
    {
        sz count = 0;
        const ListArray<T>::NodeType* node = _la.head;
        while (node != nullptr)
        {
            count += GetCount(node->array);
            node = node->next;
        }
        return count;
    }

    template <typename T>
    constexpr bool IsEmpty(const ListArray<T>& _la) noexcept
    {
        const ListArray<T>::NodeType* node = _la.head;
        while (node != nullptr)
        {
            if (!IsEmpty(node->array))
                return false;

            node = node->next;
        }
        return true;
    }

    template <typename T>
    DJO_INLINE void InsertBack(ListArray<T>* const _la, CopyOrConstRef_t<const T> _value) noexcept
    {
        DJO_ASSERT(_la->tail != nullptr);
        InsertBack(&_la->tail->array, _value);
    }

    template <typename T>
    DJO_INLINE void InsertBack(ListArray<T>* const _la, const T* const _values, const sz _valueCount) noexcept
    {
        DJO_ASSERT(_la->tail != nullptr);
        InsertBack(&_la->tail->array, _values, _valueCount);
    }

    template <typename T>
    DJO_INLINE void InsertBack(ListArray<T>* const _la, const Buffer<const T>& _values) noexcept
    {
        DJO_ASSERT(_la->tail != nullptr);
        InsertBack(&_la->tail->array, _values);
    }
}
