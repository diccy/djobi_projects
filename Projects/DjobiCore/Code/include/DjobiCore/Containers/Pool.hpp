#pragma once

#include <DjobiCore/Containers/FreeList.hpp>


namespace Djo
{
    template <typename T>
    struct Pool
    {
        using DataType = T;

        Memory::Area area;
        FreeIndices* freeIndices;
    };

    DJO_DEFINE_NULL_CONSTEXPR_STATIC_T(typename T, Pool<T>, Pool, (Memory::c_nullArea, nullptr));


    template <typename T>
    constexpr bool IsFull(const Pool<T>& _pool) noexcept
    {
        return _pool.freeIndices == nullptr;
    }

    template <typename T>
    DJO_INLINE T* Acquire(Pool<T>* const _pool) noexcept
    {
        return AcquireElement(_pool->area, &_pool->freeIndices);
    }

    template <typename T>
    DJO_INLINE void Release(Pool<T>* const _pool, T* const _elem) noexcept
    {
        DJO_ASSERT(Memory::IsInArea(_elem, _pool->area));

        ReleaseElement(_pool->area, &_pool->freeIndices, _elem);
    }
}
