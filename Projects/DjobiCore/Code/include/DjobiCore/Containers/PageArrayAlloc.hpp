#pragma once

#include <DjobiCore/Containers/PageArray.hpp>
#include <DjobiCore/Containers/ArrayAlloc.hpp>


namespace Djo
{
    template <typename T>
    void Reserve(PageArray<T>* const _pa, Memory::Arena* const _arena, const sz _allocCount) noexcept
    {
        DJO_ASSERT(_allocCount <= GetBudget(*_pa));

        if (GetCapacity(*_pa) < _allocCount)
        {
            const sz pageElemCount = Pages::GetPageElemCount(_pa->pageElemCountPow2);
            const sz newAllocCount = _allocCount - GetCapacity(*_pa);
            const sz newPageCount = (newAllocCount / pageElemCount) + 1;
            const sz pageSize = pageElemCount * sizeof(T);
            const Memory::Area area = DJO_MEMORY_ARENA_PUSH(_arena, newPageCount * pageSize);
            for (sz i = 0; i < newPageCount; ++i)
                InsertBack(&_pa->pages, reinterpret_cast<T*>(area.data + (i * pageSize)));
        }
    }

    template <typename T>
    DJO_INLINE PageArray<T> PushEmptyPageArray(Memory::Arena* const _arena, const sz _pageCount, const sz _pageElemCount, const sz _allocCount) noexcept
    {
        if (_pageCount == 0 || _pageElemCount == 0)
            return c_nullPageArray<T>;
;
        PageArray<T> pageArray = c_nullPageArray<T>;
        pageArray.pages = PushEmptyArray<T*>(_arena, _pageCount);
        pageArray.pageElemCountPow2 = GetFirstBitIndex(CeilPow2(_pageElemCount));
        Reserve(&pageArray, _arena, _allocCount);
        return pageArray;
    }

    template <typename T>
    DJO_INLINE PageArray<T> PushPageArray(Memory::Arena* const _arena, const sz _pageCount, const sz _pageElemCount, const sz _allocCount) noexcept
    {
        PageArray<T> pageArray = PushEmptyPageArray<T>(_arena, _pageCount, _pageElemCount, _allocCount);
        pageArray.count = _allocCount;
        return pageArray;
    }

    template <typename T>
    DJO_INLINE void InsertBack(PageArray<T>* const _pa, Memory::Arena* const _arena, CopyOrConstRef_t<T> _value) noexcept
    {
        Reserve(_pa, _arena, GetCount(*_pa) + 1);
        InsertBack(_pa, _value);
    }

    template <typename T>
    DJO_INLINE void InsertBack(PageArray<T>* const _pa, Memory::Arena* const _arena, const T* const _values, const sz _valueCount) noexcept
    {
        Reserve(_pa, _arena, GetCount(*_pa) + _valueCount);
        InsertBack(_pa, _values, _valueCount);
    }

    template <typename T>
    DJO_INLINE void InsertBack(PageArray<T>* const _pa, Memory::Arena* const _arena, const Buffer<const T>& _values) noexcept
    {
        InsertBack(_pa, _arena, _values.data, _values.count);
    }
}
