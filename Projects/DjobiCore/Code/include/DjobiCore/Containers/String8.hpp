#pragma once

#include <DjobiCore/Containers/Array.hpp>
#include <DjobiCore/Core/StringView.hpp>


namespace Djo
{
    struct String8
        : public Array<char>
    {
    };

    DJO_DEFINE_NULL_CONSTEXPR_STATIC(String8, String8, (c_nullArray<char>));


    constexpr StringView ToStringView(const String8& _str)
    {
        return StringView{ GetData(_str), GetCount(_str) };
    }
}
