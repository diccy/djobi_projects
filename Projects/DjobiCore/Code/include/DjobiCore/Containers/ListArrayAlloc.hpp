#pragma once

#include <DjobiCore/Containers/ListArray.hpp>
#include <DjobiCore/Containers/ArrayAlloc.hpp>


namespace Djo
{
    constexpr sz c_listArrayNewArrayDefaultCapacity = 32;

    template <typename T>
    DJO_INLINE ListArray<T>::NodeType* PushEmptyListArrayNode(Memory::Arena* const _arena, const sz _allocCount) noexcept
    {
        const Memory::Area listArrayNodeArea = DJO_MEMORY_ARENA_PUSH(_arena, sizeof(typename ListArray<T>::NodeType));
        typename ListArray<T>::NodeType* const node = reinterpret_cast<typename ListArray<T>::NodeType*>(listArrayNodeArea.data);
        node->next = nullptr;
        node->array = PushEmptyArray<T>(_arena, _allocCount);
        return node;
    }

    template <typename T>
    DJO_INLINE ListArray<T> PushEmptyListArray(Memory::Arena* const _arena, const sz _allocCount) noexcept
    {
        typename ListArray<T>::NodeType* const node = PushEmptyListArrayNode<T>(_arena, _allocCount);
        return ListArray<T>{ node, node };
    }

    template <typename T>
    DJO_INLINE ListArray<T> PushListArray(Memory::Arena* const _arena, const sz _allocCount) noexcept
    {
        ListArray<T> la = PushEmptyListArray<T>(_arena, _allocCount);
        la.tail->array.count = _allocCount;
        return la;
    }

    template <typename T>
    DJO_INLINE void PushListTailEmptyArray(ListArray<T>* const _la, Memory::Arena* const _arena, const sz _allocCount) noexcept
    {
        SLList::InsertBack(_la, PushEmptyListArrayNode<T>(_arena, _allocCount));
    }

    template <typename T>
    void InsertBack(ListArray<T>* const _la, Memory::Arena* const _arena, const T* const _values, const sz _valueCount, const sz _newArrayCapacity = c_listArrayNewArrayDefaultCapacity) noexcept
    {
        if (_la->tail == nullptr)
            PushListTailEmptyArray(_la, _arena, Max(_valueCount, _newArrayCapacity));

        const sz tailArrayCapacityLeft = GetCapacity(_la->tail->array) - GetCount(_la->tail->array);
        if (tailArrayCapacityLeft < _valueCount)
        {
            if (tailArrayCapacityLeft > 0)
                InsertBack(_la, _values, tailArrayCapacityLeft);

            PushListTailEmptyArray(_la, _arena, Max(_valueCount - tailArrayCapacityLeft, _newArrayCapacity));
            InsertBack(_la, _values + tailArrayCapacityLeft, _valueCount - tailArrayCapacityLeft);
        }
        else
        {
            InsertBack(_la, _values, _valueCount);
        }
    }

    template <typename T>
    DJO_INLINE void InsertBack(ListArray<T>* const _la, Memory::Arena* const _arena, CopyOrConstRef_t<const T> _value, const sz _newArrayCapacity = c_listArrayNewArrayDefaultCapacity) noexcept
    {
        InsertBack(_la, _arena, &_value, 1, _newArrayCapacity);
    }

    template <typename T>
    DJO_INLINE void InsertBack(ListArray<T>* const _la, Memory::Arena* const _arena, const Buffer<const T>& _values, const sz _newArrayCapacity = c_listArrayNewArrayDefaultCapacity) noexcept
    {
        InsertBack(_la, _arena, _values.data, _values.count, _newArrayCapacity);
    }

    template <typename T>
    Array<T> PushMergedArray(const ListArray<T>& _la, Memory::Arena* const _arena) noexcept
    {
        const sz count = GetCount(_la);
        Array<T> array = PushEmptyArray<T>(_arena, count);
        typename ListArray<T>::NodeType* node = _la.head;
        while (node != nullptr)
        {
            if (!IsEmpty(node->array))
                InsertBack(&array, static_cast<Buffer<const T>>(node->array));

            node = node->next;
        }
        return array;
    }
}
