#pragma once

#include <DjobiCore/Containers/String8.hpp>
#include <DjobiCore/Containers/OStream.hpp>


namespace Djo
{
    using String8OStream = OStream<char>;

    DJO_INLINE sz Push(String8OStream* const _stream, const StringView& _sv) noexcept
    {
        return Push(_stream, _sv.data, _sv.count);
    }

    sz PushPrintf(String8OStream* _stream, const char* _format, ...) noexcept;

    String8OStream MoldBufferFillerOStream(const Buffer<char>& _buffer) noexcept;
    String8OStream MoldString8FillerOStream(String8* _str) noexcept;
}
