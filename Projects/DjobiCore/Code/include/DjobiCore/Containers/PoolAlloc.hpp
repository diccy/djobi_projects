#pragma once

#include <DjobiCore/Containers/Pool.hpp>
#include <DjobiCore/Memory/Arena.hpp>


namespace Djo
{
    template <typename T>
    DJO_INLINE Pool<T> PushEmptyPool(Memory::Arena* const _arena, const sz _allocCount) noexcept
    {
        const sz elemSize = Max(sizeof(T), sizeof(FreeList));
        const Memory::Area area = DJO_MEMORY_ARENA_PUSH(_arena, _allocCount * elemSize);
        return Pool<T>{ _area, InitInplaceFreeList(_area, elemSize) };
    }
}
