#pragma once

#include <DjobiCore/Core/Buffer.hpp>
#include <DjobiCore/Core/VisitorResultEnum.hpp>

#include <string.h> // memcpy, memmove


namespace Djo
{
    template <typename T>
    struct Array
    {
        using DataType = T;

        Buffer<T> buffer;
        sz count;

        constexpr operator const Array<const T>& () const noexcept  { return *AsConst(this); }
        constexpr operator Array<const T>& () noexcept              { return *AsConst(this); }

        constexpr operator Buffer<const T>() const noexcept  { return Buffer<const T>{ this->buffer.data, this->count }; }
        constexpr operator Buffer<T>() noexcept              { return Buffer<T>{ this->buffer.data, this->count }; }

        constexpr const T& operator[](const sz _i) const noexcept  { return *GetAt(*this, _i); }
        constexpr T& operator[](const sz _i) noexcept              { return *GetAt(*this, _i); }
    };

    DJO_DEFINE_NULL_CONSTEXPR_STATIC_T(typename T, Array<T>, Array, (c_nullBuffer<T>, 0));


    template <typename T>
    constexpr T* GetData(const Array<T>& _array) noexcept
    {
        return GetData(_array.buffer);
    }

    template <typename T>
    constexpr sz GetCount(const Array<T>& _array) noexcept
    {
        return _array.count;
    }

    template <typename T>
    constexpr bool IsEmpty(const Array<T>& _array) noexcept
    {
        return IsEmpty(_array.buffer) || _array.count == 0;
    }

    template <typename T>
    constexpr sz GetCapacity(const Array<T>& _array) noexcept
    {
        return GetCapacity(_array.buffer);
    }

    template <typename T>
    constexpr sz GetSize(const Array<T>& _array) noexcept
    {
        return _array.count * sizeof(T);
    }

    template <typename T>
    constexpr T* GetAt(const Array<T>& _array, const sz _index) noexcept
    {
        DJO_ASSERT(!IsEmpty(_array) && _index < _array.count);
        return GetAt(_array.buffer, _index);
    }

    template <typename T>
    constexpr T* GetEnd(const Array<T>& _array) noexcept
    {
        return GetData(_array) + _array.count;
    }

    template <typename T>
    constexpr T* GetFirst(const Array<T>& _array) noexcept
    {
        return GetAt(_array, 0);
    }

    template <typename T>
    constexpr T* GetLast(const Array<T>& _array) noexcept
    {
        return GetAt(_array, _array.count - 1);
    }

    template <typename T>
    constexpr sz Find(const Array<T>& _array, CopyOrConstRef_t<const T> _value) noexcept
    {
        return Find(GetData(_array), _array.count, _value);
    }

    template <typename T>
    constexpr sz FindLast(const Array<T>& _array, CopyOrConstRef_t<const T> _value) noexcept
    {
        return FindLast(GetData(_array), _array.count, _value);
    }

    template <typename T>
    DJO_INLINE void Set(Array<T>* const _array, const sz _index, const T* const _values, const sz _valueCount) noexcept
    {
        DJO_ASSERT(_array->count >= _index + _valueCount);
        ::memcpy(GetData(*_array) + _index, _values, _valueCount * sizeof(T));
    }

    template <typename T>
    DJO_INLINE void Set(Array<T>* const _array, const sz _index, const Buffer<const T>& _values) noexcept
    {
        Set(_array, _index, _values.data, _values.count);
    }

    template <typename T>
    void Insert(Array<T>* const _array, const sz _index, const T* const _values, const sz _valueCount) noexcept
    {
        DJO_ASSERT(_index <= _array->count);
        DJO_ASSERT(GetCapacity(*_array) >= _array->count + _valueCount);

        const sz moveCount = _array->count - _index;
        if (moveCount > 0)
        {
            T* const insertPtr = GetData(*_array) + _index;
            ::memcpy(insertPtr + _valueCount, insertPtr, moveCount * sizeof(T));
        }

        _array->count += _valueCount;
        Set(_array, _index, _values, _valueCount);
    }

    template <typename T>
    DJO_INLINE void Insert(Array<T>* const _array, const sz _index, CopyOrConstRef_t<const T> _value) noexcept
    {
        Insert(_array, _index, &_value, 1);
    }

    template <typename T>
    DJO_INLINE void Insert(Array<T>* const _array, const sz _index, const Buffer<const T>& _values) noexcept
    {
        Insert(_array, _index, _values.data, _values.count);
    }

    template <typename T>
    DJO_INLINE void InsertBack(Array<T>* const _array, CopyOrConstRef_t<const T> _value) noexcept
    {
        Insert(_array, _array->count, _value);
    }

    template <typename T>
    DJO_INLINE void InsertBack(Array<T>* const _array, const T* const _values, const sz _valueCount) noexcept
    {
        Insert(_array, _array->count, _values, _valueCount);
    }

    template <typename T>
    DJO_INLINE void InsertBack(Array<T>* const _array, const Buffer<const T>& _values) noexcept
    {
        Insert(_array, _array->count, _values.data, _values.count);
    }

    template <typename T>
    DJO_INLINE void InsertFront(Array<T>* const _array, CopyOrConstRef_t<const T> _value) noexcept
    {
        Insert(_array, 0, _value);
    }

    template <typename T>
    DJO_INLINE void InsertFront(Array<T>* const _array, const T* const _values, const sz _valueCount) noexcept
    {
        Insert(_array, 0, _values, _valueCount);
    }

    template <typename T>
    DJO_INLINE void InsertFront(Array<T>* const _array, const Buffer<const T>& _values) noexcept
    {
        Insert(_array, 0, _values.data, _values.count);
    }

    template <typename T>
    void EraseAt(Array<T>* const _array, const sz _index, const sz _count = 1) noexcept
    {
        DJO_ASSERT((_index + _count) <= _array->count);

        const sz remainingCount = _array->count - (_index + _count);
        if (remainingCount > 0)
        {
            ::memcpy(
                GetData(*_array) + _index,
                GetData(*_array) + _index + _count,
                remainingCount * sizeof(T));
        }
        _array->count -= _count;
    }

    template <typename T>
    DJO_INLINE void EraseBack(Array<T>* const _array, const sz _count = 1) noexcept
    {
        DJO_ASSERT(_count <= _array->count);

        _array->count -= _count;
    }

    template <typename T>
    DJO_INLINE void EraseFront(Array<T>* const _array, const sz _count = 1) noexcept
    {
        EraseAt(_array, 0, _count);
    }

    template <typename T>
    void EraseMove(Array<T>* const _array, const sz _index, const sz _count = 1) noexcept
    {
        DJO_ASSERT((_index + _count) <= _array->count);

        const sz remainingCount = _array->count - (_index + _count);
        if (remainingCount > 0)
        {
            const sz moveCount = Min(_count, remainingCount);
            ::memcpy(
                GetData(*_array) + _index,
                GetData(*_array) + _array->count - moveCount,
                moveCount * sizeof(T));
        }
        _array->count -= _count;
    }

    template <typename T, typename TVisitor>
    DJO_INLINE void Visit(const Array<T>& _array, TVisitor&& _visitor) noexcept
    {
        const sz count = _array.count;
        for (sz i = 0; i < count; ++i)
        {
            if (_visitor(*GetAt(_array, i), i) == EVisitorResult::Break)
                break;
        }
    }


    template <typename T>
    struct Hasher<Array<T>>
    {
        DJO_INLINE u64 operator () (const Array<T>& _array) const noexcept
        {
            return Hash64(GetData(_array), GetSize(_array));
        }
    };
}
