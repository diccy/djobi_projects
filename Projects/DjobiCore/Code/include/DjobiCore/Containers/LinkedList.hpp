#pragma once

#include <DjobiCore/Assert/Assert.hpp>


namespace Djo
{
    template <typename TNode>
    struct LinkedList
    {
        using NodeType = TNode;

        TNode* head;
        TNode* tail;
    };

    namespace SLList // singly linked list
    {
        // node interface is
        // TNode* next;

        template <typename TNode>
        DJO_INLINE TNode* GetTail(TNode* const _node)
        {
            if (_node == nullptr)
                return nullptr;

            TNode* tail = _node;
            while (tail->next != nullptr)
                tail = tail->next;
            return tail;
        }
        
        template <typename TNode>
        DJO_INLINE void InsertAfter(TNode* const _existingNode, TNode* const _newNode) noexcept
        {
            TNode* const next = _existingNode->next;
            _existingNode->next = _newNode;
            GetTail(_newNode)->next = next;
        }

        template <typename TNode>
        DJO_INLINE void RemoveNext(TNode* const _node) noexcept
        {
            TNode* const next = _node->next;
            if (next == nullptr)
                return;

            _node->next = next->next;
            next->next = nullptr;
        }

        template <typename TNode, typename TVisitor>
        DJO_INLINE void Visit(TNode* const _node, TVisitor&& _visitor) noexcept
        {
            TNode* node = _node;
            while (node != nullptr)
            {
                if (_visitor(*node) == EVisitorResult::Break)
                    break;

                node = node->next;
            }
        }

        template <typename TNode>
        DJO_INLINE void AddFront(LinkedList<TNode>* const _list, TNode* const _newNode) noexcept
        {
            if (_list->head == nullptr)
            {
                DJO_ASSERT(_list->tail == nullptr);
                _list->tail = GetTail(_newNode);
            }
            else
            {
                InsertAfter(_newNode, _list->head);
            }

            _list->head = _newNode;
        }

        template <typename TNode>
        DJO_INLINE void InsertBack(LinkedList<TNode>* const _list, TNode* const _newNode) noexcept
        {
            if (_list->tail == nullptr)
            {
                DJO_ASSERT(_list->head == nullptr);
                _list->head = _newNode;
            }
            else
            {
                InsertAfter(_list->tail, _newNode);
            }

            _list->tail = GetTail(_newNode);
        }

        template <typename TNode>
        DJO_INLINE TNode* RemoveFront(LinkedList<TNode>* const _list) noexcept
        {
            TNode* const node = _list->head;
            if (node != nullptr)
            {
                _list->head = node->next;
                node->next = nullptr;
                if (_list->tail == node)
                    _list->tail = _list->head;
            }
            return node;
        }

        template <typename TNode, typename TVisitor>
        DJO_INLINE void Visit(const LinkedList<TNode>& _list, TVisitor&& _visitor) noexcept
        {
            Visit(_list->head, _visitor);
        }
    }

    namespace DLList // doubly linked list
    {
        // node interface is
        // TNode* previous;
        // TNode* next;

        template <typename TNode>
        DJO_INLINE TNode* GetHead(TNode* const _node)
        {
            if (_node == nullptr)
                return nullptr;
            TNode* head = _node;
            while (head->previous != nullptr)
                head = head->previous;
            return head;
        }

        template <typename TNode>
        DJO_INLINE TNode* GetTail(TNode* const _node)
        {
            if (_node == nullptr)
                return nullptr;
            TNode* tail = _node;
            while (tail->next != nullptr)
                tail = tail->next;
            return tail;
        }

        template <typename TNode>
        DJO_INLINE void InsertBefore(TNode* const _existingNode, TNode* const _newNode) noexcept
        {
            TNode* const previous = _existingNode->previous;
            TNode* const newHead = GetHead(_newNode);
            TNode* const newTail = GetTail(_newNode);

            _existingNode->previous = newTail;
            newTail->next = _existingNode;

            newHead->previous = previous;
            if (previous != nullptr)
                previous->next = newHead;
        }

        template <typename TNode>
        DJO_INLINE void InsertAfter(TNode* const _existingNode, TNode* const _newNode) noexcept
        {
            TNode* const next = _existingNode->next;
            TNode* const newHead = GetHead(_newNode);
            TNode* const newTail = GetTail(_newNode);

            _existingNode->next = newHead;
            newHead->previous = _existingNode;

            newTail->next = next;
            if (next != nullptr)
                next->previous = newTail;
        }

        template <typename TNode>
        DJO_INLINE void Remove(TNode* const _node) noexcept
        {
            TNode* const previous = _node->previous;
            TNode* const next = _node->next;

            if (previous != nullptr)
                previous->next = next;
            if (next != nullptr)
                next->previous = previous;

            _node->previous = nullptr;
            _node->next = nullptr;
        }

        template <typename TNode>
        DJO_INLINE void AddFront(LinkedList<TNode>* const _list, TNode* const _newNode) noexcept
        {
            if (_list->head == nullptr)
            {
                DJO_ASSERT(_list->tail == nullptr);
                _list->tail = GetTail(_newNode);
            }
            else
            {
                InsertAfter(_newNode, _list->head);
            }

            _list->head = _newNode;
        }

        template <typename TNode>
        DJO_INLINE void InsertBack(LinkedList<TNode>* const _list, TNode* const _newNode) noexcept
        {
            if (_list->tail == nullptr)
            {
                DJO_ASSERT(_list->head == nullptr);
                _list->head = GetHead(_newNode);
            }
            else
            {
                InsertAfter(_list->tail, _newNode);
            }

            _list->tail = GetTail(_newNode);
        }

        template <typename TNode>
        DJO_INLINE TNode* RemoveFront(LinkedList<TNode>* const _list) noexcept
        {
            TNode* const node = _list->head;
            if (node != nullptr)
            {
                _list->head = node->next;
                Remove(node);
                if (_list->tail == node)
                    _list->tail = _list->head;
            }
            return node;
        }

        template <typename TNode>
        DJO_INLINE TNode* RemoveBack(LinkedList<TNode>* const _list) noexcept
        {
            TNode* const node = _list->tail;
            if (node != nullptr)
            {
                _list->tail = node->previous;
                Remove(node);
                if (_list->head == node)
                    _list->head = _list->tail;
            }
            return node;
        }
    }
}
