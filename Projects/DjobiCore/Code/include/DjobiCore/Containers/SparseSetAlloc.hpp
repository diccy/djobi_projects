#pragma once

#include <DjobiCore/Containers/SparseSet.hpp>
#include <DjobiCore/Containers/Policies.hpp>
#include <DjobiCore/Memory/Arena.hpp>


namespace Djo
{
    template <typename T>
    void ReserveDense(SparseSet<T>* const _ss, Memory::Arena* const _arena, const u32 _denseDataAllocCount) noexcept
    {
        DJO_ASSERT(_denseDataAllocCount <= GetDenseBudget(*_ss));

        if (GetDenseCapacity(*_ss) < _denseDataAllocCount)
        {
            const u32 pageElemCount = (u32)Pages::GetPageElemCount(_ss->densePageElemCountPow2);
            const u32 newAllocCount = _denseDataAllocCount - GetDenseCapacity(*_ss);
            const u32 newPageCount = (newAllocCount / pageElemCount) + 1;
            const u32 dataPageSize = pageElemCount * (u32)sizeof(T);
            const u32 sparseIdsPageSize = pageElemCount * (u32)sizeof(u32);
            const Memory::Area dataArea = DJO_MEMORY_ARENA_PUSH(_arena, newPageCount * dataPageSize);
            const Memory::Area sparseIdsArea = DJO_MEMORY_ARENA_PUSH(_arena, newPageCount * sparseIdsPageSize);
            for (sz i = 0; i < newPageCount; ++i)
            {
                _ss->denseDataPages[_ss->densePageCount + i] = reinterpret_cast<T*>(dataArea.data + (i * dataPageSize));
                _ss->denseSparseIdsPages[_ss->densePageCount + i] = reinterpret_cast<u32*>(sparseIdsArea.data + (i * sparseIdsPageSize));
            }

            _ss->densePageCount += static_cast<u8>(newPageCount);
        }
    }

    template <typename T>
    void ReserveSparse(SparseSet<T>* const _ss, Memory::Arena* const _arena, const u32 _sparseDataAllocCount) noexcept
    {
        DJO_ASSERT(_sparseDataAllocCount <= GetSparseBudget(*_ss));

        if (GetSparseCapacity(*_ss) < _sparseDataAllocCount)
        {
            const u32 pageElemCount = (u32)Pages::GetPageElemCount(_ss->sparsePageElemCountPow2);
            const u32 newAllocCount = _sparseDataAllocCount - GetSparseCapacity(*_ss);
            const u32 newPageCount = (newAllocCount / pageElemCount) + 1;
            const u32 pageSize = pageElemCount * (u32)sizeof(u32);
            const Memory::Area area = DJO_MEMORY_ARENA_PUSH(_arena, newPageCount * pageSize);
            ::memset(area.data, SparseSetConst::c_tombstone, area.count);
            for (sz i = 0; i < newPageCount; ++i)
                _ss->sparseDenseIdsPages[_ss->sparsePageCount + i] = reinterpret_cast<u32*>(area.data + (i * pageSize));

            _ss->sparsePageCount += static_cast<u8>(newPageCount);
        }
    }

    template <typename T>
    SparseSet<T> PushEmptySparseSet(Memory::Arena* const _arena, const u8 _densePageCount, const u32 _densePageElemCount, const u32 _denseDataAllocCount, const u8 _sparsePageCount, const u32 _sparsePageElemCount, const u32 _sparseDataAllocCount) noexcept
    {
        if (_densePageCount == 0 || _densePageElemCount == 0 || _sparsePageCount == 0 || _sparsePageElemCount == 0)
            return c_nullSparseSet<T>;

        const sz densePageElemCount = CeilPow2(_densePageElemCount);
        const sz sparsePageElemCount = CeilPow2(_sparsePageElemCount);
        const Memory::Area denseDataPagesArea = DJO_MEMORY_ARENA_PUSH(_arena, _densePageCount * sizeof(T*));
        const Memory::Area denseSparseIdsPagesArea = DJO_MEMORY_ARENA_PUSH(_arena, _densePageCount * sizeof(u32*));
        const Memory::Area sparseDenseIdsPagesArea = DJO_MEMORY_ARENA_PUSH(_arena, _sparsePageCount * sizeof(u32*));
        SparseSet<T> sparseSet = c_nullSparseSet<T>;
        sparseSet.denseDataPages = reinterpret_cast<T**>(denseDataPagesArea.data);
        sparseSet.denseSparseIdsPages = reinterpret_cast<u32**>(denseSparseIdsPagesArea.data);
        sparseSet.sparseDenseIdsPages = reinterpret_cast<u32**>(sparseDenseIdsPagesArea.data);
        sparseSet.densePageCapacity = _densePageCount;
        sparseSet.densePageElemCountPow2 = GetFirstBitIndex(densePageElemCount);
        sparseSet.sparsePageCapacity = _sparsePageCount;
        sparseSet.sparsePageElemCountPow2 = GetFirstBitIndex(sparsePageElemCount);
        ReserveDense(&sparseSet, _arena, _denseDataAllocCount);
        ReserveSparse(&sparseSet, _arena, _sparseDataAllocCount);
        return sparseSet;
    }

    template <typename T>
    DJO_INLINE void SetAtSparse(SparseSet<T>* const _ss, Memory::Arena* const _arena, const u32 _i) noexcept
    {
        ReserveDense(_ss, _arena, GetCount(_ss) + 1);
        ReserveSparse(_ss, _arena, _i);
        SetAtSparse(_ss, _i);
    }
}
