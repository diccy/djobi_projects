#pragma once

#include <DjobiCore/Containers/String8.hpp>
#include <DjobiCore/Containers/ArrayAlloc.hpp>
#include <DjobiCore/Memory/Arena.hpp>


namespace Djo
{
    String8 PushEmptyString8(Memory::Arena* const _arena, const sz _length) noexcept;
    String8 PushString8(Memory::Arena* const _arena, const StringView& _sv, const sz _length = 0) noexcept;
    String8 PushString8Formatted(Memory::Arena* const _arena, const char* const _format, ...) noexcept;

    String8 AllocEmptyString8(const Memory::Allocator& _allocator, const sz _length) noexcept;
    String8 AllocString8(const Memory::Allocator& _allocator, const StringView& _sv, const sz _length = 0) noexcept;
    String8 AllocString8Formatted(const Memory::Allocator& _allocator, const char* const _format, ...) noexcept;

    DJO_INLINE void ReallocString8(String8* const _str, const Memory::Allocator& _allocator, const sz _allocCount) noexcept
    {
        ReallocArray(_str, _allocator, _allocCount);
    }

    DJO_INLINE void FreeString8(String8* const _str, const Memory::Allocator& _allocator) noexcept
    {
        FreeArray(_str, _allocator);
    }
}
