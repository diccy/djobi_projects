#pragma once

#include <DjobiCore/Containers/PageArray.hpp>


namespace Djo
{
    namespace SparseSetConst
    {
        constexpr u32 c_tombstone{ c_invalidUint<u32> };
    }

    template <typename T>
    struct SparseSet
    {
        T** denseDataPages;
        u32** denseSparseIdsPages;
        u32** sparseDenseIdsPages;
        u32 denseCount;
        u8 densePageCapacity;
        u8 densePageCount;
        u8 densePageElemCountPow2;
        u8 sparsePageCapacity;
        u8 sparsePageCount;
        u8 sparsePageElemCountPow2;
    };

    DJO_DEFINE_NULL_CONSTEXPR_STATIC_T(typename T, SparseSet<T>, SparseSet, (nullptr, nullptr, nullptr, 0, 0, 0, 0, 0, 0, 0));


    template <typename T>
    constexpr u32 GetSparseCapacity(const SparseSet<T>& _ss) noexcept
    {
        return _ss.sparsePageCount << _ss.sparsePageElemCountPow2;
    }

    template <typename T>
    constexpr u32 GetDenseCapacity(const SparseSet<T>& _ss) noexcept
    {
        return _ss.densePageCount << _ss.densePageElemCountPow2;
    }

    template <typename T>
    constexpr u32 GetSparseBudget(const SparseSet<T>& _ss) noexcept
    {
        return _ss.sparsePageCapacity << _ss.sparsePageElemCountPow2;
    }

    template <typename T>
    constexpr u32 GetDenseBudget(const SparseSet<T>& _ss) noexcept
    {
        return _ss.densePageCapacity << _ss.densePageElemCountPow2;
    }

    template <typename T>
    constexpr u32 GetCount(const SparseSet<T>& _ss) noexcept
    {
        return _ss.denseCount;
    }

    template <typename T>
    constexpr bool IsEmpty(const SparseSet<T>& _ss) noexcept
    {
        return _ss.denseDataPages == nullptr || _ss.denseCount == 0;
    }

    template <typename T>
    constexpr T* GetAt(const SparseSet<T>& _ss, const u32 _i) noexcept
    {
        DJO_ASSERT(!IsEmpty(_ss) && _i < GetCount(_ss));

        return Pages::GetAt(_ss.denseDataPages, _ss.densePageElemCountPow2, _i);
    }

    template <typename T>
    DJO_INLINE T* GetAtSparse(const SparseSet<T>& _ss, const u32 _i) noexcept
    {
        if (_i >= GetSparseCapacity(_ss))
            return nullptr;

        const u32 denseId = *Pages::GetAt(_ss.sparseDenseIdsPages, _ss.sparsePageElemCountPow2, _i);
        DJO_ASSERT(denseId == SparseSetConst::c_tombstone || denseId < GetCount(_ss));
        if (denseId == SparseSetConst::c_tombstone)
            return nullptr;

        return GetAt(_ss, denseId);
    }

    template <typename T>
    T* SetAtSparse(SparseSet<T>* const _ss, const u32 _i) noexcept
    {
        DJO_ASSERT(_i < GetSparseCapacity(*_ss));

        u32* const denseIdPtr = Pages::GetAt<u32>(_ss->sparseDenseIdsPages, _ss->sparsePageElemCountPow2, _i);
        DJO_ASSERT(*denseIdPtr == SparseSetConst::c_tombstone || *denseIdPtr < GetCount(*_ss));
        if (*denseIdPtr == SparseSetConst::c_tombstone)
        {
            DJO_ASSERT((_ss->denseCount + 1) <= GetDenseCapacity(*_ss));
            *denseIdPtr = _ss->denseCount++;
        }
        *Pages::GetAt(_ss->denseSparseIdsPages, _ss->densePageElemCountPow2, *denseIdPtr) = _i;
        return GetAt(*_ss, *denseIdPtr);
    }

    template <typename T>
    bool EraseAt(SparseSet<T>* const _ss, const u32 _i) noexcept
    {
        DJO_ASSERT(_i < GetSparseCapacity(*_ss));

        u32* const denseIdPtr = Pages::GetAt(_ss->sparseDenseIdsPages, _ss->sparsePageElemCountPow2, _i);
        if (*denseIdPtr == SparseSetConst::c_tombstone)
            return false;

        DJO_ASSERT(*denseIdPtr < _ss->denseCount);
        if (*denseIdPtr < (_ss->denseCount - 1))
        {
            ::memcpy(
                Pages::GetAt(_ss->denseDataPages, _ss->densePageElemCountPow2, _i),
                Pages::GetAt(_ss->denseDataPages, _ss->densePageElemCountPow2, _ss->denseCount - 1),
                sizeof(T));

            u32* const lastSparseIdPtr = Pages::GetAt(_ss->denseSparseIdsPages, _ss->densePageElemCountPow2, _ss->denseCount - 1);
            *Pages::GetAt(_ss->denseSparseIdsPages, _ss->densePageElemCountPow2, *denseIdPtr) = *lastSparseIdPtr;
            *Pages::GetAt(_ss->sparseDenseIdsPages, _ss->sparsePageElemCountPow2, *lastSparseIdPtr) = *denseIdPtr;
        }
        *denseIdPtr = SparseSetConst::c_tombstone;
        --_ss->denseCount;
        return true;
    }
}
