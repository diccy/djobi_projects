#pragma once

#include <DjobiCore/Containers/Array.hpp>


namespace Djo
{
    template <typename T>
    struct OStream
    {
        struct OutputParams
        {
            void (*callback)(const T* /*_data*/, sz /*_count*/, void* /*_userData*/) noexcept;
            void* userData;
        };

        Array<T> array;
        OutputParams output;
    };

    namespace _Internal
    {
        template <typename T>
        sz Output(const typename OStream<T>::OutputParams& _params, const T* const _elems, const sz _count) noexcept
        {
            if (_params.callback != nullptr)
            {
                _params.callback(_elems, _count, _params.userData);
                return _count;
            }
            return 0;
        }

        template <typename T>
        bool PreparePush(OStream<T>* const _stream, const sz _size) noexcept
        {
            if (GetCapacity(_stream->array) < _stream->array.count + _size)
                if (_stream->output.callback != nullptr)
                    Flush(_stream);

            return GetCapacity(_stream->array) >= _stream->array.count + _size;
        }
    }

    template <typename T>
    sz Push(OStream<T>* const _stream, const T* const _elems, const sz _count) noexcept
    {
        if (GetCapacity(_stream->array) < _count)
            return _Internal::Output(_stream->output, _elems, _count);

        if (!_Internal::PreparePush(_stream, _count))
            return 0;

        InsertBack(&_stream->array, _elems, _count);
        return _count;
    }

    template <typename T>
    sz Push(OStream<T>* const _stream, const T& _elem) noexcept
    {
        return Push(_stream, &_elem, 1);
    }

    template <typename T>
    sz Flush(OStream<T>* const _stream) noexcept
    {
        if (_stream->array.count == 0)
            return 0;

        const sz outputCount = _Internal::Output(_stream->output, GetData(_stream->array), _stream->array.count);
        _stream->array.count = 0;
        return outputCount;
    }
}
