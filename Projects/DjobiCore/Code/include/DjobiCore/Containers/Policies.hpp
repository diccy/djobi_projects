#pragma once

#include <DjobiCore/Core/Utils.hpp>


namespace Djo
{
    enum class GrowthPolicy : u32
    {
        Exact,
        Double,
    };

    constexpr sz Grow(const sz _baseCount, const sz _minNewCount, const GrowthPolicy _growthPolicy) noexcept
    {
        constexpr sz c_minCount = 8;
        switch (_growthPolicy)
        {
            case GrowthPolicy::Double:  return Max<sz>(c_minCount, _minNewCount, _baseCount * 2);
            case GrowthPolicy::Exact:   return _minNewCount;
            default:                    return Max<sz>(c_minCount, _minNewCount);
        }
    }
}
