#pragma once

#include <DjobiCore/Containers/Array.hpp>
#include <DjobiCore/Containers/Policies.hpp>
#include <DjobiCore/Memory/Arena.hpp>


namespace Djo
{
    namespace _Internal
    {
        template <typename T, typename TAllocate>
        DJO_INLINE Array<T> AllocateEmptyArrayT(TAllocate&& _alloc, const sz _allocCount) noexcept
        {
            if (_allocCount == 0)
                return c_nullArray<T>;

            const Memory::Area area = _alloc(_allocCount * sizeof(T));
            Array<T> array;
            array.buffer.data = reinterpret_cast<T*>(area.data);
            array.buffer.count = area.count / sizeof(T);
            array.count = 0;
            return array;
        }

        template <typename T>
        DJO_INLINE void ReallocArena(Array<T>* const _array, Memory::Arena* const _arena, const sz _allocCount) noexcept
        {
            const Memory::Area area = DJO_MEMORY_ARENA_REUSE(_arena, _array->buffer.data, _allocCount * sizeof(T));
            DJO_ASSERT(_array->buffer.data == nullptr || reinterpret_cast<const void*>(area.data) == reinterpret_cast<const void*>(_array->buffer.data));
            _array->buffer.data = reinterpret_cast<T*>(area.data);
            _array->buffer.count = area.count / sizeof(T);
        }

        template <typename T>
        DJO_INLINE void ReallocAlloc(Array<T>* const _array, const Memory::Allocator& _allocator, const sz _allocCount) noexcept
        {
            const Memory::Area area = Memory::Reallocate(_allocator, _array->buffer.data, _allocCount * sizeof(T));
            _array->buffer.data = reinterpret_cast<T*>(area.data);
            _array->buffer.count = area.count / sizeof(T);
        }

        template <typename T, typename TReallocate>
        DJO_INLINE void ReserveT(Array<T>* const _array, TReallocate&& _realloc, const sz _allocCount, const GrowthPolicy _growthPolicy = GrowthPolicy::Double) noexcept
        {
            if (_allocCount > GetCapacity(*_array))
            {
                const sz newAllocCount = Grow(GetCapacity(*_array), _allocCount, _growthPolicy);
                _realloc(_array, newAllocCount);
            }
        }

        template <typename T>
        DJO_INLINE void ReserveArena(Array<T>* const _array, Memory::Arena* const _arena, const sz _allocCount, const GrowthPolicy _growthPolicy = GrowthPolicy::Double) noexcept
        {
            return ReserveT<T>(
                _array,
                [_arena](Array<T>* const _array, const sz _allocCount) { ReallocArena(_array, _arena, _allocCount); },
                _allocCount,
                _growthPolicy);
        }

        template <typename T>
        DJO_INLINE void ReserveAlloc(Array<T>* const _array, const Memory::Allocator& _allocator, const sz _allocCount, const GrowthPolicy _growthPolicy = GrowthPolicy::Double) noexcept
        {
            return ReserveT<T>(
                _array,
                [_allocator](Array<T>* const _array, const sz _allocCount) { ReallocAlloc(_array, _allocator, _allocCount); },
                _allocCount,
                _growthPolicy);
        }
    }

    template <typename T>
    DJO_INLINE Array<T> PushEmptyArray(Memory::Arena* const _arena, const sz _allocCount) noexcept
    {
        return _Internal::AllocateEmptyArrayT<T>(
            [_arena](const sz _size) -> Memory::Area { return DJO_MEMORY_ARENA_PUSH(_arena, _size); },
            _allocCount);
    }

    template <typename T>
    DJO_INLINE Array<T> PushArray(Memory::Arena* const _arena, const sz _allocCount) noexcept
    {
        Array<T> array = PushEmptyArray<T>(_arena, _allocCount);
        array.count = _allocCount;
        return array;
    }

    template <typename T>
    DJO_INLINE Array<T> AllocEmptyArray(const Memory::Allocator& _allocator, const sz _allocCount) noexcept
    {
        return _Internal::AllocateEmptyArrayT<T>(
            [&_allocator](const sz _size) -> Memory::Area { return Memory::Allocate(_allocator, _size); },
            _allocCount);
    }

    template <typename T>
    DJO_INLINE Array<T> AllocArray(const Memory::Allocator& _allocator, const sz _allocCount) noexcept
    {
        Array<T> array = AllocEmptyArray<T>(_allocator, _allocCount);
        array.count = _allocCount;
        return array;
    }

    template <typename T>
    DJO_INLINE void ReallocArray(Array<T>* const _array, const Memory::Allocator& _allocator, const sz _allocCount) noexcept
    {
        if (_allocCount == 0)
        {
            Memory::Free(_allocator, _array->buffer.data);
            *_array = c_nullArray<T>;
            return;
        }

        const Memory::Area area = Memory::Reallocate(_allocator, _array->buffer.data, _allocCount * sizeof(T));
        Array<T> newArray;
        newArray.buffer.data = reinterpret_cast<T*>(area.data);
        newArray.buffer.count = area.count / sizeof(T);
        newArray.count = _allocCount;

        *_array = newArray;
    }

    template <typename T>
    DJO_INLINE void FreeArray(Array<T>* const _array, const Memory::Allocator& _allocator) noexcept
    {
        Memory::Free(_allocator, _array->buffer.data);
        *_array = s_nullArray<T>;
    }

    template <typename T>
    DJO_INLINE void InsertBack(Array<T>* const _array, Memory::Arena* const _arena, CopyOrConstRef_t<const T> _value, const GrowthPolicy _growthPolicy = GrowthPolicy::Double) noexcept
    {
        _Internal::ReserveArena(_array, _arena, GetCount(*_array) + 1, _growthPolicy);
        InsertBack(_array, _value);
    }

    template <typename T>
    DJO_INLINE void InsertBack(Array<T>* const _array, Memory::Arena* const _arena, const T* const _values, const sz _valueCount, const GrowthPolicy _growthPolicy = GrowthPolicy::Double) noexcept
    {
        _Internal::ReserveArena(_array, _arena, GetCount(*_array) + _valueCount, _growthPolicy);
        InsertBack(_array, _values, _valueCount);
    }

    template <typename T>
    DJO_INLINE void InsertBack(Array<T>* const _array, Memory::Arena* const _arena, const Buffer<const T>& _values, const GrowthPolicy _growthPolicy = GrowthPolicy::Double) noexcept
    {
        InsertBack(_array, _arena, _values.data, _values.count, _growthPolicy);
    }

    template <typename T>
    DJO_INLINE void InsertBack(Array<T>* const _array, const Memory::Allocator& _allocator, CopyOrConstRef_t<const T> _value, const GrowthPolicy _growthPolicy = GrowthPolicy::Double) noexcept
    {
        _Internal::ReserveAlloc(_array, _allocator, GetCount(*_array) + 1, _growthPolicy);
        InsertBack(_array, _value);
    }

    template <typename T>
    DJO_INLINE void InsertBack(Array<T>* const _array, const Memory::Allocator& _allocator, const T* const _values, const sz _valueCount, const GrowthPolicy _growthPolicy = GrowthPolicy::Double) noexcept
    {
        _Internal::ReserveAlloc(_array, _allocator, GetCount(*_array) + _valueCount, _growthPolicy);
        InsertBack(_array, _values, _valueCount);
    }

    template <typename T>
    DJO_INLINE void InsertBack(Array<T>* const _array, const Memory::Allocator& _allocator, const Buffer<const T>& _values, const GrowthPolicy _growthPolicy = GrowthPolicy::Double) noexcept
    {
        InsertBack(_array, _allocator, _values.data, _values.count, _growthPolicy);
    }

    template <typename T>
    DJO_INLINE void InsertFront(Array<T>* const _array, Memory::Arena* const _arena, CopyOrConstRef_t<const T> _value, const GrowthPolicy _growthPolicy = GrowthPolicy::Double) noexcept
    {
        _Internal::ReserveArena(_array, _arena, GetCount(*_array) + 1, _growthPolicy);
        InsertFront(_array, _value);
    }

    template <typename T>
    DJO_INLINE void InsertFront(Array<T>* const _array, Memory::Arena* const _arena, const T* const _values, const sz _valueCount, const GrowthPolicy _growthPolicy = GrowthPolicy::Double) noexcept
    {
        _Internal::ReserveArena(_array, _arena, GetCount(*_array) + _valueCount, _growthPolicy);
        InsertFront(_array, _values, _valueCount);
    }

    template <typename T>
    DJO_INLINE void InsertFront(Array<T>* const _array, Memory::Arena* const _arena, const Buffer<const T>& _values, const GrowthPolicy _growthPolicy = GrowthPolicy::Double) noexcept
    {
        InsertFront(_array, _arena, _values.data, _values.count, _growthPolicy);
    }

    template <typename T>
    DJO_INLINE void InsertFront(Array<T>* const _array, const Memory::Allocator& _allocator, CopyOrConstRef_t<const T> _value, const GrowthPolicy _growthPolicy = GrowthPolicy::Double) noexcept
    {
        _Internal::ReserveAlloc(_array, _allocator, GetCount(*_array) + 1, _growthPolicy);
        InsertFront(_array, _value);
    }

    template <typename T>
    DJO_INLINE void InsertFront(Array<T>* const _array, const Memory::Allocator& _allocator, const T* const _values, const sz _valueCount, const GrowthPolicy _growthPolicy = GrowthPolicy::Double) noexcept
    {
        _Internal::ReserveAlloc(_array, _allocator, GetCount(*_array) + _valueCount, _growthPolicy);
        InsertFront(_array, _values, _valueCount);
    }

    template <typename T>
    DJO_INLINE void InsertFront(Array<T>* const _array, const Memory::Allocator& _allocator, const Buffer<const T>& _values, const GrowthPolicy _growthPolicy = GrowthPolicy::Double) noexcept
    {
        InsertFront(_array, _allocator, _values.data, _values.count, _growthPolicy);
    }

    template <typename T>
    DJO_INLINE void Insert(Array<T>* const _array, Memory::Arena* const _arena, const sz _index, CopyOrConstRef_t<const T> _value, const GrowthPolicy _growthPolicy = GrowthPolicy::Double) noexcept
    {
        _Internal::ReserveArena(_array, _arena, GetCount(*_array) + 1, _growthPolicy);
        Insert(_array, _index, _value);
    }

    template <typename T>
    DJO_INLINE void Insert(Array<T>* const _array, Memory::Arena* const _arena, const sz _index, const T* const _values, const sz _valueCount, const GrowthPolicy _growthPolicy = GrowthPolicy::Double) noexcept
    {
        _Internal::ReserveArena(_array, _arena, GetCount(*_array) + _valueCount, _growthPolicy);
        Insert(_array, _index, _values, _valueCount);
    }

    template <typename T>
    DJO_INLINE void Insert(Array<T>* const _array, Memory::Arena* const _arena, const sz _index, const Buffer<const T>& _values, const GrowthPolicy _growthPolicy = GrowthPolicy::Double) noexcept
    {
        Insert(_array, _arena, _index, _values.data, _values.count, _growthPolicy);
    }

    template <typename T>
    DJO_INLINE void Insert(Array<T>* const _array, const Memory::Allocator& _allocator, const sz _index, CopyOrConstRef_t<const T> _value, const GrowthPolicy _growthPolicy = GrowthPolicy::Double) noexcept
    {
        _Internal::ReserveAlloc(_array, _allocator, GetCount(*_array) + 1, _growthPolicy);
        Insert(_array, _index, _value);
    }

    template <typename T>
    DJO_INLINE void Insert(Array<T>* const _array, const Memory::Allocator& _allocator, const sz _index, const T* const _values, const sz _valueCount, const GrowthPolicy _growthPolicy = GrowthPolicy::Double) noexcept
    {
        _Internal::ReserveAlloc(_array, _allocator, GetCount(*_array) + _valueCount, _growthPolicy);
        Insert(_array, _index, _values, _valueCount);
    }

    template <typename T>
    DJO_INLINE void Insert(Array<T>* const _array, const Memory::Allocator& _allocator, const sz _index, const Buffer<const T>& _values, const GrowthPolicy _growthPolicy = GrowthPolicy::Double) noexcept
    {
        Insert(_array, _allocator, _index, _values.data, _values.count, _growthPolicy);
    }
}
