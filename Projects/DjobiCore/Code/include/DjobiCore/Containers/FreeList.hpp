#pragma once

#include <DjobiCore/Memory/Memory.hpp>


namespace Djo
{
    struct FreeList
    {
        FreeList* next;
    };

    DJO_INLINE FreeList* InitInplaceFreeList(const Memory::Area& _area, const sz _elemSize) noexcept
    {
        DJO_ASSERT(_area.data != nullptr);
        const sz elemCount = _area.count / _elemSize;
        DJO_ASSERT(elemCount > 0);

        u8* freeList = _area.data;
        for (sz i = 0; i < (elemCount - 1); ++i, freeList += _elemSize)
            reinterpret_cast<FreeList*>(freeList)->next = reinterpret_cast<FreeList*>(freeList + _elemSize);
        reinterpret_cast<FreeList*>(freeList)->next = nullptr;

        return reinterpret_cast<FreeList*>(_area.data);
    }

    DJO_INLINE void* AcquireElement(FreeList** const _freeList) noexcept
    {
        FreeList* const freeElem = *_freeList;
        if (freeElem == nullptr)
            return nullptr;

        *_freeList = freeElem->next;
        return freeElem;
    }

    DJO_INLINE void ReleaseElement(FreeList** const _freeList, void* const _elem) noexcept
    {
        FreeList* const releasedElem = reinterpret_cast<FreeList*>(_elem);
        releasedElem->next = *_freeList;
        *_freeList = releasedElem;
    }

    struct FreeIndices
    {
        u32 nextOffset;
    };

    DJO_INLINE FreeIndices* InitInplaceFreeIndices(const Memory::Area& _area, const u32 _elemSize) noexcept
    {
        DJO_ASSERT(_area.data != nullptr);
        const u32 elemCount = (u32)_area.count / _elemSize;
        DJO_ASSERT(elemCount > 0);

        u32 offset = 0;
        for (sz i = 0; i < (elemCount - 1); ++i, offset += _elemSize)
            reinterpret_cast<FreeIndices*>(_area.data + offset)->nextOffset = offset + _elemSize;
        reinterpret_cast<FreeIndices*>(_area.data + offset)->nextOffset = c_invalidUint<u32>;

        return reinterpret_cast<FreeIndices*>(_area.data);
    }

    DJO_INLINE void* AcquireElement(const Memory::Area& _area, FreeIndices** const _freeIndices) noexcept
    {
        FreeIndices* const freeElem = *_freeIndices;
        if (freeElem == nullptr)
            return nullptr;

        DJO_ASSERT(Memory::IsInArea(freeElem, _area));
        const u32 offset = freeElem->nextOffset;
        DJO_ASSERT(offset == c_invalidUint<u32> || offset < _area.count);
        *_freeIndices = (offset != c_invalidUint<u32>)
            ? reinterpret_cast<FreeIndices*>(_area.data + offset)
            : nullptr;

        return freeElem;
    }

    DJO_INLINE void ReleaseElement(const Memory::Area& _area, FreeIndices** const _freeIndices, void* const _elem) noexcept
    {
        DJO_ASSERT(Memory::IsInArea(_elem, _area));
        FreeIndices* const releasedElem = reinterpret_cast<FreeIndices*>(_elem);
        releasedElem->nextOffset = static_cast<u32>(reinterpret_cast<uptr>(*_freeIndices) - reinterpret_cast<uptr>(_area.data));
        *_freeIndices = releasedElem;
    }
}
