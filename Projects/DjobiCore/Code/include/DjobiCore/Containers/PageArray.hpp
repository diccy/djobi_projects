#pragma once

#include <DjobiCore/Containers/Array.hpp>


namespace Djo
{
    namespace Pages
    {
        constexpr sz GetPageElemCount(const u8 _pageElemCountPow2) noexcept
        {
            return 1ull << _pageElemCountPow2;
        }

        constexpr sz GetPageIndexFromElemIndex(const u8 _pageElemCountPow2, const sz _i) noexcept
        {
            return _i >> _pageElemCountPow2;
        }

        template <typename T>
        constexpr T* GetAt(T** const _pages, const u8 _pageElemCountPow2, const sz _i) noexcept
        {
            return _pages[GetPageIndexFromElemIndex(_pageElemCountPow2, _i)] + (_i & (GetPageElemCount(_pageElemCountPow2) - 1));
        }
    }

    namespace PageArrayConst
    {
        static constexpr sz c_defaultPageCount = 8;
        static constexpr sz c_defaultPageElemCount = 16;
    }

    template <typename T>
    struct PageArray
    {
        using DataType = T;

        Array<T*> pages;
        sz count;
        u8 pageElemCountPow2;

        constexpr operator const PageArray<const T>& () const noexcept  { return *AsConst(this); }
        constexpr operator PageArray<const T>& () noexcept              { return *AsConst(this); }

        constexpr const T& operator[](const sz _i) const noexcept  { return *GetAt(*this, _i); }
        constexpr T& operator[](const sz _i) noexcept              { return *GetAt(*this, _i); }
    };

    DJO_DEFINE_NULL_CONSTEXPR_STATIC_T(typename T, PageArray<T>, PageArray, (c_nullArray<T*>, 0, 0));


    template <typename T>
    constexpr sz GetCapacity(const PageArray<T>& _pa) noexcept
    {
        return GetCount(_pa.pages) << _pa.pageElemCountPow2;
    }

    template <typename T>
    constexpr sz GetBudget(const PageArray<T>& _pa) noexcept
    {
        return GetCapacity(_pa.pages) << _pa.pageElemCountPow2;
    }

    template <typename T>
    constexpr sz GetCount(const PageArray<T>& _pa) noexcept
    {
        return _pa.count;
    }

    template <typename T>
    constexpr bool IsEmpty(const PageArray<T>& _pa) noexcept
    {
        return IsEmpty(_pa.pages) || _pa.count == 0;
    }

    template <typename T>
    constexpr T* GetAt(const PageArray<T>& _pa, const sz _i) noexcept
    {
        DJO_ASSERT(!IsEmpty(_pa) && _i < _pa.count);

        return Pages::GetAt(GetData(_pa.pages), _pa.pageElemCountPow2, _i);
    }

    template <typename T>
    constexpr T* GetFirst(const PageArray<T>& _pa) noexcept
    {
        return GetAt(_pa, 0);
    }

    template <typename T>
    constexpr T* GetLast(const PageArray<T>& _pa) noexcept
    {
        return GetAt(_pa, _pa.count - 1);
    }

    template <typename T>
    constexpr sz Find(const PageArray<T>& _pa, CopyOrConstRef_t<const T> _value) noexcept
    {
        const sz pageCount = _pa->pages.count;
        const sz pageElemCount = Pages::GetPageElemCount(_pa.pageElemCountPow2);
        for (sz i = 0; i < pageCount; ++i)
        {
            const sz found = Find<T>(_pa->pages[i], pageElemCount, _value);
            if (found != c_npos)
                return found;
        }
        return c_npos;
    }

    template <typename T>
    constexpr sz FindLast(const PageArray<T>& _pa, CopyOrConstRef_t<const T> _value) noexcept
    {
        const sz pageCount = _pa->pages.count;
        const sz pageElemCount = Pages::GetPageElemCount(_pa.pageElemCountPow2);
        for (sz i = pageCount; i > 0; --i)
        {
            const sz found = FindLast<T>(_pa->pages[i - 1], pageElemCount, _value);
            if (found != c_npos)
                return found;
        }
        return c_npos;
    }

    template <typename T>
    void Set(PageArray<T>* const _pa, const sz _index, const T* const _values, const sz _valueCount) noexcept
    {
        DJO_ASSERT(_pa->count >= _index + _valueCount);
        DJO_ASSERT(GetCapacity(*_pa) >= _index + _valueCount);

        const sz startPageIndex = Pages::GetPageIndexFromElemIndex(_pa->pageElemCountPow2, _index);
        const sz endPageIndex = Pages::GetPageIndexFromElemIndex(_pa->pageElemCountPow2, (_index + _valueCount));
        const sz pageElemCount = Pages::GetPageElemCount(_pa->pageElemCountPow2);
        sz localIndex = _index % pageElemCount;
        sz remainingCount = _valueCount;
        const T* values = _values;
        for (sz i = startPageIndex; i <= endPageIndex && remainingCount > 0; ++i)
        {
            const sz moveCount = Min(remainingCount, pageElemCount - localIndex);
            ::memcpy(_pa->pages[i] + localIndex, values, moveCount * sizeof(T));
            localIndex = 0;
            remainingCount -= moveCount;
            values += moveCount;
        }
    }

    template <typename T>
    DJO_INLINE void Set(PageArray<T>* const _pa, const sz _index, const Buffer<const T>& _values) noexcept
    {
        Set(_pa, _index, _values.data, _values.count);
    }

    template <typename T>
    DJO_INLINE void InsertBack(PageArray<T>* const _pa, CopyOrConstRef_t<const T> _value) noexcept
    {
        DJO_ASSERT(GetCapacity(*_pa) >= _pa->count + 1);

        ++_pa->count;
        ::memcpy(GetLast(*_pa), &_value, sizeof(T));
    }

    template <typename T>
    DJO_INLINE void InsertBack(PageArray<T>* const _pa, const T* const _values, const sz _valueCount) noexcept
    {
        DJO_ASSERT(GetCapacity(*_pa) >= _pa->count + _valueCount);

        const sz index = _pa->count;
        _pa->count += _valueCount;
        Set(_pa, index, _values, _valueCount);
    }

    template <typename T>
    DJO_INLINE void InsertBack(PageArray<T>* const _pa, const Buffer<const T>& _values) noexcept
    {
        InsertBack(_pa, _values.data, _values.count);
    }

    template <typename T>
    DJO_INLINE void EraseBack(PageArray<T>* const _pa, const sz _count = 1) noexcept
    {
        DJO_ASSERT(_count <= _pa->count);

        _pa->count -= _count;
    }

    template <typename T, typename TVisitor>
    void Visit(const PageArray<T>& _pa, TVisitor&& _visitor) noexcept
    {
        const sz pageCount = _pa.pages.count;
        const sz pageElemCount = Pages::GetPageElemCount(_pa.pageElemCountPow2);
        const sz elemCount = _pa.count;

        DJO_ASSERT(elemCount == 0 || (pageCount > 0 && (pageCount - 1) * pageElemCount <= elemCount));
        DJO_ASSERT(elemCount <= pageCount * pageElemCount);
        
        sz i = 0;
        for (sz p = 0; p < pageCount; ++p)
        {
            T* const page = _pa.pages[p];
            for (sz e = 0; e < pageElemCount && i < elemCount; ++e)
            {
                if (_visitor(page[e], i) == EVisitorResult::Break)
                    return;

                ++i;
            }
        }
    }
}
