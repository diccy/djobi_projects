#pragma once

#include <DjobiCore/Assert/Assert.hpp>
#include <DjobiCore/Core/ClassTraits.hpp>
#include <DjobiCore/Core/Types.hpp>

#include <float.h>
#include <string.h> // memset
#include <intrin.h>


namespace Djo
{
    template <typename T>
    DJO_INLINE void ZeroMem(T* const _p) noexcept
    {
        ::memset(_p, 0, sizeof(T));
    }

    template <typename T, u32 TCount>
    constexpr u32 ArrayCount(const T(&)[TCount]) noexcept
    {
        return TCount;
    }

    template <template<typename> typename T, typename U>
    constexpr const T<const U>* AsConst(const T<U>* const _o) noexcept
    {
        // This is undefined behaviour, how to strengthen type safety?
        return (const T<const U>*)_o;
    }
    template <template<typename> typename T, typename U>
    constexpr T<const U>* AsConst(T<U>* const _o) noexcept
    {
        // This is undefined behaviour, how to strengthen type safety?
        return (T<const U>*)_o;
    }
    template <template<typename> typename T, typename U>
    constexpr const T<const U>* AsConst(const T<const U>* const _o) noexcept
    {
        return _o;
    }
    template <template<typename> typename T, typename U>
    constexpr T<const U>* AsConst(T<const U>* const _o) noexcept
    {
        return _o;
    }


    // Numeric utils

    constexpr f64 c_epsilon{ DBL_EPSILON };
    constexpr f32 c_epsilon_f{ FLT_EPSILON };

    template <typename T>
    constexpr u8 GetFirstBitIndex(T _n) noexcept
    {
        DJO_STATIC_ASSERT(c_isUnsignedInt<T>);
        DJO_ASSERT(_n > 0);

        u8 i = 0;
        while ((_n & 1) == 0)
        {
            _n >>= 1;
            ++i;
        }
        return i;
    }

    template <typename T>
    constexpr T Abs(const T _a) noexcept
    {
        return _a < T{ 0 } ? -_a : _a;
    }

    constexpr bool Equals(const f32 _a, const f32 _b, const f32 _eps = c_epsilon_f) noexcept
    {
        return Abs<f32>(_a - _b) <= _eps;
    }
    constexpr bool Equals(const f64 _a, const f64 _b, const f64 _eps = c_epsilon) noexcept
    {
        return Abs<f64>(_a - _b) <= _eps;
    }

    template <typename T>
    constexpr T Min(const T _a, const T _b) noexcept
    {
        return _a <= _b ? _a : _b;
    }
    template <typename T, typename... TMore>
    constexpr T Min(const T _a, const T _b, const TMore... _more) noexcept
    {
        return Min(Min<T>(_a, _b), _more...);
    }

    template <typename T>
    constexpr T Max(const T _a, const T _b) noexcept
    {
        return _a >= _b ? _a : _b;
    }
    template <typename T, typename... TMore>
    constexpr T Max(const T _a, const T _b, const TMore... _more) noexcept
    {
        return Max(Max<T>(_a, _b), _more...);
    }

    template <typename T>
    constexpr T Clamp(const T _a, const T _min, const T _max) noexcept
    {
        return Min<T>(Max<T>(_a, _min), _max);
    }

    template <typename T, DJO_TARG_ENABLE_IF(c_isUnsignedInt<T>)>
    constexpr T Align(const T _i, const T _a) noexcept
    {
        DJO_ASSERT(_i > 0); //DJO_TODO("Fix Align function with _i value == 0");
        DJO_ASSERT(_a > 0);
        return ((_i + _a - 1) / _a) * _a;
    }

    template <typename T, typename A>
    constexpr T* Align(const T* _p, const A _a) noexcept
    {
        DJO_STATIC_ASSERT(c_isUnsignedInt<A>);
        return reinterpret_cast<T*>(Align(reinterpret_cast<uptr>(_p), static_cast<uptr>(_a)));
    }
    template <typename T>
    constexpr T* Align(const T* _p, const T* _a) noexcept
    {
        return reinterpret_cast<T*>(Align(reinterpret_cast<uptr>(_p), reinterpret_cast<uptr>(_a)));
    }

    template <typename T>
    constexpr bool IsPow2(const T _i) noexcept
    {
        DJO_STATIC_ASSERT(c_isUnsignedInt<T>);
        return (_i == 0ull) || ((_i & (_i - 1ull)) == 0ull);
    }

    template <typename T>
    constexpr T AlignPow2(const T _i, const T _a) noexcept
    {
        DJO_STATIC_ASSERT(c_isUnsignedInt<T>);
        DJO_ASSERT(_i > 0); //DJO_TODO("Fix AlignPow2 function with _i value == 0");
        DJO_ASSERT(_a > 0);
        DJO_ASSERT(IsPow2(_a)); // Only works if _a is a power of 2

        return (_i + _a - 1) & ~(_a - 1);
    }

    template <typename T>
    constexpr T AlignFloor(const T _i, const T _a) noexcept
    {
        DJO_STATIC_ASSERT(c_isUnsignedInt<T>);
        DJO_ASSERT(_a > 0);

        return (_i / _a) * _a;
    }

    template <typename T, typename A>
    constexpr T* AlignFloor(const T* _p, const A _a) noexcept
    {
        DJO_STATIC_ASSERT(c_isUnsignedInt<T>);

        return reinterpret_cast<T*>(AlignFloor(reinterpret_cast<uptr>(_p), static_cast<uptr>(_a)));
    }
    template <typename T>
    constexpr T* AlignFloor(const T* _p, const T* _a) noexcept
    {
        return reinterpret_cast<T*>(AlignFloor(reinterpret_cast<uptr>(_p), reinterpret_cast<uptr>(_a)));
    }

    template <typename T>
    constexpr T AlignFloorPow2(const T _i, const T _a) noexcept
    {
        DJO_STATIC_ASSERT(c_isUnsignedInt<T>);
        DJO_ASSERT(IsPow2(_a)); // Only works if _a is a power of 2

        return _i & ~(_a - 1);
    }

    template <typename T>
    constexpr T CeilPow2(T _i) noexcept
    {
        DJO_STATIC_ASSERT(c_isUnsignedInt<T>);
        DJO_ASSERT(_i > 0); //DJO_TODO("Fix CeilPow2 function with base value == 0");

        --_i;
        _i |= _i >> 1ull;
        _i |= _i >> 2ull;
        _i |= _i >> 4ull;
        if constexpr (sizeof(T) > 1)
            _i |= _i >> 8ull;
        if constexpr (sizeof(T) > 2)
            _i |= _i >> 16ull;
        if constexpr (sizeof(T) > 4)
            _i |= _i >> 32ull;
        ++_i;
        return _i;
    }

    template <typename T>
    constexpr T SignOf(const T _n) noexcept
    {
        return static_cast<T>(T{ 0 } < _n) - static_cast<T>(_n < T{ 0 });
    }

    // Algo

    static constexpr sz c_npos{ c_invalidUint<sz> }; // std style

    template <typename T>
    constexpr sz Find(const T* const _data, const sz _count, CopyOrConstRef_t<const T> _value) noexcept
    {
        if (_count == 0)
            return c_npos;

        const T* it = _data;
        const T* const itEnd = _data + _count;
        for (; it != itEnd; ++it)
            if (*it == _value)
                return (sz)static_cast<ptrdiff>(it - _data);
        return c_npos;
    }

    template <typename T>
    constexpr sz FindLast(const T* const _data, const sz _count, CopyOrConstRef_t<const T> _value) noexcept
    {
        if (_count == 0)
            return c_npos;

        const T* it = _data + _count - 1;
        const T* const itFirst = _data;
        for (; it != itFirst; --it)
            if (*it == _value)
                return static_cast<sz>(static_cast<ptrdiff>(it - _data));
        return (*itFirst == _value) ? 0 : c_npos;
    }

    // C str utils

    constexpr sz StrLen(const char* _s) noexcept
    {
        sz len{ 0 };
        while (*_s++ != '\0')
            ++len;
        return len;
    }

    constexpr i64 StrNCmp(const char* _s1, const char* _s2, sz _count) noexcept
    {
        for (; 0 < _count; --_count, ++_s1, ++_s2)
        {
            if (*_s1 != *_s2)
                return SignOf<i64>(*_s1 - *_s2);
            if (*_s1 == '\0')
                return 0;
        }
        return 0;
    }

    constexpr const char* StrNChr(const char* _s, sz _count, const char _c) noexcept
    {
        for (; 0 < _count; --_count, ++_s)
        {
            if (*_s == _c)
                return _s;
            if (*_s == '\0')
                return nullptr;
        }
        return nullptr;
    }

    constexpr const char* StrNRChr(const char* const _s, sz _count, const char _c) noexcept
    {
        _count = Min(_count, StrLen(_s));
        if (_count == 0)
            return nullptr;

        for (const char* s = _s + _count - 1; 0 < _count; --_count, --s)
        {
            if (*s == _c)
                return s;
            if (s == _s)
                return nullptr;
        }
        return nullptr;
    }

    template <typename TStrNChrFn>
    constexpr sz TStrNChrI(const char* _s, const sz _count, const char _c, TStrNChrFn&& _fn) noexcept
    {
        const char* const p = _fn(_s, _count, _c);
        return p ? (sz)(p - _s) : c_npos;
    }
}
