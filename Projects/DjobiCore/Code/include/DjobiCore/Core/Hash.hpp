#pragma once

#include <DjobiCore/Core/Core.hpp>
#include <DjobiCore/Core/Types.hpp>


namespace Djo
{
    // Runtime hash

    namespace _Internal
    {
        // From Zilong Tan's "fasthash"
        // https://github.com/rurban/smhasher

        constexpr u64 c_hm = 0x880355f21e6d1965ull;

        constexpr u64 mix_hash(u64 _h) noexcept
        {
            _h ^= _h >> 23ull;
            _h *= 0x2127599bf4325c37ull;
            _h ^= _h >> 47ull;
            return _h;
        }

        DJO_INLINE u64 fasthash64(const void* const _buf, const sz _len) noexcept
        {
            const u64* pos = reinterpret_cast<const u64*>(_buf);
            const u64* end = pos + (_len / 8ull);
            u64 h = 1099511628211ull ^ (_len * c_hm);
            u64 v = 0ull;

            while (pos != end)
            {
                h ^= mix_hash(*pos++);
                h *= c_hm;
            }

            const u8* const pos2 = reinterpret_cast<const u8*>(pos);
            switch (_len & 7ull)
            {
                case 7ull: v ^= (u64)(pos2[6]) << 48ull; DJO_FALLTHROUGH;
                case 6ull: v ^= (u64)(pos2[5]) << 40ull; DJO_FALLTHROUGH;
                case 5ull: v ^= (u64)(pos2[4]) << 32ull; DJO_FALLTHROUGH;
                case 4ull: v ^= (u64)(pos2[3]) << 24ull; DJO_FALLTHROUGH;
                case 3ull: v ^= (u64)(pos2[2]) << 16ull; DJO_FALLTHROUGH;
                case 2ull: v ^= (u64)(pos2[1]) << 8ull;  DJO_FALLTHROUGH;
                case 1ull:
                    v ^= (u64)(pos2[0]);
                    h ^= mix_hash(v);
                    h *= c_hm;
            }

            return mix_hash(h);
        }

        DJO_INLINE u32 fasthash32(const void* const _buf, const sz _len) noexcept
        {
            const u64 h = fasthash64(_buf, _len);
            return (u32)(h - (h >> 32ull));
        }
    }

    DJO_INLINE u64 Hash64(const void* const _buf, const sz _len) noexcept
    {
        return _Internal::fasthash64(_buf, _len);
    }

    DJO_INLINE u32 Hash32(const void* const _buf, const sz _len) noexcept
    {
        return _Internal::fasthash32(_buf, _len);
    }

    // General hasher functor
    // It's a functor rather than a function, to allow partial template specialization
    // (see Hasher<T*> for example)
    template <typename T>
    struct Hasher
    {
        DJO_INLINE u64 operator () (const T& _data) const noexcept
        {
            return Hash64(&_data, sizeof(_data));
        }
    };

    // Simplified hash for numerical values

#define DJO_HASHER_INTEGRAL(type_)\
    template <>\
    struct Hasher<type_>\
    {\
        DJO_INLINE u64 operator () (const type_ _data) const noexcept\
        {\
            return _Internal::c_hm * _Internal::mix_hash((u64)_data);\
        }\
    }\

    DJO_HASHER_INTEGRAL(i8);
    DJO_HASHER_INTEGRAL(u8);
    DJO_HASHER_INTEGRAL(i16);
    DJO_HASHER_INTEGRAL(u16);
    DJO_HASHER_INTEGRAL(i32);
    DJO_HASHER_INTEGRAL(u32);
    DJO_HASHER_INTEGRAL(i64);
    DJO_HASHER_INTEGRAL(u64);

#undef DJO_HASHER_INTEGRAL

    template <>
    struct Hasher<f32>
    {
        DJO_INLINE u64 operator () (const f32 _data) const noexcept
        {
            return _Internal::c_hm * _Internal::mix_hash((u64)*reinterpret_cast<const u32*>(&_data));
        }
    };
    template <>
    struct Hasher<f64>
    {
        DJO_INLINE u64 operator () (const f64 _data) const noexcept
        {
            return _Internal::c_hm * _Internal::mix_hash(*reinterpret_cast<const u64*>(&_data));
        }
    };

    template <typename T>
    struct Hasher<T*>
    {
        DJO_INLINE u64 operator () (const T* _data) const noexcept
        {
            return _Internal::c_hm * _Internal::mix_hash((u64)(uptr)_data);
        }
    };


    // Compile time hash

    namespace _Internal
    {
        // FNV-1a
        // http://isthe.com/chongo/tech/comp/fnv/

        constexpr u32 c_fnv1aBaseSeed32 = 2166136261u;
        constexpr u32 c_fnv1aPrime32    = 16777619u;
        constexpr u64 c_fnv1aBaseSeed64 = 14695981039346656037ull;
        constexpr u64 c_fnv1aPrime64    = 1099511628211ull;

        constexpr u32 StrFnv1a_32(char const* const _cstr, sz _size) noexcept
        {
            u32 h = c_fnv1aBaseSeed32;
            while (_size > 0)
            {
                h ^= (u32)_cstr[--_size];
                h *= c_fnv1aPrime32;
            }
            return h;
        }

        constexpr u64 StrFnv1a_64(char const* const _cstr, sz _size) noexcept
        {
            u64 h = c_fnv1aBaseSeed64;
            while (_size > 0)
            {
                h ^= (u64)_cstr[--_size];
                h *= c_fnv1aPrime64;
            }
            return h;
        }
    }

    constexpr u32 operator"" _h32(char const* const _cstr, const sz _size) noexcept
    {
        return _Internal::StrFnv1a_32(_cstr, _size);
    }

    constexpr u64 operator"" _h64(char const* const _cstr, const sz _size) noexcept
    {
        return _Internal::StrFnv1a_64(_cstr, _size);
    }

    // check constexpr works
    DJO_STATIC_ASSERT("hello"_h32 != 0);
    DJO_STATIC_ASSERT("hello"_h64 != 0);
}

// fasthash notice:
/*
   The MIT License

   Copyright (C) 2012 Zilong Tan (eric.zltan@gmail.com)

   Permission is hereby granted, free of charge, to any person
   obtaining a copy of this software and associated documentation
   files (the "Software"), to deal in the Software without
   restriction, including without limitation the rights to use, copy,
   modify, merge, publish, distribute, sublicense, and/or sell copies
   of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be
   included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
   BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
   ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/
