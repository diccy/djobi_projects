#pragma once

#include <DjobiCore/Core/Core.hpp>
#include <DjobiCore/Core/Types.hpp>


namespace Djo
{
    enum class EResult : u32
    {
        OK = 0,
        FAIL = 0xFFFFFFFF,
    };

#if DJO_IS_ASSERT_AVAILABLE

    EResult MakeResult(EResult _result, const char* _errorMsg = nullptr);

#else

    DJO_INLINE EResult MakeResult(const EResult _result, const char* const _errorMsg = nullptr)
    {
        DJO_UNUSED(_errorMsg);
        return _result;
    }

#endif

#define DJO_OK_OR_RETURN(expr_)\
    {\
        const ::Djo::EResult result__ = (expr_);\
        if (result__ != ::Djo::EResult::OK)\
            return result__;\
    }
}
