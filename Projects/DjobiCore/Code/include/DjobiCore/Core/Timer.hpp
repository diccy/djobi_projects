#pragma once

#include <DjobiCore/OS/Time.hpp>
#include <DjobiCore/Core/Core.hpp>


namespace Djo
{
    struct Timer
    {
        OS::Time::TimePoint started;
    };

    DJO_INLINE void Start(Timer* const _timer) noexcept
    {
        _timer->started = OS::Time::Now();
    }

    template <typename TTime>
    DJO_INLINE TTime GetElapsed(const Timer& _timer) noexcept
    {
        const OS::Time::TimePoint elapsed{ OS::Time::Now().t - _timer.started.t };
        return ConvertTime<TTime>(elapsed);
    }

    template <typename TTime>
    DJO_INLINE TTime Restart(Timer* const _timer) noexcept
    {
        const TTime elapsed = GetElapsed(*_timer);
        Start(_timer);
        return elapsed;
    }
}
