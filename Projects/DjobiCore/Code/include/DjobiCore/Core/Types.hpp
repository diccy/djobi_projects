#pragma once

#include <stddef.h>
#include <stdint.h>


namespace Djo
{
    using i8 = char;
    using u8 = ::uint8_t;
    using i16 = ::int16_t;
    using u16 = ::uint16_t;
    using i32 = ::int32_t;
    using u32 = ::uint32_t;
    using i64 = ::int64_t;
    using u64 = ::uint64_t;
    using f32 = float;
    using f64 = double;
    using sz = ::size_t;
    using uptr = ::uintptr_t;
    using ptrdiff = ::ptrdiff_t;
}
