#pragma once

#include <DjobiCore/Core/Types.hpp>
#include <DjobiCore/Core/ClassTraits.hpp>


namespace Djo
{
    template <typename T, u64 N, u64 D>
    struct Time
    {
        using Type = T;
        static constexpr u64 N = N;
        static constexpr u64 D = D;

        T t;
    };

    template <typename T> using SecondsT      = Time<T, 1, 1>;
    template <typename T> using MillisecondsT = Time<T, 1, 1000>;
    template <typename T> using MicrosecondsT = Time<T, 1, 1000000>;
    template <typename T> using NanosecondsT  = Time<T, 1, 1000000000>;

    using Seconds      = SecondsT<f64>;
    using Milliseconds = MillisecondsT<f64>;
    using Microseconds = MicrosecondsT<f64>;

    using Seconds32      = SecondsT<f32>;
    using Milliseconds32 = MillisecondsT<f32>;
    using Microseconds32 = MicrosecondsT<f32>;

    template <typename TTo, typename TFrom>
    constexpr TTo ConvertTime(TFrom _t) noexcept
    {
        return TTo
        {
            static_cast<typename TTo::Type>(
                static_cast<As64_t<typename TFrom::Type>>(_t.t) *
                static_cast<As64_t<typename TTo::Type>>(TTo::D) *
                static_cast<As64_t<typename TTo::Type>>(TFrom::N) /
                static_cast<As64_t<typename TTo::Type>>(TFrom::D))
        };
    }

#define DJO_TIME_CONVERT_SHORT_NAMES(structType_, funcName_)\
    template <typename TFrom>\
    constexpr structType_<typename TFrom::Type> funcName_(TFrom _t) noexcept\
    {\
        return ConvertTime<structType_<typename TFrom::Type>>(_t);\
    }

    DJO_TIME_CONVERT_SHORT_NAMES(SecondsT,      Sec);
    DJO_TIME_CONVERT_SHORT_NAMES(MillisecondsT, Ms);
    DJO_TIME_CONVERT_SHORT_NAMES(MicrosecondsT, Us);
    DJO_TIME_CONVERT_SHORT_NAMES(NanosecondsT,  Ns);

#undef DJO_TIME_CONVERT_SHORT_NAMES
}
