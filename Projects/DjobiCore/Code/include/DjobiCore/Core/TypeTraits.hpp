#pragma once

#include <DjobiCore/Core/Utils.hpp>

#include <type_traits>


namespace Djo
{
    // Metaprogramming utils

    template <typename T>
    constexpr bool c_isCopyConstructible{ std::is_copy_constructible_v<T> };
    template <typename T>
    constexpr bool c_isMoveConstructible{ std::is_move_constructible_v<T> };
    template <typename T>
    constexpr bool c_isTriviallyDestructible{ std::is_trivially_destructible_v<T> };
    template <typename T>
    constexpr bool c_isNoThrowDestructible{ std::is_nothrow_destructible_v<T> };
    template <typename T>
    constexpr bool c_isNoThrowDefaultConstructible{ std::is_nothrow_default_constructible_v<T> };
    template <typename T, typename... TArgs>
    constexpr bool c_isNoThrowConstructible{ std::is_nothrow_constructible_v<T, TArgs...> };

    // https://en.cppreference.com/w/cpp/language/classes
    template <typename T>
    constexpr bool c_isTrivial{ std::is_trivial_v<T> };
    template <typename T>
    constexpr bool c_isStdLayout{ std::is_standard_layout_v<T> };
    template <typename T>
    constexpr bool c_isPOD{ c_isTrivial<T> || c_isStdLayout<T> };
    template <typename T>
    constexpr bool c_isCStruct{ c_isTrivial<T> && c_isStdLayout<T> };

    template <typename T, typename... TTypes>
    constexpr bool c_isAnyOf = std::disjunction_v<std::is_same<T, TTypes>...>;

    template <typename T>
    using PlainType_t = RemoveRef_t<std::remove_pointer_t<std::remove_all_extents_t<T>>>;

    template <typename T>
    using PureType_t = std::remove_cv_t<PlainType_t<T>>;

    template <typename T>
    constexpr bool c_canUseMemcpy{ c_isTrivial<T> };


    // Enums
    // To make enum class less verbose to use,
    // and "standardize" Count and Invalid values
    // Example:
    // enum EMyEnum : u32
    // {
    //      Toto = 0,
    //      Tata,
    //      Tutu,
    //      DJO_ENUM_COUNT_INVALID
    // };
    // static_cast<u32>(value)            == +value
    // static_cast<EMyEnum>(12u)          == ECast<EMyEnum>(12u)
    // static_cast<u32>(EMyEnum::Count)   == ECount<EMyEnum>
    // static_cast<u32>(EMyEnum::Invalid) == EInvalid<EMyEnum>

    template <typename E>
    constexpr bool c_isClassEnum = std::is_enum_v<E> && !std::is_convertible_v<E, int>;

    template <typename E>
    constexpr std::underlying_type_t<E> operator+(const E _e) noexcept
    {
        return static_cast<std::underlying_type_t<E>>(_e);
    }
    template <typename E>
    constexpr E ECast(const std::underlying_type_t<E> _v) noexcept
    {
        return static_cast<E>(_v);
    }

    #define DJO_ENUM_COUNT_INVALID  Count, Invalid = Count

    template <typename E>
    constexpr std::underlying_type_t<E> ECount = +E::Count;

    template <typename E>
    constexpr std::underlying_type_t<E> EInvalid = +E::Invalid;


    // Grab the flag count in a flag fashioned enum
    // Exemple:
    // enum class EMyEnum : u32
    // {
    //     Tata = Flag(0),
    //     Toto = Flag(1),
    //     Tutu = Flag(2),
    //     Titi = Flag(3),
    //     DJO_ENUM_FLAG_COUNT
    // };
    // There EFlagCount<EMyEnum> will be 4
    // static_cast<u32>(EMyEnum::FlagCount) == EFlagCount<EMyEnum>

    #define DJO_ENUM_FLAG_COUNT  next__, FlagCount = (GetFirstBitIndex(next__ - 1) + 1)

    template <typename E>
    constexpr std::underlying_type_t<E> EFlagCount = +E::FlagCount;


    // Enum class bitwise operators

    #define DJO_IMPL_CLASS_ENUM_BIT_OPERATOR(op_)\
        template <typename E>\
        constexpr std::underlying_type_t<E> operator op_ (const E _e1, const E _e2)\
        {\
            return operator+(_e1) op_ operator+(_e2);\
        }\
        template <typename E>\
        constexpr std::underlying_type_t<E> operator op_ (const E _e1, const std::underlying_type_t<E> _et2)\
        {\
            return operator+(_e1) op_ _et2;\
        }\
        template <typename E>\
        constexpr std::underlying_type_t<E> operator op_ (const std::underlying_type_t<E> _et1, const E _e2)\
        {\
            return _et1 op_ operator+(_e2);\
        }

    DJO_IMPL_CLASS_ENUM_BIT_OPERATOR(|);
    DJO_IMPL_CLASS_ENUM_BIT_OPERATOR(&);

    #undef DJO_IMPL_CLASS_ENUM_BIT_OPERATOR
}
