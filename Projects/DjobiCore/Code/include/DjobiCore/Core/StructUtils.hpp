#pragma once

#include <DjobiCore/Core/ClassTraits.hpp>
#include <DjobiCore/Core/Types.hpp>
#include <DjobiCore/Core/Core.hpp>


namespace Djo
{
    // Array wrapper, for return values mostly
    template <typename T, sz N>
    struct ArrayWrapper
    {
        T a[N];
    };


    // Hard type alias
    template <typename T>
    struct HardAlias
    {
        constexpr operator const T& () const  { return value; }
        explicit constexpr operator T& ()     { return value; }

        T value;
    };

#pragma pack(push, 1)
    template <typename T, typename U>
    struct Pair { T a; U b; };
#pragma pack(pop)

    // to ensure reference removal for the Pair molding
    template <typename T, typename U>
    DJO_INLINE Pair<RemoveRef_t<T>, RemoveRef_t<U>> MoldPair(T&& _a, U&& _b) noexcept
    {
        return Pair<RemoveRef_t<T>, RemoveRef_t<U>>{ Forward<T>(_a), Forward<U>(_b) };
    }

    // let's use C++ destruction feature to defer function at end-of-scope
    namespace _Internal
    {
        template <bool TIsDataConst>
        class Scope
        {
            DJO_NO_DEFAULT_CTOR(Scope);
            DJO_NO_COPY_NO_MOVE(Scope);

        public:
            using DataType = Conditional_t<TIsDataConst, const void*, void*>;
            using OnDestroy = void (*)(DataType) noexcept;

            Scope(const DataType _userData, const Scope::OnDestroy _onDestroy) noexcept
                : m_userData{ _userData }
                , m_onDestroy{ _onDestroy }
            {
            }

            ~Scope() noexcept
            {
                m_onDestroy(m_userData);
            }

        private:
            const DataType m_userData;
            const Scope::OnDestroy m_onDestroy;
        };

        template <typename TLambda>
        class ScopeLambda
        {
            DJO_NO_DEFAULT_CTOR(ScopeLambda);
            DJO_NO_COPY_NO_MOVE(ScopeLambda);

        public:
            explicit ScopeLambda(TLambda&& _onDestroy) noexcept
                : m_onDestroy{ Forward<TLambda>(_onDestroy) }
            {
            }

            ~ScopeLambda() noexcept
            {
                m_onDestroy();
            }

        private:
            TLambda m_onDestroy;
        };
    }

    using Scope = _Internal::Scope<false>;
    using ScopeConst = _Internal::Scope<true>;

    DJO_INLINE Scope Defer(void* const _userData, const Scope::OnDestroy _fn) noexcept
    {
        return Scope{ _userData, _fn };
    }
    DJO_INLINE ScopeConst DeferConst(const void* const _userData, const ScopeConst::OnDestroy _fn) noexcept
    {
        return ScopeConst{ _userData, _fn };
    }

#define DJO_DEFER(userData_, var_, fn_)\
    const ::Djo::Scope DJO_CAT(__scope_, DJO_CAT(__LINE__, __COUNTER__)) = \
    Defer(userData_, [](void* const __userData) noexcept { auto var_ = reinterpret_cast<decltype(userData_)>(__userData); fn_; })

#define DJO_DEFER_CONST(userData_, var_, fn_)\
    const ::Djo::ScopeConst DJO_CAT(__scope_const_, DJO_CAT(__LINE__, __COUNTER__)) = \
    DeferConst(userData_, [](const void* const __userData) noexcept{ auto var_ = reinterpret_cast<decltype(userData_)>(__userData); fn_; })

    template <typename TLambda>
    DJO_INLINE _Internal::ScopeLambda<TLambda> DeferLambda(TLambda&& _fn) noexcept
    {
        return _Internal::ScopeLambda<TLambda>{ Forward<TLambda>(_fn) };
    }

#define DJO_DEFER_LAMBDA(...)\
    const auto DJO_CAT(__scope_lambda_, DJO_CAT(__LINE__, __COUNTER__)) = DeferLambda(__VA_ARGS__)
}
