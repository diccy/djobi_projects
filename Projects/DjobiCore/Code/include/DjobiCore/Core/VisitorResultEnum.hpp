#pragma once

#include <DjobiCore/Core/Types.hpp>


namespace Djo
{
    enum class EVisitorResult : u32
    {
        Continue = 0,
        Break,
    };
}
