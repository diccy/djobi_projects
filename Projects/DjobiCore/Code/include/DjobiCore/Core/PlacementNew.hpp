#pragma once

#include <DjobiCore/Core/TypeTraits.hpp>

#include <new>
#include <string.h> // memcpy


namespace Djo
{
    namespace _Internal
    {
        // from Windows std implementation
        #define DJO_INTERNAL_PLACEMENT_NEW(p_, type_)  ::new (const_cast<void*>(static_cast<const volatile void*>(p_))) type_

        template <typename T>
        DJO_INLINE void ConstructInPlace(void* const _p) noexcept(c_isNoThrowDefaultConstructible<T>)
        {
            DJO_INTERNAL_PLACEMENT_NEW(_p, T);
        }

        template <typename T, typename... TArgs>
        DJO_INLINE void ConstructInPlace(void* const _p, TArgs&&... _args) noexcept(c_isNoThrowConstructible<T, TArgs...>)
        {
            DJO_INTERNAL_PLACEMENT_NEW(_p, T){ Forward<TArgs>(_args)... };
        }

        template <typename T>
        DJO_INLINE void ConstructRangeInPlace(void* const _p, sz _count) noexcept(c_isNoThrowDefaultConstructible<T>)
        {
            T* p = static_cast<T*>(_p);
            for (; _count > 0; --_count, ++p)
                ConstructInPlace<T>(p);
        }

        template <typename T, typename... TArgs>
        DJO_INLINE void ConstructRangeInPlace(void* const _p, sz _count, TArgs&&... _args) noexcept(c_isNoThrowConstructible<T, TArgs...>)
        {
            T* p = static_cast<T*>(_p);
            for (; _count > 0; --_count, ++p)
                ConstructInPlace<T>(p, Forward<TArgs>(_args)...);
        }

        struct UseDefaultCtor{};

        template <typename T>
        DJO_INLINE void ConstructRangeInPlace(void* const _p, const sz _count, UseDefaultCtor) noexcept(c_isNoThrowDefaultConstructible<T>)
        {
            return ConstructRangeInPlace<T>(_p, _count);
        }

        template <typename T>
        DJO_INLINE void ConstructRangeInPlaceFrom(void* const _dest, void* const _source, sz _count) noexcept
        {
            if constexpr (c_canUseMemcpy<T>)
            {
                ::memmove(_dest, _source, _count * sizeof(T));
            }
            else
            {
                T* d = static_cast<T*>(_dest);
                T* s = static_cast<T*>(_source);
                for (; _count > 0; --_count, ++d, ++s)
                {
                    if constexpr (c_isMoveConstructible<T>)
                        ConstructInPlace<T>(d, Move(*s));
                    else
                        ConstructInPlace<T>(d, *s);
                }
            }
        }

        template <typename T>
        DJO_INLINE void Destroy(T& _value) noexcept(c_isNoThrowDestructible<T>)
        {
            if constexpr (!c_isTriviallyDestructible<T>)
            {
                _value.~T();
            }
        }

        template <typename T>
        DJO_INLINE void Destroy(void* _p) noexcept(c_isNoThrowDestructible<T>)
        {
            Destroy<T>(*static_cast<T*>(_p));
        }

        template <typename T>
        DJO_INLINE void DestroyRange(void* _p, sz _count) noexcept(c_isNoThrowDestructible<T>)
        {
            T* p = static_cast<T*>(_p);
            for (; _count > 0; --_count, ++p)
                Destroy<T>(*p);
        }
    }
}
