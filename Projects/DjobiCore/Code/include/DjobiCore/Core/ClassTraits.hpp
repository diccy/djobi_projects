#pragma once


#define DJO_DEFAULT_COPY(class_)\
public:\
    class_(const class_&) = default;\
    class_& operator = (const class_&) = default

#define DJO_NO_COPY(class_)\
private:\
    class_(const class_&) = delete;\
    class_& operator = (const class_&) = delete

#define DJO_DEFAULT_MOVE(class_)\
public:\
    class_(class_&&) noexcept = default;\
    class_& operator = (class_&&) noexcept = default

#define DJO_NO_MOVE(class_)\
private:\
    class_(class_&&) noexcept = delete;\
    class_& operator = (class_&&) noexcept = delete

#define DJO_DEFAULT_COPY_DEFAULT_MOVE(class_)\
    DJO_DEFAULT_COPY(class_);\
    DJO_DEFAULT_MOVE(class_)

#define DJO_NO_COPY_DEFAULT_MOVE(class_)\
    DJO_NO_COPY(class_);\
    DJO_DEFAULT_MOVE(class_)

#define DJO_NO_COPY_NO_MOVE(class_)\
    DJO_NO_COPY(class_);\
    DJO_NO_MOVE(class_)

#define DJO_DEFAULT_CTOR_DTOR(class_)\
public:\
    class_() = default;\
    ~class_() = default

#define DJO_DEFAULT_CTOR_V_DTOR(class_)\
public:\
    class_() = default;\
    virtual ~class_() = default

#define DJO_ALL_DEFAULT(class_)\
    DJO_DEFAULT_CTOR_DTOR(class_);\
    DJO_DEFAULT_COPY_DEFAULT_MOVE(class_)

#define DJO_ALL_DEFAULT_V_DTOR(class_)\
    DJO_DEFAULT_CTOR_V_DTOR(class_);\
    DJO_DEFAULT_COPY_DEFAULT_MOVE(class_)

#define DJO_NO_DEFAULT_CTOR(class_)\
private:\
    class_() = delete;
