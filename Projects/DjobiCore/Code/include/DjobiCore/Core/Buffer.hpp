#pragma once

#include <DjobiCore/Core/Utils.hpp>
#include <DjobiCore/Core/Hash.hpp>


namespace Djo
{
    template <typename T>
    struct Buffer
    {
        using DataType = T;

        T* data;
        sz count;

        constexpr operator const Buffer<const T>& () const noexcept  { return *AsConst(this); }
        constexpr operator Buffer<const T>& () noexcept              { return *AsConst(this); }

        constexpr const T& operator[](const sz _i) const noexcept  { return *GetAt(*this, _i); }
        constexpr T& operator[](const sz _i) noexcept              { return *GetAt(*this, _i); }
    };

    DJO_DEFINE_NULL_CONSTEXPR_STATIC_T(typename T, Buffer<T>, Buffer, (nullptr, 0));

    template <typename T, sz N>
    constexpr Buffer<T> ToBuffer(T (&_carray)[N]) noexcept
    {
        return Buffer<T>{ _carray, N };
    }

    template <typename T, sz N>
    constexpr Buffer<const T> ToBufferConst(T (&_carray)[N]) noexcept
    {
        return ToBuffer<const T, N>(_carray);
    }

    template <typename T>
    constexpr T* GetData(const Buffer<T>& _buffer) noexcept
    {
        return _buffer.data;
    }

    template <typename T>
    constexpr sz GetCount(const Buffer<T>& _buffer) noexcept
    {
        return _buffer.count;
    }

    template <typename T>
    constexpr bool IsEmpty(const Buffer<T>& _buffer) noexcept
    {
        return _buffer.data == nullptr || _buffer.count == 0;
    }

    template <typename T>
    constexpr sz GetCapacity(const Buffer<T>& _buffer) noexcept
    {
        return _buffer.count;
    }

    template <typename T>
    constexpr sz GetSize(const Buffer<T>& _buffer) noexcept
    {
        return _buffer.count * sizeof(T);
    }

    template <typename T>
    constexpr T* GetAt(const Buffer<T>& _buffer, const sz _index) noexcept
    {
        DJO_ASSERT(!IsEmpty(_buffer) && _index < _buffer.count);

        return _buffer.data + _index;
    }

    template <typename T>
    constexpr T* GetEnd(const Buffer<T>& _buffer) noexcept
    {
        return _buffer.data + _buffer.count;
    }

    template <typename T>
    constexpr T* GetFirst(const Buffer<T>& _buffer) noexcept
    {
        return GetAt(_buffer, 0);
    }

    template <typename T>
    constexpr T* GetLast(const Buffer<T>& _buffer) noexcept
    {
        return GetAt(_buffer, _buffer.count - 1);
    }

    template <typename T>
    constexpr sz Find(const Buffer<T>& _buffer, CopyOrConstRef_t<const T> _value) noexcept
    {
        return Find(_buffer.data, _buffer.count, _value);
    }

    template <typename T>
    constexpr sz FindLast(const Buffer<T>& _buffer, CopyOrConstRef_t<const T> _value) noexcept
    {
        return FindLast(_buffer.data, _buffer.count, _value);
    }

    template <typename T>
    DJO_INLINE void ZeroBufferMem(const Buffer<T>& _buffer) noexcept
    {
        ::memset(_buffer.data, 0, GetSize(_buffer));
    }

    template <typename T>
    DJO_INLINE void Set(Buffer<T>* const _buffer, const sz _index, const T* const _values, const sz _valueCount) noexcept
    {
        DJO_ASSERT(_buffer.count >= _index + _valueCount);

        ::memcpy(_buffer.data + _index, _values, _valueCount * sizeof(T));
    }

    template <typename T>
    DJO_INLINE void Set(Buffer<T>* const _buffer, const sz _index, const Buffer<const T>& _values) noexcept
    {
        Set(_buffer, _index, _values.data, _values.count);
    }


    template <typename T>
    struct Hasher<Buffer<T>>
    {
        DJO_INLINE u64 operator () (const Buffer<T>& _buffer) const noexcept
        {
            return Hash64(GetData(_buffer), GetSize(_buffer));
        }
    };
}
