#pragma once

// !! Keep this file without any include !!

// https://sourceforge.net/p/predef/wiki/Home/


// Config

// 4201 : nonstandard extension used : nameless struct/union
#pragma warning(disable : 4201)


// Basic macro Tools

#define DJO_EMPTY 
#define DJO_STATEMENT(expr_)  do { expr_ } while(0)
#define DJO_STATEMENT_EMPTY   do {} while(0)

#define DJO_NOOP(...)     __VA_ARGS__
#define DJO_UNPAR(...)    DJO_NOOP __VA_ARGS__
#define DJO_TUPLE(...)    DJO_NOOP(__VA_ARGS__)
#define DJO_DO(M_, ...)   DJO_NOOP(M_(__VA_ARGS__))

#define DJO_STR_(s_)      # s_
#define DJO_STR(s_)       DJO_STR_(s_)

#define DJO_CAT_(a_, b_)  a_ ## b_
#define DJO_CAT(a_, b_)   DJO_CAT_(a_, b_)

#define DJO_UNUSED(var_)  (void)var_


// Static assert

#define DJO_STATIC_ASSERT_MSG     static_assert
#define DJO_STATIC_ASSERT(expr_)  DJO_STATIC_ASSERT_MSG((expr_), "")


// Todo

#define DJO_TODO(s_)  __pragma(message(__FILE__ " (" DJO_STR(__LINE__) "): TODO: " s_))


// Keywords

// TODO
#define DJO_READONLY 

// TODO: check the differences between the two
#define DJO_PER_THREAD_STATIC  __declspec(thread)
//#define DJO_PER_THREAD_STATIC  static thread_local

// Break

namespace Djo::_Internal
{
    void Break();
}


// Flags

namespace Djo
{
    template <typename T>
    constexpr T Flag(const T _v) noexcept
    {
        return T{ 1u } << _v;
    }

    template <typename T>
    constexpr T Mask(const T _v) noexcept
    {
        return Flag(_v) - T{ 1u };
    }

    template <typename T>
    constexpr bool TestFlag(const T _mask, const T _flag) noexcept
    {
        return _flag != 0 && (_mask & _flag) == _flag;
    }

    template <typename T>
    constexpr bool TestAnyFlag(const T _mask, const T _flags) noexcept
    {
        return (_mask & _flags) != 0;
    }
}

// Attributes

#define DJO_NO_DISCARD   [[nodiscard]]
#define DJO_FALLTHROUGH  [[fallthrough]]


// Metaprogramming

namespace Djo
{
    namespace _Internal
    {
        template <bool TTest, typename T = void>
        struct EnableIf {};
        template <typename T>
        struct EnableIf<true, T> { using Type = T; };
    }
    template <bool TTest, typename T = void>
    using EnableIf_t = typename _Internal::EnableIf<TTest, T>::Type;

#define DJO_TARG_ENABLE_IF(test_)  ::Djo::EnableIf_t<(test_), int> = 0

    template <typename>
    constexpr bool c_isConst = false;
    template <typename T>
    constexpr bool c_isConst<const T> = true;

#define DJO_TARG_ENABLE_IF_CONST(type_)      DJO_TARG_ENABLE_IF( ::Djo::c_isConst<type_>)
#define DJO_TARG_ENABLE_IF_NOT_CONST(type_)  DJO_TARG_ENABLE_IF(!::Djo::c_isConst<type_>)


    namespace _Internal
    {
        template <typename T>
        struct RemoveRef { using Type = T; };
        template <typename T>
        struct RemoveRef<T&> { using Type = T; };
        template <typename T>
        struct RemoveRef<T&&> { using Type = T; };
    }
    template <typename T>
    using RemoveRef_t = typename _Internal::RemoveRef<T>::Type;

    namespace _Internal
    {
        template <typename T>
        struct RemoveConst { using Type = T; };
        template <typename T>
        struct RemoveConst<const T> { using Type = T; };
    }
    template <typename T>
    using RemoveConst_t = typename _Internal::RemoveConst<T>::Type;

    template <typename T>
    using RemoveRefConst_t = RemoveRef_t<RemoveConst_t<T>>;

    namespace _Internal
    {
        template <typename T, bool TIsConst>
        struct AsConst { using Type = const T; };
        template <typename T>
        struct AsConst<T, false> { using Type = RemoveConst_t<T>; };
    }
    template <typename T, bool TIsConst>
    using AsConst_t = typename _Internal::AsConst<T, TIsConst>::Type;

    namespace _Internal
    {
        template <bool TCondition, typename T, typename U>
        struct Conditional { using Type = T; };
        template <typename T, typename U>
        struct Conditional<false, T, U> { using Type = U; };
    }
    template <bool TCondition, typename T, typename U>
    using Conditional_t = typename _Internal::Conditional<TCondition, T ,U>::Type;

    namespace _Internal
    {
        template <typename T>
        struct CopyOrConstRef { using Type = Conditional_t<sizeof(T) <= sizeof(void*), T, const T&>; };
    }
    template <typename T>
    using CopyOrConstRef_t = typename _Internal::CopyOrConstRef<T>::Type;

    template <typename T>
    DJO_NO_DISCARD
    constexpr RemoveRef_t<T>&& Move(T&& _obj) noexcept
    {
        return static_cast<RemoveRef_t<T>&&>(_obj);
    }

    // see "reference collasping" https://en.cppreference.com/w/cpp/language/reference 
    template <typename T>
    DJO_NO_DISCARD
    constexpr T&& Forward(RemoveRef_t<T>& _Arg) noexcept
    {
        return static_cast<T&&>(_Arg);
    }
    template <typename T>
    DJO_NO_DISCARD
    constexpr T&& Forward(RemoveRef_t<T>&& _Arg) noexcept
    {
        return static_cast<T&&>(_Arg);
    }

    template <typename T, typename U = T>
    DJO_NO_DISCARD
    constexpr T Exchange(T& _v1, U&& _v2) noexcept
    {
        T v1{ Forward<T&&>(_v1) };
        _v1 = Forward<U&&>(_v2);
        return v1;
    }

    template <typename T>
    constexpr void Swap(T& _a, T& _b) noexcept
    {
        _b = Exchange(_a, Move(_b));
    }
}


// Compilation optim strategies

#define DJO_INLINE  inline

// Note : benchmarks results were not really decisive for these attributes usage
//#define DJO_LIKELY    [[likely]]
//#define DJO_UNLIKELY  [[unlikely]]
#define DJO_LIKELY
#define DJO_UNLIKELY


// Profiling

#define DJO_PROFILING_ENABLED  1


// Utilities

#define DJO_DEFINE_NULL_CONSTEXPR_(prefix_, type_, name_, args_)\
    prefix_ static constexpr type_ DJO_CAT(c_null, name_){ DJO_UNPAR(args_) }

#define DJO_DEFINE_NULL_STATIC_(prefix_, type_, name_, args_)\
    prefix_ DJO_READONLY static type_ DJO_CAT(s_null, name_){ DJO_UNPAR(args_) }

#define DJO_DEFINE_NULL_CONSTEXPR(type_, name_, args_)               DJO_DEFINE_NULL_CONSTEXPR_(, DJO_TUPLE(type_), name_, args_)
#define DJO_DEFINE_NULL_CONSTEXPR_T(template_, type_, name_, args_)  DJO_DEFINE_NULL_CONSTEXPR_(DJO_TUPLE(template <template_>), DJO_TUPLE(type_), name_, args_)

#define DJO_DEFINE_NULL_STATIC(type_, name_, args_)               DJO_DEFINE_NULL_STATIC_(, DJO_TUPLE(type_), name_, args_)
#define DJO_DEFINE_NULL_STATIC_T(template_, type_, name_, args_)  DJO_DEFINE_NULL_STATIC_(DJO_TUPLE(template <template_>), DJO_TUPLE(type_), name_, args_)

#define DJO_DEFINE_NULL_CONSTEXPR_STATIC(type_, name_, args_)\
    DJO_DEFINE_NULL_CONSTEXPR(DJO_TUPLE(type_), name_, args_);\
    DJO_DEFINE_NULL_STATIC(DJO_TUPLE(type_), name_, args_)

#define DJO_DEFINE_NULL_CONSTEXPR_STATIC_T(template_, type_, name_, args_)\
    DJO_DEFINE_NULL_CONSTEXPR_T(DJO_TUPLE(template_), DJO_TUPLE(type_), name_, args_);\
    DJO_DEFINE_NULL_STATIC_T(DJO_TUPLE(template_), DJO_TUPLE(type_), name_, args_)


#define DJO_CASE_VALUE_RETURN_STR(enum_)    case enum_: { return #enum_; }

namespace Djo
{
    template <typename T> constexpr bool c_isUnsignedInt = false;
    template <>           constexpr bool c_isUnsignedInt<unsigned char>      = true;
    template <>           constexpr bool c_isUnsignedInt<unsigned short>     = true;
    template <>           constexpr bool c_isUnsignedInt<unsigned int>       = true;
    template <>           constexpr bool c_isUnsignedInt<unsigned long long> = true;

    template <typename T> constexpr T c_invalidUint = delete("not unsigned int type"); // valid in c++17 ??
#define DJO_INVALID_UINT(type_)  template <> constexpr type_ c_invalidUint<type_>{ (type_)(~((type_)0)) }
    DJO_INVALID_UINT(unsigned char);
    DJO_INVALID_UINT(unsigned short);
    DJO_INVALID_UINT(unsigned int);
    DJO_INVALID_UINT(unsigned long long);
#undef DJO_INVALID_UINT

    namespace _Internal
    {
        template <typename T> struct As64 {};
        template <> struct As64<char>                { using Type = long long; };
        template <> struct As64<short>               { using Type = long long; };
        template <> struct As64<int>                 { using Type = long long; };
        template <> struct As64<long long>           { using Type = long long; };
        template <> struct As64<unsigned char>       { using Type = unsigned long long; };
        template <> struct As64<unsigned short>      { using Type = unsigned long long; };
        template <> struct As64<unsigned int>        { using Type = unsigned long long; };
        template <> struct As64<unsigned long long>  { using Type = unsigned long long; };
        template <> struct As64<float>               { using Type = double; };
        template <> struct As64<double>              { using Type = double; };
    }
    template <typename T>
    using As64_t = typename _Internal::As64<T>::Type;
}


// Version

namespace Djo
{
    struct Version
    {
        unsigned char major;
        unsigned char minor;
        unsigned short patch;

        constexpr operator unsigned int () const
        {
            return (unsigned int)(major << 24u)
                | (unsigned int)(minor << 16u)
                | (unsigned int)(patch);
        }
    };
    DJO_STATIC_ASSERT(sizeof(Version) == sizeof(unsigned int));

    constexpr Version c_coreVersion{ 0, 1, 0 };
}


// Memory

namespace Djo
{
    void SetupSystemMemoryLeakWatcher();
}


// Macro tools

#define DJO_COMMA0 ,
#define DJO_COMMA1 DJO_COMMA0
#define DJO_COMMA2 DJO_COMMA1
#define DJO_COMMA3 DJO_COMMA2
#define DJO_COMMA4 DJO_COMMA3
#define DJO_COMMA5 DJO_COMMA4
#define DJO_COMMA6 DJO_COMMA5
#define DJO_COMMA7 DJO_COMMA6
#define DJO_COMMA8 DJO_COMMA7
#define DJO_COMMA9 DJO_COMMA8
#define DJO_COMMA  DJO_COMMA9

#define DJO_ARG_COUNT(...)  DJO_DO(DJO_ARG_COUNT_ARGS_, __VA_ARGS__, 63, 62, 61, 60, 59, 58, 57, 56, 55, 54, 53, 52, 51, 50, 49, 48, 47, 46, 45, 44, 43, 42, 41, 40, 39, 38, 37, 36, 35, 34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0)
#define DJO_ARG_COUNT_ARGS_(_1, _2, _3, _4, _5, _6, _7, _8, _9,_10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22, _23, _24, _25, _26, _27, _28, _29, _30, _31, _32, _33, _34, _35, _36, _37, _38, _39, _40, _41, _42, _43, _44, _45, _46, _47, _48, _49, _50, _51, _52, _53, _54, _55, _56, _57, _58, _59, _60, _61, _62, _63, N, ...)  N

#define DJO_APPLY_1(M_, sep_, _1)\
    M_(_1)
#define DJO_APPLY_2(M_, sep_, _1, _2)\
    M_(_1) sep_ M_(_2)
#define DJO_APPLY_3(M_, sep_, _1, _2, _3)\
    M_(_1) sep_ M_(_2) sep_ M_(_3)
#define DJO_APPLY_4(M_, sep_, _1, _2, _3, _4)\
    M_(_1) sep_ M_(_2) sep_ M_(_3) sep_ M_(_4)
#define DJO_APPLY_5(M_, sep_, _1, _2, _3, _4, _5)\
    M_(_1) sep_ M_(_2) sep_ M_(_3) sep_ M_(_4) sep_ M_(_5)
#define DJO_APPLY_6(M_, sep_, _1, _2, _3, _4, _5, _6)\
    M_(_1) sep_ M_(_2) sep_ M_(_3) sep_ M_(_4) sep_ M_(_5) sep_ M_(_6)
#define DJO_APPLY_7(M_, sep_, _1, _2, _3, _4, _5, _6, _7)\
    M_(_1) sep_ M_(_2) sep_ M_(_3) sep_ M_(_4) sep_ M_(_5) sep_ M_(_6) sep_ M_(_7)
#define DJO_APPLY_8(M_, sep_, _1, _2, _3, _4, _5, _6, _7, _8)\
    M_(_1) sep_ M_(_2) sep_ M_(_3) sep_ M_(_4) sep_ M_(_5) sep_ M_(_6) sep_ M_(_7) sep_ M_(_8)
#define DJO_APPLY_9(M_, sep_, _1, _2, _3, _4, _5, _6, _7, _8, _9)\
    M_(_1) sep_ M_(_2) sep_ M_(_3) sep_ M_(_4) sep_ M_(_5) sep_ M_(_6) sep_ M_(_7) sep_ M_(_8) sep_ M_(_9)
#define DJO_APPLY_10(M_, sep_, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10)\
    M_(_1) sep_ M_(_2) sep_ M_(_3) sep_ M_(_4) sep_ M_(_5) sep_ M_(_6) sep_ M_(_7) sep_ M_(_8) sep_ M_(_9) sep_ M_(_10)
#define DJO_APPLY_11(M_, sep_, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11)\
    M_(_1) sep_ M_(_2) sep_ M_(_3) sep_ M_(_4) sep_ M_(_5) sep_ M_(_6) sep_ M_(_7) sep_ M_(_8) sep_ M_(_9) sep_ M_(_10) sep_ M_(_11)
#define DJO_APPLY_12(M_, sep_, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12)\
    M_(_1) sep_ M_(_2) sep_ M_(_3) sep_ M_(_4) sep_ M_(_5) sep_ M_(_6) sep_ M_(_7) sep_ M_(_8) sep_ M_(_9) sep_ M_(_10) sep_ M_(_11) sep_ M_(_12)

#define DJO_APPLY(M_, ...)            DJO_NOOP(DJO_CAT(DJO_APPLY_, DJO_ARG_COUNT(__VA_ARGS__))(M_, , __VA_ARGS__))
#define DJO_APPLY_SEP(M_, sep_, ...)  DJO_NOOP(DJO_CAT(DJO_APPLY_, DJO_ARG_COUNT(__VA_ARGS__))(M_, DJO_NOOP(sep_), __VA_ARGS__))
