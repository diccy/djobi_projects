#pragma once

#include <DjobiCore/Core/Buffer.hpp>


namespace Djo
{
    using StringView = Buffer<const char>;

    static constexpr char c_emptyStr[]{ "" };
    DJO_READONLY static constexpr char s_emptyStr[]{ "" };

    template <sz N>
    constexpr StringView ToStringView(const char (&_carray)[N]) noexcept
    {
        StringView view{ _carray, N };
        while (view.count > 0 && *(GetEnd(view) - 1) == '\0')
            --view.count;
        return view;
    }

    DJO_DEFINE_NULL_CONSTEXPR_STATIC(StringView, StringView, (ToStringView(c_emptyStr)));

    constexpr StringView ToStringView(const char* const _cstr) noexcept
    {
        return StringView{ _cstr, StrLen(_cstr) };
    }

    constexpr sz GetLength(const StringView& _sv) noexcept
    {
        return _sv.count;
    }

    constexpr bool operator == (const StringView& _lhs, const StringView& _rhs) noexcept
    {
        return _lhs.count == _rhs.count
            && (_lhs.data == _rhs.data || (StrNCmp(_lhs.data, _rhs.data, _lhs.count) == 0));
    }
    constexpr bool operator != (const StringView& _lhs, const StringView& _rhs) noexcept
    {
        return !(operator==(_lhs, _rhs));
    }

    template <sz N>
    constexpr bool operator == (const StringView& _lhs, const char (&_rhs)[N]) noexcept
    {
        return _lhs == ToStringView(_rhs);
    }
    template <sz N>
    constexpr bool operator != (const StringView& _lhs, const char (&_rhs)[N]) noexcept
    {
        return !(operator==(_lhs, _rhs));
    }

    constexpr StringView ShrinkBack(const StringView& _sv, const sz _pos) noexcept
    {
        return StringView{ _sv.data, Min(_pos, _sv.count) };
    }

    constexpr StringView ShrinkFront(const StringView& _sv, const sz _pos) noexcept
    {
        return (_pos < _sv.count)
            ? StringView{ _sv.data + _pos, _sv.count - _pos }
            : StringView{ _sv.data + _sv.count, 0 };
    }

    constexpr StringView Shrink(const StringView& _sv, const sz _begin, const sz _end) noexcept
    {
        return ShrinkFront(ShrinkBack(_sv, _end), _begin);
    }


    template <>
    struct Hasher<StringView>
    {
        DJO_INLINE u64 operator () (const StringView& _sv) const noexcept
        {
            return _Internal::StrFnv1a_64(
                reinterpret_cast<const char*>(_sv.data),
                _sv.count);
        }
    };
}
