#pragma once

#include <DjobiCore/Core/Types.hpp>


// One of George Marsaglia's algorithms

namespace Djo
{
    namespace FastRandom
    {
        static constexpr u64 c_baseSeed{ 362436069ull };
        static constexpr u64 c_baseMult{ 2083801278ull };
        static constexpr u32 c_baseAdd{ 36931u };

        struct State
        {
            u64 n;
            u64 mult;
            u32 add;
        };

        State MoldState(const u64 _seed = c_baseSeed) noexcept
        {
            return State{ _seed, c_baseMult, c_baseAdd };
        }
    }

    namespace Random
    {
        // FastRandom

        constexpr u64 U64(FastRandom::State* const _inoutState) noexcept
        {
            _inoutState->n = _inoutState->n * _inoutState->mult + (_inoutState->n >> 32ull) + _inoutState->add;
            return _inoutState->n;
        }

        // Utils

        template <typename TRandState>
        constexpr u32 U32(TRandState* const _inoutState) noexcept
        {
            return (u32)(U64(_inoutState) >> 32ull);
        }

        template <typename TRandState>
        constexpr u32 U32(TRandState* const _inoutState, const u32 _min, const u32 _max) noexcept
        {
            return _min + U32(_inoutState) % (_max - _min);
        }

        template <typename TRandState>
        constexpr u16 U16(TRandState* const _inoutState) noexcept
        {
            return (u16)(U64(_inoutState) >> 48ull);
        }

        template <typename TRandState>
        constexpr u8 U8(TRandState* const _inoutState) noexcept
        {
            return (u8)(U64(_inoutState) >> 56ull);
        }

        template <typename TRandState>
        constexpr f64 F64(TRandState* const _inoutState) noexcept
        {
            return (1. / (f64)((1 << 24u) - 1)) * (f64)(U32(_inoutState) >> 8u);
        }

        template <typename TRandState>
        constexpr f64 F64(TRandState* const _inoutState, const f64 _min, const f64 _max) noexcept
        {
            return _min + F64(_inoutState) * (_max - _min);
        }

        template <typename TRandState>
        constexpr f32 F32(TRandState* const _inoutState) noexcept
        {
            return (f32)F64(_inoutState);
        }

        template <typename TRandState>
        constexpr f32 F32(TRandState* const _inoutState, const f32 _min, const f32 _max) noexcept
        {
            return (f32)F64(_inoutState, _min, _max);
        }
    }
}
