#pragma once

#include <DjobiCore/Core/Buffer.hpp>


namespace Djo
{
    namespace Memory
    {
        static constexpr sz c_baseAlign{ sizeof(void*) };

        using Area = Buffer<u8>;
        DJO_DEFINE_NULL_CONSTEXPR_STATIC(Area, Area, (nullptr, 0));

        constexpr Area MoldArea(void* const _p, const sz _size) noexcept
        {
            return Area{ static_cast<u8*>(_p), _size };
        }

        template <sz N>
        constexpr Area ToArea(char (&_carray)[N]) noexcept
        {
            return Area{ _carray, N };
        }

        template <typename T>
        constexpr Area ToArea(const Buffer<T>& _buffer) noexcept
        {
            return Area{ static_cast<u8*>(const_cast<T*>(_buffer.data)), GetSize(_buffer) };
        }

        constexpr bool IsInArea(const void* _p, const Area& _area) noexcept
        {
            return _area.data <= _p && _p < GetEnd(_area);
        }
    }
}
