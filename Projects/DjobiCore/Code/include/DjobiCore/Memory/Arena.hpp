#pragma once

#include <DjobiCore/Memory/Allocator.hpp>
#include <DjobiCore/Core/StringView.hpp>


// see
// https://blog.molecular-matters.com/2011/08/03/memory-system-part-5/
// https://www.rfleury.com/p/untangling-lifetimes-the-arena-allocator

namespace Djo
{
    namespace Memory
    {
        namespace ArenaConst
        {
            static constexpr sz c_defaultReserveSize{ 64 * 1024 * 1024 };
            static constexpr sz c_defaultCommitSize{ 64 * 1024 };
        }

        struct ArenaState
        {
            sz localCursor;
            sz lastAllocStart;
        };

        struct Arena
        {
            ArenaState state;
            sz reservedSize;
            sz commitedSize;
            sz configCommitSize;
        };

        DJO_DEFINE_NULL_CONSTEXPR_STATIC(Arena, Arena, ({ 0, 0 }, 0, 0, 0));

    #define DJO_MEMORY_ARENA_ALLOCATE_SIZES(name_, reserve_, commit_)\
        ::Djo::Memory::_Internal::AllocateArena(reserve_, commit_)

    #define DJO_MEMORY_ARENA_PUSH(arena_, size_)\
        ::Djo::Memory::_Internal::Push(arena_, size_)

    #define DJO_MEMORY_ARENA_REUSE(arena_, p_, size_)\
        ::Djo::Memory::_Internal::Reuse(arena_, p_, size_)

    #define DJO_MEMORY_ARENA_ALLOCATE(name_)\
        DJO_MEMORY_ARENA_ALLOCATE_SIZES(name_, ::Djo::Memory::ArenaConst::c_defaultReserveSize, ::Djo::Memory::ArenaConst::c_defaultCommitSize)

    #define DJO_MEMORY_ARENA_PUSH_TYPE(arena_, type_)\
        (type_*)DJO_MEMORY_ARENA_PUSH(arena_, sizeof(type_)).data

    #define DJO_MEMORY_ARENA_MARK(arena_)\
        const ::Djo::Memory::ArenaMark DJO_CAT(__arenaMark_, __COUNTER__){ arena_ }

        void SetArenaState(Arena* _arena, ArenaState _state) noexcept;

        class ArenaMark
        {
            DJO_NO_DEFAULT_CTOR(ArenaMark);
            DJO_NO_COPY_NO_MOVE(ArenaMark);

        public:
            Arena* const arena;

            DJO_INLINE explicit ArenaMark(Arena* const _arena) noexcept
                : arena{ _arena }
                , m_mark{ this->arena->state }
            {}

            DJO_INLINE ~ArenaMark() noexcept { SetArenaState(this->arena, m_mark); }

        private:
            const ArenaState m_mark;
        };

        void FreeArena(Arena* _arena) noexcept;

        namespace _Internal
        {
            Arena* AllocateArena(sz _reserveSize, sz _commitSize) noexcept;
            Area Push(Arena* _arena, sz _size) noexcept;
            Area Reuse(Arena* _arena, void* _p, sz _size) noexcept;
        }

        Allocator MoldArenaAllocator(Arena* _arena) noexcept;
    }
}
