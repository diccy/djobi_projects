#pragma once

#include <DjobiCore/Memory/Memory.hpp>


namespace Djo
{
    namespace Memory
    {
        struct Allocator
        {
            void* context;
            Area (*alloc)(sz /*_size*/, void* /*_context*/) noexcept;
            Area (*realloc)(void* /*_p*/, sz /*_size*/, void* /*_context*/) noexcept;
            void (*free)(void* /*_p*/, void* /*_context*/) noexcept;
        };

        DJO_DEFINE_NULL_CONSTEXPR_STATIC(Allocator, Allocator, (nullptr, nullptr, nullptr, nullptr));

        DJO_INLINE Area Allocate(const Allocator& _allocator, const sz _size) noexcept
        {
            return _allocator.alloc(_size, _allocator.context);
        }

        template <typename T>
        DJO_INLINE T* AllocateType(const Allocator& _allocator) noexcept
        {
            return reinterpret_cast<T*>(Allocate(_allocator, sizeof(T)).data);
        }

        DJO_INLINE Area Reallocate(const Allocator& _allocator, void* const _p, const sz _size) noexcept
        {
            return _allocator.realloc(_p, _size, _allocator.context);
        }

        DJO_INLINE void Free(const Allocator& _allocator, void* const _p) noexcept
        {
            return _allocator.free(_p, _allocator.context);
        }

        Allocator MoldMallocAllocator() noexcept;
    }
}
