#pragma once

#include <DjobiCore/Memory/Memory.hpp>
#include <DjobiCore/Core/StructUtils.hpp>


namespace Djo::OS
{
    sz GetPageSize() noexcept;

    Memory::Area ReserveAddressSpace(sz _size) noexcept;
    Memory::Area AllocatePhysicalMemory(void* _p, sz _size) noexcept;
    bool FreePhysicalMemory(void* _p, sz _size) noexcept;
    bool ReleaseAddressSpace(void* _p, sz _size) noexcept;
}
