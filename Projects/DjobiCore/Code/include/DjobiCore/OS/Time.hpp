#pragma once

#include <DjobiCore/Core/Time.hpp>


namespace Djo::OS::Time
{
    using TimePoint = NanosecondsT<u64>;

    TimePoint Now() noexcept;
    void ThreadSleep(TimePoint _time) noexcept;
}
