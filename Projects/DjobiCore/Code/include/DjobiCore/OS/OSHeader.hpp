#pragma once

#define WIN32_LEAN_AND_MEAN 1
#define NOMINMAX 1
#include <windows.h>
#undef NOMINMAX
#undef WIN32_LEAN_AND_MEAN

namespace Djo::OS::Window
{
    typedef ::HWND Handle;
}
