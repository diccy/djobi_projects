#pragma once

#include <DjobiCore/Containers/String8OStream.hpp>


namespace Djo::IDE
{
    void Output(const char* const _str);
    void Output(const StringView& _sv);
    void Output(const String8& _str);

    String8OStream MoldOutputStringStream() noexcept;

#if __DJO_DEBUG__
    extern String8OStream g_debugLogger;
#endif
}
