#pragma once

//#define GLM_FORCE_AVX 1
#define GLM_FORCE_DEPTH_ZERO_TO_ONE 1 // Vulkan
#define GLM_FORCE_LEFT_HANDED 1 // Vulkan
#define GLM_FORCE_RADIANS 1

#include <glm/detail/qualifier.hpp>


namespace Djo
{
    constexpr glm::qualifier c_glmQualifier{ glm::highp };
}
