#pragma once

#include <DjobiCore/Format/Format.hpp>
#include <DjobiCore/Math/Vector2.hpp>
#include <DjobiCore/Math/Vector3.hpp>
#include <DjobiCore/Math/Vector4.hpp>
#include <DjobiCore/Math/Quaternion.hpp>
#include <DjobiCore/Math/Matrix33.hpp>
#include <DjobiCore/Math/Matrix44.hpp>


namespace Djo
{
    namespace Format
    {
        template <typename T>
        DJO_INLINE sz PushFormatted(String8OStream* const _sink, const StringView&, const Vec2T<T>& _v) noexcept
        {
            return PushPrintf(_sink, "[%.3f, %.3f]", _v.x, _v.y);
        }

        template <typename T>
        DJO_INLINE sz PushFormatted(String8OStream* const _sink, const StringView&, const Vec3T<T>& _v) noexcept
        {
            return PushPrintf(_sink, "[%.3f, %.3f, %.3f]", _v.x, _v.y, _v.z);
        }

        template <typename T>
        DJO_INLINE sz PushFormatted(String8OStream* const _sink, const StringView&, const Vec4T<T>& _v) noexcept
        {
            return PushPrintf(_sink, "[%.3f, %.3f, %.3f, %.3f]", _v.x, _v.y, _v.z, _v.w);
        }

        template <typename T>
        DJO_INLINE sz PushFormatted(String8OStream* const _sink, const StringView&, const QuatT<T>& _v) noexcept
        {
            return PushPrintf(_sink, "[%.3f, %.3f, %.3f, %.3f]", _v.x, _v.y, _v.z, _v.w);
        }

        template <typename T>
        DJO_INLINE sz PushFormatted(String8OStream* const _sink, const StringView&, const Mat33T<T>& _m) noexcept
        {
            return PushPrintf(_sink, "[%.3f, %.3f, %.3f] [%.3f, %.3f, %.3f] [%.3f, %.3f, %.3f]",
                _m[0].x, _m[0].y, _m[0].z,
                _m[1].x, _m[1].y, _m[1].z, 
                _m[2].x, _m[2].y, _m[2].z);
        }

        template <typename T>
        DJO_INLINE sz PushFormatted(String8OStream* const _sink, const StringView&, const Mat44T<T>& _m) noexcept
        {
            return PushPrintf(_sink, "[%.3f, %.3f, %.3f, %.3f] [%.3f, %.3f, %.3f, %.3f] [%.3f, %.3f, %.3f, %.3f] [%.3f, %.3f, %.3f, %.3f]",
                _m[0].x, _m[0].y, _m[0].z, _m[0].w,
                _m[1].x, _m[1].y, _m[1].z, _m[1].w, 
                _m[2].x, _m[2].y, _m[2].z, _m[2].w,
                _m[3].x, _m[3].y, _m[3].z, _m[3].w);
        }
    }
}
