#pragma once

#include <DjobiCore/Math/Vector3.hpp>
#include <DjobiCore/Math/Vector4.hpp>

#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>


namespace Djo
{
    template <typename T>
    using Mat44T = glm::tmat4x4<T, c_glmQualifier>;
    using Mat44f = Mat44T<f32>;
    using Mat44d = Mat44T<f64>;

    DJO_STATIC_ASSERT(sizeof(Mat44f) == sizeof(f32) * 4 * 4);
    DJO_STATIC_ASSERT(sizeof(Mat44d) == sizeof(f64) * 4 * 4);

    template <typename T>
    constexpr const Vec4T<T>& Mat44Right(const Mat44T<T>& _m) noexcept
    {
        return _m[0];
    }
    template <typename T>
    constexpr Vec4T<T>& Mat44Right(Mat44T<T>& _m) noexcept
    {
        return _m[0];
    }

    template <typename T>
    constexpr const Vec4T<T>& Mat44Up(const Mat44T<T>& _m) noexcept
    {
        return _m[1];
    }
    template <typename T>
    constexpr Vec4T<T>& Mat44Up(Mat44T<T>& _m) noexcept
    {
        return _m[1];
    }

    template <typename T>
    constexpr const Vec4T<T>& Mat44Forward(const Mat44T<T>& _m) noexcept
    {
        return _m[2];
    }
    template <typename T>
    constexpr Vec4T<T>& Mat44Forward(Mat44T<T>& _m) noexcept
    {
        return _m[2];
    }

    template <typename T>
    constexpr const Vec3T<T>& Mat44Pos(const Mat44T<T>& _m) noexcept
    {
        return reinterpret_cast<const Vec3T<T>&>(_m[3]);
    }
    template <typename T>
    constexpr Vec3T<T>& Mat44Pos(Mat44T<T>& _m) noexcept
    {
        return reinterpret_cast<Vec3T<T>&>(_m[3]);
    }
}
