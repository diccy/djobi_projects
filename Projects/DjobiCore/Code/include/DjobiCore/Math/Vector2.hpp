#pragma once

#include <DjobiCore/Math/GlmConfig.hpp>
#include <DjobiCore/Core/Core.hpp>
#include <DjobiCore/Core/Types.hpp>

#include <glm/vec2.hpp>


namespace Djo
{
    template <typename T>
    using Vec2T = glm::tvec2<T, c_glmQualifier>;
    using Vec2f = Vec2T<f32>;
    using Vec2d = Vec2T<f64>;
    using Vec2i = Vec2T<i32>;
    using Vec2u = Vec2T<u32>;

    DJO_STATIC_ASSERT(sizeof(Vec2f) == sizeof(f32) * 2);
    DJO_STATIC_ASSERT(sizeof(Vec2d) == sizeof(f64) * 2);
    DJO_STATIC_ASSERT(sizeof(Vec2i) == sizeof(i32) * 2);
    DJO_STATIC_ASSERT(sizeof(Vec2u) == sizeof(u32) * 2);
}
