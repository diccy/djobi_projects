#pragma once

#include <DjobiCore/Core/Core.hpp>
#include <DjobiCore/Core/Types.hpp>


namespace Djo
{
    // c_name for f64, c_name_f for f32
#define DJO_MATH_CONST(name_, val_)\
    constexpr f64 DJO_CAT(c_, name_){ val_ };\
    constexpr f32 DJO_CAT(DJO_CAT(c_, name_), _f){ static_cast<f32>(DJO_CAT(c_, name_)) }

    // From std <numbers> content from c++20

    DJO_MATH_CONST(e,               2.718281828459045);
    DJO_MATH_CONST(log2e,           1.4426950408889634);
    DJO_MATH_CONST(log10e,          0.4342944819032518);
    DJO_MATH_CONST(pi,              3.141592653589793);
    DJO_MATH_CONST(inv_pi,          0.3183098861837907);
    DJO_MATH_CONST(inv_sqrtpi,      0.5641895835477563);
    DJO_MATH_CONST(ln2,             0.6931471805599453);
    DJO_MATH_CONST(ln10,            2.302585092994046);
    DJO_MATH_CONST(sqrt2,           1.4142135623730951);
    DJO_MATH_CONST(sqrt3,           1.7320508075688772);
    DJO_MATH_CONST(inv_sqrt3,       0.5773502691896257);
    DJO_MATH_CONST(egamma,          0.5772156649015329);
    DJO_MATH_CONST(phi,             1.618033988749895);

    DJO_MATH_CONST(pi_2,            c_pi / 2.0);
    DJO_MATH_CONST(pi_4,            c_pi / 4.0);
    DJO_MATH_CONST(2pi,             c_pi * 2.0);

#undef DJO_MATH_CONST

    // Fonctions

    template <typename T>
    constexpr T Lerp(const T& _min, const T& _max, const f32 _factor) noexcept
    {
        return static_cast<T>((_min * (1.f - _factor)) + (_max * _factor));
    }
    template <typename T>
    constexpr T Lerp(const T& _min, const T& _max, const f64 _factor) noexcept
    {
        return static_cast<T>((_min * (1.0 - _factor)) + (_max * _factor));
    }

    template <typename T>
    constexpr f64 LerpFactor(const T _min, const T _max, const T _val) noexcept
    {
        return static_cast<f64>(_val - _min) / static_cast<f64>(_max - _min);
    }

    template <typename T>
    constexpr T LogInterp(const T _min, const T _max, const f32 _factor) noexcept
    {
        return ::powf(_min, 1.f - _factor) * ::powf(_max, _factor);
    }
    template <typename T>
    constexpr T LogInterp(const T _min, const T _max, const f64 _factor) noexcept
    {
        return ::pow(_min, 1.0 - _factor) * ::pow(_max, _factor);
    }
}
