#pragma once

#include <DjobiCore/Memory/Arena.hpp>


namespace Djo
{
    struct ThreadContext
    {
        Memory::Arena* arena;
    };

    void InitAndSetThreadContext(ThreadContext* _ctx) noexcept;
    ThreadContext* GetThreadContext() noexcept;
    Memory::ArenaMark GetThreadLocalArenaMark() noexcept;
}
