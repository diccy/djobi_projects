#pragma once

#include <DjobiCore/Math/GlmConfig.hpp>
#include <glm/mat3x3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

#include <new>
#include <type_traits>
#include <chrono>
#include <thread>

#include <stddef.h>
#include <stdint.h>
#include <float.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <math.h>

#define _CRTDBG_MAP_ALLOC 1
#include <crtdbg.h>
#undef _CRTDBG_MAP_ALLOC

#include <intrin.h>

#include <DjobiCore/OS/OSHeader.hpp>
