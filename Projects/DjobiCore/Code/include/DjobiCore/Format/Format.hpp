#pragma once

#include <DjobiCore/Containers/String8OStream.hpp>

#include <DjobiCore/Core/Time.hpp>
#include <DjobiCore/Core/TypeTraits.hpp>


namespace Djo
{
    namespace _Internal
    {
        sz ConsumeText(StringView* _format, String8OStream* _sink) noexcept;
        bool ConsumeParams(StringView* _format, StringView* _params) noexcept;

        template <typename T>
        sz PushFormatted(String8OStream* const _sink, StringView* const _format, const T& _arg) noexcept;

        template <typename T, typename... TArgs>
        sz PushFormatted(String8OStream* const _sink, StringView* const _format, const T& _arg, TArgs&& ..._args) noexcept;
    }

    DJO_INLINE sz PushFormatted(String8OStream* const _sink, const StringView& _format) noexcept
    {
        return Push(_sink, _format);
    }

    template <typename T>
    DJO_INLINE sz PushFormatted(String8OStream* const _sink, StringView _format, const T& _arg) noexcept
    {
        return _Internal::PushFormatted<RemoveRefConst_t<T>>(_sink, &_format, _arg);
    }

    template <typename T, typename... TArgs>
    DJO_INLINE sz PushFormatted(String8OStream* const _sink, StringView _format, const T& _arg, TArgs&& ..._args) noexcept
    {
        sz wrote = _Internal::PushFormatted<RemoveRefConst_t<T>>(_sink, &_format, _arg);
        wrote += _Internal::PushFormatted<TArgs...>(_sink, &_format, Forward<TArgs>(_args)...);
        return wrote;
    }

    DJO_INLINE sz PushFormatted(Buffer<char>* const _buffer, const StringView& _format) noexcept
    {
        String8OStream stream = MoldBufferFillerOStream(*_buffer);
        return Push(&stream, _format);
    }

    template <typename... TArgs>
    DJO_INLINE sz PushFormatted(Buffer<char>* const _buffer, StringView _format, TArgs&& ..._args) noexcept
    {
        String8OStream stream = MoldBufferFillerOStream(*_buffer);
        return _Internal::PushFormatted<TArgs...>(&stream, &_format, Forward<TArgs>(_args)...);
    }

    DJO_INLINE sz PushFormatted(String8* const _str, const StringView& _format) noexcept
    {
        String8OStream stream = MoldString8FillerOStream(_str);
        return Push(&stream, _format);
    }

    template <typename... TArgs>
    DJO_INLINE sz PushFormatted(String8* const _str, StringView _format, TArgs&& ..._args) noexcept
    {
        String8OStream stream = MoldString8FillerOStream(_str);
        _str->count = 0;
        return _Internal::PushFormatted<TArgs...>(&stream, &_format, Forward<TArgs>(_args)...);
    }

#define DJO_FORMAT(sink_, litteralFormat_, ...)     ::Djo::PushFormatted(sink_, ::Djo::ToStringView(litteralFormat_), __VA_ARGS__)
#define DJO_FORMAT_L(sink_, litteralFormat_, ...)   DJO_FORMAT(sink_, DJO_CAT(litteralFormat_, "\n"), __VA_ARGS__)
#define DJO_FORMAT_LF(sink_, litteralFormat_, ...)  { DJO_FORMAT_L(sink_, litteralFormat_, __VA_ARGS__); ::Djo::Flush(sink_); }

    // Core types formatting functions

    namespace Format
    {
        /*
            Use this prototype, in ::Djo::Format namespace, to provide your own formatting, returning wrote char count:

            sz PushFormatted(String8OStream* const _sink, const StringView& _formatParams, const T& _value) noexcept;
        */

        // Numeric

    #define DJO_DEFINE_FORMATTER_NUM(type_, cast_, f_)\
        DJO_INLINE sz PushFormatted(String8OStream* const _sink, const StringView&, const type_ _value) noexcept\
        {\
            return PushPrintf(_sink, f_, static_cast<cast_>(_value));\
        }

        DJO_DEFINE_FORMATTER_NUM(i8,  char, "%c");
        DJO_DEFINE_FORMATTER_NUM(i16, i64,  "%lld");
        DJO_DEFINE_FORMATTER_NUM(i32, i64,  "%lld");
        DJO_DEFINE_FORMATTER_NUM(i64, i64,  "%lld");
        DJO_DEFINE_FORMATTER_NUM(u8,  u64,  "%llu");
        DJO_DEFINE_FORMATTER_NUM(u16, u64,  "%llu");
        DJO_DEFINE_FORMATTER_NUM(u32, u64,  "%llu");
        DJO_DEFINE_FORMATTER_NUM(u64, u64,  "%llu");
        DJO_DEFINE_FORMATTER_NUM(f32, f64,  "%.3f");
        DJO_DEFINE_FORMATTER_NUM(f64, f64,  "%.4f");

        // may not compile here, due to collision with types declared above?
        DJO_DEFINE_FORMATTER_NUM(signed char,   char, "%c");
        DJO_DEFINE_FORMATTER_NUM(signed long,   i64,  "%lld");
        DJO_DEFINE_FORMATTER_NUM(unsigned long, u64,  "%llu");

#undef DJO_DEFINE_FORMATTER_NUM


        // String

        DJO_INLINE sz PushFormatted(String8OStream* const _sink, const StringView&, const StringView& _sv) noexcept
        {
            return Push(_sink, _sv);
        }

        DJO_INLINE sz PushFormatted(String8OStream* const _sink, const StringView&, const char* const _cstr) noexcept
        {
            return PushFormatted(_sink, ToStringView(_cstr));
        }

        template <sz N>
        DJO_INLINE sz PushFormatted(String8OStream* const _sink, const StringView&, const char (&_carray)[N]) noexcept
        {
            return PushFormatted(_sink, ToStringView(_carray));
        }

        DJO_INLINE sz PushFormatted(String8OStream* const _sink, const StringView& _formatParams, const String8& _str) noexcept
        {
            return PushFormatted(_sink, _formatParams, ToStringView(_str));
        }


        // Logging level

        enum class ELogLevel
        {
            Trace = 0,
            Info,
            Warning,
            Error,

            DJO_ENUM_COUNT_INVALID
        };

        DJO_INLINE sz PushFormatted(String8OStream* const _sink, const StringView&, const ELogLevel _value) noexcept
        {
            static const StringView s_str[ECount<ELogLevel>]
            {
                ToStringView("[Trace]"),
                ToStringView("[Info] "),
                ToStringView("[Warn] "),
                ToStringView("[Error]"),
            };

            return PushFormatted(_sink, c_nullStringView, s_str[+_value]);
        }


        // Indentation

        struct IndentStream { u32 n; };
        DJO_INLINE sz PushFormatted(String8OStream* const _sink, const StringView&, const IndentStream& _indent) noexcept
        {
            return PushPrintf(_sink, "% *s", _indent.n, "");
        }


        // Time

        template <typename TTime>
        static constexpr StringView c_timeUnit = ToStringView("UNKNOWN_TIME_UNIT");

        template <typename T> static constexpr StringView c_timeUnit<SecondsT<T>> =      ToStringView("s");
        template <typename T> static constexpr StringView c_timeUnit<MillisecondsT<T>> = ToStringView("ms");
        template <typename T> static constexpr StringView c_timeUnit<MicrosecondsT<T>> = ToStringView("us");
        template <typename T> static constexpr StringView c_timeUnit<NanosecondsT<T>> =  ToStringView("ns");

        template <typename T, u32 N, u32 D>
        DJO_INLINE sz PushFormatted(String8OStream* const _sink, const StringView& _formatParams, const Time<T, N, D>& _t) noexcept
        {
            if (_t.t < T{ 0.001 })
            {
                if constexpr (D < 1000000u)
                    return PushFormatted(_sink, _formatParams, Time<T, N, D * 1000u>{ _t.t * T{ 1000. } });
            }
            else if (_t.t > T{ 1000. })
            {
                if constexpr (D > 1u)
                    return PushFormatted(_sink, _formatParams, Time<T, N, D / 1000u>{ _t.t / T{ 1000. } });
            }

            sz wrote = PushFormatted(_sink, _formatParams, _t.t);
            wrote += PushFormatted(_sink, c_nullStringView, c_timeUnit<Time<T, N, D>>);
            return wrote;
        }
    }

    namespace _Internal
    {
        template <typename T>
        sz PushFormatted(String8OStream* const _sink, StringView* const _format, const T& _arg) noexcept
        {
            sz wrote = ConsumeText(_format, _sink);
            if (_format->count > 0)
            {
                StringView params = c_nullStringView;
                if (ConsumeParams(_format, &params))
                    wrote += ::Djo::Format::PushFormatted(_sink, params, _arg);

                wrote += ConsumeText(_format, _sink);
            }
            return wrote;
        }

        template <typename T, typename... TArgs>
        DJO_INLINE sz PushFormatted(String8OStream* const _sink, StringView* const _format, const T& _arg, TArgs&& ..._args) noexcept
        {
            sz wrote = PushFormatted<RemoveRefConst_t<T>>(_sink, _format, _arg);
            wrote += PushFormatted<TArgs...>(_sink, _format, Forward<TArgs>(_args)...);
            return wrote;
        }
    }
}
