#pragma once

#include <DjobiCore/Core/Core.hpp>


// to improve, see https://blog.molecular-matters.com/2011/07/22/an-improved-assert/

// Runtime assert

#if __DJO_DEBUG__

    #define DJO_IS_ASSERT_AVAILABLE 1

#endif

#if DJO_IS_ASSERT_AVAILABLE

    #define DJO_FAILED_ASSERTION(expr_)      ::Djo::_Internal::Break()
    #define DJO_ASSERT(expr_)                DJO_STATEMENT( if (!(expr_)){ DJO_FAILED_ASSERTION(expr_); } )
    #define DJO_ASSERT_MSG(expr_, msg_)      DJO_ASSERT(expr_)
    #define DJO_DISABLE_ASSERT_WHILE(expr_)  expr_

#else

    #define DJO_FAILED_ASSERTION(expr_)      DJO_STATEMENT_EMPTY
    #define DJO_ASSERT(expr_)                DJO_STATEMENT_EMPTY
    #define DJO_ASSERT_MSG(expr_, msg_)      DJO_STATEMENT_EMPTY
    #define DJO_DISABLE_ASSERT_WHILE(expr_)  expr_

#endif


// Missing implementation marker

#define DJO_NOT_IMPLEMENTED(mess_)  DJO_STATEMENT( DJO_TODO(mess_); DJO_FAILED_ASSERTION(mess_); )
