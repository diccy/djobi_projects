#!lua
require("ProjectCommon")

require("DjobiCore")


function Project_Components_SetupPCH(_project)

    ForEachSupportedPlatformAndConfig( function(_p, _c)
        pchheader (_project.name.."_pch.h")
        pchsource (_project.sourcesDir..CodeDir.._project.name.."_pch.cpp")
    end )
    filter {}
end

Project_Components = NewProjectCommon({
    name = "Components",
    dependencies = {Project_DjobiCore},
    SetupPCH = Project_Components_SetupPCH,
    DefineProject = CommonProjectDefinition_Exe,
})
