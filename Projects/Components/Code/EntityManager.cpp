#include "Components_pch.h"

#include "EntityManager.hpp"


namespace Djo
{
    EntityManager PushEntityManager(Memory::Arena* const _arena, const u32 _componentTypeCountBudget, const u32 _entityCountBudget) noexcept
    {
        EntityManager entityManager;
        entityManager.componentManager = PushComponentManager(_arena, _componentTypeCountBudget, _entityCountBudget);
        const Memory::Area nextFreeEntitiesArea = DJO_MEMORY_ARENA_PUSH(_arena, _entityCountBudget * sizeof(Entity::Id));
        entityManager.nextFreeEntities.data = reinterpret_cast<Entity::Id*>(nextFreeEntitiesArea.data);
        entityManager.nextFreeEntities.count = nextFreeEntitiesArea.count / sizeof(Entity::Id);
        for (u32 i = 0; i < entityManager.nextFreeEntities.count - 1; ++i)
            *GetAt(entityManager.nextFreeEntities, i) = i + 1;
        *GetLast(entityManager.nextFreeEntities) = Entity::c_invalidEntity;
        entityManager.nextFreeEntity = 0;
        return entityManager;
    }
}
