#pragma once

#include <DjobiCore/Core/Types.hpp>


namespace Djo
{
    namespace Entity
    {
        using Id = u32;

        constexpr Id c_invalidEntity = c_invalidUint<Id>;
    }
}
