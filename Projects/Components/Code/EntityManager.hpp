#include "Components_pch.h"

#include "ComponentManager.hpp"
#include "Entity.hpp"

#include <DjobiCore/Memory/Arena.hpp>


namespace Djo
{
    struct EntityManager
    {
        ComponentManager componentManager;
        Buffer<Entity::Id> nextFreeEntities;
        Entity::Id nextFreeEntity;
    };

    EntityManager PushEntityManager(Memory::Arena* const _arena, const u32 _componentTypeCountBudget, const u32 _entityCountBudget) noexcept;

    DJO_INLINE Entity::Id MoldEntity(EntityManager* const _entityManager) noexcept
    {
        DJO_ASSERT(_entityManager->nextFreeEntity != Entity::c_invalidEntity);
        const Entity::Id entity = _entityManager->nextFreeEntity;
        _entityManager->nextFreeEntity = _entityManager->nextFreeEntities[entity];
        _entityManager->nextFreeEntities[entity] = Entity::c_invalidEntity; // not needed, but cool for debug
        return entity;
    }

    template <typename TComponent>
    DJO_INLINE Component::ContainerHandle<TComponent> RegisterComponent(EntityManager* const _entityManager, Memory::Arena* const _arena) noexcept
    {
        return AddComponentContainer<Component::Type<TComponent>>(&_entityManager->componentManager, _arena, (u32)_entityManager->nextFreeEntities.count);
    }

    template <typename TComponent>
    DJO_INLINE Component::Type<TComponent>* GetComponent(EntityManager* const _entityManager, const Entity::Id _entity) noexcept
    {
        DJO_ASSERT(_entity < _entityManager->nextFreeEntities.count);
        return GetComponentAt<Component::Type<TComponent>>(&_entityManager->componentManager, _entity);
    }

    template <typename TComponent>
    DJO_INLINE Component::Type<TComponent>* AddComponent(EntityManager* const _entityManager, const Entity::Id _entity) noexcept
    {
        DJO_ASSERT(_entity < _entityManager->nextFreeEntities.count);
        return AddComponentAt<Component::Type<TComponent>>(&_entityManager->componentManager, _entity);
    }

    DJO_INLINE void EraseEntity(EntityManager* const _entityManager, const Entity::Id _entity) noexcept
    {
        EraseComponentsAt(&_entityManager->componentManager, _entity);
        if (_entityManager->nextFreeEntity != Entity::c_invalidEntity)
            _entityManager->nextFreeEntities[_entity] = _entityManager->nextFreeEntity;
        _entityManager->nextFreeEntity = _entity;
    }
}
