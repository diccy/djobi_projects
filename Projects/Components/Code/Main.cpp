#include "Components_pch.h"

#include "EntityManager.hpp"

#include <DjobiCore/Math/Vector2.hpp>
#include <DjobiCore/Math/Matrix44.hpp>
#include <DjobiCore/Math/Quaternion.hpp>
#include <DjobiCore/OS/IDE.hpp>
#include <DjobiCore/Format/Format.hpp>
#include <DjobiCore/Core/Timer.hpp>


namespace Djo
{
    struct ProfileEntry
    {
        StringView name;
        Milliseconds total;
        Milliseconds min;
        Milliseconds max;
        u64 count;
    };

    struct AAA
    {
        f32 a;
        Vec2f aa;
        Quatf aaaa;
    };

    struct BBB
    {
        Quatf bbbb;
        Vec2f bb;
        f32 b;
    };

    struct CCC
    {
        Mat44f cccc;
    };

    void Walk1(const Component::ContainerHandle<AAA> _aaas) noexcept
    {
        const u32 aaaCount = Component::GetCount(_aaas);
        for (u32 i = 0; i < aaaCount; ++i)
        {
            AAA* const aaa = Component::GetAt(_aaas, i);
            aaa->aaaa.x = aaa->a;
            aaa->aaaa.y = aaa->aa.x;
            aaa->aaaa.z = aaa->aa.y;
            aaa->aaaa.w = aaa->aaaa.x + aaa->aaaa.y + aaa->aaaa.z;
        }
    }
}

int main()
{
    using namespace ::Djo;

    SetupSystemMemoryLeakWatcher();

    Timer timer;
    Milliseconds elapsed;
    String8OStream logger = IDE::MoldOutputStringStream();

    ProfileEntry componentsProfileBuffer[50]{};
    ZeroMem(&componentsProfileBuffer);
    Array<ProfileEntry> componentsProfile{};
    componentsProfile.buffer = ToBuffer(componentsProfileBuffer);
    sz profileId;

#define DJO_REGISTER_PROFILE_ENTRY(name_)\
    DJO_STATEMENT(\
        elapsed = GetElapsed<Milliseconds>(timer);\
        if (run == 0)\
            ++componentsProfile.count;\
        ProfileEntry& profileEntry = componentsProfile[profileId++];\
        if (run == 0)\
        {\
            profileEntry.name = ToStringView(name_);\
            profileEntry.min = Milliseconds{ DBL_MAX };\
        }\
        profileEntry.total.t += elapsed.t;\
        profileEntry.min = Milliseconds{ Min(elapsed.t, profileEntry.min.t) };\
        profileEntry.max = Milliseconds{ Max(elapsed.t, profileEntry.max.t) };\
        ++profileEntry.count;\
        Start(&timer);\
    )

    constexpr sz ENTITY_COUNT = 500000;
    constexpr sz RUN_COUNT = 20;

    for (sz run = 0; run < RUN_COUNT; ++run)
    {
        DJO_FORMAT_LF(&logger, "run: %/%", (run + 1), RUN_COUNT);

        profileId = 0;
        {
            Start(&timer);

            AAA* const buff = new AAA[ENTITY_COUNT];

            DJO_REGISTER_PROFILE_ENTRY("new AAA[]");

            for (sz i = 0; i < ENTITY_COUNT; ++i)
            {
                AAA aaa{};
                aaa.a = i * 0.236f;
                aaa.aa = Vec2f{ i * 0.736f, i * 0.936f };
                aaa.aaaa = Quatf{ i * 0.136f, i * 0.905f, i * 0.666f, i * 0.111f };
                buff[i] = aaa;
            }

            DJO_REGISTER_PROFILE_ENTRY("Add component");

            for (sz i = 0; i < ENTITY_COUNT; ++i)
            {
                AAA& aaa = buff[i];
                aaa.aaaa.x = aaa.a;
                aaa.aaaa.y = aaa.aa.x;
                aaa.aaaa.z = aaa.aa.y;
                aaa.aaaa.w = aaa.aaaa.x + aaa.aaaa.y + aaa.aaaa.z;
            }

            DJO_REGISTER_PROFILE_ENTRY("Walk components");

            delete[] buff;

            DJO_REGISTER_PROFILE_ENTRY("delete[]");
        }

        {
            Start(&timer);

            Memory::Arena* const arrayArena = DJO_MEMORY_ARENA_ALLOCATE("Array arena");

            DJO_REGISTER_PROFILE_ENTRY("Init arena");

            Array<AAA> da = PushEmptyArray<AAA>(arrayArena, ENTITY_COUNT);

            DJO_REGISTER_PROFILE_ENTRY("PushEmptyArray");

            for (sz i = 0; i < ENTITY_COUNT; ++i)
            {
                AAA aaa{};
                aaa.a = i * 0.236f;
                aaa.aa = Vec2f{ i * 0.736f, i * 0.936f };
                aaa.aaaa = Quatf{ i * 0.136f, i * 0.905f, i * 0.666f, i * 0.111f };
                InsertBack(&da, arrayArena, aaa);
            }

            DJO_REGISTER_PROFILE_ENTRY("Add component");

            for (sz i = 0; i < ENTITY_COUNT; ++i)
            {
                AAA& aaa = da[i];
                aaa.aaaa.x = aaa.a;
                aaa.aaaa.y = aaa.aa.x;
                aaa.aaaa.z = aaa.aa.y;
                aaa.aaaa.w = aaa.aaaa.x + aaa.aaaa.y + aaa.aaaa.z;
            }

            DJO_REGISTER_PROFILE_ENTRY("Walk component");

            Memory::FreeArena(arrayArena);

            DJO_REGISTER_PROFILE_ENTRY("Free arena");
        }
        {
            Start(&timer);

            Memory::Arena* const entityManagerArena = DJO_MEMORY_ARENA_ALLOCATE("EntityManager arena");

            DJO_REGISTER_PROFILE_ENTRY("Init arena");

            EntityManager entityManager = PushEntityManager(entityManagerArena, 10, ENTITY_COUNT);

            DJO_REGISTER_PROFILE_ENTRY("PushEntityManager");

            RegisterComponent<AAA>(&entityManager, entityManagerArena);

            DJO_REGISTER_PROFILE_ENTRY("Component registration");

            for (sz i = 0; i < ENTITY_COUNT; ++i)
                MoldEntity(&entityManager);

            DJO_REGISTER_PROFILE_ENTRY("Mold entities");

            for (Entity::Id e = 0; e < ENTITY_COUNT; ++e)
            {
                AAA* const aaa = AddComponent<AAA>(&entityManager, e);
                aaa->a = e * 0.236f;
                aaa->aa = Vec2f{ e * 0.736f, e * 0.936f };
                aaa->aaaa = Quatf{ e * 0.136f, e * 0.905f, e * 0.666f, e * 0.111f };
            }

            DJO_REGISTER_PROFILE_ENTRY("Add component");

            Walk1(GetComponentContainer<AAA>(&entityManager.componentManager));

            DJO_REGISTER_PROFILE_ENTRY("Walk component");

            Memory::FreeArena(entityManagerArena);

            DJO_REGISTER_PROFILE_ENTRY("Free arena");
        }
    }

    for (sz i = 0; i < componentsProfile.count; ++i)
    {
        ProfileEntry& profileEntry = componentsProfile[i];
        DJO_FORMAT_LF(&logger, "%: %",
            profileEntry.name,
            Milliseconds{ profileEntry.total.t / profileEntry.count });
    }

    return 0;
}
