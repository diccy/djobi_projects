#pragma once

#include <DjobiCore/Containers/SparseSetAlloc.hpp>
#include <DjobiCore/Core/TypeTraits.hpp>


namespace Djo
{
    namespace Component
    {
        using Id = u64;

        template <typename TComponent>
        using Type = PureType_t<TComponent>;

        namespace _Internal
        {
            extern Id s_generatedComponentIdCount;

            Id GetNewComponentId() noexcept;

            template <typename TComponent>
            Id GetComponentId() noexcept
            {
                static const Id s_typeId = GetNewComponentId();
                return s_typeId;
            }

            template <typename TComponent>
            struct DummyHandle { TComponent* dummy; };
        }

        template <typename T>
        DJO_INLINE Id GetComponentId() noexcept
        {
            return _Internal::GetComponentId<Type<T>>();
        }

        template <typename TComponent>
        using ContainerHandle = _Internal::DummyHandle<TComponent>*;

        template <typename TComponent>
        DJO_INLINE SparseSet<Type<TComponent>> PushContainer(Memory::Arena* const _arena, const u32 _componentCountBudget, const u32 _entityCountBudget) noexcept
        {
            return PushEmptySparseSet<Type<TComponent>>(
                _arena,
                10, _componentCountBudget, _componentCountBudget,
                10, _entityCountBudget, _entityCountBudget);
        }

        template <typename TComponent>
        DJO_INLINE u32 GetCount(const ContainerHandle<TComponent> _cch) noexcept
        {
            const SparseSet<Type<TComponent>>* const ss = reinterpret_cast<const SparseSet<Type<TComponent>>*>(_cch);
            return GetCount(*ss);
        }

        template <typename TComponent>
        DJO_INLINE Type<TComponent>* GetAt(const ContainerHandle<TComponent> _cch, const u32 _entity) noexcept
        {
            const SparseSet<Type<TComponent>>* const ss = reinterpret_cast<const SparseSet<Type<TComponent>>*>(_cch);
            return GetAtSparse(*ss, _entity);
        }

        template <typename TComponent>
        DJO_INLINE Type<TComponent>* SetAt(const ContainerHandle<TComponent> _cch, const u32 _entity) noexcept
        {
            SparseSet<Type<TComponent>>* const ss = reinterpret_cast<SparseSet<Type<TComponent>>*>(_cch);
            return SetAtSparse(ss, _entity);
        }
    }
}
