#include "Components_pch.h"

#include "Component.hpp"


namespace Djo
{
    namespace Component
    {
        namespace _Internal
        {
            Id s_generatedComponentIdCount{ 0 };

            Id GetNewComponentId() noexcept
            {
                const Id componentId =
                    _InterlockedIncrement64(&reinterpret_cast<volatile i64&>(s_generatedComponentIdCount));

                return componentId - 1;
            }
        }
    }
}
