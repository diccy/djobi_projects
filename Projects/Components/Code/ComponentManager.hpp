#pragma once

#include "Component.hpp"

#include <DjobiCore/Containers/PageArrayAlloc.hpp>


namespace Djo
{
    namespace _Internal
    {
        struct ComponentContainerInterface
        {
            void* ptr;
            bool (*eraseAt)(void*, u32) noexcept;
        };

        template <typename TComponent>
        bool SparseArrayEraseAt(void* const _sparseArrayPtr, const u32 _i) noexcept
        {
            SparseSet<Component::Type<TComponent>>* const sparseArray = reinterpret_cast<SparseSet<Component::Type<TComponent>>*>(_sparseArrayPtr);
            return EraseAt(sparseArray, _i);
        }
    }

    struct ComponentManager
    {
        PageArray<_Internal::ComponentContainerInterface> componentContainers;
        u32 entityCountBudget;
    };

    DJO_INLINE ComponentManager PushComponentManager(Memory::Arena* const _arena, const u32 _componentTypeCountBudget, const u32 _entityCountBudget) noexcept
    {
        ComponentManager cm;
        cm.componentContainers = PushEmptyPageArray<_Internal::ComponentContainerInterface>(_arena, 10, _componentTypeCountBudget, _componentTypeCountBudget);
        cm.entityCountBudget = _entityCountBudget;
        return cm;
    }

    namespace _Internal
    {
        template <typename TComponent>
        Component::ContainerHandle<TComponent> AddComponentContainer(ComponentManager* const _cm, Memory::Arena* const _arena, const u32 _componentCount) noexcept
        {
            const Component::Id cid = Component::GetComponentId<Component::Type<TComponent>>();
            if (GetCount(_cm->componentContainers) <= cid)
            {
                const sz previousCount = GetCount(_cm->componentContainers);
                const sz newCount = cid + 1;
                Reserve(&_cm->componentContainers, _arena, newCount);
                _cm->componentContainers.count = newCount;
                for (sz i = previousCount; i < newCount; ++i)
                    ZeroMem(GetAt(_cm->componentContainers, i));
            }

            ComponentContainerInterface* const componentContainerInterface = GetAt(_cm->componentContainers, cid);
            if (componentContainerInterface->ptr == nullptr)
            {
                SparseSet<Component::Type<TComponent>>* const container = DJO_MEMORY_ARENA_PUSH_TYPE(_arena, SparseSet<Component::Type<TComponent>>);
                *container = Component::PushContainer<Component::Type<TComponent>>(_arena, _componentCount, _cm->entityCountBudget);
                componentContainerInterface->ptr = container;
                componentContainerInterface->eraseAt = &SparseArrayEraseAt<Component::Type<TComponent>>;
            }
            return reinterpret_cast<Component::ContainerHandle<TComponent>>(componentContainerInterface->ptr);
        }

        template <typename TComponent>
        DJO_INLINE Component::ContainerHandle<TComponent> GetComponentContainer(ComponentManager* const _cm) noexcept
        {
            const Component::Id cid = Component::GetComponentId<Component::Type<TComponent>>();
            return (cid < GetCount(_cm->componentContainers))
                ? reinterpret_cast<Component::ContainerHandle<TComponent>>(GetAt(_cm->componentContainers, cid)->ptr)
                : nullptr;
        }
    }

    template <typename TComponent>
    DJO_INLINE Component::ContainerHandle<TComponent> AddComponentContainer(ComponentManager* const _cm, Memory::Arena* const _arena, const u32 _componentCount) noexcept
    {
        return _Internal::AddComponentContainer<Component::Type<TComponent>>(_cm, _arena, _componentCount);
    }

    template <typename TComponent>
    DJO_INLINE Component::ContainerHandle<TComponent> GetComponentContainer(ComponentManager* const _cm) noexcept
    {
        return _Internal::GetComponentContainer<Component::Type<TComponent>>(_cm);
    }

    template <typename TComponent>
    DJO_INLINE Component::Type<TComponent>* GetComponentAt(ComponentManager* const _cm, const u32 _i) noexcept
    {
        const Component::ContainerHandle<TComponent> cch = _Internal::GetComponentContainer<Component::Type<TComponent>>(_cm);
        return (cch != nullptr)
            ? Component::GetAt<Component::Type<TComponent>>(cch, _i)
            : nullptr;
    }

    template <typename TComponent>
    DJO_INLINE Component::Type<TComponent>* AddComponentAt(ComponentManager* const _cm, const u32 _i) noexcept
    {
        const Component::ContainerHandle<TComponent> cch = _Internal::GetComponentContainer<Component::Type<TComponent>>(_cm);
        return (cch != nullptr)
            ? Component::SetAt<Component::Type<TComponent>>(cch, _i)
            : nullptr;
    }

    DJO_INLINE void EraseComponentsAt(ComponentManager* const _cm, const u32 _i) noexcept
    {
        const sz containerCount = GetCount(_cm->componentContainers);
        for (sz i = 0; i < containerCount; ++i)
        {
            _Internal::ComponentContainerInterface* const componentContainerInterface = GetAt(_cm->componentContainers, i);
            if (componentContainerInterface->ptr != nullptr)
                componentContainerInterface->eraseAt(componentContainerInterface->ptr, _i);
        }
    }
}
