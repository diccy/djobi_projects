#pragma once

#include <DjobiCore/Core/Core.hpp>
#include <DjobiCore/Core/Types.hpp>


namespace Djo
{
    namespace Tests
    {
        struct Result
        {
            u32 success;
            u32 count;
        };

        constexpr Result operator + (const Result& _lhs, const Result& _rhs) noexcept
        {
            return Result{ _lhs.success + _rhs.success, _lhs.count + _rhs.count };
        }

        constexpr Result& operator += (Result& _lhs, const Result& _rhs) noexcept
        {
            _lhs.success += _rhs.success;
            _lhs.count += _rhs.count;
            return _lhs;
        }
    }
}
