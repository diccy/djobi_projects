#pragma once

#include <DjobiCore/Assert/Assert.hpp>


// uncomment this to enable break on failed test call
#define DJO_ENABLE_BREAK_ON_FAILED_ASSERTION 1

#if DJO_ENABLE_BREAK_ON_FAILED_ASSERTION

    #define DJO_TESTS_FAILED_ASSERTION  DJO_FAILED_ASSERTION

#else

    #define DJO_TESTS_FAILED_ASSERTION

#endif


// Tests main macros

#define DJO_UTESTS_CTX             ctx__
#define DJO_UTESTS_ARGS            ::Djo::Tests::Context* const DJO_UTESTS_CTX
#define DJO_UTESTS_PARAMS          DJO_UTESTS_CTX
#define DJO_UTESTS_LAMBDA_CAPTURE  DJO_UTESTS_CTX

#define DJO_UTESTS_OPEN_NAMED_SCOPE(scopeName_)\
    ::Djo::Tests::OpenScope(DJO_UTESTS_CTX, __FILE__, scopeName_);\
    const ::Djo::Tests::_Internal::ScopeInfo* const ctx_currentScopeInfo__ = DJO_UTESTS_CTX->currentScopeInfo

#define DJO_UTESTS_OPEN_SCOPE   DJO_UTESTS_OPEN_NAMED_SCOPE(__FUNCTION__)

#define DJO_UTESTS_CLOSE_SCOPE \
    DJO_ASSERT_MSG(DJO_UTESTS_CTX->currentScopeInfo == ctx_currentScopeInfo__, "Tests::CloseScope not called after previous Tests::OpenScope");\
    DJO_UNUSED(ctx_currentScopeInfo__);\
    ::Djo::Tests::CloseScope(DJO_UTESTS_CTX)

#define DJO_UTEST(expr_)  ::Djo::Tests::Test(DJO_UTESTS_CTX, (expr_), DJO_STR(expr_), __LINE__)

#define DJO_STATIC_UTEST(expr_)  DJO_STATEMENT( DJO_STATIC_ASSERT(expr_); DJO_UTEST(expr_); )
