#pragma once

#include <DjobiCore/Core/Core.hpp>
#include <DjobiCore/Core/ClassTraits.hpp>


namespace Djo
{
    namespace Tests
    {
        class AllDefault
        {
            DJO_ALL_DEFAULT(AllDefault);
        public:
            AllDefault(int _i) noexcept : i{ _i } {}
            constexpr operator int() noexcept { return i; }
            int i;
        };

        class NoDefaultCtor
        {
            DJO_NO_DEFAULT_CTOR(NoDefaultCtor);
            DJO_DEFAULT_COPY_DEFAULT_MOVE(NoDefaultCtor);
        public:
            NoDefaultCtor(int _i) noexcept : i{ _i } {}
            ~NoDefaultCtor() noexcept = default;
            constexpr operator int() noexcept { return i; }
            int i;
        };

        class NoCopyCtor
        {
            DJO_NO_COPY_DEFAULT_MOVE(NoCopyCtor);
        public:
            NoCopyCtor() noexcept : i{ 0 } {}
            NoCopyCtor(int _i) noexcept : i{ _i } {}
            ~NoCopyCtor() noexcept = default;
            constexpr operator int() noexcept { return i; }
            int i;
        };

        class NoMoveCtor
        {
            DJO_NO_COPY_NO_MOVE(NoMoveCtor);
        public:
            NoMoveCtor() noexcept : i{ 0 } {}
            NoMoveCtor(int _i) noexcept : i{ _i } {}
            ~NoMoveCtor() noexcept = default;
            constexpr operator int() noexcept { return i; }
            int i;
        };

        class StaticCount
        {
        public:
            static int s_defaultCtorCount;
            static int s_intCtorCount;
            static int s_copyCtorCount;
            static int s_moveCtorCount;
            static int s_copyAssignCount;
            static int s_moveAssignCount;
            static int s_dtorCount;

            static void ZeroCounts()
            {
                s_defaultCtorCount = 0;
                s_intCtorCount = 0;
                s_copyCtorCount = 0;
                s_moveCtorCount = 0;
                s_copyAssignCount = 0;
                s_moveAssignCount = 0;
                s_dtorCount = 0;
            }

            StaticCount() noexcept
                : i{ 0 }
            {
                ++s_defaultCtorCount;
            }
            StaticCount(int _i) noexcept
                : i{ _i }
            {
                ++s_intCtorCount;
            }
            StaticCount(const StaticCount& _other) noexcept
                : i{ _other.i }
            {
                ++s_copyCtorCount;
            }
            StaticCount(StaticCount&& _other) noexcept
                : i{ _other.i }
            {
                ++s_moveCtorCount;
                _other.i = 0;
            }
            StaticCount& operator = (const StaticCount& _other) noexcept
            {
                ++s_copyAssignCount;
                this->i = _other.i;
                return *this;
            }
            StaticCount& operator = (StaticCount&& _other) noexcept
            {
                ++s_moveAssignCount;
                Swap(this->i, _other.i);
                return *this;
            }
            ~StaticCount() noexcept
            {
                ++s_dtorCount;
                this->i = 0;
            }

            int i;
        };

#define DJO_UTEST_STATIC_COUNTS(defaultCtorCount_, intCtorCount_, copyCtorCount_, moveCtorCount_, copyAssignCount_, moveAssignCount_, dtorCount_)\
        DJO_UTEST(\
            StaticCount::s_defaultCtorCount == (defaultCtorCount_) &&\
            StaticCount::s_intCtorCount == (intCtorCount_) &&\
            StaticCount::s_copyCtorCount == (copyCtorCount_) &&\
            StaticCount::s_moveCtorCount == (moveCtorCount_) &&\
            StaticCount::s_copyAssignCount == (copyAssignCount_) &&\
            StaticCount::s_moveAssignCount == (moveAssignCount_) &&\
            StaticCount::s_dtorCount == (dtorCount_))
    }
}
