#pragma once

#include <DjobiCore/Containers/String8OStream.hpp>


namespace Djo
{
    namespace Tests
    {
        using Logger = String8OStream;
    }
}
