#pragma once

#include <DjobiTests/Result.hpp>
#include <DjobiTests/Logger.hpp>

#include <DjobiCore/Memory/Arena.hpp>
#include <DjobiCore/Containers/LinkedList.hpp>
#include <DjobiCore/Core/ResultEnum.hpp>
#include <DjobiCore/Core/Time.hpp>


namespace Djo
{
    namespace Tests
    {
        namespace _Internal
        {
            struct ScopeInfo
            {
                struct BeginState
                {
                    const char* filePath;
                    const char* function;
                    Memory::ArenaState userArenaState;
                    u32 scopeLevel;
                    Microseconds time;
                };
                struct EndState
                {
                    Result result;
                    sz memory;
                    Microseconds time;
                };

                ScopeInfo* next;
                ScopeInfo* previous;
                ScopeInfo* parent;
                BeginState beginState;
                EndState   endState;
            };
        }

        struct Context
        {
            Memory::Arena* internalArena;
            Memory::Arena* userArena;
            Memory::Allocator userAllocator;
            Logger logger;
            LinkedList<_Internal::ScopeInfo> scopeInfoList;
            _Internal::ScopeInfo* currentScopeInfo;
            Result lastResult;
        };

        Context MoldContext(Memory::Arena* _internalArena, Memory::Arena* _userArena, const Memory::Allocator& _userAllocator) noexcept;
        void Init(Context* _ctx) noexcept;
        void LogSummary(Context* _ctx) noexcept;

        void OpenScope(Context* _ctx, const char* _filePath, const char* _function) noexcept;
        void Test(Context* _ctx, bool _test, const char* _expr, i32 _line) noexcept;
        void CloseScope(Context* _ctx) noexcept;
    }
}
