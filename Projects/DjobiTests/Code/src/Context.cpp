#include "DjobiTests_pch.h"

#include <DjobiTests/Context.hpp>

#include <DjobiTests/Tests.hpp>

#include <DjobiCore/OS/IDE.hpp>
#include <DjobiCore/OS/Time.hpp>
#include <DjobiCore/Format/Format.hpp>
#include <DjobiCore/Containers/ArrayAlloc.hpp>


namespace Djo
{
    namespace Tests
    {
        namespace _Internal
        {
            void ContextLogging(const char* const _data, const sz _count, void* const) noexcept
            {
                IDE::Output(StringView{ _data, _count });
            }

            void LogScopeInfoSummary(Context* const _ctx, const _Internal::ScopeInfo& _scopeInfo) noexcept
            {
                if (_scopeInfo.endState.result.count == _scopeInfo.endState.result.success)
                {
                    DJO_FORMAT_LF(&_ctx->logger, "% %<Success> % (%/%) - % [%B]",
                        Format::ELogLevel::Info, Format::IndentStream{ _scopeInfo.beginState.scopeLevel * 2 },
                        _scopeInfo.beginState.function,
                        _scopeInfo.endState.result.success, _scopeInfo.endState.result.count,
                        _scopeInfo.endState.time,
                        _scopeInfo.endState.memory);
                }
                else
                {
                    DJO_FORMAT_LF(&_ctx->logger, "% %<Failed> % | % failed tests (%/%) - % [%B]",
                        Format::ELogLevel::Error, Format::IndentStream{ _scopeInfo.beginState.scopeLevel * 2 },
                        _scopeInfo.beginState.function,
                        (_scopeInfo.endState.result.count - _scopeInfo.endState.result.success),
                        _scopeInfo.endState.result.success, _scopeInfo.endState.result.count,
                        _scopeInfo.endState.time,
                        _scopeInfo.endState.memory);
                }
            }
        }

        Context MoldContext(Memory::Arena* const _internalArena, Memory::Arena* const _userArena, const Memory::Allocator& _userAllocator) noexcept
        {
            Context ctx{};
            ctx.internalArena = _internalArena;
            ctx.userArena = _userArena;
            ctx.userAllocator = _userAllocator;
            ctx.logger.array = PushEmptyArray<char>(ctx.internalArena, 2048);
            return ctx;
        }

        void Init(Context* const _ctx) noexcept
        {
            _ctx->logger.output.callback = _Internal::ContextLogging;
            _ctx->logger.output.userData = _ctx;
        }

        void OpenScope(Context* const _ctx, const char* const _filePath, const char* const _function) noexcept
        {
            _Internal::ScopeInfo* const parentScopeInfo = _ctx->currentScopeInfo;

            _Internal::ScopeInfo* const scopeInfo = DJO_MEMORY_ARENA_PUSH_TYPE(_ctx->internalArena, _Internal::ScopeInfo);
            ZeroMem(scopeInfo);
            scopeInfo->parent = parentScopeInfo;
            scopeInfo->beginState.userArenaState = _ctx->userArena->state;
            scopeInfo->beginState.scopeLevel = (parentScopeInfo != nullptr) ? parentScopeInfo->beginState.scopeLevel + 1 : 0;
            scopeInfo->beginState.filePath = _filePath;
            scopeInfo->beginState.function = _function;
            scopeInfo->beginState.time = ConvertTime<Microseconds>(OS::Time::Now());

            DLList::InsertBack(&_ctx->scopeInfoList, scopeInfo);

            _ctx->currentScopeInfo = scopeInfo;
        }

        void CloseScope(Context* const _ctx) noexcept
        {
            const Microseconds end = ConvertTime<Microseconds>(OS::Time::Now());

            DJO_ASSERT(_ctx->currentScopeInfo != nullptr);

            _Internal::ScopeInfo* const currentScopeInfo = _ctx->currentScopeInfo;

            currentScopeInfo->endState.time.t = end.t - currentScopeInfo->beginState.time.t;

            DJO_ASSERT(_ctx->userArena->state.localCursor >= currentScopeInfo->beginState.userArenaState.localCursor);
            currentScopeInfo->endState.memory += _ctx->userArena->state.localCursor - currentScopeInfo->beginState.userArenaState.localCursor;

            if (currentScopeInfo->parent != nullptr)
            {
                currentScopeInfo->parent->endState.result += currentScopeInfo->endState.result;
                currentScopeInfo->parent->endState.memory += currentScopeInfo->endState.memory;
            }

            Memory::SetArenaState(_ctx->userArena, currentScopeInfo->beginState.userArenaState);

            _ctx->currentScopeInfo = currentScopeInfo->parent;
        }

        void Test(Context* const _ctx, const bool _test, const char* const _expr, const i32 _line) noexcept
        {
            DJO_ASSERT(_ctx->currentScopeInfo != nullptr);

            _Internal::ScopeInfo* const scopeInfo = _ctx->currentScopeInfo;
            ++scopeInfo->endState.result.count;
            if (_test)
            {
                ++scopeInfo->endState.result.success;
            }
            else
            {
                DJO_FORMAT_LF(&_ctx->logger, "% Test: Failed: %, %, %", Format::ELogLevel::Error, scopeInfo->beginState.filePath, _line, scopeInfo->beginState.function);
                DJO_FORMAT_LF(&_ctx->logger, "%    %", Format::ELogLevel::Error, _expr);
                DJO_TESTS_FAILED_ASSERTION(_expr);
            }
        }

        void LogSummary(Context* const _ctx) noexcept
        {
            // Logs head scope on top and on bottom of summary

            if (_ctx->scopeInfoList.head != nullptr)
                _Internal::LogScopeInfoSummary(_ctx, *_ctx->scopeInfoList.head);

            const _Internal::ScopeInfo* scopeInfo = _ctx->scopeInfoList.tail;
            while (scopeInfo != nullptr)
            {
                _Internal::LogScopeInfoSummary(_ctx, *scopeInfo);
                scopeInfo = scopeInfo->previous;
            }
        }
    }
}
