#include "DjobiTests_pch.h"

#include <DjobiTests/Utils.hpp>


namespace Djo
{
    namespace Tests
    {
        int StaticCount::s_defaultCtorCount{ 0 };
        int StaticCount::s_intCtorCount{ 0 };
        int StaticCount::s_copyCtorCount{ 0 };
        int StaticCount::s_moveCtorCount{ 0 };
        int StaticCount::s_copyAssignCount{ 0 };
        int StaticCount::s_moveAssignCount{ 0 };
        int StaticCount::s_dtorCount{ 0 };
    }
}
