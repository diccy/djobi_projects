#!lua
require("ProjectCommon")

require("DjobiCore")


Project_DjobiTests = NewProjectCommon({
    name = "DjobiTests",
    dependencies = {Project_DjobiCore},
    DefineProject = CommonProjectDefinition_Lib,
})
